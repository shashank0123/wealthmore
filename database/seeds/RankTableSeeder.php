<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ranks = [
            [
                'rank_name'   =>  'Star',
                'rank_number' => 1,
                'status'      =>  1
            ],
            [
                'rank_name' =>  'Silver',
                'rank_number' => 2,
                'status' =>  1
            ],
            [
                'rank_name' =>  'Gold',
                'rank_number' => 3,
                'status' =>  1
            ],
            [
                'rank_name' =>  'Platinum',
                'rank_number' => 4,
                'status' =>  1
            ],
            [
                'rank_name' =>  'Ruby',
                'rank_number' => 5,
                'status' =>  1
            ],
            [
                'rank_name' =>  'Sapphire',
                'rank_number' => 6,
                'status' =>  1
            ],
            [
                'rank_name' =>  'Emarald',
                'rank_number' => 7,
                'status' =>  1
            ]
        ];

        foreach ($ranks as $rank) {
        	$rank['created_at'] = Carbon::now();
        	$rank['updated_at'] = Carbon::now();
        	\DB::table('ranks')->insert($rank);
        }
    }
}
