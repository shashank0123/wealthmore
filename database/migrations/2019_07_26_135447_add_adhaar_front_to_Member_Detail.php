<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdhaarFrontToMemberDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Member_Detail', function (Blueprint $table) {
            $table->string('adhaar_front');
            $table->string('adhaar_back');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Member_Detail', function (Blueprint $table) {
                      
        });
    }
}
