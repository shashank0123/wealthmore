<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdhaarCardAndPanColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Member_Detail', function (Blueprint $table) {
            $table->string('adhaar')->nullable();
            $table->string('pan')->nullable();
            $table->dropColumn('phone1');
            $table->dropColumn('phone2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Member_Detail', function (Blueprint $table) {
            $table->string('phone1')->nullable();
            $table->string('phone2')->nullable();
            $table->dropColumn('adhaar');
            $table->dropColumn('pan');
        });
    }
}
