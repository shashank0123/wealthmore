<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
             $table->bigIncrements('id');
            $table->string('name');
            $table->string('short_descriptions');
            $table->text('long_descriptions');
            $table->string('mrp', 8, 2);
            $table->string('sell_price', 8, 2);            
            $table->string('slug_name',120)->nullable();            
            $table->string('image1')->nullable();
            $table->string('image2')->nullable();
            $table->string('image3')->nullable();
            $table->string('weight')->nullable();
            $table->string('jbv')->nullable();
            $table->string('rvv')->nullable();
            $table->string('product_color')->nullable();
            $table->string('product_size')->nullable();
            $table->string('quantity')->nullable();
            $table->string('page_title')->nullable();
            $table->text('page_description')->nullable();
            $table->string('page_keywords')->nullable();
            $table->string('trending')->default('no');
            $table->bigInteger('category_id');
            $table->string('availability')->default('yes');
            $table->string('status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
