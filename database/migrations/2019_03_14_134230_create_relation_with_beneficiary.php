<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationWithBeneficiary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Member_Detail', function (Blueprint $table) {
            $table->string('relation_with_beneficiary')->nullable();
            $table->dropColumn('spouse_name');
            $table->dropColumn('spouse_dob');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Member_Detail', function (Blueprint $table) {
            //
        });
    }
}
