<?php

return [
	'title' => 'Register New Member',
	'notice' => '<span class="theme-text bold">0-50%</span> of your promotion point, <span class="theme-text bold">50-100%</span> of your register point.', // don't remove the <span></span>
	'package' => 'Package',
	'name' => 'Full Name',
	'username' => 'Username',
	'email' => 'Email',
	'password' => 'Password',
	'repassword' => 'Confirm Password',
	'spassword' => 'Security Password',
	'respassword' => 'Confirm Security Password',
	'tpassword' => 'Transaction Password',
	'retpassword' => 'Confirm Transaction Password',
	'binary' => 'Parent ID',
	'direct' => 'Sponsor ID',
	'nationality' => 'Nationality',
	'mobile' => 'Mobile Phone',
	'state' => 'State',
	'dob' => 'Date of Birth',
	'gst' => 'Date of Birth',
	'marital' => 'Date of Birth',
	'uplineNotice' => 'case SENSITIVE',
	'uplineLink' => 'hierarchy',
	'position' => 'Placement',
	'position.left' => 'LEFT',
	'position.right' => 'RIGHT',
	'security' => 'Your Security Password',
	'registerPoint' => 'PROMOTION POINT amount',
	'checkID' => 'Check ID',
	'registerNeed' => 'Register Point Needed:',
	'promoNeed' => 'Promotion Point Needed:',
	'modal.title' => 'Member Detail',
	'modal.notFound' => 'Member not found with that ID',
	'modal.username' => 'Username',
	'modal.name' => 'Full Name',
	'modal.join' => 'Joined Since',
	'registerNotice' => 'will be used on register point.', // like "90% will be used on promotion point"
	'agree' => 'I agree on <a href="' . route('terms', ['lang' => \App::getLocale()]) . '" target="_blank">terms and agreement.</a>'
];
