<?php

return [
	'bonusTitle' => 'Bonus Statement',
	'bonusSubtitle' => 'Your Bonus Statement.',
	'summaryTitle' => 'Summary Statement',
	'summarySubtitle' => 'Your bonus, transfer, and withdraw statement.',
	'direct' => 'Direct Income',
	'binary' => 'Binary Income',
	'override' => 'Override Bonus',
	'pairing' => 'Pairing Bonus',
	'group' => 'Group Bonus',
	'create' => 'Created Date',
	'cash' => 'Cash',
	'admin' => 'Admin(10%)',
	'tds' => 'TDS(5%)',
	'repurchase' => 'Repurchase(10%)',
	'promotion' => 'Promotion',
	'total' => 'Total',
	'transfer' => 'Transfer Statement',
	'transfer.to' => 'To',
	'transfer.type' => 'Type',
	'transfer.amount' => 'Amount',
	'withdraw' => 'Withdraw Statement',
	'groupPending' => 'Pending Group Bonus',
	'groupPending.btnLabel' => 'See Pending Bonuses',
	'groupPendingSubtitle' => 'Your pending group bonus.',
	'groupPending.join' => 'Join Date',
	'groupPending.active' => 'Active On',
	'groupPending.username' => 'Username',
	'groupPending.amount' => 'Amount',
	'groupPending.alert' => 'You are not qualified for group bonus yet.'
];
