<?php
use App\Faqs;

$faqs = Faqs::where('status','Active')->get();
?>

@extends('layouts/home')

@section('content')

    <!-- page title begin-->
    <div class="page-title">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-8">
                    <h2 class="extra-margin">Faq</h2>
                    <p>Enjoy real benefits and rewards on your accrue investing.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- page title end -->

    <!-- faq begin-->
    <div class="faq">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-6">
                    <div class="section-title text-center">
                        <h2>Questions you often ask</h2>
                        <p>Put your ideas into action with full range of investments.
                            Enjoy real benefits and rewards on your accure investing.</p>
                    </div>
                </div>
            </div>
    
            <div class="row">
                <div class="col-xl-2 col-lg-2"></div>
                <div class="col-xl-8 col-lg-8">
                    <div class="accordion" id="accordionExample">
                        <div class="row">
                            @if(!empty($faqs))

                            
                            <div class="col-xl-12 col-lg-12">
                                @foreach($faqs as $faq)
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseOne"
                                                aria-expanded="false" aria-controls="collapseOne">
                                                {{$faq->questions}}
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseOne" class="collapse" data-parent="#accordionExample">
                                        <div class="card-body">
                                            {{$faq->answers}}
                                        </div>
                                    </div>
                                </div>
                                @endforeach

    {{-- 
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                I have questions about the updated trems
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse" data-parent="#accordionExample">
                                        <div class="card-body">
                                            We are full service Digital Marketing Agency all the tools
                                            you need for inbound success. With this module theres
                                            no need to go another day.
                                        </div>
                                    </div>
                                </div>
    
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                User Guide: Getting Started
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseThree" class="collapse" data-parent="#accordionExample">
                                        <div class="card-body">
                                            We are full service Digital Marketing Agency all the tools
                                            you need for inbound success. With this module theres
                                            no need to go another day.
                                        </div>
                                    </div>
                                </div>
    
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#collapse4" aria-expanded="false" aria-controls="collapseThree">
                                                Are you plan to open a brance on Dhaka?
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapse4" class="collapse" data-parent="#accordionExample">
                                        <div class="card-body">
                                            We are full service Digital Marketing Agency all the tools
                                            you need for inbound success. With this module theres
                                            no need to go another day.
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                             --}}
                            @endif
    
                        </div>
    
                    </div>
    
                </div>
                <div class="col-xl-2 col-lg-2"></div>
            </div>
        </div>
    </div>
    <!-- faq end -->
@endsection