@extends('layouts/home')
<style>
    #name-error,#email-error,#subject-error,#message-error{ color: red !important; }
</style>
@section('content')
<!-- page title begin-->
<div class="page-title contact-page">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-8">
                <h2>Contact Us</h2>
                <p>Enjoy real benefits and rewards on your accrue investing.</p>
            </div>
        </div>
    </div>
</div>
<!-- page title end -->

<!-- contact begin-->
<div class="address-area">
    <div class="container">
        <div class="tsk-contact-info">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="contact-info-item">
                        <div class="icon-bar">
                            <i class="fas fa-map-marker-alt"></i>
                        </div>
                        <div class="info-details" style="text-align: center !important; ">
                            <h5>Our Address</h5>
                            <a>WZ-31,
                            New Delhi-110045</a>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="contact-info-item">
                        <div class="icon-bar">
                            <i class="fas fa-envelope"></i>
                        </div>
                        <div class="info-details">
                            <h5>Email Address</h5>
                            <a href="mailto:info@wealthindia.global">info@wealthindia.global</a>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="contact-info-item">
                        <div class="icon-bar">
                            <i class="fas fa-mobile-alt"></i>
                        </div>
                        <div class="info-details">
                            <h5>Phone Number</h5>
                            <a href="tel:+8801234567890">18001038581</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- <div class="google-map" id="map"></div> --}}
<div class="contact ">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="contact-form-outer">
                    <div class="row justify-content-center">
                        <div class="col-xl-8 col-lg-8">
                            <div class="section-title text-center">
                                <h2>Contact Us For <span>Support</span></h2>
                                <p>Put your investing ideas into action with full range of investments.
                                Enjoy real benefits and rewards on your accrue investing.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12 col-lg-12">
                            <form id="contact-form" action="/contact/form" method="POST">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12">
                                        <div class="form-group">
                                            <label for="InputName">Name<span class="requred">*</span></label>
                                            <input type="text" name="name" class="form-control" id="InputName" placeholder="Enter Your Name"
                                            required>
                                        </div>
                                        <p id="name-error"></p>
                                    </div>
                                    <div class="col-xl-12 col-lg-12">
                                        <div class="form-group">
                                            <label for="InputMail">E-mail<span class="requred">*</span></label>
                                            <input type="email" name="email" class="form-control" id="InputMail" placeholder="Enter Your E-mail Address"
                                            required>
                                        </div>
                                        <p id="email-error"></p>
                                    </div>
                                    {{-- <div class="col-xl-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="InputPhone">Phone<span class="requred">*</span></label>
                                            <input type="phone" name="phone" class="form-control" id="InputPhone" placeholder="Enter Your Phone Number"
                                            required>
                                        </div>
                                    </div> --}}
                                    <div class="col-xl-12 col-lg-12">
                                        <div class="form-group">
                                            <label for="InputSubject">Subject<span class="requred">*</span></label>
                                            <input type="text" name="subject" class="form-control" id="InputSubject" placeholder="Enter Your Subject"
                                            required>
                                        </div>
                                        <p id="subject-error"></p>
                                    </div>
                                    <div class="col-xl-12 col-lg-12">
                                        <div class="form-group">
                                            <label for="exampleFormControlTextarea1">Message<span class="requred">*</span></label>
                                            <textarea name="message" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Enter Your Meassage"
                                            required></textarea>
                                        </div>
                                        <p id="message-error"></p>
                                    </div>
                                    <div class="col-xl-12 col-lg-12" style="text-align: center;">
                                        <input type="button" class="btn btn-primary" style="background-color: #38B64A" value="Send Now" onclick="submitForm()">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- contact end -->

@endsection
@section('script')
<script>
  function submitForm(){

     var name = $('#InputName').val();
     var email = $('#InputMail').val();
     var subject = $('#InputSubject').val();
     var message = $('#exampleFormControlTextarea1').val();
     if(name == ""){
        $('#name-error').text('* Name is required.');
    }
    else{
        $('#name-error').hide();

    }

    if(email == ""){
        $('#email-error').text('* Email is required.');
    }
    else{
        $('#email-error').hide();

    }

    if(subject == ""){
        $('#subject-error').text('* Subject is required.');
    }
    else{
        $('#subject-error').hide();

    }

    if(message == ""){
        $('#message-error').text('* Message is required.');
    }
    else{
        $('#message-error').hide();

    }

    if(name!="" && email !="" && subject!="" && message!=""){

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            /* the route pointing to the post function */
            url: '/post-contact-form',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            data: {_token: CSRF_TOKEN, email: email, name:name , subject:subject, message:message},
            success: function (data) { 
                Swal('Thank You. We will reply you soon.');
                document.getElementById("contact-form").reset();
                
            }
        });
    }
} 


</script>

@endsection