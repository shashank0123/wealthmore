
@extends('layouts/home')

<style>
    
</style>

@section('content')
<!-- header end -->
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
 <!--  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
</ol> -->
<div class="carousel-inner">
    @if($banners != null)
    @foreach($banners as $banner)
    <div class="carousel-item <?php if($count_banner == 1){ echo 'active'; $count_banner++; } ?>">
      <img style="width: 100%; height: 480px; margin-top: 70px; filter: brightness(100%);" src="/assetsss/images/AdminProduct/banner/{{$banner->image_url}}" alt="...">  
  </div>
  @endforeach
  @endif
  
</div>
<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
</a>
<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
</a>
</div>



<!-- about us-->
<div class="choosing-reason">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-8">
                <div class="section-title">
                    <h2>About Us</h2>                    
                </div>
            </div>
        </div>
        
        <div class="row">

            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="single-reason" style="height: 480px">
                    <div class="part-img">
                        <img src="/img/shopping.png" alt="" class="service-img">
                    </div>
                    <div class="part-text">
                        <h3>Ecommerce</h3>
                        <p>Ecommerce, also known as electronic commerce or internet commerce, refers to the buying 
                            and selling of goods or services using the internet, and the transfer of money and data to execute these 
                            transactions. Ecommerce is often used to refer to the sale of physical products online, but it can also describe
                        any kind of commercial transaction that is facilitated through the internet.</p>
                        {{-- <a href="#">Read More</a> --}}
                    </div>
                </div>
            </div>


            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="single-reason" style="height: 480px; text-align: center;">
                    <div class="part-img">
                        <img src="/img/mlm.png" alt="" class="service-img">
                    </div>
                    <div class="part-text">
                        <h3>MultiLevel Marketing (MLM)</h3>
                        <p>MultiLevel Marketing (MLM), also called pyramid selling,
                         network marketing, and referral marketing,
                         is a marketing strategy for the sale of products or services where the revenue
                         of the MLM company is derived from a non-salaried workforce selling the company's products/services,
                         while the earnings of the participants are derived from a pyramid-shaped or binary 
                     compensation commission system .</p>
                     {{-- <a href="#">Read More</a> --}}
                 </div>
             </div>
         </div>


         <div class="col-xl-4 col-lg-4 col-md-6">
            <div class="single-reason" style="height: 480px">
                <div class="part-img">
                    <img src="/img/crowd.png" alt="" class="service-img">
                </div>
                <div class="part-text">
                    <h3>Crowdfunding</h3>
                    <p>Crowdfunding is the practice of funding a project or venture by raising small amounts of money from a
                       large number of people, typically via the Internet. Crowdfunding is a form of crowdsourcing and alternative
                   finance.</p>
                   {{-- <a href="#">Read More</a> --}}
               </div>
           </div>
       </div>
   </div>
</div>
</div>
<!-- About Us -->

<!-- Business Plan-->
<div class="we-think-global">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-xl-6 col-lg-6 col-md-10">
                <div class="part-left">
                    <h2>Business Plan<br/>
                    </h2>
                    <p>The people or nations of the world, considered as being closely 
                    connected by modern telecommunications and as being economically, socially, and politically interdependent.</p>
                    <a href="#">Learn More</a>
                </div>
            </div>

            <div class="col-xl-6 col-lg-6 col-md-10 d-flex align-items-center">
                {{-- Slider Here --}}
                
                <div id="demo" class="carousel slide" data-ride="carousel">

                  <!-- Indicators -->
                  <ul class="carousel-indicators">
                    <li data-target="#demo" data-slide-to="0" class="active"></li>
                    <li data-target="#demo" data-slide-to="1"></li>
                    <li data-target="#demo" data-slide-to="2"></li>
                </ul>

                <!-- The slideshow -->
                <div class="carousel-inner">
                    @if(!empty($business))
                    <?php $i=0; ?>
                    @foreach($business as $bus)
                    <div class="carousel-item <?php if($i==0){echo 'active'; $i++;} ?>">
                      <img src="/assetsss/images/AdminProduct/banner/{{$bus->image_url}}" alt="Los Angeles" style="width: 100%; height: auto">
                  </div>
                  @endforeach
                  @endif
    {{-- <div class="carousel-item">
      <img src="https://static.toiimg.com/photo/68301634.cms" alt="Chicago" style="width: 100%; height: 300px">
    </div>
    <div class="carousel-item">
      <img src="https://static.toiimg.com/photo/68301634.cms" alt="New York" style="width: 100%; height: 300px">
  </div> --}}
</div>

<!-- Left and right controls -->
<a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
</a>
<a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
</a>

</div>

{{-- Slider Here --}}
</div>
</div>
</div>
</div>
<!-- Busniess Plan -->


<!-- Latest Products -->
<div class="about">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-8 img_align wow slideInLeft latest" data-wow-duration="2s" data-wow-delay="0.5s">
                <div class="section-title">
                    <h2>Latest Products</h2>                   
                </div>
            </div>
        </div>
        <div class="row img_align wow slideInLeft latest" >
            @if(!empty($products))
            <?php $i=0; ?>
            @foreach($products as $product)
            @if($i==8)
            <?php break; ?>
            @else
            <?php $i++; ?>
            <div class="col-xl-3 col-lg-3 col-md-3">
                <a href="/product-detailw/{{$product->slug_name}}">
                    <div class="single-about">
                        <div class="heading">
                            <img src="\assetsss\images\AdminProduct\{{$product->image1}}" style="height: 300px; width: 100%">
                        </div>
                        <h5>{{strtoupper($product->name)}}</h5>
                        <h6>Rs. {{$product->sell_price}}</h6>
                        
                    </div>
                </a>
            </div>
            @endif
            @endforeach
            @endif            

        </div>
    </div>
</div>
<!-- Latest Products -->
<div style="text-align: center;">
    <a href="/en/shop" class="btn btn-primary" style="background-color: #38B64A">See More</a>
</div>

<br>
<br>

<!-- transaction begin-->
<div class="transaction">
    <div class="container">

        <div class="row" style="text-align: center;">
            <div class="col-sm-3">
                <i class="fa fa-users"></i>
                <h1 class="count">{{$total_members}}</h1>
                <h3>Members</h3>
            </div>
            <div class="col-sm-3">
                <i class="fa fa-shopping-cart"></i>
                <h1 class="count">{{$total_products}}</h1>
                <h3>Products</h3>
            </div>
            <div class="col-sm-3">
                <i class="fa fa-user"></i>
                <h1 class="count">0</h1>
                <h3>Income</h3>
            </div>
            <div class="col-sm-3">
                <i class="fa fa-plus"></i>
                <h1 class="count">0</h1>
                <h3>Profit</h3>
            </div>
        </div>
        
    </div>
</div>
<!-- transaction end -->

<!-- inventor begin-->
<div id="investors">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-6">
                <div class="section-title text-center">
                    <h2>Our Top Achievers</h2>
                    <p>Put your investing ideas into action with full range of investments.
                    Enjoy real benefits and rewards on your accrue investing.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="box">
                    <div class="image">
                        <img class="img-fluid" src="/home-assets/img/t1.png" alt="">
                        <div class="social_icon">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-google-plus-g"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-linkedin-in"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fas fa-envelope"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="info">
                        <h5>Tamim Ubaidah
                        </h5>
                        <p>Web Designer</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="box">
                    <div class="image">
                        <img class="img-fluid" src="/home-assets/img/t2.png" alt="">
                        <div class="social_icon">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-google-plus-g"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-linkedin-in"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fas fa-envelope"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="info">
                        <h5>Mamunur Rashid</h5>
                        <p>Web Designer</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="box">
                    <div class="image">
                        <img class="img-fluid" src="/home-assets/img/t3.png" alt="">
                        <div class="social_icon">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-google-plus-g"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-linkedin-in"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fas fa-envelope"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="info">
                        <h5>Rex Rifat</h5>
                        <p>Web Designer</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="box">
                    <div class="image">
                        <img class="img-fluid" src="/home-assets/img/t4.png" alt="">
                        <div class="social_icon">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-google-plus-g"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-linkedin-in"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fas fa-envelope"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="info">
                        <h5>David Martin</h5>
                        <p>Web Designer</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- inventor end -->



{{-- Testimonials --}}



<!-- TESTIMONIALS -->
<section class="testimonials">

    <h2> What our clients say about us.</h2>
    <div class="container">

      <div class="row">
        <div class="col-sm-12">
          <div id="customers-testimonials" class="owl-carousel">

            <!--TESTIMONIAL 1 -->
            @if(!empty($testimonials))
            @foreach($testimonials as $test)
            <div class="items">
              <div class="shadow-effect">
                <img src="{{URL::to('/')}}/assetsss/images/AdminProduct/{{$test->image_url}}" alt="{{$test->client_name}}">
                <p>{{$test->review}}</p>
            </div>
            <div class="testimonial-name">{{strtoupper($test->client_name)}}</div>
        </div>
        @endforeach
        @endif

        <!--END OF TESTIMONIAL 1 -->
           {{--  <!--TESTIMONIAL 2 -->
            <div class="items">
              <div class="shadow-effect">
                <img class="img-circle" src="http://themes.audemedia.com/html/goodgrowth/images/testimonial3.jpg" alt="">
                <p>Dramatically maintain clicks-and-mortar solutions without functional solutions. Completely synergize resource taxing relationships via premier niche markets. Professionally cultivate.</p>
              </div>
              <div class="testimonial-name">ANNA ITURBE</div>
            </div>
            <!--END OF TESTIMONIAL 2 -->
            <!--TESTIMONIAL 3 -->
            <div class="items">
              <div class="shadow-effect">
                <img class="img-circle" src="http://themes.audemedia.com/html/goodgrowth/images/testimonial3.jpg" alt="">
                <p>Dramatically maintain clicks-and-mortar solutions without functional solutions. Completely synergize resource taxing relationships via premier niche markets. Professionally cultivate.</p>
              </div>
              <div class="testimonial-name">LARA ATKINSON</div>
            </div>
            <!--END OF TESTIMONIAL 3 -->
            <!--TESTIMONIAL 4 -->
            <div class="items">
              <div class="shadow-effect">
                <img class="img-circle" src="http://themes.audemedia.com/html/goodgrowth/images/testimonial3.jpg" alt="">
                <p>Dramatically maintain clicks-and-mortar solutions without functional solutions. Completely synergize resource taxing relationships via premier niche markets. Professionally cultivate.</p>
              </div>
              <div class="testimonial-name">IAN OWEN</div>
            </div>
            <!--END OF TESTIMONIAL 4 -->
            <!--TESTIMONIAL 5 -->
            <div class="items">
              <div class="shadow-effect">
                <img class="img-circle" src="http://themes.audemedia.com/html/goodgrowth/images/testimonial3.jpg" alt="">
                <p>Dramatically maintain clicks-and-mortar solutions without functional solutions. Completely synergize resource taxing relationships via premier niche markets. Professionally cultivate.</p>
              </div>
              <div class="testimonial-name">MICHAEL TEDDY</div>
            </div>
            <!--END OF TESTIMONIAL 5 --> --}}
        </div>
    </div>
</div>
</div>
</section>
<!-- END OF TESTIMONIALS -->

{{-- Testimonials --}}




{{-- News & Updates --}}

@if(!empty($updates))
<div class="blog-post">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-8">
                <div class="section-title text-center">
                    <h2 class="extra-margin">News & Updates </span></h2>
                    
                </div>
            </div>
        </div>

        <div class="row">
            
            @foreach($updates as $update)
            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="single-blog">
                    <div class="part-img">
                        <img src="/assetsss/images/AdminProduct/updates/{{$update->image_url}}" alt="{{$update->title}}" style="width: 100%; height: 250px">
                    </div>
                    <div class="part-text">
                        <h3><a href="#">{{ucfirst($update->title)}}</a></h3>
                        <h4>
                            <span class="admin">By Admin </span>.
                            <span class="date">{{$update->created_at}}</span>.
                            <span class="category">Tags: {{$update->tags}} </span>
                        </h4>
                        <p><?php echo $update->description; ?></p>
                        {{-- <a class="read-more" href="#"><span><i class="fas fa-book-reader"></i></span> Read More</a> --}}
                    </div>
                </div>
            </div>
            @endforeach
           
            

        </div>
    </div>
</div>

 @endif


{{-- News & Updates --}}









<!-- payment begin-->
<div class="payment">
    <div class="container">
        <div class="row d-flex reorder-xs">

            <div class="col-xl-5 co-lg-5 col-md-6">
                <div class="part-form">
                    <h3>Bank Detail</h3>
                    <form class="payment-form" style="color: #fff">
                        <div class="form-group">
                            <div class="row">
                                <div class="cl-sm-4">
                                    &nbsp;&nbsp;&nbsp;Name : 
                                </div>
                                <div class="cl-sm-8"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="cl-sm-4">&nbsp;&nbsp;&nbsp;Branch : </div>
                                <div class="cl-sm-8"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="cl-sm-4">&nbsp;&nbsp;&nbsp;Ac/No. :</div>
                                <div class="cl-sm-8"></div>
                            </div>                           
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-xl-5 col-lg-5 col-md-6 d-flex align-items-center offset-xl-1 offset-lg-1">
                <div class="row justify-content-center">
                    <div class="col-xl-12 col-lg-12">
                        <div class="section-title" id="accepted">
                            <h2>We Accepted<br /><span>Payment Method</span></h2>
                            <p>You can pay us by using the following payment methds : </p>
                        </div>
                    </div>
                    
                    <div class="col-xl-12 col-lg-12">
                        <div class="part-accept">
                            <div class="single-accept">
                                {{-- <img src="/home-assets/img/accept-card-1.jpg" alt=""> --}}
                                <img src="/img/payumoney.png" alt="" style="height: 60px; width: auto;"> 
                            </div>
                            {{-- <div class="single-accept">
                                <img src="/home-assets/img/accept-card-2.jpg" alt="">
                            </div>
                            <div class="single-accept">
                                <img src="/home-assets/img/accept-card-3.jpg" alt="">
                            </div>
                            <div class="single-accept">
                                <img src="/home-assets/img/accept-card-4.jpg" alt="">
                            </div> --}}
                        </div>
                        <div class="part-accept">
                            {{-- <div class="single-accept">
                                <img src="/home-assets/img/accept-card-5.jpg" alt="">
                            </div>
                            <div class="single-accept">
                                <img src="/home-assets/img/accept-card-6.jpg" alt="">
                            </div>
                            <div class="single-accept">
                                <img src="/home-assets/img/accept-card-7.jpg" alt="">
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- payment end -->

<!-- price begin-->
        <!-- <div class="price">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-6 col-lg-6">
                        <div class="section-title text-center">
                            <h2>Grab our mega package</h2>
                            <p>Put your investing ideas into action with full range of investments.
                                Enjoy real benefits and rewards on your accrue investing.</p>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-xl-12 col-lg-12">
                        <ul class="nav nav-tabs" id="myTab2" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="monthly-tab" data-toggle="tab" href="#monthly" role="tab" aria-selected="true">Monthly</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="yearly-tab" data-toggle="tab" href="#yearly" role="tab" aria-selected="false">Yearly</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent2">
                            <div class="tab-pane fade show active" id="monthly" role="tabpanel" aria-labelledby="monthly-tab">
                                <div class="row">
                                    <div class="col-xl-4 col-lg-4 col-md-6">
                                        <div class="single-price">
                                            <div class="part-top">
                                                <h3>Basic Plan</h3>
                                                <h4>5.50%<br /><span>Per Month</span></h4>
                                            </div>
                                            <div class="part-bottom">
                                                <ul>
                                                    <li>Features</li>
                                                    <li>Minimum Deposit $10</li>
                                                    <li>Miximum Deposit $10,000</li>
                                                    <li>Enhanced security</li>
                                                    <li>Access to all features</li>
                                                    <li>24/7Support</li>
                                                </ul>
                                                <a href="#">Buy Now</a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-xl-4 col-lg-4 col-md-6">
                                        <div class="single-price special">
                                            <div class="part-top">
                                                <h3>Basic Plan</h3>
                                                <h4>5.50%<br /><span>Per Month</span></h4>
                                            </div>
                                            <div class="part-bottom">
                                                <ul>
                                                    <li>Features</li>
                                                    <li>Minimum Deposit $10</li>
                                                    <li>Miximum Deposit $10,000</li>
                                                    <li>Enhanced security</li>
                                                    <li>Access to all features</li>
                                                    <li>24/7Support</li>
                                                </ul>
                                                <a href="#">Buy Now</a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-xl-4 col-lg-4 col-md-6">
                                        <div class="single-price">
                                            <div class="part-top">
                                                <h3>Basic Plan</h3>
                                                <h4>5.50%<br /><span>Per Month</span></h4>
                                            </div>
                                            <div class="part-bottom">
                                                <ul>
                                                    <li>Features</li>
                                                    <li>Minimum Deposit $10</li>
                                                    <li>Miximum Deposit $10,000</li>
                                                    <li>Enhanced security</li>
                                                    <li>Access to all features</li>
                                                    <li>24/7Support</li>
                                                </ul>
                                                <a href="#">Buy Now</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="yearly" role="tabpanel" aria-labelledby="yearly-tab">
                                <div class="row">
                                    <div class="col-xl-4 col-lg-4 col-md-6">
                                        <div class="single-price">
                                            <div class="part-top">
                                                <h3>Basic Plan</h3>
                                                <h4>5.50%<br /><span>Per Month</span></h4>
                                            </div>
                                            <div class="part-bottom">
                                                <ul>
                                                    <li>Features</li>
                                                    <li>Minimum Deposit $10</li>
                                                    <li>Miximum Deposit $10,000</li>
                                                    <li>Enhanced security</li>
                                                    <li>Access to all features</li>
                                                    <li>24/7Support</li>
                                                </ul>
                                                <a href="#">Buy Now</a>
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="col-xl-4 col-lg-4 col-md-6">
                                        <div class="single-price special">
                                            <div class="part-top">
                                                <h3>Basic Plan</h3>
                                                <h4>5.50%<br /><span>Per Month</span></h4>
                                            </div>
                                            <div class="part-bottom">
                                                <ul>
                                                    <li>Features</li>
                                                    <li>Minimum Deposit $10</li>
                                                    <li>Miximum Deposit $10,000</li>
                                                    <li>Enhanced security</li>
                                                    <li>Access to all features</li>
                                                    <li>24/7Support</li>
                                                </ul>
                                                <a href="#">Buy Now</a>
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="col-xl-4 col-lg-4 col-md-6">
                                        <div class="single-price">
                                            <div class="part-top">
                                                <h3>Basic Plan</h3>
                                                <h4>5.50%<br /><span>Per Month</span></h4>
                                            </div>
                                            <div class="part-bottom">
                                                <ul>
                                                    <li>Features</li>
                                                    <li>Minimum Deposit $10</li>
                                                    <li>Miximum Deposit $10,000</li>
                                                    <li>Enhanced security</li>
                                                    <li>Access to all features</li>
                                                    <li>24/7Support</li>
                                                </ul>
                                                <a href="#">Buy Now</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div> -->
        <!-- price end -->

        

        
        







        <!-- newsletter begin-->
        <div class="newsletter">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-7 col-lg-7">
                        <div class="section-title text-center">
                            <h2>Our Newsletter</h2>
                            <p>We bring the right people together to challenge established thinking and drive transformation.
                            We will show the way to successive.</p>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-xl-7 col-lg-7">
                        <form class="newsletter-form" id="news-form">
                            <input type="email" placeholder="Enter your email..." required id="email-news">
                            <button type="button" onclick="submitNews()">Subscribe Now</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- newsletter end -->


        @endsection

        @section('script')
        <script>
          $('.count').each(function () {
              jQuery({ Counter: 0 }).animate({ Counter: $(this).text() }, {
                duration: 1000,
                easing: 'swing',
                step: function () {
                  $(this).text(Math.ceil(this.Counter));
              }
          });
          });

          jQuery(document).ready(function($) {
            "use strict";
                //  TESTIMONIALS CAROUSEL HOOK
                $('#customers-testimonials').owlCarousel({
                    loop: true,
                    center: true,
                    items: 3,
                    margin: 0,
                    autoplay: true,
                    dots:true,
                    autoplayTimeout: 8500,
                    smartSpeed: 450,
                    responsive: {
                      0: {
                        items: 1
                    },
                    768: {
                        items: 2
                    },
                    1170: {
                        items: 3
                    }
                }
            });
            });

          function submitNews(){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var email = $('#email-news').val();

            if(email == ""){
                Swal('First Enter your Email.');
            }
            else{

                $.ajax({
                    /* the route pointing to the post function */
                    url: '/post-newsletter',
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */
                    data: {_token: CSRF_TOKEN, email: email},
                    success: function (data) { 
                        Swal(data.message);
                        document.getElementById("news-form").reset();
                    }
                });
            }
        }
    </script>

    @endsection