<?php
use App\State;
use App\City;

$states = State::all();
$cities = City::all();
?>

@extends('front.loginlayout')
<style>
  body{
    background:url(../assets/img/login.png) no-repeat center center fixed !important;
  }
  .pink-text {  font-size: 24px;  }

  .card{margin-top:0% auto; max-width:50%;margin-left: 25%;margin-top: 10%; background-color: #fff;border-radius: 4%;}

  /*#registered-detail , #referal-detail{
    display: none;
    }*/

    .pull-right button { margin-bottom: 20px !important }
    @media screen and (max-width: 991px)
    {
      .card { max-width:70%;margin-left: 15%;margin-top: 20% !important }
    }

    @media screen and (max-width: 580px)
    {
      .card { max-width:90%;margin-left: 5%; }
    }

    #login-head { width: 100% !important; background-color: #009933 }
    #otpcontainer,#proofupload { display: none; }

    #form-left div{ padding: 0px 20px }
    #form-right div{ padding: 3px 20px }

     #clickBro { display: none; }
  </style>

  @section('title')
  Register - {{ config('app.name') }}
  @stop

  @section('content')
  <div class="clearfix"></div>
  <div class="center">
    <div  class="col-lg-12 col-md-12 col-sm-12" style="background: #009933; position: absolute; top: 0;">
      <div style="background-color: #009933;padding-left: 10px; padding-right: 10px; padding-top: 5px;
      padding-bottom: 0;" class="pull-right" >
      <ul>
        <li style="list-style: none">
          <a href="#" style="color: #fff; font-size: 14px">Help Line: 18001038581</a><br>
        </li>

      </ul>
    </div>
  </div>

  <div class="card bordered z-depth-2" style="margin-top: 70px">
    <div class="card-header">
      <div class="brand-logo" style="text-align: center;">
        <img src="{{ asset('/wealth-assets/img/logo-bella-shop1.png') }}" width="40%" height="auto">
      </div>
    </div>
    <div id="formcontainer" class="container m-b-30" style="max-width: 90%;">
      <form class="form-floating action-form" id="register" http-type="post" data-url="{{ route('registerforuse', ['lang' => \App::getLocale()]) }}" enctype="multipart/form-data">

        <div class="card-content">
          <div class="m-b-30">
            <div class="card-title strong black-text" style="font-weight: bold;text-transform: uppercase !important;">Registration Form - Become a member</div>
          </div>
          <div class="row">

            <div class="col-sm-12" id="form-left">

              <div class="form-group">
                <label for="registered_by">Referal ID</label>
                <input type="text" style="text-transform: uppercase;" @if (isset($_GET['referal_id'])) {{'readonly'}} @endif value="@if (isset($_GET['referal_id'])) {{$_GET['referal_id']}} @endif" name="registered_by" class="form-control" id="registered_by" required="required" onchange = "getUserDetail1()">
              </div>

              <p id="registered-detail" style="text-align: center; color : #f00"></p>

              <div class="form-group">
                <label for="referal_id">Location ID</label>
                <input type="text" style="text-transform: uppercase;" @if (isset($_GET['referal_id'])) {{'readonly'}} @endif value="@if (isset($_GET['referal_id'])) {{$_GET['referal_id']}} @endif" name="referal_id" class="form-control" id="referal_id" required="required" onchange = "getUserDetail2()">
              </div>

              <p id="referal-detail" style="text-align: center; color : #f00"></p>

              <div class="form-group">
                <label for="placement">Placement</label>
                <select  class="form-control" name="placement" id="placement">
                  <option value="left">Left</option>
                  <option value="right">Right</option>
                </select>
                {{-- <input type="text" name="username" class="form-control" id="username" required="required"> --}}
              </div>

              <div class="form-group">
                <label for="fullname">Name</label>
                <input type="text" name="fullname" class="form-control" id="fullname" required="required">
              </div>

              <div class="form-group">
                <label for="email">Email</label>
                <input type="text" name="email" class="form-control" id="email" required="required">
              </div>


              <div class="form-group">
                <label for="username">Username</label>
                <input type="text" name="username" class="form-control" id="username" required="required"  onchange="checkExistance()">
              </div>
              <p id="check-availability" style="text-align: center; color : #f00"></p>

              <div class="form-group">
                <label for="mobile">Mobile</label>

                <input type="phone" name="mobile" class="form-control" id="mobile" pattern="[789][0-9]{9}" required="required">
              </div>


          

              <div class="form-group">
                <label for="dob">Date of Birth</label>

                <input type="date" name="dob" class="form-control" id="dob" required="required">
              </div>

              <div class="form-group">
                <label for="state">State</label>
                <select name="state" id="state" class="form-control" required>
                  <option>Select State</option>
                  @foreach($states as $state)
                  <option value="{{$state->id}}">{{$state->state}}</option>
                  @endforeach
                </select>
                {{-- <input type="text" name="mobile" class="form-control" id="mobile" pattern="[789][0-9]{9}" required="required"> --}}
              </div>

              <div class="form-group">
                <label for="city">City</label>
                <select name="city" id="city" class="form-control" required>
                  <option>Select City</option>
                  @foreach($cities as $city)
                  <option value="{{$city->id}}">{{$city->city}}</option>
                  @endforeach
                </select>
                {{-- <input type="text" name="mobile" class="form-control" id="mobile" pattern="[789][0-9]{9}" required="required"> --}}
              </div>

        {{-- <div class="form-group">
          <label for="password">Password</label>
          <input type="password" name="password" class="form-control" id="password" required="required">
        </div>

        <div class="form-group">
          <label for="password">Confirm Password</label>
          <input type="password" name="cpassword" class="form-control" id="cpassword" required="required">
        </div> --}}

        {{-- <div class="form-group">
          <label for="password">Transaction Password</label>
          <input type="password" name="secret_password" class="form-control" id="secret_password" required="required">
        </div>

        <div class="form-group">
          <label for="password">Confirm Transaction Password</label>
          <input type="password" name="spassword" class="form-control" id="spassword" required="required">
        </div> --}}
      
      </div>

  </div>

  <h5 style="text-align: left; font-weight: bold;">&nbsp;&nbsp;<a href="/term" style=" color: blue!important"><input type="checkbox"> &nbsp;&nbsp;Terms & conditions</a></h5>

        {{-- <div class="form-group">
          <label for="username">PAN Number</label>
          <input type="text" name="pan" class="form-control" id="pan" pattern="[A-Z]{5}[0-9]{4}[A-Z]{1}" required="required">
        </div> --}}
      </div>

      <div class="card-action clearfix">
        <div class="pull-left">
          <a href="/en/login" class="btn btn-link black-text">              
            <span style="color:#00f; font-weight: bold;">Login</span>
          </a>
        </div>

        <div class="pull-right">
          <span id="submit" onclick="showDiv()" class="btn btn-link black-text">
            <span class="btn-preloader">
              <i class="md md-cached md-spin"></i>
            </span>
            <span style="font-weight: bold; color: #f00">@lang('login.savetitle')</span>
          </span>
        </div>
      </div>
       </form>


      <br>
    </div>


    <div id="proofupload" class="container m-b-30" style="max-width: 100%;">
       <form method="post" action="/upload-adhaar-front" enctype="multipart/form-data" id="upload_image">
        <div class="card-content">
          <div class="m-b-30">
            <div class="card-title strong black-text" style="font-weight: bold;">Verify Your OTP</div>
          </div>
          <input type="hidden" name="usernameproof" id="proofuser">
          {{-- <input type="hidden" name="username" id="proofemailvalue"> --}}
          {{-- <input type="hidden" name="sponsoredId" id="sponsoredId"> --}}




          <div class="form-group">
          <label for="adhaar">Adhaar Number</label>
          <input type="text" name="adhaar" class="form-control" id="adhaar" pattern="[0-9]{12}" required="required">
        </div>


       
          <label for="adhaar_front">Upload Adhaar Card image (Front Side)</label>
          <input type="file" name="adhaar_front" class="form-control" id="adhaar_front"  required="required" >
          
          {{-- <input type="file" name="file" id="file" onchange='submitMe()'/> --}}
        

        <div class="form-group">
          <label for="adhaar_back">Upload Adhaar Card image (Back Side)</label>
          <input type="file" name="adhaar_back" class="form-control" id="adhaar_back"  required="required" onchange="submitImage()">
        </div>

        <div class="card-action clearfix"> 
          <div class="pull-right" >
          <input type="submit" name="submit" id="clickBro" />
            <span id="verify" onclick="showOtpDiv()" class="btn btn-link black-text">
              <span class="btn-preloader">
                <i class="md md-cached md-spin"></i>
              </span>
              <span style="font-weight: bold; color: #f00">@lang('login.savetitle')</span>
            </span>
          </div>
        </div>
      </form>
      <h5 style="text-align: center;font-weight: bold;"><a href="#" style="color: blue!important">Terms & conditions</a></h5>
    </div>





    <div id="otpcontainer" class="container m-b-30" style="max-width: 100%;">
      <form id="otpform" class="form-floating action-form" http-type="post" data-url="{{ route('verifyotp', ['lang' => \App::getLocale()]) }}">
        <div class="card-content">
          <div class="m-b-30">
            <div class="card-title strong black-text" style="font-weight: bold;">Verify Your OTP</div>
          </div>
          <input type="hidden" name="username" id="emailvalue">
          <input type="hidden" name="sponsoredId" id="sponsoredId">

          <div class="form-group">
            <label for="otp">Enter OTP</label>
            <input type="text" name="otp" class="form-control" id="otp" required="">
          </div>
        </div>
        <input type="hidden" name="verify_otp" class="form-control" id="verify_otp" required="">

        <div class="card-action clearfix"> 
          <div class="pull-right" >
            <span id="verify" onclick="verifyOTP()" class="btn btn-link black-text">
              <span class="btn-preloader">
                <i class="md md-cached md-spin"></i>
              </span>
              <span style="font-weight: bold; color: #f00">@lang('login.savetitle')</span>
            </span>
          </div>
        </div>
      </form>
      <h5 style="text-align: center;font-weight: bold;"><a href="#" style="color: blue!important">Terms & conditions</a></h5>
    </div>
  </div>

</div>

@stop

<script>    


function submitMe() {
   
    $("#clickBro").click();
  }


  function getUserDetail1(){
    var name = $('#registered_by').val();
      // alert(name);
      $.ajax({
        type: "POST",
        url: "/user-name",
        data:{ uname: name },
        success:function(msg){
          if(msg.message == 'Valid ID'){
            $('#registered-detail').text('Name : ' + msg.name);
          }
          else{
            $('#registered-detail').text(msg.message);
          }          
        }
      });
    }

    function getUserDetail2(){
      var name = $('#referal_id').val();
      // alert(name);
      $.ajax({
        type: "POST",
        url: "/user-name",
        data:{ uname: name },
        success:function(msg){
          if(msg.message == 'Valid ID'){

            $('#referal-detail').html("Name : "+msg.name);
          }
          else{
            $('#referal-detail').html(msg.message);
          }          
        }
      });
    }

    function checkExistance(){
      var name = $('#username').val();
      // alert(name);
      $.ajax({
        type: "POST",
        url: "/user-availability",
        data:{ uname: name },
        success:function(msg){
          if(msg.message == 'Username already exists. Try another') {
            $('#check-availability').html(msg.message);
          } 
          else{
            $('#check-availability').hide();              
          } 
        }


      });
    }


    function showDiv(event){  
    // event .preventDefault();    
    var registered_by = $("#registered_by").val();
    var referal_id = $("#referal_id").val();
    var placement = $("#placement").val();
    var fullname = $("#fullname").val();
    var email = $("#email").val();
    var username = $("#username").val();
    var mobile = $("#mobile").val();
    var dob = $("#dob").val();
    var state = $("#state").val();
    var city = $("#city").val();
    // var adhaar = $("#adhaar").val();
    // var adhaar_front = $("#adhaar_front").val();
    // var adhaar_back = $("#adhaar_back").val(); 

    if(registered_by==""||referal_id==""||placement==""||fullname==""||email==""||username==""||mobile==""||dob==""||state==""||city==""){
      Swal('All fields are mendatory');
    }

    else{

      var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;     
      if (reg.test(email) == false) {
        Swal('Invalid Email Address'); 
      }

      else{

        var phoneNum = mobile.replace(/[^\d]/g, '');
        if(phoneNum.length >9 && phoneNum.length < 11) {       


         // var adhaarNum = adhaar.replace(/[^\d]/g, '');

         // if(adhaarNum.length > 11 && adhaarNum.length < 17) { 

           var csrf_token = $('meta[name="_t"]').attr('content');


           alert(registered_by+" / "+referal_id+" / "+placement+" / "+fullname+" / "+email+" / "+username+" / "+mobile+" / "+dob+" / "+state+" / "+city);

           $.ajax({
            url: 'register',
            type: 'POST',
            data: { _CSRF: csrf_token, referal_id: referal_id, registered_by: registered_by, username:username, fullname: fullname, placement: placement, email: email, mobile: mobile, dob: dob, state: state, city: city},
          // cache: false,
          // contentType: false,
          // processData: false,

          success:function(msg) {
            if (msg.message == 'Data Inserted Successfully'){
              $('#formcontainer').hide();
              $("#otpcontainer").hide();
              $("#proofupload").show();
              $('#get_pass').val(msg.password);
              $('#get_user').val(msg.username);
              $('#proofuser').val($('#usernameproof').val());
              $('#emailvalue').val($('#username').val());
              console.log(msg);
            }
            else{
              Swal(msg.message);                  
            }
          }, 
        });  
        //  }
        //  else{       
        //   Swal('Invalid Adhaar Number Format')
        // }
      }
      else{ 
        Swal('Invalid Phone Nember Format');
      }
    }
  }
}

function submitImage(){
  $('#clickBro').click();
}


function showOtpDiv(){
  $('#formcontainer').hide();
  $('#proofupload').hide();
  $('#otpcontainer').show();
}

function verifyOTP(){    
  var request = $.ajax({
    type: "POST",
    url: "verifyotp",
    data: $('#otpform').serialize(),      
  });
  request.done(function(msg) {
    if (msg.message == 'Registered Successfully. Please Wait we will contact you shortly.'){

      // Swal(msg.message);
      window.location.href = "/en/login";      
    }
    else{
      Swal(msg.message);
    }
  });

  request.fail(function(msg) {
    Swal(msg.message);
  });
}  
</script>
