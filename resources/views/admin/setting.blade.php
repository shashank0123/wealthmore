@extends('layouts.admin')

@section('content')


<section style="margin-top: 50px;">
<div class="container">
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Email</th>
        <th>Mobile </th>
        <th>Password</th>
        <th>User type</th>
        <th>Remember token</th>
        <th>Created at</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1</td>
        <td>Shivam jha</td>
        <td>ry696973@gmail.com</td>
        <td>1234567890</td>
        <td>shivam@1234</td>
        <td>admin</td>
        <td>32</td>
        <td>15/04/2019</td>
        <td><button class="btn btn-primary" style="width: 100px; height: 35px">Edit</button></td>
      </tr>
       <tr>
        <td>2</td>
        <td>Shivam jha</td>
        <td>ry696973@gmail.com</td>
        <td>1234567890</td>
        <td>shivam@1234</td>
        <td>admin</td>
        <td>32</td>
        <td>15/04/2019</td>
        <td><button class="btn btn-primary" style="width: 100px; height: 35px">Edit</button></td>
      </tr>
       <tr>
        <td>3</td>
        <td>Shivam jha</td>
        <td>ry696973@gmail.com</td>
        <td>1234567890</td>
        <td>shivam@1234</td>
        <td>member</td>
        <td>32</td>
        <td>15/04/2019</td>
        <td><button class="btn btn-primary" style="width: 100px; height: 35px">Edit</button></td>
      </tr>
       <tr>
        <td>4</td>
        <td>Shivam jha</td>
        <td>ry696973@gmail.com</td>
        <td>1234567890</td>
        <td>shivam@1234</td>
        <td>member</td>
        <td>32</td>
        <td>15/04/2019</td>
        <td><button class="btn btn-primary" style="width: 100px; height: 35px">Edit</button></td>
      </tr>
       <tr>
        <td>5</td>
        <td>Shivam jha</td>
        <td>ry696973@gmail.com</td>
        <td>1234567890</td>
        <td>shivam@1234</td>
        <td>user</td>
        <td>32</td>
        <td>15/04/2019</td>
        <th><button class="btn btn-primary" style="width: 100px; height: 35px">Edit</button></th>
      </tr>
       <tr>
        <td>6</td>
        <td>Shivam jha</td>
        <td>ry696973@gmail.com</td>
        <td>1234567890</td>
        <td>shivam@1234</td>
        <td>user</td>
        <td>32</td>
        <td>15/04/2019</td>
        <td><button class="btn btn-primary" style="width: 100px; height: 35px">Edit</button></td>
      </tr>
       <tr>
        <td>7</td>
        <td>Shivam jha</td>
        <td>ry696973@gmail.com</td>
        <td>1234567890</td>
        <td>shivam@1234</td>
        <td>admin</td>
        <td>32</td>
        <td>15/04/2019</td>
        <td><button class="btn btn-primary" style="width: 100px; height: 35px">Edit</button></td>
        
      </tr>
       <tr>
        <td>8</td>
        <td>Shivam jha</td>
        <td>ry696973@gmail.com</td>
        <td>1234567890</td>
        <td>shivam@1234</td>
        <td>admin</td>
        <td>32</td>
        <td>15/04/2019</td>
        <td><button class="btn btn-primary" style="width: 100px; height: 35px">Edit</button></td>
      </tr>
       <tr>
        <td>9</td>
        <td>Shivam jha</td>
        <td>ry696973@gmail.com</td>
        <td>1234567890</td>
        <td>shivam@1234</td>
        <td>member</td>
        <td>32</td>
        <td>15/04/2019</td>
        <td><button class="btn btn-primary" style="width: 100px; height: 35px">Edit</button></td>
        
      </tr>
       <tr>
        <td>10</td>
        <td>Shivam jha</td>
        <td>ry696973@gmail.com</td>
        <td>1234567890</td>
        <td>shivam@1234</td>
        <td>member</td>
        <td>32</td>
        <td>15/04/2019</td>
        <td><button class="btn btn-primary" style="width: 100px; height: 35px">Edit</button></td>
        
      </tr>
    </tbody>
  </table>
</div>
</section>


@endsection
