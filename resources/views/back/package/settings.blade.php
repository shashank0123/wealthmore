<?php
use App\Models\Package;
$packages = Package::orderBy('id', 'asc')->get();
?>

@extends('back.app')

@section('title')
Package Settings | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">Front Page</a></li>
  <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
  <li class="active">Package Settings</li>
</ul>
@stop

@section('content')
<main>
  {{-- @include('back.include.sidebar') --}}
  <div class="main-container">
    {{-- @include('back.include.header') --}}
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section class="tables-data">
        <div class="page-header">
          <h1><i class="md md-wallet-giftcard"></i> Package</h1>
        </div>

        <div class="card">
          <div>
            <table id="packageListTable" class="table table-full table-bordered table-full-small" cellspacing="0" width="100%" role="grid">
              <thead>
                <tr>
                  <th>Amount</th>
                  <th>Package Name</th>
                  <th>Group Level</th>
                  <th>Max Payout</th>
                  <th>Payout Days</th>
                  <th>Purchase Point</th>
                  <th>Color</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @if (count($packages) > 0)
                @foreach ($packages as $package)
                <tr>
                  <td>
                    <div class="input-group">
                      <span class="input-group-addon">RS</span>
                      <input type="number" class="form-control" name="package_amount" value="{{ (float) $package->package_amount }}" required="" min="0">
                    </div>
                  </td>

                  <td>
                    <div class="input-group">
                      <input type="text" class="form-control" name="package_value" value="{{ $package->package_name }}" required="">
                    </div>
                  </td>

                  
                  <td>
                    <div class="input-group">
                      <input type="number" name="group_level" class="form-control" value="{{ (integer) $package->group_level }}" required="" min="0">
                      <span class="input-group-addon">level(s)</span>
                    </div>
                  </td>

                  <td>
                    <div class="input-group">
                      <span class="input-group-addon">RS</span>
                      <input type="text" class="form-control" name="max_pair" value="{{ $package->max_pair }}" required="">
                    </div>
                  </td>

                  <td>
                    <div class="input-group">
                      <!--<span class="input-group-addon">$</span>-->
                      <input type="text" class="form-control" name="max_pairing_bonus" value="{{ $package->max_pairing_bonus }}" required="">
                    </div>
                  </td>

                  
                  <td>
                    <div class="input-group">
                      <!--<span class="input-group-addon">$</span>-->
                      <input type="number" class="form-control" name="purchase_point" value="{{ (float) $package->purchase_point }}" required="" min="0">
                    </div>
                  </td>

                  <td>
                    <div class="input-group">
                      <input type="text" class="form-control" name="package_color" value="{{ $package->package_color }}" required="">
                    </div>
                  </td>

                  <td>
                    <button class="btn btn-warning btn-flat-border btn-update" data-url="{{ route('admin.package.update', ['id' => $package->id]) }}" type="submit">
                      <i class="md md-mode-edit"></i> Update
                    </button>
                  </td>
                </tr>
                @endforeach
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </section>
    </div>
  </div>
</main>
@stop
