<?php
  use App\Models\Member;
  $members = Member::all();

  use App\Models\Package;
  $packages = Package::all();
?>

@extends('back.app')

@section('title')
  Upgrade Package| {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">Front Page</a></li>
  <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
  <li class="active">Upgrade Package</li>
</ul>
@stop

@section('content')
<main>
  {{-- @include('back.include.sidebar') --}}
  <div class="main-container">
    {{-- @include('back.include.header') --}}
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section style="padding: 5px 20px">
        <div class="page-header">
          {{-- <h1><i class="md md-person-add"></i> New Epoint</h1> --}}
          <p class="lead">Upgrade Package</p>
        </div>
        <div class="well white">
          <form class="action-form" data-parsley-validate="" role="form" onsubmit="reset()"  id="buyepoint" http-type="post" data-url="{{ route('admin.package.upgrade-package') }}">
            <fieldset>
              <div class="form-group">
                <label class="control-label" for="memberName">Username</label>
                <div class="input-group">
                  <input type="text" name="username" class="form-control" id="inputMember" required="" placeholder="Enter User Name">
                  {{-- <select class="form-control" name="member_id" id="inputMember" required="">
                    @if (count($members) > 0)
                    @foreach ($members as $member)
                    <option value="{{ $member->id }}">
                      {{ $member->username }}
                    </option>
                    @endforeach
                    @endif
                  </select> --}}
                  {{-- <span class="input-group-addon"></span> --}}
                </div>
              </div>

              <div class="form-group">
                <label class="control-label">Package</label>
                <select class="form-control" name="package_id" id="inputPackage" required="">
                    @if (count($packages) > 0)
                    @foreach ($packages as $package)
                    <option value="{{ $package->id }}">
                      {{ number_format($package->package_amount, 0) }}
                    </option>
                    @endforeach
                    @endif
                  </select>
              </div>

              <div class="form-group">
                <button type="submit" class="btn btn-primary">
                  <span class="btn-preloader">
                    <i class="md md-cached md-spin"></i>
                  </span>
                  <span>Submit</span>
                </button>
                <button type="reset" class="btn btn-default">Cancel</button>
              </div>
            </fieldset>
          </form>
        </div>
      </section>
    </div>
  </div>
</main>
@stop
