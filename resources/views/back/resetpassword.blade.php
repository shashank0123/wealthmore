@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')

<style>
  .block-content { width: 90%; margin: 0 5% }
  .page-header { margin-left: 1% }
  h2 { font-weight: 500; }
  .form-control { width: 50% }
</style>
<div class="page-header">
          <h2><i class="md md-person-add"></i> Reset Password</h2>
          {{-- <p class="lead">Add Your Own Blogs</p> --}}
        </div>


{{-- @include('back.include.header') --}}
{{-- @include('back.include.sidebar') --}}
<!-- Page Content -->
<div class="content" style="background: white; width: 98%; margin: 1%">
  <br>
  {{-- <a href="blog" style="margin-left: 4%"><button type="submit" class="btn btn-danger">Back</button> </a><br><br> --}}

  @if($errors->any())
  <div class="alert alert-danger">
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </div>
  @endif

  @if($message = Session::get('message'))
  <div class="alert alert-primary">
    <p>{{ $message }}</p>
  </div>
  @endif
  <div class="row justify-content-center">
    <div class="block-content">
      <form data-parsley-validate="" role="form" class="action-form" id="accountBasicForm" http-type="post" data-url="{{ route('account.passwordUpdate') }}" data-nationality="true" enctype="multipart/form-data">
                  <fieldset>               

                    <div class="form-group">
                      <label class="control-label" for="inputSecret">@lang('settings.old')</label>
                      <input type="password" class="form-control" name="old-password" id="inputSecret" required="">
                    </div>

                   

                    <div class="form-group">
                      <label class="control-label" for="inputPassword">@lang('settings.password')</label>
                      <input type="password" name="password" class="form-control" id="inputPassword" minlength="5">
                    </div>

                    <div class="form-group">
                      <label class="control-label">@lang('settings.repassword')</label>
                      <input type="password" name="confirm-password" class="form-control" data-parsley-equalto="#inputPassword" minlength="5">
                    </div>
                    
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary">
                        <span class="btn-preloader">
                          <i class="md md-cached md-spin"></i>
                        </span>
                        <span>@lang('common.submit')</span>
                      </button>
                      <button type="reset" class="btn btn-default">@lang('common.cancel')</button>
                    </div>
                  </fieldset>
                </form>
  </div>
</div>
</div>
<!-- END Page Content -->
@endsection

