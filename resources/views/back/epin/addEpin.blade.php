<?php
  use App\Models\Member;
  $members = Member::all();

  use App\Models\Package;
  $packages = Package::all();
?>

@extends('back.app')

@section('title')
  Add New Epin | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">Front Page</a></li>
  <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
  <li class="active">Create New Epin</li>
</ul>
@stop

@section('content')
<main>
  {{-- @include('back.include.sidebar') --}}
  <div class="main-container">
    {{-- @include('back.include.header') --}}
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section>
        <div class="page-header">
          <h1><!-- <i class="md md-person-add"></i> --> New Epin</h1>
          <p class="lead">Create New Epin</p>
        </div>
        <div class="well white">
          <form class="action-form" data-parsley-validate="" role="form" id="buyepin" http-type="post" data-url="{{ route('admin.epin.addepin', ['type' => 'root']) }}">
            <fieldset>
              <div class="form-group">
                <label class="control-label" for="memberName">User</label>
                <div class="input-group">
                  <select class="form-control" name="member_id" id="inputMember" required="">
                    @if (count($members) > 0)
                    @foreach ($members as $membere)
                    <option value="{{ $membere->id }}">
                      {{ $membere->username }}
                    </option>
                    @endforeach
                    @endif
                  </select>
                  <span class="input-group-addon"></span>
                </div>
              </div>

              <div class="form-group">
                <label class="control-label">Number Of Epin</label>
                <input type="number" name="no_of_epins" class="form-control" required="">
              </div>

              <div class="form-group">
                <label class="control-label" for="inputPackage">Package</label>
                <div class="input-group">
                  <select class="form-control" name="package_id" id="inputPackage" required="">
                    @if (count($packages) > 0)
                    @foreach ($packages as $package)
                    <option value="{{ $package->id }}">
                      {{ number_format($package->package_amount, 0) }}
                    </option>
                    @endforeach
                    @endif
                  </select>
                  <span class="input-group-addon"></span>
                </div>
              </div>

              <div class="form-group">
                <button type="submit" class="btn btn-primary">
                  <span class="btn-preloader">
                    <i class="md md-cached md-spin"></i>
                  </span>
                  <span>Submit</span>
                </button>
                <button type="reset" class="btn btn-default">Cancel</button>
              </div>
            </fieldset>
          </form>
        </div>
      </section>
    </div>
  </div>
</main>
@stop
