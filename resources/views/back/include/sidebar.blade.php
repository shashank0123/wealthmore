
<div id="sidebar" style="min-height: 750px !important; height: 100% !important">
    <!-- Wrapper for scrolling functionality -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <div class="sidebar-content">
            <!-- Brand -->
            <a href="index.html" class="sidebar-brand">
                <i class="gi gi-flash"></i><span class="sidebar-nav-mini-hide"><strong>Wealthmore</strong>India</span>
            </a>
            <!-- END Brand -->

            <!-- User Info -->
            <div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
                <div class="sidebar-user-avatar">


                    <a href="/en">
                        @if(isset($image->image))
                        <img class="img-avatar" src="/assetsss/images/logo.jpeg" alt="">
                        @else
                        <img class="img-avatar" src="/img/placeholders/avatars/avatar2.jpg" alt="">
                        @endif
                        {{-- <img src="/img/placeholders/avatars/avatar2.jpg" alt="avatar"> --}}
                    </a>
                </div>
                <div class="sidebar-user-name">ABC</div>
                <div class="sidebar-user-links">
                    <a href="#" data-toggle="tooltip" data-placement="bottom" title="Profile"><i class="gi gi-user"></i></a>
                    <a href="#" data-toggle="tooltip" data-placement="bottom" title="Messages"><i class="gi gi-envelope"></i></a>
                    <!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
                    <a href="javascript:void(0)" class="enable-tooltip" data-placement="bottom" title="Settings" onclick="$('#modal-user-settings').modal('show');"><i class="gi gi-cogwheel"></i></a>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit"></i></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
            <!-- END User Info -->

            <!-- Theme Colors -->
            <!-- Change Color Theme functionality can be found in js/app.js - templateOptions() -->
            <ul class="sidebar-section sidebar-themes clearfix sidebar-nav-mini-hide">
                                <!-- You can also add the default color theme
                                <li class="active">
                                    <a href="javascript:void(0)" class="themed-background-dark-default themed-border-default" data-theme="default" data-toggle="tooltip" title="Default Blue"></a>
                                </li>
                            -->
                            <li>
                                <a href="javascript:void(0)" class="themed-background-dark-night themed-border-night" data-theme="css/themes/night.css" data-toggle="tooltip" title="Night"></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="themed-background-dark-amethyst themed-border-amethyst" data-theme="css/themes/amethyst.css" data-toggle="tooltip" title="Amethyst"></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="themed-background-dark-modern themed-border-modern" data-theme="css/themes/modern.css" data-toggle="tooltip" title="Modern"></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="themed-background-dark-autumn themed-border-autumn" data-theme="css/themes/autumn.css" data-toggle="tooltip" title="Autumn"></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="themed-background-dark-flatie themed-border-flatie" data-theme="css/themes/flatie.css" data-toggle="tooltip" title="Flatie"></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="themed-background-dark-spring themed-border-spring" data-theme="css/themes/spring.css" data-toggle="tooltip" title="Spring"></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="themed-background-dark-fancy themed-border-fancy" data-theme="css/themes/fancy.css" data-toggle="tooltip" title="Fancy"></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="themed-background-dark-fire themed-border-fire" data-theme="css/themes/fire.css" data-toggle="tooltip" title="Fire"></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="themed-background-dark-coral themed-border-coral" data-theme="css/themes/coral.css" data-toggle="tooltip" title="Coral"></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="themed-background-dark-lake themed-border-lake" data-theme="css/themes/lake.css" data-toggle="tooltip" title="Lake"></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="themed-background-dark-forest themed-border-forest" data-theme="css/themes/forest.css" data-toggle="tooltip" title="Forest"></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="themed-background-dark-waterlily themed-border-waterlily" data-theme="css/themes/waterlily.css" data-toggle="tooltip" title="Waterlily"></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="themed-background-dark-emerald themed-border-emerald" data-theme="css/themes/emerald.css" data-toggle="tooltip" title="Emerald"></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="themed-background-dark-blackberry themed-border-blackberry" data-theme="css/themes/blackberry.css" data-toggle="tooltip" title="Blackberry"></a>
                            </li>
                        </ul>
                        <!-- END Theme Colors -->

                        <!-- Sidebar Navigation -->
                        <ul class="sidebar-nav">
                            <li>

                                <a href="/admin/dashboard" class="{{ request()->is('dashboard') ? ' active' : '' }}"><i class="gi gi-stopwatch sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">@lang('sidebar.home')</span></a>
                            </li>
                                {{-- <li>
                                    <a href="index2.html"><i class="gi gi-leaf sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard 2</span></a>
                                </li> --}}
                                {{-- <li>
                                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-shopping_cart sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">eCommerce</span></a>
                                    <ul>
                                        <li>
                                            <a href="page_ecom_dashboard.html">Dashboard</a>
                                        </li>
                                        <li>
                                            <a href="page_ecom_orders.html">Orders</a>
                                        </li>
                                        <li>
                                            <a href="page_ecom_order_view.html">Order View</a>
                                        </li>
                                        <li>
                                            <a href="page_ecom_products.html">Products</a>
                                        </li>
                                        <li>
                                            <a href="page_ecom_product_edit.html">Product Edit</a>
                                        </li>
                                        <li>
                                            <a href="page_ecom_customer_view.html">Customer View</a>
                                        </li>
                                    </ul>
                                </li> --}}
                                {{-- <li class="sidebar-header">
                                    <span class="sidebar-header-options clearfix"><a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a><a href="javascript:void(0)" data-toggle="tooltip" title="Create the most amazing pages with the widget kit!"><i class="gi gi-lightbulb"></i></a></span>
                                    <span class="sidebar-header-title">Widget Kit</span>
                                </li> --}}
                                {{-- <li>
                                    <a href="page_widgets_stats.html"><i class="gi gi-charts sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Statistics</span></a>
                                </li>
                                <li>
                                    <a href="page_widgets_social.html"><i class="gi gi-share_alt sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Social</span></a>
                                </li>
                                <li>
                                    <a href="page_widgets_media.html"><i class="gi gi-film sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Media</span></a>
                                </li>
                                <li>
                                    <a href="page_widgets_links.html"><i class="gi gi-link sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Links</span></a>
                                </li>
                                <li class="sidebar-header">
                                    <span class="sidebar-header-options clearfix"><a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a></span>
                                    <span class="sidebar-header-title">Menu</span>
                                </li> --}}
                                <li>
                                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-certificate sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">MLM Features</span></a>
                                    <ul>
                                        <li >
                                           <a class="{{ request()->is('admin/member/all') ? ' active' : '' }}" href="/admin/member/all">Members List</a>
                                       </li>
                                       <li>
                                        <a class="{{ request()->is('admin/member/register') ? ' active' : '' }}" href="/admin/member/register">Add New Root Member</a>
                                    </li>
                                    <li>
                                        <a class="{{ request()->is('admin/epoint/all') ? ' active' : '' }}" href="/admin/epoint/allepoint">Epoint List</a>
                                    </li>
                                    <li>
                                        <a class="{{ request()->is('admin/epoint/add') ? ' active' : '' }}" href="/admin/epoint/add">Add Epoint</a>
                                    </li>
                                    <li>
                                        <a class="{{ request()->is('admin/upgrade-package') ? ' active' : '' }}" href="/admin/upgrade-package">Upgrade Package</a>
                                    </li>
                                        {{-- <li>
                                            <a href="page_ui_horizontal_menu.html">Horizontal Menu</a>
                                        </li>
                                        <li>
                                            <a href="page_ui_progress_loading.html">Progress &amp; Loading</a>
                                        </li>
                                        <li>
                                            <a href="page_ui_preloader.html">Page Preloader</a>
                                        </li>
                                        <li>
                                            <a href="page_ui_color_themes.html">Color Themes</a>
                                        </li> --}}
                                    </ul>
                                </li>
                                <li>
                                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-notes_2 sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Shop Products</span></a>
                                    <ul>
                                        <li>
                                            <a class="{{ request()->is('admin/main-category') ? ' active' : '' }}" href="/admin/main-category">Manage Category</a>
                                        </li>
                                        <li>
                                            <a class="{{ request()->is('admin/product') ? ' active' : '' }}" href="/admin/product">Manage Product</a>
                                        </li>
                                        <li>
                                            <a class="{{ request()->is('admin/banner') ? ' active' : '' }}" href="/admin/banner">Manage Banner Images</a>
                                        </li>
                                        <li>
                                            <a class="{{ request()->is('admin/review') ? ' active' : '' }}" href="/admin/review">Manage Reviews</a>
                                        </li>
                                        <li>
                                            <a class="{{ request()->is('admin/testimonial') ? ' active' : '' }}" href="/admin/testimonial">Manage Testimonial</a>
                                        </li>
                                        <li>
                                            <a class="{{ request()->is('admin/faq') ? ' active' : '' }}" href="/admin/faq">Manage FAQ</a>
                                        </li>
                                        <li>
                                            <a class="{{ request()->is('admin/static-pages') ? ' active' : '' }}" href="/admin/static-pages">Manage Static Pages</a>
                                        </li>

                                        <li>
                                            <a class="{{ request()->is('admin/update') ? ' active' : '' }}" href="/admin/update">Manage Updates</a>
                                        </li>
                                        {{-- <li>
                                            <a href="{{ route('settings.password', ['lang' => \App::getLocale()]) }}">@lang('sidebar.resetpassword')</a>
                                        </li> --}}
                                        {{-- <li>
                                            <a href="page_forms_wizard.html">Wizard</a>
                                        </li> --}}
                                    </ul>
                                </li>

                                <li>
                                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-cup sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Package</span></a>
                                    <ul>
                                        <li>
                                            <a href="{{ route('admin.settings.package') }}">
                                              <span>Package Settings</span>
                                          </a>
                                      </li>

                                        {{-- <li>
                                            <a href="{{ route('income.binary', ['lang' => \App::getLocale()]) }}">Daily binary Income</a>
                                        </li> --}}
                                    </ul>
                                </li>

                                <?php
                                $announcement = \Cache::remember('announcement.new', 60, function () {
                                  return \App\Models\Announcement::whereRaw('Date(created_at) = CURDATE()')->first();
                              });
                              ?>
                            <li>
                                <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-cup sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Income</span></a>
                                <ul>
                                    {{-- <li>
                                        <a href="#">
                                          <span>Daily Payout Income</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Allowance Income</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Car Fund Income</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>House Fund Income</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Reward Income</span>
                                        </a>
                                    </li> --}}
                                </ul>
                            </li>


          
                            <li>
                                <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-cup sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Announcement</span></a>
                                <ul>
                                   {{--  <li>
                                    <a href="{{ route('admin.announcement.create') }}">
                                      <span>Create</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('admin.announcement.list') }}">
                                      <span>List</span>
                                    </a>
                                  </li> --}}
                                </ul>
                            </li>


                            <li>
                                <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-cup sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Shares</span></a>
                                <ul>
                                    {{-- <li>
                                    <a href="{{ route('admin.shares.sellAdmin') }}">
                                      <span>Sell</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('admin.settings.shares') }}">
                                      <span>Settings</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('admin.shares.split') }}">
                                      <span>Split</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('admin.shares.buy') }}">
                                      <span>Buy List</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('admin.shares.sell') }}">
                                      <span>Sell List</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('admin.shares.lock') }}">
                                      <span>Freeze List</span>
                                    </a>
                                  </li> --}}
                                </ul>
                            </li>


                            <li>
                                <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-cup sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Bonus</span></a>
                                <ul>
                                    {{-- <li>
                                    <a href="{{ route('admin.bonus.addStatement') }}">
                                      <span>Add Statement</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('admin.bonus.all') . '?t=direct' }}">
                                      <span>Direct Statement</span>
                                    </a>
                                  </li>

                                  <li>
                                    <a href="{{ route('admin.bonus.all') . '?t=group' }}">
                                      <span>Group Statement</span>
                                    </a>
                                  </li>

                                  <li>
                                    <a href="{{ route('admin.bonus.all') . '?t=override' }}">
                                      <span>Override Statement</span>
                                    </a>
                                  </li>

                                  <li>
                                    <a href="{{ route('admin.bonus.all') . '?t=pairing' }}">
                                      <span>Pairing Statement</span>
                                    </a>
                                  </li> --}}
                                </ul>
                            </li>


                            <li>
                                <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-cup sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Withdraw</span></a>
                                <ul>
                                    {{-- <li>
                                    <a href="{{ route('admin.withdraw.addStatement') }}">
                                      <span>Add Statement</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('admin.withdraw.all') }}">
                                      <span>Statement</span>
                                    </a>
                                  </li> --}}
                                </ul>
                            </li>


                            <li>
                                <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-cup sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Transfer</span></a>
                                <ul>
                                    {{-- <li>
                                    <a href="{{ route('admin.transfer.addStatement') }}">
                                      <span>Add Statement</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('admin.transfer.all') }}">
                                      <span>Transfer statement</span>
                                    </a>
                                  </li> --}}
                                </ul>
                            </li>


                            <li>
                                <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-cup sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Coin</span></a>
                                <ul>
                                    {{-- <li>
                                    <a href="{{ route('admin.coin.transaction') }}">
                                      <span>Transaction List</span>
                                    </a>
                                  </li>
                                  <li>
                                    <a href="{{ route('admin.coin.list') }}">
                                      <span>Wallet List</span>
                                    </a>
                                  </li> --}}
                                </ul>
                            </li>


                            <li icon="md md-settings-power">
                                <a href="{{ route('admin.logout') }}"><i class="md md-settings-power"></i>&nbsp;<span>Logout</span></a>
                              </li>

                                {{-- <li>
                                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-show_big_thumbnails sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Page Layouts</span></a>
                                    <ul>
                                        <li>
                                            <a href="page_layout_static.html">Static</a>
                                        </li>
                                        <li>
                                            <a href="page_layout_static_fixed_footer.html">Static + Fixed Footer</a>
                                        </li>
                                        <li>
                                            <a href="page_layout_fixed_top.html">Fixed Top Header</a>
                                        </li>
                                        <li>
                                            <a href="page_layout_fixed_top_footer.html">Fixed Top Header + Footer</a>
                                        </li>
                                        <li>
                                            <a href="page_layout_fixed_bottom.html">Fixed Bottom Header</a>
                                        </li>
                                        <li>
                                            <a href="page_layout_fixed_bottom_footer.html">Fixed Bottom Header + Footer</a>
                                        </li>
                                        <li>
                                            <a href="page_layout_static_main_sidebar_mini.html">Mini Main Sidebar</a>
                                        </li>
                                        <li>
                                            <a href="page_layout_static_main_sidebar_partial.html">Partial Main Sidebar</a>
                                        </li>
                                        <li>
                                            <a href="page_layout_static_main_sidebar_visible.html">Visible Main Sidebar</a>
                                        </li>
                                        <li>
                                            <a href="page_layout_static_alternative_sidebar_partial.html">Partial Alternative Sidebar</a>
                                        </li>
                                        <li>
                                            <a href="page_layout_static_alternative_sidebar_visible.html">Visible Alternative Sidebar</a>
                                        </li>
                                        <li>
                                            <a href="page_layout_static_no_sidebars.html">No Sidebars</a>
                                        </li>
                                        <li>
                                            <a href="page_layout_static_both_partial.html">Both Sidebars Partial</a>
                                        </li>
                                        <li>
                                            <a href="page_layout_static_animated.html">Animated Sidebar Transitions</a>
                                        </li>
                                    </ul>
                                </li> --}}
                                {{-- <li class="sidebar-header">
                                    <span class="sidebar-header-options clearfix"><a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a></span>
                                    <span class="sidebar-header-title">Develop Kit</span>
                                </li> --}}
                                {{-- <li>
                                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-brush sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Ready Pages</span></a>
                                    <ul>
                                        <li>
                                            <a href="#" class="sidebar-nav-submenu"><i class="fa fa-angle-left sidebar-nav-indicator"></i>Errors</a>
                                            <ul>
                                                <li>
                                                    <a href="page_ready_400.html">400</a>
                                                </li>
                                                <li>
                                                    <a href="page_ready_401.html">401</a>
                                                </li>
                                                <li>
                                                    <a href="page_ready_403.html">403</a>
                                                </li>
                                                <li>
                                                    <a href="page_ready_404.html">404</a>
                                                </li>
                                                <li>
                                                    <a href="page_ready_500.html">500</a>
                                                </li>
                                                <li>
                                                    <a href="page_ready_503.html">503</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#" class="sidebar-nav-submenu"><i class="fa fa-angle-left sidebar-nav-indicator"></i>Get Started</a>
                                            <ul>
                                                <li>
                                                    <a href="page_ready_blank.html">Blank</a>
                                                </li>
                                                <li>
                                                    <a href="page_ready_blank_alt.html">Blank Alternative</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="page_ready_search_results.html">Search Results (4)</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_article.html">Article</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_user_profile.html">User Profile</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_contacts.html">Contacts</a>
                                        </li>
                                        <li>
                                            <a href="#" class="sidebar-nav-submenu"><i class="fa fa-angle-left sidebar-nav-indicator"></i>e-Learning</a>
                                            <ul>
                                                <li>
                                                    <a href="page_ready_elearning_courses.html">Courses</a>
                                                </li>
                                                <li>
                                                    <a href="page_ready_elearning_course_lessons.html">Course - Lessons</a>
                                                </li>
                                                <li>
                                                    <a href="page_ready_elearning_course_lesson.html">Course - Lesson Page</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#" class="sidebar-nav-submenu"><i class="fa fa-angle-left sidebar-nav-indicator"></i>Message Center</a>
                                            <ul>
                                                <li>
                                                    <a href="page_ready_inbox.html">Inbox</a>
                                                </li>
                                                <li>
                                                    <a href="page_ready_inbox_compose.html">Compose Message</a>
                                                </li>
                                                <li>
                                                    <a href="page_ready_inbox_message.html">View Message</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="page_ready_chat.html">Chat</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_timeline.html">Timeline</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_files.html">Files</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_tickets.html">Tickets</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_bug_tracker.html">Bug Tracker</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_tasks.html">Tasks</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_faq.html">FAQ</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_pricing_tables.html">Pricing Tables</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_invoice.html">Invoice</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_forum.html">Forum (3)</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_coming_soon.html">Coming Soon</a>
                                        </li>
                                        <li>
                                            <a href="#" class="sidebar-nav-submenu"><i class="fa fa-angle-left sidebar-nav-indicator"></i>Login, Register &amp; Lock</a>
                                            <ul>
                                                <li>
                                                    <a href="login.html">Login</a>
                                                </li>
                                                <li>
                                                    <a href="login_full.html">Login (Full Background)</a>
                                                </li>
                                                <li>
                                                    <a href="login_alt.html">Login 2</a>
                                                </li>
                                                <li>
                                                    <a href="login.html#reminder">Password Reminder</a>
                                                </li>
                                                <li>
                                                    <a href="login_alt.html#reminder">Password Reminder 2</a>
                                                </li>
                                                <li>
                                                    <a href="login.html#register">Register</a>
                                                </li>
                                                <li>
                                                    <a href="login_alt.html#register">Register 2</a>
                                                </li>
                                                <li>
                                                    <a href="page_ready_lock_screen.html">Lock Screen</a>
                                                </li>
                                                <li>
                                                    <a href="page_ready_lock_screen_alt.html">Lock Screen 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li> --}}
                                {{-- <li>
                                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-wrench sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Components</span></a>
                                    <ul>
                                        <li>
                                            <a href="#" class="sidebar-nav-submenu"><i class="fa fa-angle-left sidebar-nav-indicator"></i>3 Level Menu</a>
                                            <ul>
                                                <li>
                                                    <a href="#">Link 1</a>
                                                </li>
                                                <li>
                                                    <a href="#">Link 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="page_comp_maps.html">Maps</a>
                                        </li>
                                        <li>
                                            <a href="page_comp_charts.html">Charts</a>
                                        </li>
                                        <li>
                                            <a href="page_comp_gallery.html">Gallery</a>
                                        </li>
                                        <li>
                                            <a href="page_comp_carousel.html">Carousel</a>
                                        </li>
                                        <li>
                                            <a href="page_comp_calendar.html">Calendar</a>
                                        </li>
                                        <li>
                                            <a href="page_comp_animations.html">CSS3 Animations</a>
                                        </li>
                                        <li>
                                            <a href="page_comp_syntax_highlighting.html">Syntax Highlighting</a>
                                        </li>
                                    </ul>
                                </li> --}}
                            </ul>
                            <!-- END Sidebar Navigation -->

                            <!-- Sidebar Notifications -->
                            <div class="sidebar-header sidebar-nav-mini-hide">
                                <span class="sidebar-header-options clearfix">
                                    <a href="javascript:void(0)" data-toggle="tooltip" title="Refresh"><i class="gi gi-refresh"></i></a>
                                </span>
                                <span class="sidebar-header-title">Activity</span>
                            </div>
                            <div class="sidebar-section sidebar-nav-mini-hide">
                                <div class="alert alert-success alert-alt">
                                    <small>5 min ago</small><br>
                                    <i class="fa fa-thumbs-up fa-fw"></i> You had a new sale ($10)
                                </div>
                                <div class="alert alert-info alert-alt">
                                    <small>10 min ago</small><br>
                                    <i class="fa fa-arrow-up fa-fw"></i> Upgraded to Pro plan
                                </div>
                                
                                
                            </div>
                            <!-- END Sidebar Notifications -->
                        </div>
                        <!-- END Sidebar Content -->
                    </div>
                    <!-- END Wrapper for scrolling functionality -->
                </div>
                <!-- END Main Sidebar -->

                <!-- Main Container -->
                <div id="main-container">
                    <!-- Header -->
                    <!-- In the PHP version you can set the following options from inc/config file -->
                    <!--
                        Available header.navbar classes:

                        'navbar-default'            for the default light header
                        'navbar-inverse'            for an alternative dark header

                        'navbar-fixed-top'          for a top fixed header (fixed sidebars with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar())
                            'header-fixed-top'      has to be added on #page-container only if the class 'navbar-fixed-top' was added

                        'navbar-fixed-bottom'       for a bottom fixed header (fixed sidebars with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar()))
                            'header-fixed-bottom'   has to be added on #page-container only if the class 'navbar-fixed-bottom' was added
                        -->
                        <header class="navbar navbar-default">
                            <!-- Left Header Navigation -->
                            <ul class="nav navbar-nav-custom">
                                <!-- Main Sidebar Toggle Button -->
                                <li>
                                    <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
                                        <i class="fa fa-bars fa-fw"></i>
                                    </a>
                                </li>
                                <!-- END Main Sidebar Toggle Button -->

                                <!-- Template Options -->
                                <!-- Change Options functionality can be found in js/app.js - templateOptions() -->
                                <li class="dropdown">
                                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="gi gi-settings"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-custom dropdown-options">
                                        <li class="dropdown-header text-center">Header Style</li>
                                        <li>
                                            <div class="btn-group btn-group-justified btn-group-sm">
                                                <a href="javascript:void(0)" class="btn btn-primary" id="options-header-default">Light</a>
                                                <a href="javascript:void(0)" class="btn btn-primary" id="options-header-inverse">Dark</a>
                                            </div>
                                        </li>
                                        <li class="dropdown-header text-center">Page Style</li>
                                        <li>
                                            <div class="btn-group btn-group-justified btn-group-sm">
                                                <a href="javascript:void(0)" class="btn btn-primary" id="options-main-style">Default</a>
                                                <a href="javascript:void(0)" class="btn btn-primary" id="options-main-style-alt">Alternative</a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                <!-- END Template Options -->
                            </ul>
                            <!-- END Left Header Navigation -->

                            <!-- Search Form -->
                            <form action="page_ready_search_results.html" method="post" class="navbar-form-custom">
                                <div class="form-group">
                                    <input type="text" id="top-search" name="top-search" class="form-control" placeholder="Search..">
                                </div>
                            </form>
                            <!-- END Search Form -->

                            <!-- Right Header Navigation -->
                            <ul class="nav navbar-nav-custom pull-right">
                                <!-- Alternative Sidebar Toggle Button -->
                                <li>
                                    <!-- If you do not want the main sidebar to open when the alternative sidebar is closed, just remove the second parameter: App.sidebar('toggle-sidebar-alt'); -->
                                    <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar-alt', 'toggle-other');this.blur();">
                                        <i class="gi gi-share_alt"></i>
                                        <span class="label label-primary label-indicator animation-floating">4</span>
                                    </a>
                                </li>
                                <!-- END Alternative Sidebar Toggle Button -->

                                <!-- User Dropdown -->
                                <li class="dropdown">
                                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                       @if(isset($image->image))
                        <img class="img-avatar" src="/assetsss/images/logo.jpeg" alt="">
                        @else
                        <img class="img-avatar" src="/img/placeholders/avatars/avatar2.jpg" alt="">
                        @endif<i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                                        {{-- <li class="dropdown-header text-center">Account</li>
                                        <li>
                                            <a href="page_ready_timeline.html">
                                                <i class="fa fa-clock-o fa-fw pull-right"></i>
                                                <span class="badge pull-right">10</span>
                                                Updates
                                            </a>
                                            <a href="page_ready_inbox.html">
                                                <i class="fa fa-envelope-o fa-fw pull-right"></i>
                                                <span class="badge pull-right">5</span>
                                                Messages
                                            </a>
                                            <a href="page_ready_pricing_tables.html"><i class="fa fa-magnet fa-fw pull-right"></i>
                                                <span class="badge pull-right">3</span>
                                                Subscriptions
                                            </a>
                                            <a href="page_ready_faq.html"><i class="fa fa-question fa-fw pull-right"></i>
                                                <span class="badge pull-right">11</span>
                                                FAQ
                                            </a>
                                        </li> --}}
                                        <li class="divider"></li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-user fa-fw pull-right"></i>
                                                Dashboard
                                            </a>
                                            <!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
                                            <a href="/admin/resetpassword" data-toggle="modal">
                                                <i class="fa fa-cog fa-fw pull-right"></i>
                                                Reset Password
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                           {{--  <a href="page_ready_lock_screen.html"><i class="fa fa-lock fa-fw pull-right"></i> Lock Account</a> --}}
                                            <a href="login.html"><i class="fa fa-ban fa-fw pull-right"></i> Logout</a>
                                        </li>
                                        {{-- <li class="dropdown-header text-center">Activity</li> --}}
                                       {{--  <li>
                                            <div class="alert alert-success alert-alt">
                                                <small>5 min ago</small><br>
                                                <i class="fa fa-thumbs-up fa-fw"></i> You had a new sale ($10)
                                            </div>
                                            <div class="alert alert-info alert-alt">
                                                <small>10 min ago</small><br>
                                                <i class="fa fa-arrow-up fa-fw"></i> Upgraded to Pro plan
                                            </div>
                                            <div class="alert alert-warning alert-alt">
                                                <small>3 hours ago</small><br>
                                                <i class="fa fa-exclamation fa-fw"></i> Running low on space<br><strong>18GB in use</strong> 2GB left
                                            </div>
                                            <div class="alert alert-danger alert-alt">
                                                <small>Yesterday</small><br>
                                                <i class="fa fa-bug fa-fw"></i> <a href="javascript:void(0)" class="alert-link">New bug submitted</a>
                                            </div>
                                        </li> --}}
                                    </ul>
                                </li>
                                <!-- END User Dropdown -->
                            </ul>
                            <!-- END Right Header Navigation -->
                        </header>
                    <!-- END Header