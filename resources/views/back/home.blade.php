<?php
use Carbon\Carbon;
use App\Repositories\SharesRepository;
$repo = new SharesRepository;
$state = $repo->getCurrentShareState();
if(!empty($state)){
  $lastUpdate = Carbon::createFromFormat('Y-m-d H:i:s', $state->updated_at);
}
$mt = \App::isDownForMaintenance();
?>

@extends('back.app')

@section('title')
Admin | {{ config('app.name') }} 
@stop

@section('content')
<main>
  {{-- @include('back.include.header') --}}
  {{-- @include('back.include.sidebar') --}}
  <div class="main-container"  style="width: 98%; margin: 0 1% ; background-color: #eee"  >
   <!-- Page Content -->
   <div class="content">
    <div class="my-50 text-center">
      <h2 class="font-w700 text-black mb-10">Dashboard</h2>
      <h3 class="h5 text-muted mb-0">Welcome to your app.</h3>
    </div>
    <div class="row justify-content-center">
      <div class="col-md-2"></div>
      <div class="col-md-8 col-xl-5"><br>
        <div class="block">
          <div class="block-content">
            <p class="text-muted">
              We’ve put everything together, so you can start working on your Laravel project as soon as possible! Codebase assets are integrated and work seamlessly with Laravel Mix, so you can use the npm scripts as you would in any other Laravel project.
            </p>
            <p class="text-muted">
              Feel free to use any examples you like from the full versions to build your own pages. <strong>Wish you all the best and happy coding!</strong>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END Page Content -->
  @stop
