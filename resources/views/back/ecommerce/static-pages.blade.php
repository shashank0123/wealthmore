@extends('back.app')

@section('content')

{{-- @include('back.include.header') --}}
{{-- @include('back.include.sidebar') --}}
<style>
  @media screen and (max-width: 580px){
    #table-response { height: 400px; overflow: auto; }
  } 
</style>
<section style="margin-top: 50px;">
<div class="container">
  {{-- <a href="add-product"><button type="submit" class="btn btn-alt-primary">Add Product</button> </a><br><br>
 --}} 
  @if($errors->any())
  <div class="alert alert-danger">
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </div>
  @endif

  @if($message = Session::get('message'))
  <div class="alert alert-primary">
    <p>{{ $message }}</p>
  </div>
  @endif

<div style="overflow: auto;" id="table-response">
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>S.N</th>        
        <th>Page</th>      
        <th>Page Heading</th>         
        <th>Description</th>
        <th>Action</th>        
      </tr>
    </thead>
    <?php $count=1; ?>
    <tbody>

      @foreach ($pages as $page)

      <tr>
        <td>{{$count++}}</td>        
        <td>{{$page->page_name}}</td>         
        <td>{{$page->page_heading}}</td>         
        <td><?php echo substr(htmlspecialchars_decode($page->description),0,100); ?>. . . <a href="/admin/edit-static-pages/{{$page->id}}">read more</a></td>        
        <td>{{-- <form action="edit-main-category/{{$page->id}}" method="GET"><input type="hidden" name="page_id" value="{{$page->id}}" > --}}<a href="edit-static-pages/{{$page->id}}"><input type="submit" name="Edit" value="Edit" class="btn btn-danger"></a>{{-- </form> --}}<br>
          
      </tr>
      
       @endforeach
    </tbody>
  </table>
</div>
</div>
</section>


@endsection
