@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')

<style>
	.block-content { width: 90%; margin: 0 5% }
	.page-header { margin-left: 1% }
	h2 { font-weight: 500; }
</style>
<div class="page-header">
	<h2><i class="md md-person-add"></i> Add Product</h2>
	<p class="lead">Add Product Information</p>
</div>


<?php
use App\MainCategory;
$maincat = MainCategory::where('category_id',0)->count();

?>
<style>
	#getSize input { margin-left: 20px }
</style>

{{-- @include('back.include.header') --}}
{{-- @include('back.include.sidebar') --}}
<!-- Page Content -->
<div class="content" style="background: white; width: 98%; margin: 1%">
	<br>
	<a href="product" style="margin-left: 4%"><button type="submit" class="btn btn-danger">Back</button> </a><br><br>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">
		<div class="block-content">
			<form action="add-product" method="POST" enctype="multipart/form-data">
				{{-- @csrf --}}

				{{-- Row 1 --}}
				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="name">Product</label>
							<input type="text" class="form-control" id="name" name="name">
							
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="email">Category</label>
							<select name="category_id" class="form-control">
								<option value=""></option>
								@foreach($categoryarray as $row)
								<option value="{{$row->id}}">{{$row->Mcategory_name}}</option>
								@endforeach
								
							</select>
							
						</div>
					</div>
				</div>
				
				{{-- Row 3 --}}
				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="text">MRP</label>
							<input type="number" class="form-control" id="mrp" name="mrp">
							
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="text">DP</label>
							<input type="number" class="form-control" id="sell_price" name="sell_price">
							
						</div>
					</div>
				</div>
				{{-- Row 4 --}}
				<div class="form-group row">					
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="text">Image 1</label>
							<input type="file" class="form-control" id="image1" name="image1" >
							
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="text">Image 2</label>
							<input type="file" class="form-control" id="image2" name="image2"  >
							
						</div>
					</div>
				</div>
				{{-- Row 5 --}}
				<div class="form-group row">					
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="text">Image 3</label>
							<input type="file" class="form-control" id="image3" name="image3" >
							
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="text">Featured</label>
							<select name="trending" class="form-control">
								<option value=""></option>
								<option value="yes">yes</option>
								<option value="no">no</option>
							</select>							
						</div>
					</div>
				</div>

				{{-- <div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="text">Color</label>
							<input type="text" class="form-control" id="product_color" name="product_color">
							
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="text">Size</label>
							<div class="form-control" id="getSize">
								<input type="checkbox" name="product_size[]" value="S">S
								<input type="checkbox" name="product_size[]" value="M">M
								<input type="checkbox" name="product_size[]" value="L">L
								<input type="checkbox" name="product_size[]" value="XL">XL
								<input type="checkbox" name="product_size[]" value="XXL">XXL
							</div>							
						</div>
					</div>
				</div> --}}

				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="text">Short Description</label>
							<input type="text" class="form-control" id="short_descriptions" name="short_descriptions">
							
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="text">Page Keywords</label>
							<input type="text" class="form-control" id="page_keywords" name="page_keywords">
							
						</div>
					</div>
				</div>

				

				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							
							<label for="text">Long Description</label>
							
							<textarea name="long_descriptions" class="form-control" id="long_descriptions"></textarea><br>  
							<script>
								CKEDITOR.replace( 'long_descriptions' );
							</script>

						</div>					
					</div>
				</div>

				{{-- Row 7 --}}
				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							
							<label for="text">Page Description</label>
							
							<textarea name="page_description" class="form-control" id="page_description"></textarea><br>  
							<script>
								CKEDITOR.replace( 'page_description' );
							</script>

						</div>					
					</div>
				</div>

				{{-- Row 6 --}}
				<div class="form-group row">					
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="text">Page Title</label>
							
							<input type="text" class="form-control" id="page_title" name="page_title">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="availability">Availability</label>
							<select name="availability" class="form-control">
								<option value=""></option>
								<option value="no">Out of Stock</option>
								<option value="yes">In Stock</option>
							</select>
						</div>
					</div>
				</div>

				<div class="form-group row">					
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="text">BV</label>
							<input type="text" class="form-control" id="bv" name="bv" >		
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="text">JBV</label>
							<input type="text" class="form-control" id="jbv" name="jbv" >
						</div>
					</div>
				</div>

				{{-- Row 7 --}}
				<div class="form-group row">	
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="rvv">RBV</label>
							<input type="text" class="form-control" id="rvv" name="rvv">
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-material floating">
							<label for="mobile">Status</label>
							<select name="status" class="form-control">
								<option value=""></option>
								<option value="Active">Active</option>
								<option value="Deactive">Deactive</option>
							</select>
						</div>
					</div>
				</div>

				<button type="submit" class="btn btn-success">Submit</button><br>
				
			</div>
		</form>
	</div>
</div>
</div>
<!-- END Page Content -->
@endsection

