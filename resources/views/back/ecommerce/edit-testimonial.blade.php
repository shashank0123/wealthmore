@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')

<style>
	.logo-img{ width: 100px; height: auto; }
	.block-content { width: 90%; margin: 0 5% }
	.page-header { margin-left: 1% }
	h2 { font-weight: 500; }
</style>
<div class="page-header">
	<h2><i class="md md-person-add"></i> Add Image</h2>
	<p class="lead">Home page Banner Images</p>
</div>

{{-- @include('back.include.header') --}}
{{-- @include('back.include.sidebar') --}}
<!-- Page Content -->
<div class="content" style="background: white; width: 98%; margin: 1%">
	<br>
	<a href="/admin/testimonial" style="margin-left: 4%"><button type="submit" class="btn btn-danger">Back</button> </a><br><br>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">
		<div class="block-content">
			<form action="" method="POST" enctype="multipart/form-data">
				{{-- @csrf --}}
				
				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="client_name">Client Name</label>
							<input type="text" class="form-control" id="client_name" name="client_name" value="{{ $testimonial->client_name }}">
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-material floating">
							<label for="city">City</label>
							<input type="text" class="form-control" id="city" name="city" value="{{ $testimonial->city }}">
						</div>
					</div>
				</div>


				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="rating">Rating</label>
							<input type="number" class="form-control" id="rating" name="rating" value="{{ $testimonial->rating }}">
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-material floating">
							<label for="review">Review</label>
							<input type="text" class="form-control" id="review" name="review" value="{{ $testimonial->review }}">
						</div>
					</div>
				</div>

				<div class="form-group row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-sm-8">
								<div class="form-material floating">
									<label for="image_url">Image 3</label>
									<input type="file" class="form-control" id="image_url" name="image_url">
								</div>
							</div>
							<div class="col-sm-4">
								<img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $testimonial->image_url }} " class="logo-img">
							</div>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-material floating">
							<label for="status">Status</label>
							<select name="status" class="form-control">
								<option value="Deactive" {{ $testimonial->status=='Deactive'? 'selected': null }}>Deactive</option>
								<option value="Active" {{ $testimonial->status=='Active'? 'selected': null }}>Active</option>
							</select>
						</div>
					</div>				
				</div>		

				
				<button type="submit" class="btn btn-success">Submit</button><br>
			</div>
		</form>
	</div>
</div>
</div>
<!-- END Page Content -->
@endsection

