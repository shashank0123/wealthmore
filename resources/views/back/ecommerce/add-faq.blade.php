@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')

<style>
  .block-content { width: 90%; margin: 0 5% }
  .page-header { margin-left: 1% }
  h2 { font-weight: 500; }
</style>
<div class="page-header">
          <h2><i class="md md-person-add"></i> Add Faq</h2>
          <p class="lead">Add Your Own FAQ</p>
        </div>

{{-- @include('back.include.header') --}}
{{-- @include('back.include.sidebar') --}}
<!-- Page Content -->
<div class="content" style="background: white; width: 98%; margin: 1%">
<br>
  <a href="/admin/faq" style="margin-left: 4%"><button type="submit" class="btn btn-danger">Back</button> </a><br><br>

  @if($errors->any())
  <div class="alert alert-danger">
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </div>
  @endif

  @if($message = Session::get('message'))
  <div class="alert alert-primary">
    <p>{{ $message }}</p>
  </div>
  @endif
  <div class="row justify-content-center">
    <div class="block-content">
      <form action="add-faq" method="POST" enctype="multipart/form-data">
        {{-- @csrf --}}
        
        
        <div class="form-group row">
        <div class="col-md-12">
          <div class="form-material floating">
            <label>Question </label>
            <input type="text" class="form-control" id="question" name="question">
            
          </div>
        </div>
      </div>

      <div class="form-group row">
        <div class="col-md-12">
          <div class="form-material floating">
            <label>Answer </label>
            <input type="text" class="form-control" id="answer" name="answer">
            
          </div>
        </div>
      </div>

      <div class="form-group row">
        <div class="col-md-12">
          <div class="form-material floating">
            <label>Status</label>
            <select class="form-control" name="status">
              <option value="Active">Active</option>
              <option value="Deactive">Deactive</option>
            </select><br>
            
          </div>
        </div>
      </div>

      <button type="submit" class="btn btn-success">Submit</button><br>
      </div>
    </form>
  </div>
</div>
</div>
<!-- END Page Content -->
@endsection

