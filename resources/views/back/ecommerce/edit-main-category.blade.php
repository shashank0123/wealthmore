@extends('back.app')

@section('content')

<style>
  .block-content { width: 90%; margin: 0 5% }
  .page-header { margin-left: 1% }
  h2 { font-weight: 500; }
</style>
<div class="page-header">
          <h2><i class="md md-person-add"></i>Update Category</h2>
          <p class="lead">Edit Category Detail</p>
        </div>


{{-- @include('back.include.header') --}}
{{-- @include('back.include.sidebar') --}}
<!-- Page Content -->
<div class="content" style="background: white; width: 98%; margin: 1%">
<br>
  <a href="/admin/main-category" style="margin-left: 4%"><button type="submit" class="btn btn-danger">Back</button> </a><br>
    @if($errors->any())
    <div class="alert alert-danger">
      @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </div>
  @endif

  @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
  @endif
  <div class="row justify-content-center">
    <div class="block-content">
      <form action="/admin/edit-main-category/{{ $maincategory->id }}" method="POST" enctype="multipart/form-data">

        <div class="form-group row">
          <div class="col-md-6">
            <div class="form-material ">
              <label for="name">Category</label>
              <input type="text" class="form-control" id="name" name="Mcategory_name" value="{{$maincategory->Mcategory_name}}">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-material ">
              <label for="email">Description</label>
              <input type="text" class="form-control" id="Mcategory_description" name="Mcategory_description" value="{{$maincategory->Mcategory_description}}">
            </div>
          </div>
        </div><br>

        <div class="form-group row">
          <div class="col-md-6">
            <div class="form-material ">
              <label for="text">Featured</label>
              <input type="text" class="form-control" id="featured" name="featured" value="{{ $maincategory->featured }}">
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-material floating">
              <label for="mobile">Parent Category</label>
              <select name="category_id" class="form-control">
                <option value=""></option>
                @foreach($categoryarray as $data)
                {{-- <option value="{{$data->id}}">{{$data->Mcategory_name}}</option> --}}
                <option value="{{ $data->id}}" {{ $maincategory->category_id==$data->id? 'selected': null }}>{{ $data->Mcategory_name  }}</option>
                @endforeach
                </select>
            </div>
          </div>        
        </div>

        <div class="form-group row">
         <div class="col-md-6">
            <div class="form-material ">
              <label for="mobile">Status</label>
              <select name="status" class="form-control">
                <option value="Deactive" {{ $maincategory->status=='Deactive'? 'selected': null }}>Deactive</option>
                <option value="Active" {{ $maincategory->status=='Active'? 'selected': null }}>Active</option>
              </select>
            </div>
          </div>
        </div>
        <div class="form-group row">                           
          <button type="submit" class="btn btn-success">Submit</button><br>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- END Page Content -->
@endsection

