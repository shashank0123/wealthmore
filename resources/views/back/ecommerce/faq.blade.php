@extends('back.app')

@section('content')

{{-- @include('back.include.header') --}}
{{-- @include('back.include.sidebar') --}}

<section style="margin-top: 50px;">
<div class="container">
  {{-- <a href="add-product"><button type="submit" class="btn btn-alt-primary">Add Product</button> </a><br><br>
 --}} 
  <div class="row">
    <div class="col-sm-6">
      <br>
  <a href="/admin/add-faq" style="margin-left: 4%"><button type="submit" class="btn btn-danger">Add FAQ</button> </a><br><br></div>
  {{-- <div class="col-sm-6 form-group">

          <form action="/admin/product" method="POST" class="searchform" >
            <input type="text" class="form-set" name="product_keyword" placeholder="Search for something">
            <input type="submit" name="search" class="btn btn-primary " value="Search">
          </form>
        
  </div> --}}
</div>
  @if($errors->any())
  <div class="alert alert-danger">
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </div>
  @endif

  @if($message = Session::get('message'))
  <div class="alert alert-primary">
    <p>{{ $message }}</p>
  </div>
  @endif
<div style="overflow: auto;">
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>S.N</th>        
        <th>Question</th>               
        <th>Status</th>
        <th>Created At</th>
        <th>Action</th>        
      </tr>
    </thead>
    <?php $count=1; ?>
    <tbody>

      @foreach ($faq as $row)

      <tr>
        <td>{{$count++}}</td>        
        <td>{{$row->questions}}</td>         
        <td>{{$row->status}}</td>
        <td>{{$row->created_at}}</td>

        <td>{{-- <form action="edit-main-category/{{$row->id}}" method="GET"><input type="hidden" name="row_id" value="{{$row->id}}" > --}}<a href="edit-faq/{{$row->id}}"><input type="submit" name="Edit" value="Edit" class="btn btn-danger"></a>{{-- </form> --}}<br>
          {{-- <form action="/admin/product" method="GET"><input type="hidden" name="row_id" value="{{$row->id}}" > --}}<a href="faq/{{ $row->id }}"><input type="submit" name="delete" value="Delete" class="btn btn-danger"></a>{{-- </form> --}}</td>
      </tr>
      
       @endforeach
    </tbody>
  </table>
</div>
</div>
</section>


@endsection
