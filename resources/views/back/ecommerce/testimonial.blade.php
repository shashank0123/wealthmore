@extends('back.app')

@section('content')

<style>
    .logo-img{ width:100px; height: 100px; }
    @media screen and (max-width: 580px){
    #table-response { height: 400px; overflow: auto; }
  } 
</style>
{{-- @include('back.include.header') --}}
{{-- @include('back.include.sidebar') --}}

<section style="margin-top: 50px;">
<div class="container">
  <a href="add-testimonial"><button type="submit" class="btn btn-danger">Add Testimonial</button> </a><br><br>
  @if($errors->any())
  <div class="alert alert-danger">
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </div>
  @endif

  @if($message = Session::get('message'))
  <div class="alert alert-primary">
    <p>{{ $message }}</p>
  </div>
  @endif

<div style="overflow: auto;" id="table-response">
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>S.No</th>    
        <th>Client Name</th>     
        <th>City</th>     
        <th>Review</th>     
        <th>Image</th>                
        <th>Created_at</th>                
        <th>Status</th>
        <th>Action</th>        
      </tr>
    </thead>
    <?php $count=1; ?>
    <tbody>

      @foreach ($testimonial as $row)

      <tr>
        <td>{{$count++}}</td>
        <td>{{$row->client_name}}</td>
        <td>{{$row->city}}</td>
        <td>{{$row->review}}</td>
        <td><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $row->image_url }} " class="logo-img" /></td>
        
        <td>{{$row->created_at}}</td>
        <td>{{$row->status}}</td>
        <td>{{-- <form action="edit-main-category/{{$row->id}}" method="GET"><input type="hidden" name="row_id" value="{{$row->id}}" > --}}<a href="edit-testimonial/{{$row->id}}"><input type="submit" name="Edit" value="Edit" class="btn btn-danger"></a>{{-- </form> --}}<br>
          {{-- <form action="/admin/product" method="GET"><input type="hidden" name="row_id" value="{{$row->id}}" > --}}<a href="testimonial/{{ $row->id }}"><input type="submit" name="delete" value="Delete" class="btn btn-danger"></a> {{-- </form> --}}</td>
      </tr>
      
       @endforeach
    </tbody>
  </table>
</div>
</div>
</section>


@endsection
