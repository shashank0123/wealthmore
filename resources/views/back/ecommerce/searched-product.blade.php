@extends('back.app')

@section('content')

<style>
    .logo-img{ width: 100px; height: auto; }
    .form-set { width:50%; border: 1px solid #ddd; border-radius: 2px; padding: 6px; }
    @media screen and (max-width: 580px){
    #table-response { height: 400px; overflow: auto; }
  } 
</style>
{{-- @include('back.include.header') --}}
{{-- @include('back.include.sidebar') --}}

<section style="margin-top: 50px;">
<div class="container">
  <div class="row">
    <div class="col-sm-6">
  <a href="product"><button type="submit" class="btn btn-danger">Back</button> </a><br><br></div>
  <div class="col-sm-6 form-group">

          <form action="/admin/product" method="POST" class="searchform" >
            <input type="text" class="form-set" name="product_keyword" placeholder="Search for something">
            <input type="submit" name="search" class="btn btn-primary " value="Search">
          </form>
        
  </div>
</div>
  @if($errors->any())
  <div class="alert alert-danger">
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </div>
  @endif

  @if($message = Session::get('message'))
  <div class="alert alert-primary">
    <p>{{ $message }}</p>
  </div>
  @endif
<div style="overflow: auto;" id="table-response">
  <table class="table table-bordered" >
    <thead>
      <tr>
        <th>S.N</th>
        <th>Product</th>
        <th>Category</th>
        <th>Description</th>
        <th>MRP</th>
        <th>Selling Price</th>
        <th>Image</th>
        <th>Created_at</th>
        <th>Status</th>
        <th>Action</th>        
      </tr>
    </thead>
    <?php $count=1; ?>
    <tbody>

      @foreach ($product as $row)

      <tr>
        <td>{{$count++}}</td>
        <td>{{$row->name}}</td>
         <td>{{$row->Mcategory_name}}</td>       
        <td>{{$row->short_descriptions}}</td>
        <td>Rs.{{$row->mrp}}</td>
        <td>Rs.{{$row->sell_price}}</td>
        <td>{{$row->created_at}}</td>
        <td><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $row->image1 }} " class="logo-img"  /></td>
        <td>{{$row->status}}</td>
        <td>{{-- <form action="edit-main-category/{{$row->id}}" method="GET"><input type="hidden" name="row_id" value="{{$row->id}}" > --}}<a href="edit-product/{{$row->id}}"><input type="submit" name="Edit" value="Edit" class="btn btn-danger"></a>{{-- </form> --}}<br>
          {{-- <form action="/admin/product" method="GET"><input type="hidden" name="row_id" value="{{$row->id}}" > --}}<a href="product/{{ $row->id }}"><input type="submit" name="delete" value="Delete" class="btn btn-danger"></a>{{-- </form> --}}</td>
      </tr>
      
       @endforeach
    </tbody>
  </table>
</div>
</div>
</section>


@endsection
