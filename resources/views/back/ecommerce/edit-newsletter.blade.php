@extends('back.app')

@section('content')


<style>
  .block-content { width: 90%; margin: 0 5% }
  .page-header { margin-left: 1% }
  h2 { font-weight: 500; }
</style>
<div class="page-header">
          <h2><i class="md md-person-add"></i>Manage Newletters</h2>
          <p class="lead">Select a user to send Updates</p>
        </div>


{{-- @include('back.include.header') --}}
{{-- @include('back.include.sidebar') --}}
<!-- Page Content -->
<div class="content" style="background: white; width: 98%; margin: 1%">
<br>
  <a href="/admin/newsletter" style="margin-left: 4%"><button type="submit" class="btn btn-danger">Back</button> </a><br><br>
    @if($errors->any())
    <div class="alert alert-danger">
      @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </div>
  @endif

  @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
  @endif
  <div class="row justify-content-center">
    <div class="block-content">
      <form action="/admin/edit-newsletter/{{$newsletter->id}}" method="POST">

        <div class="form-group row">          
          <div class="col-md-12">
            <div class="form-material ">
              <label for="email">Email</label>
              <input type="text" class="form-control" id="email" name="email" value="{{$newsletter->email}}" readonly>
            </div>
          </div>
        </div><br>       
              

        <div class="form-group row">
         <div class="col-md-12">
            <div class="form-material ">
              <label for="mobile">Status</label>
              <select name="status" class="form-control">
                <option value="Deactive" {{ $newsletter->status=='Deactive'? 'selected': null }}>Deactive</option>
                <option value="Active" {{ $newsletter->status=='Active'? 'selected': null }}>Active</option>
              </select>
            </div>
          </div>
        </div>


        <div class="form-group row">                           
          <button type="submit" class="btn btn-success" name="submit">Submit</button>      <br>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- END Page Content -->
@endsection

