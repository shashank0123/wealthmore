@extends('back.app')
<?php
  use App\Product;
?>
@section('content')

<style>
  .block-content { width: 90%; margin: 0 5% }
  .page-header { margin-left: 1% }
  h2 { font-weight: 500; }
</style>
<div class="page-header">
          <h2><i class="md md-person-add"></i>Manage Reviews</h2>
          <p class="lead">See reviews of users</p>
        </div>



{{-- @include('back.include.header') --}}
{{-- @include('back.include.sidebar') --}}
<!-- Page Content -->
<div class="content" style="background: white; width: 98%; margin: 1%">

<br>
  <a href="/admin/review" style="margin-left: 4%"><button type="submit" class="btn btn-danger">Back</button> </a><br><br>
    @if($errors->any())
    <div class="alert alert-danger">
      @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </div>
  @endif

  @if($message = Session::get('message'))
    <div class="alert alert-primary">
      <p>{{ $message }}</p>
    </div>
  @endif
  <div class="row justify-content-center">
    <div class="block-content">
      <form action="/admin/edit-review/{{$review->id}}" method="POST">

        <div class="form-group row">

          <div class="col-md-6">
            <div class="form-material ">
              <label for="name">Name</label>
              <input type="text" class="form-control" id="name" name="name" value="{{$review->name}}">
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-material ">
              <label for="email">Email</label>
              <input type="text" class="form-control" id="email" name="email" value="{{$review->email}}" readonly>
            </div>
          </div>
        </div><br>

        <div class="form-group row">
          <div class="col-md-6">
            <div class="form-material ">
              <label for="text">Rating</label>
              <input type="text" class="form-control" id="rating" name="rating" value="{{ $review->rating }}">
            </div>
          </div>
          
          <div class="col-md-6">
            <div class="form-material ">
              <label for="text">Review</label>
              <input type="text" class="form-control" id="review" name="review" value="{{ $review->review }}">
            </div>
          </div>
        </div>


        <div class="form-group row">
          <div class="col-md-6">
            <div class="form-material floating">
             <?php $prod = Product::where('id',$review->product_id)->first(); ?>
              <label for="mobile">Product</label>
             <input type="text" class="form-control" id="product_id" name="product_id" value="{{ $prod->name }}" readonly>
            </div>
          </div>        

        
         <div class="col-md-6">
            <div class="form-material ">
              <label for="mobile">Status</label>
              <select name="status" class="form-control">
                <option value="Deactive" {{ $review->status=='Deactive'? 'selected': null }}>Deactive</option>
                <option value="Active" {{ $review->status=='Active'? 'selected': null }}>Active</option>
              </select>
            </div>
          </div>
        </div>
        <div class="form-group row">                           
          <button type="submit" class="btn btn-success" name="submit">Submit</button> <br>     
        </div>
      </form>
    </div>
  </div>
</div>
<!-- END Page Content -->
@endsection

