@extends('back.app')

@section('content')

{{-- @include('back.include.header') --}}
{{-- @include('back.include.sidebar') --}}
<style>
  @media screen and (max-width: 580px){
    #table-response { height: 400px; overflow: auto; }
  } 
</style>
<section style="margin-top: 50px;">
<div class="container">
  {{-- <a href="add-product"><button type="submit" class="btn btn-alt-primary">Add Product</button> </a><br><br>
 --}} 
  @if($errors->any())
  <div class="alert alert-danger">
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </div>
  @endif

  @if($message = Session::get('message'))
  <div class="alert alert-primary">
    <p>{{ $message }}</p>
  </div>
  @endif

<div style="overflow: auto;" id="table-response">
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>S.N</th>        
        <th>Email</th>               
        <th>Status</th>
        <th>Action</th>        
      </tr>
    </thead>
    <?php $count=1; ?>
    <tbody>

      @foreach ($newsletter as $row)

      <tr>
        <td>{{$count++}}</td>        
        <td>{{$row->email}}</td>         
        <td>{{$row->status}}</td>        
        <td>{{-- <form action="edit-main-category/{{$row->id}}" method="GET"><input type="hidden" name="row_id" value="{{$row->id}}" > --}}<a href="edit-newsletter/{{$row->id}}"><input type="submit" name="Edit" value="Edit" class="btn btn-danger"></a>{{-- </form> --}}<br>
          {{-- <form action="/admin/product" method="GET"><input type="hidden" name="row_id" value="{{$row->id}}" > --}}<a href="newsletter/{{ $row->id }}"><input type="submit" name="delete" value="Delete" class="btn btn-danger"></a>{{-- </form> --}}</td>
      </tr>
      
       @endforeach
    </tbody>
  </table>
</div>
</div>
</section>


@endsection
