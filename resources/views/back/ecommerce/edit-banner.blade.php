@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')

<style>
	.logo-img{ width: 200px; height: 200px; }


	.block-content { width: 90%; margin: 0 5% }
	.page-header { margin-left: 1% }
	h2 { font-weight: 500; }
</style>
<div class="page-header">
          <h2><i class="md md-person-add"></i>Edit Banner Image</h2>
          <p class="lead">Change Banner Image</p>
        </div>



<?php
use App\MainCategory;
$maincat = MainCategory::where('category_id',0)->count();

?>

{{-- @include('back.include.header') --}}
{{-- @include('back.include.sidebar') --}}
<!-- Page Content -->
<div class="content" style="background: white; width: 98%; margin: 1%">
<br>
	<a href="/admin/banner" style="margin-left: 4%"><button type="submit" class="btn btn-danger">Back</button> </a><br>
	<br>
	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">
		<div class="block-content">
			<form action="" method="POST" enctype="multipart/form-data">
				{{-- @csrf --}}
				
				
				<div class="form-group row">					
					
					<div class="col-md-6 from-inline">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-material floating">
									<label for="image_url">Image 1</label>
									<input type="file" class="form-control" id="image_url" name="image_url">
								</div>
							</div>							
						</div><br>

						<div class="row">
							<div class="col-sm-12">
								<div class="form-material floating">
									<label for="keyword">Keyword For Banner Image</label>
									<input type="text" class="form-control" id="keyword" name="keyword" value="{{$banner->keyword}}">
								</div>
							</div>							
						</div><br>


						<div class="row">
							<div class="col-sm-12">
								<div class="form-material floating">
									<label for="image_type">Image Type</label>
									<select name="image_type" class="form-control">
										<option value="shop" {{ $banner->image_type=='shop'? 'selected': null }}>Shopping Site Banner</option>
										<option value="home" {{ $banner->image_type=='home'? 'selected': null }}>Home Page Full Size Banner</option>				
										<option value="medium" {{ $banner->image_type=='medium'? 'selected': null }}>Home Page Medium Size Banner</option>
										<option value="brand" {{ $banner->image_type=='brand'? 'selected': null }}>Brand Logo</option>	
										<option value="business" {{ $banner->image_type=='business'? 'selected': null }}>Business Plan</option>				
									</select>
								</div>
							</div>							
						</div><br>

						<div class="row">
							<div class="col-sm-12">
								<div class="form-material floating">
									<label for="status">Status</label>
									<select name="status" class="form-control">
										<option value="Deactive" {{ $banner->status=='Deactive'? 'selected': null }}>Deactive</option>
										<option value="Active" {{ $banner->status=='Active'? 'selected': null }}>Active</option>
									</select>
								</div>
							</div>							
						</div>

						<div class="row">
							
						</div>

					</div>
					<div class="col-sm-6  from-inline" style="text-align: center;">
						<br><br>
						<img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/banner/{{ $banner->image_url }} " class="logo-img">
					</div>
				</div>
				
				
				
                    {{-- <div class="form-group row">
	                    <div class="col-md-6">
	                        <div class="form-material floating">
	                            <input type="text" class="form-control" id="sponsorid" name="sponsorid">
	                            <label for="sponsorid">SponsorID</label>
	                        </div>
	                    </div> --}}
	                    

	                    
	                    
                    {{-- </div>
                    <div class="form-group row">
                        <div class="col-md-9">
                        	<a href="/member/checkid/" class="btn btn-alt-success">Verify SponsorID</a> --}}
                        	<button type="submit" class="btn btn-success">Submit</button><br>
                        {{-- </div> --}}
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
    @endsection

