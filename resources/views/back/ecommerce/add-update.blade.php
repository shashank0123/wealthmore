@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')

<style>
.block-content { width: 90%; margin: 0 5% }
.page-header { margin-left: 1% }
h2 { font-weight: 500; }
</style>
<div class="page-header">
      <h2><i class="md md-person-add"></i> Add Update</h2>
      <p class="lead">Add New Update</p>
    </div>


{{-- @include('back.include.header') --}}
{{-- @include('back.include.sidebar') --}}
<!-- Page Content -->
<div class="content" style="background: white; width: 98%; margin: 1%">
<br>
<a href="update" style="margin-left: 4%"><button type="submit" class="btn btn-danger">Back</button> </a><br><br>

@if($errors->any())
<div class="alert alert-danger">
	@foreach($errors->all() as $error)
	<li>{{ $error }}</li>
	@endforeach
</div>
@endif

@if($message = Session::get('message'))
<div class="alert alert-primary">
	<p>{{ $message }}</p>
</div>
@endif
<div class="row justify-content-center">
	<div class="block-content">
		<form action="add-update" method="POST" enctype="multipart/form-data">
			{{-- @csrf --}}

			
			
			{{-- Row 3 --}}
			<div class="form-group row">
				<div class="col-md-12">
					<div class="form-material floating">
						<label for="title">Title</label>
						<input type="text" class="form-control" id="title" name="title">							
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-material floating">
						<label for="tags">Tags</label>
						<input type="text" class="form-control" id="tags" name="tags">							
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-material floating">
						<label for="text">Image</label>
						<input type="file" class="form-control" id="image_url" name="image_url" style="margin-left: 15% ; width: 85%">
						
					</div>
				</div>
			
				<div class="col-md-12">
					<div class="form-material floating">
						
							<label for="description">Description</label>
						
							<textarea name="description" class="form-control" id="description"></textarea><br>  
							<script>
								CKEDITOR.replace( 'description' );
							</script>
						</div>								
					</div>					
				
				<div class="col-md-12">
					<div class="form-material floating">
						<label for="status">Status</label>
						<select name="status" class="form-control">
							<option value=""></option>
							<option value="Active">Active</option>
							<option value="Deactive">Deactive</option>
						</select>
						
					</div>
				</div>

		<br><br>


			<button type="submit" class="btn btn-success">Submit</button><br>


		</div>
	</form>
</div>
</div>
</div>
<!-- END Page Content -->
@endsection

