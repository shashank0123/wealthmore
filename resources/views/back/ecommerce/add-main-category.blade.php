@extends('back.app')

@section('content')

<style>
	.block-content { width: 90%; margin: 0 5% }
	.page-header { margin-left: 1% }
	h2 { font-weight: 500; }
</style>
<div class="page-header">
          <h2><i class="md md-person-add"></i> Add Category</h2>
          <p class="lead">Add Category Information</p>
        </div>


{{-- @include('back.include.header') --}}
{{-- @include('back.include.sidebar') --}}
<!-- Page Content -->
<div class="content" style="background: white; width: 98%; margin: 1%">
<br>
	<a href="main-category" style="margin-left: 4%"><button type="submit" class="btn btn-danger">Back</button> </a><br><br>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">
		<div class="block-content">
			<form action="add-main-category" method="POST" enctype="multipart/form-data">
				{{-- @csrf --}}
				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="name">Category</label>
							<input type="text" class="form-control" id="name" name="Mcategory_name">
							
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="email">Description</label>
							<input type="text" class="form-control" id="Mcategory_description" name="Mcategory_description">
							
						</div>
					</div>
				</div>
				{{--  <input type="hidden" name="pool" value="@if (isset($_GET['pool'])){{ $_GET['pool'] }} @else {{ 1 }} @endif"> --}}
				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="text">Featured</label>
							<select name="featured" class="form-control">
								<option value=""></option>
								<option value="yes">Yes</option>
								<option value="no">No</option>
							</select>
							{{-- <input type="text" class="form-control" id="featured" name="featured"> --}}
							
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="mobile">Parent Category</label>
							<select name="category_id" class="form-control">
								<option value=""></option>
								@foreach($maincategory as $row)
								<option value="{{$row->id}}">{{$row->Mcategory_name}}</option>

								@endforeach
								</select>
							
						</div>
					</div>
				</div>
				<div class="form-group row">
					
						<div class="col-md-6">
						<div class="form-material floating">
							<label for="mobile">Status</label>
							<select name="status" class="form-control">
								<option value=""></option>
								<option value="Active">Active</option>
								<option value="Deactive">Deactive</option>
							</select>
							
						</div>
					</div>
	                </div>

                    {{-- <div class="form-group row">
	                    <div class="col-md-6">
	                        <div class="form-material floating">
	                            <input type="text" class="form-control" id="sponsorid" name="sponsorid">
	                            <label for="sponsorid">SponsorID</label>
	                        </div>
	                    </div> --}}
	                    

	                    
	                    
                    {{-- </div>
                    <div class="form-group row">
                        <div class="col-md-9">
                        	<a href="/member/checkid/" class="btn btn-alt-success">Verify SponsorID</a> --}}
                        	<button type="submit" class="btn btn-success">Submit</button><br><br>
                        {{-- </div> --}}
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
    @endsection

