@extends('back.app')

@section('content')


{{-- @include('back.include.header') --}}
{{-- @include('back.include.sidebar') --}}

<style>
  .form-set { width:50%; border: 1px solid #ddd; border-radius: 2px; padding: 6px; }
  @media screen and (max-width: 580px){
    #table-response { height: 400px; overflow: auto; }
  } 
</style>
<section style="margin-top: 50px;">
<div class="container">
  <div class="row">
    <div class="col-sm-6">
  <a href="main-category"><button type="submit" class="btn btn-danger">Back</button> </a><br><br></div>
  <div class="col-sm-6 form-group">

          <form action="/admin/main-category" method="POST" class="searchform" >
            <input type="text" class="form-set" name="product_keyword" placeholder="Search for something">
            <input type="submit" name="search" class="btn btn-primary " value="Search">
          </form>
        
  </div>
</div>
  @if($errors->any())
  <div class="alert alert-danger">
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </div>
  @endif

  @if($message = Session::get('message'))
  <div class="alert alert-primary">
    <p>{{ $message }}</p>
  </div>
  @endif

<div style="overflow: auto;" id="table-response">
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>S.N</th>
        <th>Category</th>
        <th>Description</th>
        <th>Features</th>
        <th>Created_at</th>
        <th>Status</th>
        <th>Action</th>
        {{-- <th>Status</th> --}}
        <!-- <th>row type</th>
        <th>Remember token</th>
        <th>Created at</th> -->
      </tr>
    </thead>
    <?php $count=1; ?>
    <tbody>
      @foreach ($maincategory as $row)
      <tr>
        <td>{{$count++}}</td>
        <td>{{ $row->Mcategory_name }}</td>
        <td>{{$row->Mcategory_description}}</td>
         <td>{{$row->featured}}</td>       
         <td>{{$row->created_at}}</td>       
        <td>{{$row->status}}</td>
        {{-- <td></td> --}}
        <td>{{-- <form action="edit-main-category/{{$row->id}}" method="GET"><input type="hidden" name="row_id" value="{{$row->id}}" > --}}<a href="edit-main-category/{{$row->id}}"><input type="submit" name="Edit" value="Edit" class="btn btn-danger"></a>{{-- </form> --}}<br>
          {{-- <form action="/admin/main-category" method="GET"><input type="hidden" name="row_id" value="{{$row->id}}" > --}}<a href="main-category/{{ $row->id }}"><input type="submit" name="delete" value="Delete" class="btn btn-danger"></a>{{-- </form> --}}</td>
      </tr>
      
       @endforeach
    </tbody>
  </table>
</div>
</div>
</section>


@endsection
