@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')

<style>
	.block-content { width: 90%; margin: 0 5% }
	.page-header { margin-left: 1% }
	h2 { font-weight: 500; }
</style>
<div class="page-header">
          <h2><i class="md md-person-add"></i> Add Blog</h2>
          <p class="lead">Add Your Own Blogs</p>
        </div>


{{-- @include('back.include.header') --}}
{{-- @include('back.include.sidebar') --}}
<!-- Page Content -->
<div class="content" style="background: white; width: 98%; margin: 1%">
	<br>
	<a href="blog" style="margin-left: 4%"><button type="submit" class="btn btn-danger">Back</button> </a><br><br>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">
		<div class="block-content">
			<form action="add-blog" method="POST" enctype="multipart/form-data">
				{{-- @csrf --}}

				
				
				{{-- Row 3 --}}
				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="heading">Blog Heading</label>
							<input type="text" class="form-control" id="heading" name="heading">							
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="text">Image</label>
							<input type="file" class="form-control" id="image_url" name="image_url" style="margin-left: 15% ; width: 85%">
							
						</div>
					</div>
				</div>
				
				{{-- Row 7 --}}
				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							
								<label for="text">Blog Description</label>
							
								<textarea name="description" class="form-control" id="description"></textarea><br>  
								<script>
									CKEDITOR.replace( 'description' );
								</script>
							</div>								
						</div>					
					</div>
				</div>

				{{-- Row 6 --}}
				<div class="form-group row">					
					
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="status">Status</label>
							<select name="status" class="form-control">
								<option value=""></option>
								<option value="Active">Active</option>
								<option value="Deactive">Deactive</option>
							</select>
							
						</div>
					</div>

				</div>


				<button type="submit" class="btn btn-success">Submit</button><br>

			</div>
		</form>
	</div>
</div>
</div>
<!-- END Page Content -->
@endsection

