@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')

<?php
use App\MainCategory;
$maincat = MainCategory::where('category_id',0)->count();

?>
<style>
	.block-content { width: 90%; margin: 0 5% }
	.page-header { margin-left: 1% }
	h2 { font-weight: 500; }
</style>
<div class="page-header">
          <h2><i class="md md-person-add"></i> Add Image</h2>
          <p class="lead">Home page Banner Images</p>
        </div>

{{-- @include('back.include.header') --}}
{{-- @include('back.include.sidebar') --}}
<!-- Page Content -->
<div class="content" style="background: white; width: 98%; margin: 1%">

<br>
	<a href="banner" style="margin-left: 4%"><button type="submit" class="btn btn-danger">Back</button> </a><br><br>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">
		
		<div class="block-content" >
			<form action="add-banner" method="POST" enctype="multipart/form-data">
				{{-- @csrf --}}

				{{-- Row 1 --}}
				<div class="container">
					{{-- <ul>
						<li>{{"Banner Size should be of 1920 * 450 for better Quality"}}</li>
						<li>{{"Other images should be of 1920 * 450 for better Quality"}}</li>
					</ul> --}}
				</div><br>
				<div class="form-group row">
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="keyword">Banner Image</label>
							<input type="file" class="form-control" id="image_url" name="image_url">							
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-material floating">
							<label for="status">Image Type</label>
							<select name="image_type" class="form-control">
								<option value=""></option>
								<option value="shop">Shopping Site Banner</option>
								<option value="home">Home Page Full Size Banner</option>
								<option value="medium">Home Page Mediun Size Banner</option>
								<option value="brand">Brand Logo</option>					
								<option value="business">Business Plan</option>					
							</select>														
						</div>
					</div>
				</div>

				<div class="form-group row">
					
					<div class="col-md-6">
						<div class="form-material floating">
							<label for="status">Keyword For Banner Images</label>
							<input type="text" class="form-control" name="keyword" id="keyword">														
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-material floating">
							<label for="status">Status</label>
							<select name="status" class="form-control">
								<option value=""></option>
								<option value="Active">Active</option>
								<option value="Deactive">Deactive</option>
							</select>														
						</div>
					</div>
				</div>
				
				


				{{-- Row 6 --}}
				{{-- <div class="form-group row">					
					
					<div class="col-md-6">
						<div class="form-material floating">
							
						</div>
					</div>

				</div> --}}



                    {{-- <div class="form-group row">
	                    <div class="col-md-6">
	                        <div class="form-material floating">
	                            <input type="text" class="form-control" id="sponsorid" name="sponsorid">
	                            <label for="sponsorid">SponsorID</label>
	                        </div>
	                    </div> --}}
	                    

	                    
	                    
                    {{-- </div>
                    <div class="form-group row">
                        <div class="col-md-9">
                        	<a href="/member/checkid/" class="btn btn-alt-success">Verify SponsorID</a> --}}
                        	<button type="submit" class="btn btn-success">Submit</button><br>
                        {{-- </div> --}}
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
    @endsection

