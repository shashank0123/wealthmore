@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')

<style>
	.logo-img{ width: 100px; height: auto; }

	
	.block-content { width: 90%; margin: 0 5% }
	.page-header { margin-left: 1% }
	h2 { font-weight: 500; }
</style>
<div class="page-header">
          <h2><i class="md md-person-add"></i>Update FAQ</h2>
          <p class="lead">Edit questions according to your need</p>
        </div>



{{-- @include('back.include.header') --}}
{{-- @include('back.include.sidebar') --}}
<!-- Page Content -->
<div class="content" style="background: white; width: 98%; margin: 1%">
<br>
	<a href="/admin/faq" style="margin-left: 4%"><button type="submit" class="btn btn-danger">Back</button> </a><br><br>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">
		<div class="block-content">
			<form action="/admin/edit-faq/{{$faq->id}}" method="POST" enctype="multipart/form-data">
				{{-- @csrf --}}

				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<label for="question">Question</label>
							<input type="text" class="form-control" id="question" name="question" value="{{ $faq->questions }}">
						</div>
					</div>					
				</div>


				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<label for="answers">Answer</label>
							<input type="text" class="form-control" id="answer" name="answer" value="{{ $faq->answers }}">
						</div>
					</div>

					
				</div>

				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<label for="status">Status</label>
							<select name="status" class="form-control">
								<option value="Deactive" {{ $faq->status=='Deactive'? 'selected': null }}>Deactive</option>
								<option value="Active" {{ $faq->status=='Active'? 'selected': null }}>Active</option>
							</select>
						</div>
					</div>				
				</div>                   
				<button type="submit" class="btn btn-success">Submit</button><br>
			</div>
		</form>
	</div>
</div>
</div>
<!-- END Page Content -->
@endsection

