@extends('back.app')
<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
@section('content')

<style>
	.logo-img{ width: 100px; height: auto; }
	.block-content { width: 90%; margin: 0 5% }
	.page-header { margin-left: 1% }
	h2 { font-weight: 500; }
</style>
<div class="page-header">
          <h2><i class="md md-person-add"></i>Edit Content</h2>
          <p class="lead">Add your own content</p>
        </div>

{{-- @include('back.include.header') --}}
{{-- @include('back.include.sidebar') --}}
<!-- Page Content -->
<div class="content" style="background: white; width: 98%; margin: 1%">
<br>
	<a href="/admin/static-pages" style="margin-left: 4%"><button type="submit" class="btn btn-danger">Back</button> </a><br><br>

	@if($errors->any())
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="alert alert-primary">
		<p>{{ $message }}</p>
	</div>
	@endif
	<div class="row justify-content-center">
		<div class="block-content">
			<form action="" method="POST" enctype="multipart/form-data">
				{{-- @csrf --}}			
				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							<label for="heading">Page</label>
							<input type="text" class="form-control" id="page_name" name="page_name" value="{{ $page->page_name }}" readonly>
						</div>
					</div>							
					
					<div class="col-md-12">						
						<div class="form-material floating">
							<label for="heading">Page Heading</label>
							<input type="text" class="form-control" id="page_heading" name="page_heading" value="{{ $page->page_heading }}">
						</div>
					</div>					
				</div>
				
				<div class="form-group row">
					<div class="col-md-12">
						<div class="form-material floating">
							
								<label for="text">Page Description</label>
							
								<textarea name="description" class="form-control" id="description" >{{$page->description}}</textarea><br>  
								<script>
									CKEDITOR.replace( 'description' );
								</script>
														
						</div>					
					</div>
				</div>

				<button type="submit" class="btn btn-success">Submit</button><br>
			</div>
		</form>
	</div>
</div>
</div>
<!-- END Page Content -->
@endsection

