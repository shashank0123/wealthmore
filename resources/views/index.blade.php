@extends('layouts.frontpage')
@section('content')
<style type="text/css">
 .ps-product--fashion .ps-product__cart {
  width: 130px;
 }
 #buytxt {
  display: inline-block;
  float: right;
  display: block;
  color: #fff;
  margin-left: 5px;
  font-size: 18px;
 }
</style>
<div class="container-fluid" style="margin-top: 20px">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="images/slider/newimage1.jpeg" alt="image" style="width:100%;">
      </div>
      <div class="item">
        <img src="images/slider/newimage3.jpg" alt="image" style="width:100%;">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

<div class="container-fluid">
  <div class="container col-lg-12" >
    <div class="row" style="background-color: #fff; padding-top: 30px; padding-bottom:  30px;">     
      <div class="col-lg-4" style="padding: 10px 50px 0px 50px">   
        <a class="ps-collection" href="product-listing"><img src="images/collection/home-1.jpg" alt=""></a>
      </div>
      <div class="col-lg-4" style="padding: 10px 50px 0px 50px">       
        <a class="ps-collection" href="product-listing"><img src="images/collection/home-2.jpg" alt=""></a>
      </div>
      <div class="col-lg-4" style="padding: 10px 50px 0px 50px">         
        <a class="ps-collection" href="product-listing"><img src="images/collection/home-3.png" alt=""></a>
      </div>
     </div>
   </div>
</div>
    <div class="ps-section pt-80 pb-30">
        <div class="ps-section__header text-center">
          <h2 class="ps-section__title">OTHERS PRODUCT</h2>
          <p>Featured collections created and curated by our editors</p>
        </div>
        <div class="ps-section__content">
          <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 ">
                  <div class="ps-product--fashion">
                    <div class="ps-product__thumbnail"><a class="ps-product__overlay" href="product-detail"></a><img class="lazy" src="images/product/fashion-1.jpg" alt="">
                      <div class="ps-badge ps-badge--sale-off"><span>-40%</span></div>
                      <ul class="ps-product__actions">
                        <li><a class="ps-modal-open" href="#quickview" title="Quick View"><i class="ps-icon-eye"></i></a></li>
                        <li><a href="#" title="Compare"><i class="ps-icon-compare"></i></a></li>
                        <li><a href="#" title="Favorite"><i class="ps-icon-heart"></i></a></li>
                      </ul>
                    </div>
                    <div class="ps-product__content"><a class="ps-product__title" href="product-detail">D TSHIRT</a>
                      <p class="ps-product__price">
                        <del>Rs. 1500.89</del>Rs. 1050
                      </p><a class="ps-product__cart" href="#" title="Add To Cart"><i class="ps-icon-cart-2"> <span id="buytxt">Buy </span></i></a>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 ">
                  <div class="ps-product--fashion">
                    <div class="ps-product__thumbnail"><a class="ps-product__overlay" href="product-detail"></a><img class="lazy" src="images/product/fashion-2.jpg" alt="">
                      <ul class="ps-product__actions">
                        <li><a class="ps-modal-open" href="#quickview" title="Quick View"><i class="ps-icon-eye"></i></a></li>
                        <li><a href="#" title="Compare"><i class="ps-icon-compare"></i></a></li>
                        <li><a href="#" title="Favorite"><i class="ps-icon-heart"></i></a></li>
                      </ul>
                    </div>
                    <div class="ps-product__content"><a class="ps-product__title" href="product-detail">Designer Kurti</a>
                      <p class="ps-product__price">Rs. 1050.00</p><a class="ps-product__cart" href="#" title="Add To Cart"><i class="ps-icon-cart-2"> <span id="buytxt">Buy </span></i></a>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 ">
                  <div class="ps-product--fashion">
                    <div class="ps-product__thumbnail"><a class="ps-product__overlay" href="product-detail"></a><img class="lazy" src="images/product/fashion-3.jpg" alt="">
                      <ul class="ps-product__actions">
                        <li><a class="ps-modal-open" href="#quickview" title="Quick View"><i class="ps-icon-eye"></i></a></li>
                        <li><a href="#" title="Compare"><i class="ps-icon-compare"></i></a></li>
                        <li><a href="#" title="Favorite"><i class="ps-icon-heart"></i></a></li>
                      </ul>
                    </div>
                    <div class="ps-product__content"><a class="ps-product__title" href="product-detail">Designer Saree</a>
                      <p class="ps-product__price">Rs. 2250.00</p><a class="ps-product__cart" href="#" title="Add To Cart"><i class="ps-icon-cart-2"> <span id="buytxt">Buy </span></i></a>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 ">
                  <div class="ps-product--fashion">
                    <div class="ps-product__thumbnail"><a class="ps-product__overlay" href="product-detail"></a><img class="lazy" src="images/product/fashion-4.jpg" alt="">
                      <div class="ps-badge"><span>New</span></div>
                      <ul class="ps-product__actions">
                        <li><a class="ps-modal-open" href="#quickview" title="Quick View"><i class="ps-icon-eye"></i></a></li>
                        <li><a href="#" title="Compare"><i class="ps-icon-compare"></i></a></li>
                        <li><a href="#" title="Favorite"><i class="ps-icon-heart"></i></a></li>
                      </ul>
                    </div>
                    <div class="ps-product__content"><a class="ps-product__title" href="product-detail">Designer Saree</a>
                      <p class="ps-product__price">Rs. 1050.00</p><a class="ps-product__cart" href="#" title="Add To Cart"><i class="ps-icon-cart-2"> <span id="buytxt">Buy </span></i></a>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 ">
                  <div class="ps-product--fashion">
                    <div class="ps-product__thumbnail"><a class="ps-product__overlay" href="product-detail"></a><img class="lazy" src="images/product/fashion-5.jpg" alt="">
                      <ul class="ps-product__actions">
                        <li><a class="ps-modal-open" href="#quickview" title="Quick View"><i class="ps-icon-eye"></i></a></li>
                        <li><a href="#" title="Compare"><i class="ps-icon-compare"></i></a></li>
                        <li><a href="#" title="Favorite"><i class="ps-icon-heart"></i></a></li>
                      </ul>
                    </div>
                    <div class="ps-product__content"><a class="ps-product__title" href="product-detail">Designer T-Shirt</a>
                      <p class="ps-product__price">Rs. 1050.00</p><a class="ps-product__cart" href="#" title="Add To Cart"><i class="ps-icon-cart-2"> <span id="buytxt">Buy </span></i></a>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 ">
                  <div class="ps-product--fashion">
                    <div class="ps-product__thumbnail"><a class="ps-product__overlay" href="product-detail"></a><img class="lazy" src="images/product/fashion-6.jpg" alt="">
                      <div class="ps-badge ps-badge--sale-off"><span>-40%</span></div>
                      <ul class="ps-product__actions">
                        <li><a class="ps-modal-open" href="#quickview" title="Quick View"><i class="ps-icon-eye"></i></a></li>
                        <li><a href="#" title="Compare"><i class="ps-icon-compare"></i></a></li>
                        <li><a href="#" title="Favorite"><i class="ps-icon-heart"></i></a></li>
                      </ul>
                    </div>
                    <div class="ps-product__content"><a class="ps-product__title" href="product-detail">Designer Kurti</a>
                      <p class="ps-product__price">
                        <del>Rs. 3150.89</del>Rs. 2250
                      </p><a class="ps-product__cart" href="#" title="Add To Cart"><i class="ps-icon-cart-2"> <span id="buytxt">Buy </span></i></a>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 ">
                  <div class="ps-product--fashion">
                    <div class="ps-product__thumbnail"><a class="ps-product__overlay" href="product-detail"></a><img class="lazy" src="images/product/fashion-7.jpg" alt="">
                      <ul class="ps-product__actions">
                        <li><a class="ps-modal-open" href="#quickview" title="Quick View"><i class="ps-icon-eye"></i></a></li>
                        <li><a href="#" title="Compare"><i class="ps-icon-compare"></i></a></li>
                        <li><a href="#" title="Favorite"><i class="ps-icon-heart"></i></a></li>
                      </ul>
                    </div>
                    <div class="ps-product__content"><a class="ps-product__title" href="product-detail">Designer Saree</a>
                      <p class="ps-product__price">Rs. 1050.00</p><a class="ps-product__cart" href="#" title="Add To Cart"><i class="ps-icon-cart-2"> <span id="buytxt">Buy </span></i></a>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 ">
                  <div class="ps-product--fashion">
                    <div class="ps-product__thumbnail"><a class="ps-product__overlay" href="product-detail"></a><img class="lazy" src="images/product/fashion-8.jpg" alt="">
                      <ul class="ps-product__actions">
                        <li><a class="ps-modal-open" href="#quickview" title="Quick View"><i class="ps-icon-eye"></i></a></li>
                        <li><a href="#" title="Compare"><i class="ps-icon-compare"></i></a></li>
                        <li><a href="#" title="Favorite"><i class="ps-icon-heart"></i></a></li>
                      </ul>
                    </div>
                    <div class="ps-product__content"><a class="ps-product__title" href="product-detail">Designer Kurti</a>
                      <p class="ps-product__price">Rs. 1050.00</p><a class="ps-product__cart" href="#" title="Add To Cart"><i class="ps-icon-cart-2"> <span id="buytxt">Buy </span></i></a>
                    </div>
                  </div>
                </div>
          </div>
        </div>
      </div>
    </div>
    <div class="ps-section pt-60 pb-30">
      <div class="ps-container">
        <div class="ps-section__header text-center">
          <h2 class="ps-section__title">POPULAR PRODUCT</h2>
          <p>Here are key products that bring fashionistas to FGrouth Store.</p>
        </div>
        <div class="ps-section__content">
          <div class="ps-slider--center owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="30" data-owl-nav="true" data-owl-dots="false" data-owl-item="4" data-owl-item-xs="1" data-owl-item-sm="2" data-owl-item-md="3" data-owl-item-lg="4" data-owl-duration="1000" data-owl-mousedrag="on" data-owl-nav-left="&lt;i class='ps-icon-arrow-left'&gt;&lt;/i&gt;" data-owl-nav-right="&lt;i class='ps-icon-arrow-right'&gt;&lt;/i&gt;">
            <div class="ps-product--fashion">
              <div class="ps-product__thumbnail"><a class="ps-product__overlay" href="product-detail"></a><img class="lazy" src="images/product/fashion-1.jpg" alt="">
                <div class="ps-badge ps-badge--sale-off"><span>-40%</span></div>
                <ul class="ps-product__actions">
                  <li><a href="#" title="Quick View"><i class="ps-icon-eye"></i></a></li>
                  <li><a href="#" title="Compare"><i class="ps-icon-compare"></i></a></li>
                  <li><a href="#" title="Favorite"><i class="ps-icon-heart"></i></a></li>
                </ul>
              </div>
              <div class="ps-product__content"><a class="ps-product__title" href="product-detail">PARKER TSHIRT</a>
                <p class="ps-product__price">
                  <del>Rs. 1500.89</del>Rs. 1050
                </p><a class="ps-product__cart" href="#" title="Add To Cart"><i class="ps-icon-cart-2"> <span id="buytxt">Buy </span></i></a>
              </div>
            </div>
            <div class="ps-product--fashion">
              <div class="ps-product__thumbnail"><a class="ps-product__overlay" href="product-detail"></a><img class="lazy" src="images/product/fashion-2.jpg" alt="">
                <ul class="ps-product__actions">
                  <li><a href="#" title="Quick View"><i class="ps-icon-eye"></i></a></li>
                  <li><a href="#" title="Compare"><i class="ps-icon-compare"></i></a></li>
                  <li><a href="#" title="Favorite"><i class="ps-icon-heart"></i></a></li>
                </ul>
              </div>
              <div class="ps-product__content"><a class="ps-product__title" href="product-detail">Designer Kurti</a>
                <p class="ps-product__price">Rs. 1050.00</p><a class="ps-product__cart" href="#" title="Add To Cart"><i class="ps-icon-cart-2"> <span id="buytxt">Buy </span></i></a>
              </div>
            </div>
            <div class="ps-product--fashion">
              <div class="ps-product__thumbnail"><a class="ps-product__overlay" href="product-detail"></a><img class="lazy" src="images/product/fashion-3.jpg" alt="">
                <ul class="ps-product__actions">
                  <li><a href="#" title="Quick View"><i class="ps-icon-eye"></i></a></li>
                  <li><a href="#" title="Compare"><i class="ps-icon-compare"></i></a></li>
                  <li><a href="#" title="Favorite"><i class="ps-icon-heart"></i></a></li>
                </ul>
              </div>
              <div class="ps-product__content"><a class="ps-product__title" href="product-detail">Designer Saree</a>
                <p class="ps-product__price">Rs. 1050.00</p><a class="ps-product__cart" href="#" title="Add To Cart"><i class="ps-icon-cart-2"> <span id="buytxt">Buy </span></i></a>
              </div>
            </div>
            <div class="ps-product--fashion">
              <div class="ps-product__thumbnail"><a class="ps-product__overlay" href="product-detail"></a><img class="lazy" src="images/product/fashion-4.jpg" alt="">
                <div class="ps-badge"><span>New</span></div>
                <ul class="ps-product__actions">
                  <li><a href="#" title="Quick View"><i class="ps-icon-eye"></i></a></li>
                  <li><a href="#" title="Compare"><i class="ps-icon-compare"></i></a></li>
                  <li><a href="#" title="Favorite"><i class="ps-icon-heart"></i></a></li>
                </ul>
              </div>
              <div class="ps-product__content"><a class="ps-product__title" href="product-detail">Designer Saree</a>
                <p class="ps-product__price">Rs. 1050.00</p><a class="ps-product__cart" href="#" title="Add To Cart"><i class="ps-icon-cart-2"> <span id="buytxt">Buy </span></i></a>
              </div>
            </div>
            <div class="ps-product--fashion">
              <div class="ps-product__thumbnail"><a class="ps-product__overlay" href="product-detail"></a><img class="lazy" src="images/product/fashion-5.jpg" alt="">
                <ul class="ps-product__actions">
                  <li><a href="#" title="Quick View"><i class="ps-icon-eye"></i></a></li>
                  <li><a href="#" title="Compare"><i class="ps-icon-compare"></i></a></li>
                  <li><a href="#" title="Favorite"><i class="ps-icon-heart"></i></a></li>
                </ul>
              </div>
              <div class="ps-product__content"><a class="ps-product__title" href="product-detail">T-Shirt</a>
                <p class="ps-product__price">Rs. 1050.00</p><a class="ps-product__cart" href="#" title="Add To Cart"><i class="ps-icon-cart-2"> <span id="buytxt">Buy </span></i></a>
              </div>
            </div>
            <div class="ps-product--fashion">
              <div class="ps-product__thumbnail"><a class="ps-product__overlay" href="product-detail"></a><img class="lazy" src="images/product/fashion-6.jpg" alt="">
                <div class="ps-badge ps-badge--sale-off"><span>-40%</span></div>
                <ul class="ps-product__actions">
                  <li><a href="#" title="Quick View"><i class="ps-icon-eye"></i></a></li>
                  <li><a href="#" title="Compare"><i class="ps-icon-compare"></i></a></li>
                  <li><a href="#" title="Favorite"><i class="ps-icon-heart"></i></a></li>
                </ul>
              </div>
              <div class="ps-product__content"><a class="ps-product__title" href="product-detail">Designer Kurti</a>
                <p class="ps-product__price">
                  <del>Rs. 3150.89</del>Rs. 2250
                </p><a class="ps-product__cart" href="#" title="Add To Cart"><i class="ps-icon-cart-2"> <span id="buytxt">Buy </span></i></a>
              </div>
            </div>
            <div class="ps-product--fashion">
              <div class="ps-product__thumbnail"><a class="ps-product__overlay" href="product-detail"></a><img class="lazy" src="images/product/fashion-7.jpg" alt="">
                <ul class="ps-product__actions">
                  <li><a href="#" title="Quick View"><i class="ps-icon-eye"></i></a></li>
                  <li><a href="#" title="Compare"><i class="ps-icon-compare"></i></a></li>
                  <li><a href="#" title="Favorite"><i class="ps-icon-heart"></i></a></li>
                </ul>
              </div>
              <div class="ps-product__content"><a class="ps-product__title" href="product-detail">Designer Saree</a>
                <p class="ps-product__price">Rs. 1050.00</p><a class="ps-product__cart" href="#" title="Add To Cart"><i class="ps-icon-cart-2"> <span id="buytxt">Buy </span></i></a>
              </div>
            </div>
            <div class="ps-product--fashion">
              <div class="ps-product__thumbnail"><a class="ps-product__overlay" href="product-detail"></a><img class="lazy" src="images/product/fashion-8.jpg" alt="">
                <ul class="ps-product__actions">
                  <li><a href="#" title="Quick View"><i class="ps-icon-eye"></i></a></li>
                  <li><a href="#" title="Compare"><i class="ps-icon-compare"></i></a></li>
                  <li><a href="#" title="Favorite"><i class="ps-icon-heart"></i></a></li>
                </ul>
              </div>
              <div class="ps-product__content"><a class="ps-product__title" href="product-detail">Designer Kurti</a>
                <p class="ps-product__price">Rs. 1050.00</p><a class="ps-product__cart" href="#" title="Add To Cart"><i class="ps-icon-cart-2"> <span id="buytxt">Buy </span></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>   
    @endsection