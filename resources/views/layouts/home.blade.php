<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Wealthmore India </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- favicon -->
    <link rel="shortcut icon" href="/assets/img/favicon.png" type="image/x-icon">
    <!-- bootstrap -->
    <link rel="stylesheet" href="/home-assets/css/bootstrap.min.css">
    <!-- fontawesome icon  -->
    <link rel="stylesheet" href="/home-assets/css/fontawesome.min.css">
    <!-- flaticon css -->
    <link rel="stylesheet" href="/home-assets/fonts/flaticon.css">
    <!-- animate.css -->
    <link rel="stylesheet" href="/home-assets/css/animate.css">
    {{-- <link rel="stylesheet" href="/home-assets/css/animate2.css"> --}}
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="/home-assets/css/owl.carousel.min.css">
    <!-- magnific popup -->
    <link rel="stylesheet" href="/home-assets/css/magnific-popup.css">
    <!-- stylesheet -->
    <link rel="stylesheet" href="/home-assets/css/style.css">
    <!-- responsive -->
    <link rel="stylesheet" href="/home-assets/css/responsive.css">


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

   
</head>

<body>
    <!-- preloader begin-->
    <div class="preloader">
        <img src="img/preloader.gif">
        {{-- <div class='loader'>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--dot'></div>
            <div class='loader--text'></div>
        </div> --}}
    </div>
    <!-- preloader end -->

    <!-- header begin-->
    {{-- <header class="header" style="background: -webkit-linear-gradient(-30deg, #052157 0%, #91039f 100%);"> --}}
        <header class="header">
            <div class="header-top">
                <div class="container">
                    <div class="row justify-content-between d-flex">
                        <div class="col-xl-8 col-lg-8 d-flex align-items-center">
                            <div class="support-bar">
                                <ul>
                                    {{-- <li><span class="icon"><i class="fas fa-globe-africa"></i></span>Language: <span>
                                        <select>
                                            <option>English</option>
                                            <option>Bangla</option>
                                            <option>Arabic</option>
                                        </select>
                                    </span></li> --}}
                                    <li><span class="icon"><i class="fas fa-envelope"></i></span>info@wealthindia.global</li>
                                    <li><span class="icon"><i class="fas fa-phone"></i></span>18001038581</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4" style="text-align: right;">
                            @if(empty($member->id))
                            
                                    <a href="/en/login" class=" btn-default" style="color: #fff">Sign In</a> &nbsp;&nbsp;&nbsp;
                                
                            
                                    <a href="/register" class=" btn-default" style="color: #fff">Sign Up</a>
                                
                            @else

                            
                                    <a href="/en/logout">Logout</a>
                                

                            @endif

                        </div>
                    </div>
                </div>
            </div>
            <div class="header-bottom">
                <div class="container">
                    <div class="row d-flex">
                        <div class="col-xl-3 col-lg-3 col-12 d-block d-xl-flex d-lg-flex align-items-center">
                            <div class="mobile-special">
                                <div class="row d-flex">
                                    <div class="col-6 col-xl-12 col-lg-12 d-flex align-items-center">
                                        <div class="logo">
                                            <a href="/">
                                                <img src="/wealth-assets/img/logo-bella-shop1.png" alt="" >
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-6 d-block d-xl-none d-lg-none">
                                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                        <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-9 col-lg-9">
                        <div class="mainmenu">
                            <nav class="navbar navbar-expand-lg">

                                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav mr-auto justify-content-center">
                                        <li class="nav-item active">
                                            <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="/aboutUs">About</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="/investment">Bussiness Plan</a>
                                        </li>

                                        <li class="nav-item ">
                                            <a class="nav-link " href="/services">
                                                Service
                                            </a>
                                        </li>
                                        <li class="nav-item ">
                                            <a class="nav-link" href="/{{App::getLocale()}}/shop">
                                                Shopping
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Legal documents</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="/contactUs">Contact</a>
                                        </li>

                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>



                    
                </div>
            </div>
        </div>
    </header>

    @yield('content')



    <!-- footer begin -->
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-6">
                    <div class="box">
                        <div class="logo">
                            <a href="#">
                                <img src="/wealth-assets/img/logo-bella-shop1.png" alt="" style="width: auto ; height: 120px">
                            </a>
                        </div>
                        <p style="color: #fff !important">
                            We are a full service Digital Marketing Agency all
                            the foundational tool you need inbound success.
                            With this module theres no need to another day.
                        </p>
                        <div class="social_links">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-youtube"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-6">
                    <div class="box box2">
                        <h2>
                            Links
                        </h2>
                        <ul>
                            <li>
                                <a href="/">
                                    Home
                                </a>
                            </li>
                            <li>
                                <a href="/aboutUs">
                                    About
                                </a>
                            </li>
                            <li>
                                <a href="/services">
                                    Services
                                </a>
                            </li>
                            <li>
                                <a href="/faqs">
                                    FAQ
                                </a>
                            </li>
                            <li>
                                <a href="/contactUs">
                                    Contact
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-6">
                    <div class="box box3">
                        <h2>
                            About Us
                        </h2>
                        <ul>
                            <li>
                                <a href="/aboutUs">
                                    About Us
                                </a>
                            </li>
                            {{-- <li>
                                <a href="#">
                                    Blog
                                </a>
                            </li> --}}
                            <li>
                                <a href="/contactUs">
                                    Contact Us
                                </a>
                            </li>
                            {{-- <li>
                                <a href="#">
                                    Sign in
                                </a>
                            </li> --}}
                        </ul>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6">
                    <div class="box box4">
                        <h2>
                            Contact Us
                        </h2>
                        <p style="color: #fff !important">
                            WZ-31,<br>
New Delhi-110045
                        </p>
                        <a href="#">
                            info@wealthindia.global
                        </a>
                        <a href="#">
                            18001038581
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            <p class="text-center">
                © 2019. All Rights Reserved.
            </p>
        </div>
    </footer>
    <!-- footer end -->

    <!-- scroll top button begin -->
    <div class="scroll-to-top">
        <a><i class="fas fa-long-arrow-alt-up"></i></a>
    </div>
    <!-- scroll top button end -->


    <!-- jquery -->
    <script src="/home-assets/js/jquery.js"></script>
    <!-- bootstrap -->
    <script src="/home-assets/js/bootstrap.min.js"></script>
    <!-- owl carousel -->
    <script src="/home-assets/js/owl.carousel.js"></script>
    <!-- magnific popup -->
    <script src="/home-assets/js/jquery.magnific-popup.js"></script>
    <!-- way poin js-->
    <script src="/home-assets/js/waypoints.min.js"></script>
    <!-- counter up js-->
    <script src="/home-assets/js/jquery.counterup.min.js"></script>
    <!-- wow js-->
    <script src="/home-assets/js/wow.min.js"></script>
    <!-- main -->
    <script src="/home-assets/js/main.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.js"></script>
    
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script> --}}

    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.css"></script>

   
    @yield('script')
</body>



</html>