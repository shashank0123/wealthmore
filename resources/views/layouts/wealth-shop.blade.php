<?php
use App\MainCategory;
use App\Models\MemberDetail;
use App\Wishlist;

$main_categories = MainCategory::where('category_id',0)->where('status','Active')->get();
$count_wishlist = 0;
if(!empty($member->id)){  
    $count_wishlist = Wishlist::where('user_id',$member->id)->count();
    $memberDetail = MemberDetail::where('member_id',$member->id)->first();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>WealthIndia</title>

    <link href="https://fonts.googleapis.com/css?family=PT+Serif:400,400i,700,700ii%7CRoboto:300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic" rel="stylesheet">

    <link rel="stylesheet" href="/new-asset/css/font-awesome.min.css">
    <link rel="stylesheet" href="/new-asset/css/bootstrap.min.css">
    <link rel="stylesheet" href="/new-asset/css/ion.rangeSlider.css">
    <link rel="stylesheet" href="/new-asset/css/ion.rangeSlider.skinFlat.css">
    <link rel="stylesheet" href="/new-asset/css/jquery.bxslider.css">
    <link rel="stylesheet" href="/new-asset/css/jquery.fancybox.css">
    <link rel="stylesheet" href="/new-asset/css/flexslider.css">
    <link rel="stylesheet" href="/new-asset/css/swiper.css">
    <link rel="stylesheet" href="/new-asset/css/style.css">
    <link rel="stylesheet" href="/new-asset/css/media.css">
    <style>
        a { cursor: pointer !important; }
    </style>
</head>
<body>
    <input type="hidden" id="u_id" value="<?php if(!empty($member->id))echo $member->id;?>">
    <!-- Header - start -->
    <header class="header">

        <!-- Topbar - start -->
        <div class="header_top">
            <div class="container">
                <ul class="contactinfo nav nav-pills">
                    <li>
                        <i class='fa fa-phone'></i> 18001038581
                    </li>
                    <li>
                        <i class="fa fa-envelope"></i> info@wealthindia.global
                    </li>
                </ul>
                <!-- Social links -->
                <ul class="social-icons nav navbar-nav">
                    <li>
                        <a href="http://facebook.com" rel="nofollow" target="_blank">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="http://google.com" rel="nofollow" target="_blank">
                            <i class="fa fa-google-plus"></i>
                        </a>
                    </li>
                    <li>
                        <a href="http://twitter.com" rel="nofollow" target="_blank">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>                
                    <li>
                        <a href="http://instagram.com" rel="nofollow" target="_blank">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </li>
                </ul>		</div>
            </div>
            <!-- Topbar - end -->

            <!-- Logo, Shop-menu - start -->
            <div class="header-middle">
                <div class="container header-middle-cont">
                    <div class="toplogo">
                        <a href="/en/shop">
                            <img src="/wealth-assets/img/logo-bella-shop1.png" alt="AllStore - MultiConcept eCommerce Template" style="height: 100px; width: auto">
                        </a>
                    </div>
                    <div class="shop-menu">
                        <ul>
                    {{-- <li>
                        <a href="/comparew">
                            <i class="fa fa-bar-chart"></i>
                            <span class="shop-menu-ttl">Compare</span> (5)
                        </a>
                    </li> --}}

                    @if(!empty($member->id))

                    <li class="topauth">
                        <a href="/en">
                            <i class="fa fa-lock"></i>
                            <span class="shop-menu-ttl">My Account</span>
                        </a>
                        <a href="/en/logout">
                            <span class="shop-menu-ttl">Logout</span>
                        </a>
                    </li>

                    @else

                    <li class="topauth">
                        <a href="/register">
                            <i class="fa fa-lock"></i>
                            <span class="shop-menu-ttl">Registration</span>
                        </a>
                        <a href="/en/login">
                            <span class="shop-menu-ttl">Login</span>
                        </a>
                    </li>

                    @endif

                    <li>
                        <a onclick="getWishlist()">
                            <i class="fa fa-heart"></i>
                            <span class="shop-menu-ttl">Wishlist</span>
                            (<span id="topbar-favorites" id="wishlist-count">{{$count_wishlist}}</span>)
                        </a>
                    </li>

                    <li>
                        <div class="h-cart">
                            <a onclick="getCart()">
                                <i class="fa fa-shopping-cart"></i>
                                <span class="shop-menu-ttl" >Cart</span>
                                <?php             
                                if(!empty(session()->get('count')))                       
                                    $show=session()->get('count');
                                else $show=0;
                                ?>
                                (<b id="cart-count">{{$show}}</b>)
                            </a>
                        </div>
                    </li>

                </ul>
            </div>
        </div>
    </div>
    <!-- Logo, Shop-menu - end -->

    <!-- Topmenu - start -->
    <div class="header-bottom">
        <div class="container">
            <nav class="topmenu">
                <!-- Catalog menu - start -->
                <div class="topcatalog">
                    <a class="topcatalog-btn" href="#"><span>All</span> catalog</a>
                    @if(!empty($main_categories))
                    <ul class="topcatalog-list">
                        @foreach($main_categories as $main)
                        <li>
                            <a href="/categorywise-productsw/{{$main->slug_name}}">
                                {{strtoupper($main->Mcategory_name)}}
                            </a>
                            <?php
                            $categories = MainCategory::where('category_id',$main->id)->get();
                            ?>
                            @if(!empty($categories))
                            <i class="fa fa-angle-right"></i>                           
                            <ul>
                               @foreach($categories as $cat)
                               <li>
                                <a href="/categorywise-productsw/{{$cat->slug_name}}">
                                    {{strtoupper($cat->Mcategory_name)}}
                                </a>                                
                                <?php
                                $sub_categories = MainCategory::where('category_id',$cat->id)->get();
                                ?>
                                @if(!empty($sub_categories))                                
                                <i class="fa fa-angle-right"></i>
                                <ul>
                                    @foreach($sub_categories as $sub)
                                    <li>
                                        <a href="/categorywise-productsw/{{$sub->slug_name}}">
                                            {{strtoupper($sub->Mcategory_name)}}
                                        </a>
                                    </li>
                                    @endforeach                                        
                                </ul>
                                @endif
                            </li>
                            @endforeach                            
                        </ul>
                        @endif
                    </li>
                    @endforeach                      
                </ul>
                @endif
            </div>
            <!-- Catalog menu - end -->

            <!-- Main menu - start -->
            <button type="button" class="mainmenu-btn">Menu</button>

            <ul class="mainmenu">
                <li>
                    <a href="/en/shop" class="active">
                        Home
                    </a>
                </li>
                @if(!empty($main_categories))
                @foreach($main_categories as $main)
                <li class="menu-item-has-children">
                    <a href="/categorywise-productsw/{{$main->slug_name}}">
                        {{strtoupper($main->Mcategory_name)}}
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <?php
                    $categories = MainCategory::where('category_id',$main->id)->get();
                    ?>
                    @if(!empty($categories))

                    <ul class="sub-menu">
                        @foreach($categories as $cat)
                        <li>
                            <a href="/categorywise-productsw/{{$cat->slug_name}}">
                                {{strtoupper($cat->Mcategory_name)}}
                            </a>                        
                            <?php
                            $sub_categories = MainCategory::where('category_id',$cat->id)->get();
                            ?>
                            @if(!empty($sub_categories))
                            <i class="fa fa-angle-right"></i>
                            <ul>

                                @foreach($sub_categories as $sub)
                                <li>
                                    <a href="/categorywise-productsw/{{$sub->slug_name}}">
                                        {{strtoupper($sub->Mcategory_name)}}
                                    </a>
                                </li>
                                @endforeach                                        
                            </ul>
                            @endif
                        </li>
                        @endforeach
                    </ul>
                    @endif
                </li>    
                @endforeach
                @endif
                <li class="mainmenu-more">
                    <span>...</span>
                    <ul class="mainmenu-sub"></ul>
                </li>
            </ul>
            <!-- Main menu - end -->

            <!-- Search - start -->
            <div class="topsearch">
                <a id="topsearch-btn" class="topsearch-btn" href="#"><i class="fa fa-search"></i></a>
                <form class="topsearch-form" action="/searched-productsw">
                    <input type="text" placeholder="Search products" id="product-keyword" name="product_keyword">
                    <button type="button" onclick="searchValidate()"><i class="fa fa-search"></i></button>
                </form>
            </div>
            <!-- Search - end -->

        </nav>		</div>
    </div>
    <!-- Topmenu - end -->

</header>
<!-- Header - end -->

@yield('content')



<!-- Footer - start -->
<footer class="footer-wrap">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                {{-- <div class="companyinfo">
                    <a href="/en/shop">
                        <img src="/wealth-assets/img/wealth-india-logo.png" alt="AllStore - MultiConcept eCommerce Responsive HTML5 Template" style="">                
                        <p>At Wealth India we value getting results, honesty, teamwork, building exceptional products & services & most of all putting clients & partners first by offering the best services possible to them.</p>            
                    </a>
                </div> --}}
                {{-- <div class="f-block-list"> --}}
                    {{-- <div class="row">
                                            <div class="col-sm-5">
                    <h3>Subscribe to news</h3>                            
                        </div>
                        <div class="col-sm-7">                            
                    <form class="f-subscribe-form" action="#">
                        <input placeholder="Your e-mail" type="text">
                        <button type="submit"><i class="fa fa-paper-plane"></i></button>
                    </form>
                    <p>Enter your email address if you want to receive our newsletter. Subscribe now!</p>
                       
                </div>
            </div> --}}
                        {{-- <div class="f-block-wrap">
                            <div class="f-block">
                                <a href="#" class="f-block-btn" data-id="#f-block-modal-1">
                                    <div class="iframe-img">
                                        <img src="http://placehold.it/300x127" alt="About us">
                                    </div>
                                    <div class="overlay-icon">
                                        <i class="fa fa-info-circle"></i>
                                    </div>
                                </a>
                                <p class="f-info-ttl">About us</p>
                                <p>Shipping and payment information.</p>
                            </div>
                        </div>
                        <div class="f-block-wrap">
                            <div class="f-block">
                                <a href="#" class="f-block-btn" data-id="#f-block-modal-2">
                                    <div class="iframe-img">
                                        <img src="http://placehold.it/300x127" alt="Ask questions">
                                    </div>
                                    <div class="overlay-icon">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                </a>
                                <p class="f-info-ttl">Ask questions</p>
                                <p>We call back within 10 minutes</p>
                            </div>
                        </div>
                        <div class="f-block-wrap">
                            <div class="f-block">
                                <a href="#" class="f-block-btn" data-id="#f-block-modal-3" data-content="<iframe width='853' height='480' src='https://www.youtube.com/embed/kaOVHSkDoPY?rel=0&amp;showinfo=0' allowfullscreen></iframe>">
                                    <div class="iframe-img">
                                        <img src="http://placehold.it/300x127" alt="Video (2 min)">
                                    </div>
                                    <div class="overlay-icon">
                                        <i class="fa fa-play-circle"></i>
                                    </div>
                                </a>
                                <p class="f-info-ttl">Video (2 min)</p>
                                <p>Watch a video about our store</p>
                            </div>
                        </div>
                        <div class="f-block-wrap">
                            <div class="f-block">
                                <a href="#" class="f-block-btn" data-id="#f-block-modal-4">
                                    <div class="iframe-img">
                                        <img src="http://placehold.it/300x127" alt="Our address">
                                    </div>
                                    <div class="overlay-icon">
                                        <i class="fa fa-map-marker"></i>
                                    </div>
                                </a>
                                <p class="f-info-ttl">Our address</p>
                                <p>Spain, Madrid, 45</p>
                            </div>
                        </div> --}}
                    {{-- </div> --}}

                   {{--  <div class="stylization f-block-modal f-block-modal-content" id="f-block-modal-1">
                        <img class="f-block-modal-img" src="http://placehold.it/500x334" alt="About us">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam natus iste ullam vero, tenetur ab ipsa consectetur deleniti officiis ex debitis incidunt alias voluptatum, maxime placeat dolores veniam sunt at atque velit, soluta. Neque ea alias quia provident molestias, ratione aut esse placeat beatae sequi sed laudantium. Unde animi nihil esse, repellendus exercitationem dicta harum ab labore, voluptates explicabo in, quidem dolorum voluptas!
                    </div> --}}
                    {{-- <div class="stylization f-block-modal f-block-modal-callback" id="f-block-modal-2">
                        <div class="modalform">
                            <form action="#" method="POST" class="form-validate">
                                <p class="modalform-ttl">Callback</p>
                                <input type="text" placeholder="Your name" data-required="text" name="name">
                                <input type="text" placeholder="Your phone" data-required="text" name="phone">
                                <button type="submit"><i class="fa fa-paper-plane"></i> Send</button>
                            </form>
                        </div>
                    </div> --}}
                    {{-- <div class="stylization f-block-modal f-block-modal-video" id="f-block-modal-3">

                    </div>
                    <div class="stylization f-block-modal f-block-modal-map" id="f-block-modal-4">
                        <div class="allstore-gmap">
                            <div class="marker" data-zoom="15" data-lat="-37.81485261872975" data-lng="144.95655298233032" data-marker="img/marker.png">534-540 Little Bourke St, Melbourne VIC 3000, Australia</div>
                        </div>
                    </div>
                    <div class="f-delivery">
                        <img src="img/map.png" alt="">
                        <h4>Free delivery in London</h4>
                        <p>We will deliver within 1 hour</p>
                    </div> --}}
                </div>
            </div>
        </div>

        <div class="container f-menu-list">
            <div class="row">
                <div class="col-sm-3" >

                    <div class="ftr1" id="footer-set" style="text-align: center;">
                        <a href="/en/shop" >
                            <img src="/wealth-assets/img/wealth-india-logo.png" alt="AllStore - MultiConcept eCommerce Responsive HTML5 Template" style="width: 100px; height: auto">                
                            <p>At Wealth India we value getting results, honesty, teamwork, building exceptional products & services & most of all putting clients & partners first by offering the best services possible to them.</p>            
                        </a>
                    </div>

                </div>
                <div class="col-sm-3" >
                    <div class="ftr2" id="footer-set">
                        <h3>
                            About us
                        </h3>
                        <ul class="nav nav-pills nav-stacked">

                            <li><a href="/aboutw">About Us</a></li>
                            <li><a href="/contactw">Contact-Us</a></li>
                            <li><a href="/terms&conditon">Terms & Conditions </a></li>
                            <li><a href="/privacy-policyw">Privacy Policy</a></li>
                            <li><a href="/return-policyw">Return Policy</a></li>
                            {{-- <li><a href="/blogw">FAQ</a></li> --}}
                            {{-- <li><a href="/contactw">Contacts</a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3" >
                    <div class="ftr2" id="footer-set">
                        <h3>
                            Shop
                        </h3>
                        <ul class="nav nav-pills nav-stacked">
                            @if(!empty($main_categories))
                            <?php $i=0; ?>
                            @foreach($main_categories as $main)
                            @if($i++<=5)
                            <li><a href="/categorywise-productw/{{$main->slug_name}}">{{strtoupper($main->Mcategory_name)}}</a></li>
                            @endif
                            @endforeach
                            @endif

                        </ul>
                    </div>
                </div>
                <div class="col-sm-3" >
                    <div class="ftr2" id="footer-set">
                        <h3>
                            Address :
                        </h3>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a>WZ-31, New Delhi-110045</a></li>
                        </ul>
                        <h3>
                            Email :
                        </h3>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="mailto:info@wealthindia.global" target="blank">info@wealthindia.global</a></li>
                        </ul>
                        <h3>
                            Contact :
                        </h3>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="tel:18001038581">18001038581</li>
                            </ul>
                        </div>
                    </div>
                {{-- <div class="f-menu">
                    <h3>
                        Pages
                    </h3>
                    <ul class="nav nav-pills nav-stacked">
                        <li><a href="/contactw">About us</a></li>
                        <li><a href="/contactw">Delivery</a></li>
                        <li><a href="/contactw">Guarantees</a></li>
                        <li><a href="/contactw">Contacts</a></li>
                        <li><a href="/404w">Page 404</a></li>
                    </ul>
                </div> --}}
                
            </div>
        </div>

        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <ul class="social-icons nav navbar-nav">
                        <li>
                            <a href="http://facebook.com/" rel="nofollow" target="_blank">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="http://google.com/" rel="nofollow" target="_blank">
                                <i class="fa fa-google-plus"></i>
                            </a>
                        </li>
                        <li>
                            <a href="http://twitter.com/" rel="nofollow" target="_blank">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="http://vk.com/" rel="nofollow" target="_blank">
                                <i class="fa fa-vk"></i>
                            </a>
                        </li>
                        <li>
                            <a href="http://instagram.com/" rel="nofollow" target="_blank">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                    </ul>
                    <div class="footer-copyright">
                        <i><a href="https://www.backstagesupporters.companyinfo" target="blank">Backstagesupporters.com</a></i> © Copyright 2019
                    </div>
                </div>
            </div>
        </div>

    </footer>
    <!-- Footer - end -->


    <!-- jQuery plugins/scripts - start -->
    <script src="/new-asset/js/jquery-1.11.2.min.js"></script>
    <script src="/new-asset/js/jquery.bxslider.min.js"></script>
    <script src="/new-asset/js/fancybox/fancybox.js"></script>
    <script src="/new-asset/js/fancybox/helpers/jquery.fancybox-thumbs.js"></script>
    <script src="/new-asset/js/jquery.flexslider-min.js"></script>
    <script src="/new-asset/js/swiper.jquery.min.js"></script>
    <script src="/new-asset/js/jquery.waypoints.min.js"></script>
    <script src="/new-asset/js/progressbar.min.js"></script>
    <script src="/new-asset/js/ion.rangeSlider.min.js"></script>
    <script src="/new-asset/js/chosen.jquery.min.js"></script>
    <script src="/new-asset/js/jQuery.Brazzers-Carousel.js"></script>
    <script src="/new-asset/js/plugins.js"></script>
    <script src="/new-asset/js/main.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDhAYvx0GmLyN5hlf6Uv_e9pPvUT3YpozE"></script>
    <script src="/new-asset/js/gmap.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.js"></script>
    <!-- jQuery plugins/scripts - end -->

    <script>
        function getWishlist(){
            var user_id = $('#u_id').val();
            if(user_id == 0){
                Swal("Please Login First");
            }
            else{
                window.location.href = '/wishlistw';
            }

        }

        function getCart(){
            var user_id = $('#u_id').val();
            if(user_id == 0){
                Swal("Please Login First");
            }
            else{
                window.location.href = '/cartw';
            }
        }

        function searchValidate(){
            var key = $('#product-keyword').val();
            if(key == ""){
                Swal("First Enter your search.");
            }
            else{
                window.location.href = '/searched-productsw';
            }
        }


    </script>

    @yield('script')

</body>
</html>