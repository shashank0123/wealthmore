<?php

use App\MainCategory;
use App\Product;
use App\Review;
use App\Models\Member;
use App\Models\MemberDetail;
use App\City;
use App\User;
use App\State;
$total=0;
$i=0;
$states = state::all();

if(!empty($member->id)){    
    $memberDetail = MemberDetail::where('member_id',$member->id)->first();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>WealthMore</title>
       
    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/wealth-assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="shortcut icon" href="/wealth-assets/img/favicon.png">

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <!-- CSS Global -->
    <link href="/wealth-assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/wealth-assets/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="/wealth-assets/plugins/fontawesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="/wealth-assets/plugins/prettyphoto/css/prettyPhoto.css" rel="stylesheet">
    <link href="/wealth-assets/plugins/owl-carousel2/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="/wealth-assets/plugins/owl-carousel2/assets/owl.theme.default.min.css" rel="stylesheet">
    <link href="/wealth-assets/plugins/animate/animate.min.css" rel="stylesheet">
    <link href="/wealth-assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
        <link href="/wealth-assets/plugins/countdown/jquery.countdown.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="/wealth-assets/css/theme.css" rel="stylesheet">
    <link href="/wealth-assets/css/theme-green-1.css" rel="stylesheet" id="theme-config-link">

    <!-- Head Libs -->
    <script src="/wealth-assets/plugins/modernizr.custom.js"></script>

        <!--[if lt IE 9]>
        <script src="/wealth-assets/plugins/iesupport/html5shiv.js"></script>
        <script src="/wealth-assets/plugins/iesupport/respond.min.js"></script>
    <![endif]-->
    <style>
        .top-bar { background-color: #394263 !important;}
        .sf-menu li.active { background-color: #394263; }
        .sign-color { color: #000 !important; font-weight: bold; }

        /*.theme-config-wrap { display: none !important; }*/

        #enquirypopup .modal-dialog {
            width: 40% !important;
            padding: 0px;
            position: relative;
        }

        #confirmationpopup .modal-dialog {
            width: 30% !important;
            padding: 10px;
            position: relative;
        }

        #enquirypopup .modal-dialog {
            width: 400px;
            padding: 0px ;
            position: relative;
        }
        #enquirypopup .modal-dialog:before {
            content: '';
            height: 0px;
            width: 0px;
            border-left: 50px solid #394263;
            border-right: 50px solid transparent;
            border-bottom: 50px solid transparent;
            position: absolute;
            top: 1px;
            left: -14px;
            z-index: 99;
        }

        .custom-modal-header {
            text-align: center;
            color: #394263;
            text-transform: uppercase;
            letter-spacing: 2px;
            border-top: 4px solid;
        }

        #enquirypopup .modal-dialog .close {
            z-index: 99999999;
            color: white;
            text-shadow: 0px 0px 0px;
            font-weight: normal;
            top: 4px;
            right: 6px;
            position: absolute;
            opacity: 1;
        }

        .custom-modal-header .modal-title {
            /* font-weight: bold; */
            font-size: 18px;
        }

        #enquirypopup .modal-dialog:after {
            content: '';
            height: 0px;
            width: 0px;
            /* border-right: 50px solid rgba(255, 0, 0, 0.98); */
            border-right: 50px solid #394263;
            border-bottom: 50px solid transparent;
            position: absolute;
            top: 1px;
            right: -14px;
            z-index: 999999;
        }

        .form-group {
            margin-bottom: 5px !important;
        }

        .form-inline .form-control, .form-group  {
            display: inline-block;
            width: 100%;
            vertical-align: middle;
        }
        
    </style>
</head>
<body id="home" class="wide" >
    <!-- PRELOADER -->
    <div id="preloader">
        <div id="preloader-status">
            <div class="spinner">
                <div class="rect1"></div>
                <div class="rect2"></div>
                <div class="rect3"></div>
                <div class="rect4"></div>
                <div class="rect5"></div>
            </div>
            <div id="preloader-title">Loading</div>
        </div>
    </div>
    <!-- /PRELOADER -->

    <!-- WRAPPER -->
    <div class="wrapper">


        <div id="enquirypopup" class="modal fade in" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content row">
                    <form name="info_form" class="form-inline" action="#" method="GET" id="info_form">
                        <div class="modal-header custom-modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4 class="modal-title">Personal Information</h4>
                        </div>
                        <div class="modal-body">
                            <p style="text-align: center;">First Fill Your Details.</p>
                            {{-- //Readonly data --}}
                            <input type="hidden" name="user_id" value="<?php if(!empty($member->id)){ echo $member->id; } ?>">
                            <div class="col-md-12">
                                <label for="username">User Name</label>
                                <div class="form-group"><input class="form-control" type="text" placeholder="Full Name" value="<?php if(!empty($user->username)) {echo $user->username;} ?>" name="username" readonly></div>
                            </div>
                            <div class="col-md-12">
                                <label for="email">Email</label>
                                <div class="form-group"><input class="form-control" type="text" placeholder="Email" name="email" value="<?php if(!empty($user->email)) {echo $user->email;} ?>" readonly></div>
                            </div>
                            <div class="col-md-12">
                                <label for="pan">PAN Number</label>
                                <div class="form-group"><input class="form-control" type="text" placeholder="PAN Card" name="pan" value="<?php if(!empty($memberDetail->pan)) {echo $memberDetail->pan;} ?>" <?php if(!empty($memberDetail->mobile_phone)) {echo 'readony';}?>></div>
                            </div>
                            <div class="col-md-12">
                                <label for="phone">Phone</label>
                                <div class="form-group"><input class="form-control" type="text" placeholder="Phone Number" name="mobile_phone" value="<?php if(!empty($memberDetail->mobile_phone)) {echo $memberDetail->mobile_phone;} ?>" <?php if(!empty($memberDetail->mobile_phone)) {echo 'readony';}?>></div>
                            </div>

                            {{-- Fillable Fields --}}

                            {{-- <div class="col-md-12">
                                <div class="form-group"><input class="form-control" type="text" placeholder="Referal ID" name="referal_id" value="Referal ID" required="required"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group"><input class="form-control" type="text" placeholder="Placement ID" name="placement" value="Placement ID"></div>
                            </div> --}}
                            <div class="col-md-12">
                                <label for="date_of_birth">Date of Birth</label>
                                <div class="form-group"><input class="form-control" type="date" placeholder="date_of_birth" value="" name="date_of_birth" required="required"></div>
                            </div>
                            <div class="col-md-12">
                                <label for="gender">Gender</label>
                                <div class="form-group">
                                    <select class="selectpicker input-price" data-width="100%" name="gender" required="required">
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>              
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <label for="adhaar">Adhaar</label>
                                <div class="form-group"><input class="form-control" type="text" placeholder="Adhaar Number" name="adhaar" value="" required="required"></div>
                            </div>

                            <div class="col-md-12">
                                <label for="address">Address</label>
                                <div class="form-group"><input class="form-control" type="text" placeholder="Address" name="address" value="" required="required"></div>
                            </div>
                            {{-- <div class="col-md-12">
                                <label for="state">Country</label>
                                <div class="form-group"><select class="selectpicker input-price" data-width="100%" name="state" id="state" required="required">
                                    @foreach($states as $state)
                                    <option  value="{{$state->id}}"> {{$state->state}} </option>
                                    @endforeach
                                </select></div>
                            </div> --}}
                            <div class="col-md-12">
                                <label for="state">State</label>
                                <div class="form-group"><select class="selectpicker input-price" data-width="100%" name="state" id="state" required="required">
                                    @foreach($states as $state)
                                    <option  value="{{$state->id}}"> {{$state->state}} </option>
                                    @endforeach
                                </select></div>
                            </div>
                            <?php $cities = City::all(); ?>
                            <div class="col-md-12">
                                <label for="city">City</label>
                                <div class="form-group"><select class="selectpicker input-price" name="city" data-width="100%" id="city" required="required">
                                    @if(!empty($cities))
                                    @foreach($cities as $city)
                                    <option  value="{{$city->id}}"> {{$city->city}} </option>
                                    @endforeach
                                    @endif
                                </select></div>
                            </div>

                            <div class="col-md-12">
                                <label for="username">PIN Code</label>
                                <div class="form-group"><input class="form-control" id="state" type="text" placeholder="Pin Code" name="pin_code" value="" required="required"></div>
                            </div>

                            <div class="col-md-12">
                                <label for="beneficiary_name">Beneficiary Name</label>
                                <div class="form-group"><input class="form-control" id="beneficiary_name" type="text" placeholder="Beneficiary Name" name="beneficiary_name" value="" required="required"></div>
                            </div>

                            <div class="col-md-12">
                                <label for="relation_with_beneficiary">Relation with Beneficiary</label>
                                <div class="form-group"><input class="form-control" id="relation_with_beneficiary" type="text" placeholder="Relation with Beneficiary" name="relation_with_beneficiary" value="" required="required"></div>
                            </div>
{{-- 
                            <div class="form-group col-sm-12">
                                <button type="button" onclick="showConfirmation();" class="btn btn-theme pull-right">Submit</button>
                            </div> --}}


                            <h4 class="modal-title" style="text-align: center;"><br><br>Bank Details</h4>
                            {{-- //Readonly data --}}
                            <div class="col-md-12">
                                <label for="bank_name">Bank Name</label>
                                <div class="form-group"><input class="form-control" type="text" placeholder="Bank Name" value="" id="bank_name" name="bank_name"></div>
                            </div>
                            <div class="col-md-12">
                                <label for="bank_account_number">Bank Account Number</label>
                                <div class="form-group"><input class="form-control" type="text" placeholder="Bank Account Number" value="" id="bank_account_number" name="bank_account_number"></div>
                            </div>
                            <div class="col-md-12">
                                <label for="bank_account_holder">Bank Account Holder</label>
                                <div class="form-group"><input class="form-control" type="text" placeholder="Bank Account Holder" value="" id="bank_account_holder" name="bank_account_holder"></div>
                            </div>
                            <div class="col-md-12">
                                <label for="bank_address">Bank Address</label>
                                <div class="form-group"><input class="form-control" type="text" placeholder="Bank Address" value="" id="bank_address" name="bank_address"></div>
                            </div>
                            <div class="col-md-12">
                                <label for="bank_branch">Bank Branch</label>
                                <div class="form-group"><input class="form-control" type="text" placeholder="Bank Branch" value="" id="bank_branch" name="bank_branch"></div>
                            </div>
                            <div class="col-md-12">
                                <label for="ifsc">IFSC Code</label>
                                <div class="form-group"><input class="form-control" type="text" placeholder="IFSC Code" value="" id="ifsc_code" name="ifsc"></div>
                            </div>

                            {{-- Fillable Fields --}}

                            {{-- <div class="col-md-12">
                                <div class="form-group"><input class="form-control" type="text" placeholder="Referal ID" name="referal_id" value="Referal ID" required="required"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group"><input class="form-control" type="text" placeholder="Placement ID" name="placement" value="Placement ID"></div>
                            </div> --}}
                            

                            <div class="form-group col-sm-12">
                                <button type="button" onclick="showConfirmation();" class="btn btn-theme pull-right">Submit</button>
                            </div>

                        </div>                 
                    </div>
                </form>
            </div>
        </div>

        <div id="confirmationpopup" class="modal fade in" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content row">
                    <div class="modal-header custom-modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Confirmation</h4>
                    </div>
                    <div class="modal-body" style="text-align: center;">
                        <h4>Once you submit your form, then you cannot change your info.</h4><br/>
                        <p>Do you want to continue... ?</p>
                        <div class="form-group col-sm-12">
                            <button type="button" onclick="cancelSubmit()" class="btn btn-theme pull-left btn-theme-dark">Cancel</button>
                            <button type="button" onclick="submitInfoForm()" class="btn btn-theme pull-right">OK</button>
                        </div>
                    </div>                     
                </div>
            </div>
        </div>

        <!-- Popup: Shopping cart items -->
        <div class="modal fade popup-cart" id="popup-cart" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="container">
                    <div class="cart-items">
                        <div class="cart-items-inner" id=showminicart>
                            @if(session()->get('cart') != null)
                            <?php $i=0; ?>
                            @foreach($cartproducts as $product)
                            <div class="media">
                                <a class="pull-left" href="/productdetail/{{$product->id}}"><img class="media-object item-image" src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}" alt="{{$product->name}}" style="width: 70px; height: 70px"></a>
                                <p class="pull-right item-price">Rs. {{$product->sell_price}}</p>
                                <div class="media-body">
                                    <h4 class="media-heading item-title"><a href="/productdetail/{{$product->id}}">{{$quantity[$i]}}x {{$product->name}}</a></h4>
                                    <p class="item-desc">Lorem ipsum dolor</p>
                                </div>
                            </div>
                            <?php $i++; ?>
                            @endforeach
                            @endif
                            <div class="media">
                                <p class="pull-right item-price" id="bill"> Rs. </p>
                                <div class="media-body">
                                    <h4 class="media-heading item-title summary">Subtotal</h4>
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-body">
                                    <div>
                                        <a href="#" class="btn btn-theme btn-theme-dark" data-dismiss="modal">Close</a>
                                        <a href="<?php if(!empty($member->id)){ echo '/cart'; } else { echo '/en/login';} ?>" class="btn btn-theme btn-theme-transparent btn-call-checkout">Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Popup: Shopping cart items -->

        <!-- Header top bar -->
        <div class="top-bar">
            <div class="container">
                <div class="top-bar-left">
                    <ul class="list-inline">
                        @if(empty($member->id))
                        <li class="icon-user"><a href="/en/login"><img src="/wealth-assets/img/icon-1.png" alt=""/> <span>Login</span></a></li>
                        <li class="icon-form"><a href="/register"><img src="/wealth-assets/img/icon-2.png" alt=""/> <span>Not a Member? <span class="sign-color">Sign Up</span></span></a></li>
                        @endif
                        <li><a href="mailto:info@backstagesupporters.com"><i class="fa fa-envelope"></i> <span>info@backstagesupporters.com</span></a></li>
                    </ul>
                </div>
                <div class="top-bar-right">
                    <ul class="list-inline">

                       @if(empty($member->id))
                       <li class="hidden-xs"><a href="/about">About</a></li>
                       <li class="hidden-xs"><a href="/contact-us">Contact</a></li>
                       <li class="hidden-xs"><a href="/faq">FAQ</a></li>
                       @else                           
                       <li class="hidden-xs"><a href="/en/member">My Account</a></li>
                       <li class="hidden-xs"><a href="/wishlist">My Wishlist</a></li>
                       @endif

                       @if(empty($member->id))

                       @else
                       <li class="hidden-xs"><a href="/en/logout">Logout</a></li>
                       @endif

                   </ul>
               </div>
           </div>
       </div>
       <!-- /Header top bar -->

       <!-- HEADER -->
       <header class="header fixed">
        <div class="header-wrapper">
            <div class="container">

                <!-- Logo -->
                <div class="logo">
                    <a href="/"><img src="/wealth-assets/img/logo-bella-shop.png" alt="WealthMore" style="width: 235px; height: 45px; top: -10px; position: absolute;" /></a>
                </div>
                <!-- /Logo -->

                <!-- Header search -->
                <form action="/searchproduct" method="GET" class="search-form">
                    <div class="header-search">
                        <input class="form-control" type="text" name="product_keyword" placeholder="What are you looking?"/>
                        <button type="submit" name="search"><i class="fa fa-search" style="margin-top: 7px"></i></button>
                    </div>
                </form>
                <!-- /Header search -->

                <!-- Header shopping cart -->
                <div class="header-cart">
                    <div class="cart-wrapper">
                        <a href="/wishlist" class="btn btn-theme-transparent hidden-xs hidden-sm"><i class="fa fa-heart"></i></a>
                        {{-- <a href="compare-products.html" class="btn btn-theme-transparent hidden-xs hidden-sm"><i class="fa fa-exchange"></i></a> --}}
                        <a href="#" class="btn btn-theme-transparent" data-toggle="modal" data-target="#popup-cart" onclick="showCart()"><i class="fa fa-shopping-cart"></i> 
                            <?php
                            $a=0;
                            
                                                 
                                $show=session()->get('count');
                                ?>

                                @if(session()->get('count') != null)
                                <span class="hidden-xs" id="show-total">{{$show}}</span>
                                @else
                                <span class="hidden-xs" id="show-total">0</span>
                                @endif
                            </span><span>item(s) </span> <i class="fa fa-angle-down"></i></a>
                            <!-- Mobile menu toggle button -->
                            <a href="#" class="menu-toggle btn btn-theme-transparent"><i class="fa fa-bars"></i></a>
                            <!-- /Mobile menu toggle button -->
                        </div>
                    </div>
                    <!-- Header shopping cart -->

                </div>
            </div>
            <div class="navigation-wrapper">
                <div class="container">
                    <!-- Navigation -->
                    <nav class="navigation closed clearfix">
                        <a href="#" class="menu-toggle-close btn"><i class="fa fa-times"></i></a>
                        <ul class="nav sf-menu">

                            <li class="active"><a href="/en/shop">Home</a></li>

                            @foreach($maincategory as $row)                               

                            <li class="megamenu"><a href="/products/{{$row->id}}">{{$row->Mcategory_name}}</a>
                                <ul>
                                    <li class="row">
                                        <?php 
                                        $category = MainCategory::where('category_id',$row->id)->where('status','Active')->get();
                                        ?>
                                        @foreach($category as $rows)
                                        <div class="col-md-3">
                                            <h4 class="block-title"><a href="/products/{{$rows->id}}"><span>{{$rows->Mcategory_name}}</span></a></h4>
                                            <ul>
                                                <?php
                                                $subcategory = MainCategory::where('category_id',$rows->id)->where('status','Active')->get();
                                                ?>
                                                @foreach($subcategory as $col)
                                                <li><a href="/products/{{$col->id}}">{{$col->Mcategory_name}}</a></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endforeach                                   


                                    </li>
                                </ul>
                            </li>
                            @endforeach

                        </ul>
                    </nav>
                    <!-- /Navigation -->
                </div>
            </div>
        </header>
        <!-- /HEADER -->



        @yield('content')



        <!-- FOOTER -->
        <footer class="footer">
            <div class="footer-widgets">
                <div class="container">
                    <div class="row">

                        <div class="col-md-3">
                            <div class="widget">
                                <h4 class="widget-title">About Us</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sollicitudin ultrices suscipit. Sed commodo vel mauris vel dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                <ul class="social-icons">
                                    <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="widget">
                                <h4 class="widget-title">News Letter</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                <form action="/newsletter" method="POST" id="newsletter">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="email" placeholder="Enter Your Mail "/>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" onclick="submitNews()" class="btn btn-theme btn-theme-transparent">Subscribe</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="widget widget-categories">
                                <h4 class="widget-title">Information</h4>
                                <ul>
                                    <li><a href="/about">About Us</a></li>
                                    <li><a href="#">Delivery Information</a></li>
                                    <li><a href="/contact-us">Contact Us</a></li>
                                    <li><a href="/tnc">Terms and Conditions</a></li>
                                    <li><a href="/privacypolicy">Private Policy</a></li>
                                </ul>
                            </div>
                        </div>
                        <?php
                        $subcat = array();
                        $subid = array();
                        $sub_i = 0;
                        $maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
                        foreach($maincategory as $main){                            
                            $category = MainCategory::where('category_id',$main->id)->where('status','Active')->get();

                            if($category != null){
                                foreach($category as $cat){
                                    $subcategory = MainCategory::where('category_id',$cat->id)->where('status','Active')->get();
                                    if($subcategory != null){
                                        foreach($subcategory as $sub){
                                            $subcat[$sub_i] = $sub->Mcategory_name;
                                            $subid[$sub_i] = $sub->id;
                                            $sub_i++;
                                        }
                                    }
                                }
                            }
                        }



                        ?>
                        <div class="col-md-3">
                            <div class="widget widget-tag-cloud">
                                <h4 class="widget-title">Item Tags</h4>
                                <ul>
                                    @if($subcat != null)
                                    @for($j=0 ; $j<10 ; $j++)                                    
                                    <li><a href="/products/{{$subid[$j]}}">{{$subcat[$j]}}</a></li>
                                    @endfor
                                    @endif                                    
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="footer-meta">
                <div class="container">
                    <div class="row">

                        <div class="col-sm-6">
                            <div class="copyright">Copyright 2019 Backstage Supporters  |   All Rights Reserved   |   Designed By Me</div>
                        </div>
                        <div class="col-sm-6">
                            <div class="payments">
                                <ul>
                                    <li><img src="/wealth-assets/img/preview/payments/visa.jpg" alt=""/></li>
                                    <li><img src="/wealth-assets/img/preview/payments/mastercard.jpg" alt=""/></li>
                                    <li><img src="/wealth-assets/img/preview/payments/paypal.jpg" alt=""/></li>
                                    <li><img src="/wealth-assets/img/preview/payments/american-express.jpg" alt=""/></li>
                                    <li><img src="/wealth-assets/img/preview/payments/visa-electron.jpg" alt=""/></li>
                                    <li><img src="/wealth-assets/img/preview/payments/maestro.jpg" alt=""/></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </footer>
        <!-- /FOOTER -->

        <div id="to-top" class="to-top"><i class="fa fa-angle-up"></i></div>


    </div>
    <!-- /WRAPPER -->


    <!-- JS Global -->
    <script src="/wealth-assets/plugins/jquery/jquery-1.11.1.min.js"></script>
    <script src="/wealth-assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="/wealth-assets/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
    <script src="/wealth-assets/plugins/superfish/js/superfish.min.js"></script>
    <script src="/wealth-assets/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script>
    <script src="/wealth-assets/plugins/owl-carousel2/owl.carousel.min.js"></script>
    <script src="/wealth-assets/plugins/jquery.sticky.min.js"></script>
    <script src="/wealth-assets/plugins/jquery.easing.min.js"></script>
    <script src="/wealth-assets/plugins/jquery.smoothscroll.min.js"></script>
    <script src="/wealth-assets/plugins/smooth-scrollbar.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.js"></script>

    
    <!-- JS Page Level -->
    <script src="/wealth-assets/plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="/wealth-assets/plugins/countdown/jquery.plugin.min.js"></script>
    <script src="/wealth-assets/plugins/countdown/jquery.countdown.min.js"></script>
    <script src="/wealth-assets/js/theme.js"></script>

    <!--[if (gte IE 9)|!(IE)]><!-->
    <script src="/wealth-assets/plugins/jquery.cookie.js"></script>
    <script src="/wealth-assets/js/theme-config.js"></script>
    <!--<![endif]-->
    <script>
        function showCart(){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                /* the route pointing to the post function */
                url: '/cart/show',
                type: 'POST',
                /* send the csrf-token and the input to the controller */                   
                data: {_token: CSRF_TOKEN},
                success: function (data) {          
                    $('#showminicart').html(data);
                    $("#bill").html(data.bill);         
                }
            }); 
        }

        function deleteProduct(id){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');          
                // var std = $("#show-total").html();
                // count_product--;         
                $('#listhide'+id).hide();
                $('#cart'+id).hide();
                $.ajax({
                    /* the route pointing to the post function */
                    url: '/cart/delete-product/id',
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */               
                    data: {_token: CSRF_TOKEN, id: id},
                    success: function (data) { 
                        $("#show-total").html(data.showcount);          
                        $("#bill").html(data.bill);         
                    }
                }); 
            }


            function submitNews(){
                var request = $.ajax({
                    type: "POST",
                    url: "/newsletter",
                    data: $('#newsletter').serialize(),

                });
                request.done(function(data) {
                    if (data.message == 'Thank you .We will reply you soon'){
                      console.log(data);
                      Swal(data.message);             
                  }
                  else
                      Swal(data.message);                  
              });

                request.fail(function(data) {
                    Swal(data.message);
                });
            }




            $(document).ready(function() {
                $('#state_id').on('change', function() {
                    var stateID = $(this).val();
                    if(stateID) {
                        $.ajax({
                            url: 'city/'+stateID,
                            type: "GET",
                            data : {"_token":"{{ csrf_token() }}",state_id:stateID},
                            dataType: "json",
                            success:function(data) {                       
                                if(data){

                                    $('#city_id').empty();
                                    $('#city_id').focus;
                                    $('#city_id').append('<option value="">-- Select City --</option>'); 
                                    $.each(data, function(key, value){
                                        $('select[name="city_id"]').append('<option value="'+ value.id +'">' + value.city+ '</option>');
                                    });
                                }else{
                                    $('#city_id').empty();
                                }
                            }
                        });
                    }else{
                        $('#city_id').empty();
                    }
                });
            });


            
            // @if(!empty($member->id)){
            //     @if($memberDetail->mobile_phone=="" || $memberDetail->bank_name=="" || $memberDetail->gender=="" || $memberDetail->pan=="" || $memberDetail->bank_account_number=="" || $memberDetail->bank_account_holder=="" || $memberDetail->bank_branch=="" || $memberDetail->date_of_birth=="" || $memberDetail->beneficiary_name=="" || $memberDetail->address=="" || $memberDetail->bank_address==""|| $memberDetail->adhaar==""|| $memberDetail->ifsc==""|| $memberDetail->relation_with_beneficiary==""){
            //         $(window).load(function(){
            //             setTimeout(function() {
            //                 $('#enquirypopup').modal('show');
            //             }, 1000);
            //         });
            //     }
            //     @endif                
            // }
            // @endif    

            // function showConfirmation(){
            //     $('#enquirypopup').hide();
            //     $('#confirmationpopup').show();
            // }

            // function cancelSubmit(){
            //     $('#confirmationpopup').hide();
            //     $('#enquirypopup').show();                
            // }

            function submitInfoForm()
            {
                var request = $.ajax({
                    type: "GET",
                    url: "/updateMemberInfo",
                    data: $('#info_form').serialize(),      
                });
                request.done(function(msg) {
                    if (msg.message == 'Data Updated Successfully'){
                      console.log(msg);
                      Swal('Account Updated Successfully');
                      setTimeout(function(){window.location.href = "/en/shop";},3000);      
                  }
                  else{
                      Swal("Oops!!! Something Went Wrong");
                  }
              });

                request.fail(function(msg) {
                    Swal(msg.message);
                });
            }
        </script>

        @yield('script')
    </body>
    </html>