<!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7"><![endif]-->
<!--[if IE 8]><html class="ie ie8"><![endif]-->
<!--[if IE 9]><html class="ie ie9"><![endif]-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="apple-touch-icon.png" rel="apple-touch-icon">
    <link href="/assets/img/favicon.png" rel="icon">
    <meta name="author" content="Nghia Minh Luong">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>FGrowth - Homepage</title>
    <!-- Fonts--><link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CLibre+Baskerville:400,700" rel="stylesheet">
    <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="plugins/ps-icon/style.css">
    <!-- CSS Library-->
    <link rel="stylesheet" href="plugins/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="plugins/owl-carousel/assets/owl.carousel.css">
    <link rel="stylesheet" href="plugins/bootstrap-select/dist/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="plugins/jquery-bar-rating/dist/themes/fontawesome-stars.css">
    <link rel="stylesheet" href="plugins/slick/slick/slick.css">
    <link rel="stylesheet" href="plugins/lightGallery-master/dist/css/lightgallery.min.css">
    <!-- Custom-->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="plugins/revolution/css/settings.css">
    <link rel="stylesheet" href="plugins/revolution/css/layers.css">
    <link rel="stylesheet" href="plugins/revolution/css/navigation.css">
    <style type="text/css">
      body {
        /*background: url(../../img/login.jpg) no-repeat center center fixed;*/
      }
    </style>
    <!--HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries-->
    <!--WARNING: Respond.js doesn't work if you view the page via file://-->
    <!--[if lt IE 9]><script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script><script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <!--[if IE 7]><body class="ie7 lt-ie8 lt-ie9 lt-ie10"><![endif]-->
  <!--[if IE 8]><body class="ie8 lt-ie9 lt-ie10"><![endif]-->
  <!--[if IE 9]><body class="ie9 lt-ie10"><![endif]-->
  <body class="ps-loading">
    <div class="header--sidebar">
      <form class="ps-form--search" action="do_action" method="post">
        <input class="form-control" type="text" placeholder="Search Product…">
        <button><i class="ps-icon-search"></i></button>
      </form>
    </div>
    
<style type="text/css">  

/*////////////////TOP NAV BAR////////////////*/
.top-bar{background-color:black;min-height:40px;padding-top:5px;padding-bottom: 0px;}
.top-bar .nav-text {
  color: #00BCD4;
  display: block;
  margin-top: 5px;
}
.top-bar .social{color:#FFF;display:inline-block;padding:5px;text-decoration: none;}

.top-bar .tools{margin:0px;padding:0px;list-style-type:none;}
.top-bar .tools li{list-style-type: none;display:inline-block;}
.top-bar .tools li a {
  display: block;
  text-decoration: none;
  color: #fff;
  padding-left: 15px;
  padding-top: 5px;
}
.navbar-brand {
  height: 50px;
  padding: 3px 15px;
  font-size: 18px;
  line-height: 20px;
  position: absolute;
  z-index: 11;
  margin: 0 auto;
  right: 47%;
}
.navbar-inverse {
    background-color: #ffeed5;
}
.pt-80 {
  padding-top: 32px;
}
.navbar {
  position: relative;
  min-height: 50px;
  margin-bottom: 0px;
  border: 0px solid #000;;
  border-radius:0px;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}

.navbar-inverse .navbar-nav > li > a {
  color: #671d32;
  font-weight: 600;
  font-size: 15px;
  padding-top: 22px;
}
.navbar-inverse .navbar-nav > li > a:hover{
   color: #ffa927;
}
.nav > li > a {
  position: relative;
  display: block;
  padding: 15px 11px;
    
  }

.form-control{
    border-color:#eaca99;
    border-radius:0px;
    background-color:#fff;
}

.btn-default-1 {
    color: #eaca99;
    background-color: #ffeed5;
    border-radius: 0px;
    font-size: 24px;
}

.cart-heart a{display:inline-block;color:#671d32;font-size:20px;padding:5px;}
.cart-heart a:hover{
  color: #ffa927;
}

@media screen and (max-width: 600px){
#logoimg{
  height: 45px !important;
}
.headeraccount{
  text-align: center !important;
}
.navbar-inverse {
  background-color: #fff;
  border-color: #080808;
}
.navbar-inverse .navbar-collapse, .navbar-inverse .navbar-form{
  border-color: #fff;
}
. {
  border-bottom: 1px solid #333;
}
.navbar-toggle  {
  background-color: #000;
}
.navbar-brand {
  left: 0% !important;
}
.nav li:hover {
  background-color: #ffeed5;
}
.navbar-header {
  background-color: #ffeed5;
}
.cart-heart a{
   color: #eaca99;
}
.cart-heart a:hover{
   color: #671d32;
}
.btn-default-1 {
  color: #671d32;
  background-color: transparent;
}
.navbar-inverse .navbar-nav > li > a {
  padding-top: 15px;
}
.cart-heart a{
  display: block;
  float: right;
}
}
</style>
 <nav class="top-bar">
    <div class="container">
      <div class="row">
        <div class="col-sm-4 ">
          <span class="nav-text">
            <i class="fa fa-phone" aria-hidden="true"></i> 9891721752 &nbsp;
            <i class="fa fa-envelope" aria-hidden="true"></i> info@feelgrowth.in
          </span>
        </div>
        <div class="col-sm-4 hidden-xs text-center">
          <a href="#" class="social"><i class="fa fa-facebook" aria-hidden="true"></i></a>
          <a href="#" class="social"><i class="fa fa-twitter" aria-hidden="true"></i></a>
          <a href="#" class="social"><i class="fa fa-instagram" aria-hidden="true"></i></a>
          <a href="#" class="social"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
          <a href="#" class="social"><i class="fa fa-google" aria-hidden="true"></i></a>
        </div>
        <div class="col-sm-4 text-right headeraccount">
          <ul class="tools">                                     
            <li class="dropdown">
              <a class="" href="login"><i class="fa fa-user" aria-hidden="true"></i> My Account</a>                  
            </li>                 
          </ul>
         </div>
        </div>
      </div>
    </nav> 
    <nav class="navbar navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>                        
          </button>
          <a class="navbar-brand" href="#"><img src="/wealth-assets/img/logo-bella-shop.png" style="height:60px;" id="logoimg"></a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav navbar-left">
            <li><a href="/about">About Us</a></li>
            <li><a href="">Products</a></li>
            <li><a href="">Legal</a></li>
            <li><a href="/contact">Contact Us</a></li>     
          </ul>
      <form class="navbar-form navbar-right">
      <div class="input-group">        
        <div class="input-group-btn">
          <button class="btn btn-default-1" type="submit">
            <i class="glyphicon glyphicon-search"></i>
          </button>
        </div>
        <input type="text" class="form-control" placeholder="Search">
      </div>      
      <span class="cart-heart  hidden-sm hidden-xs"> 
        <a href="login"><i class="fa fa-user" aria-hidden="true"></i></a>
        <a href="#"><i class="fa fa-cart-plus" aria-hidden="true"></i></a>
      </span> 
      <span class="cart-heart  hidden-md hidden-lg">          
        <a href="#"><i class="fa fa-heart" aria-hidden="true"></i></a>
        <a href="#"><i class="fa fa-cart-plus" aria-hidden="true"></i></a>
        <a href="#"><i class="fa fa-user" aria-hidden="true"></i></a>
      </span>   
     </form>     
    </div>
  </div>
</nav>

    <div class="ps-modal" id="subscribe-modal">
      <div class="ps-modal__content ps-subscribe--popup bg--cover" data-background="images/subscribe.jpg">
        <div class="ps-modal__remove"><i class="fa fa-remove"></i></div>
        <h3>subscribe email</h3>
        <p>Follow us & get<span>20% OFF</span>coupon for first purchase !!!!!</p>
        <form class="ps-form--subscribe-email" action="do_action" method="post">
          <div class="form-group">
            <div class="form-group__icon"><i class="fa fa-envelope"></i></div>
            <input class="form-control" type="text" placeholder="Type Your Email">
          </div>
          <button><i class="ps-icon-arrow-right"></i></button>
        </form>
      </div>
    </div>
		@yield('content')

		<footer class="ps-footer site-footer" style="background-color:  #ffa927";>
      <div class="ps-footer__content">
        <div class="ps-container">
          <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                  <div class="ps-site-info site-info"><a class="ps-logo" href="index"><img src="images/logo.png" alt=""></a>
                    <p>FGrowth is a global fashion destination for 20-somethings. We sell cutting-edge fashion and offer a wide variety of fashion-related content, making Xuper Store the hub of a thriving fashion community.</p>
                    <div class="ps-site-info__contact">
                      <h4>Contact Info</h4>
                      <span>For Any Complaint : <b style="color:#000000">9891721752</b>   <br>   For New Entry Call Us : <b style="color:#000000">info@feelgrowth.in</b></span>
                      <!-- <p>#</p> -->
                    </div>
                    <ul class="ps-social">
                      <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                      <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                      <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                    </ul>
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                  <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                          <div class="ps-widget ps-widget--footer widget widget_footer">
                            <h2 class="ps-widget__title">Contact Us</h2>
                            <ul class="ps-list--line">
                              <li><a class="text-uppercase" href="#">Payout related issue</a></li>
                              <li><a class="text-uppercase" href="#">Stock related issue</a></li>
                              <li><a class="text-uppercase" href="#">KYC related issue</a></li>
                              <li><a class="text-uppercase" href="#">About knowing plan</a></li>
                              <!-- <li><a class="text-uppercase" href="#">CHANEL</a></li> -->
                            </ul>
                          </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                          <div class="ps-widget ps-widget--footer widget widget_footer">
                            <h2 class="ps-widget__title">Customer Care</h2>
                            <ul class="ps-list--line">
                              <li><a href="#">My Account</a></li>
                              <li><a href="#">Track your Order</a></li>
                              <li><a href="#">Customer Service</a></li>
                              <li><a href="#">FAQs</a></li>
                              <li><a href="#">Product Support</a></li>
                            </ul>
                          </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                          <div class="ps-widget ps-widget--footer widget widget_footer">
                            <h2 class="ps-widget__title">Your Account</h2>
                            <ul class="ps-list--line">
                              <li><a href="#">Your account</a></li>
                              <li><a href="#">Newsletter</a></li>
                              <li><a href="#">Information</a></li>
                              <li><a href="#">Payment menthod</a></li>
                              <li><a href="#">Your wishlist</a></li>
                            </ul>
                          </div>
                        </div>
                  </div>
                </div>
          </div>
        </div>
      </div>
      <div class="ps-footer__copyright">
        <div class="ps-container">
          <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                  <p>&copy; Copyright by <span>FGrowth</span>.  Design by <a href="#">Backstage</a>.</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                  <div class="ps-footer__payment-methods"><a href="#"><img src="images/payment-method/paypal.png" alt=""></a><a href="#"><img src="images/payment-method/visa.png" alt=""></a><a href="#"><img src="images/payment-method/master-card.png" alt=""></a></div>
                </div>
          </div>
        </div>
      </div>
    </footer>
    <div class="ps-modal default" id="quickview">
      <div class="ps-modal__content">
        <div class="ps-modal__remove"><i class="fa fa-remove"></i></div>
        <div class="ps-product--detail">
          <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                  <div class="ps-product__thumbnail">
                    <div class="ps-product__preview">
                      <div class="ps-product__variants">
                        <div class="item"><img src="images/shoe-detail/1.jpg" alt=""></div>
                        <div class="item"><img src="images/shoe-detail/2.jpg" alt=""></div>
                        <div class="item"><img src="images/shoe-detail/3.jpg" alt=""></div>
                        <div class="item"><img src="images/shoe-detail/3.jpg" alt=""></div>
                        <div class="item"><img src="images/shoe-detail/3.jpg" alt=""></div>
                      </div>
                    </div>
                    <div class="ps-product__image">
                      <div class="item"><img src="images/shoe-detail/1.jpg" alt="" data-zoom-image="images/shoe-detail/1.jpg"></div>
                      <div class="item"><img src="images/shoe-detail/2.jpg" alt="" data-zoom-image="images/shoe-detail/2.jpg"></div>
                      <div class="item"><img src="images/shoe-detail/3.jpg" alt="" data-zoom-image="images/shoe-detail/3.jpg"></div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                  <div class="ps-product__info">
                    <div class="ps-product__rating">
                      <select class="ps-rating">
                        <option value="1">1</option>
                        <option value="1">2</option>
                        <option value="1">3</option>
                        <option value="1">4</option>
                        <option value="2">5</option>
                      </select>
                    </div>
                    <h1>Air strong  training</h1>
                    <p class="ps-product__category"><a href="#">  Men shoes</a>,<a href="#">  Nike</a>,<a href="#">  Jordan</a></p>
                    <h3 class="ps-product__price"><span>Rs. </span> 115 <del><span>Rs. </span> 330</del></h3>
                    <div class="ps-product__short-desc">
                      <p>The Nike Free RN 2017 Men's Running Shoe weighs less than previous versions and features an updated knit material…</p>
                    </div>
                    <div class="ps-product__block ps-product__style">
                      <h4>CHOOSE YOUR COLOR</h4>
                          <div class="ps-radio ps-radio--inline">
                            <input class="form-control" type="radio" id="radio-0" name="type-1">
                            <label for="radio-0"></label>
                          </div>
                          <div class="ps-radio ps-radio--inline ps-radio--color1">
                            <input class="form-control" type="radio" id="radio1" name="type-1">
                            <label for="radio1"></label>
                          </div>
                          <div class="ps-radio ps-radio--inline ps-radio--color2">
                            <input class="form-control" type="radio" id="radio2" name="type-1">
                            <label for="radio2"></label>
                          </div>
                          <div class="ps-radio ps-radio--inline ps-radio--color3">
                            <input class="form-control" type="radio" id="radio3" name="type-1">
                            <label for="radio3"></label>
                          </div>
                    </div>
                    <div class="ps-product__block ps-product__size">
                      <h4>CHOOSE SIZE & QUANTITY<a href="#">Size chart</a></h4>
                      <select class="ps-select selectpicker" title="Select Size">
                        <option value="6">7</option>
                      </select>
                      <div class="form-group ps-number">
                        <input class="form-control" type="text" value="1"><span class="up"></span><span class="down"></span>
                      </div>
                    </div>
                    <div class="ps-product__shopping"><a class="ps-btn" href="cart">Order Now</a>
                      <div class="ps-product__actions"><a class="mr-10" href="whishlist"><i class="ps-icon-heart"></i></a><a href="compare" title="Compare"><i class="ps-icon-compare"></i></a></div>
                    </div>
                  </div>
                </div>
          </div>
        </div>
      </div>
    </div>
    <!-- JS Library-->
    <script src="plugins/jquery/dist/jquery.min.js"></script>
    <script src="plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="plugins/owl-carousel/owl.carousel.min.js"></script>
    <script src="plugins/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="plugins/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
    <script src="plugins/imagesloaded.pkgd.js"></script>
    <script src="plugins/masonry.pkgd.min.js"></script>
    <script src="plugins/isotope.pkgd.min.js"></script>
    <script src="plugins/slick/slick/slick.min.js"></script>
    <script src="plugins/jquery.matchHeight-min.js"></script>
    <script src="plugins/gmap3.min.js"></script>
    <script src="plugins/lightGallery-master/dist/js/lightgallery-all.min.js"></script><script src="plugins/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="plugins/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="plugins/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="js/slider-1.js"></script>
    <!-- Custom scripts-->
    <script src="js/main.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDsUcTjt43mTheN9ruCsQVgBE-wgN6_AfY&amp;region=GB"></script>
  </body>
</html>