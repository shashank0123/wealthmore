<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>FGrowth - Bootstrap Crowdfunding Site</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="">

    <link href='http://fonts.googleapis.com/css?family=Patua+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,700,200,300' rel='stylesheet' type='text/css'>
    <link href="css/style.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/resp.css" rel="stylesheet">
    <link href="css/colorbox.css" rel="stylesheet">

    <link rel="shortcut icon" href="/assets/img/favicon.png" />
    <link rel="apple-touch-icon" href="img/touch-icon.png" />
    <link rel="image_src" href="img/touch-icon.png" />

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!--[if IE 8]>
    <style type="text/css">
        input[type="checkbox"], input[type="radio"] {
            display: inline;
        }
        input[type="checkbox"] + label, input[type="radio"] + label{
            display: inline;
            background: none;
            margin-bottom: 15px;
        }
        .zone {
            background-color: #727272;
            filter: alpha(Opacity=70);
        }
        .black {
            background-color: #727272;
            filter: alpha(Opacity=70);
        }
    </style>
    <![endif]-->

    <script src="js/jquery.js"></script>
    <script>
        /* <![CDATA[ */
        $(window).scroll(function fade() {
            if ($(window).scrollTop() > 400) {
                $("#navtop").css("display", "block");
            }
            else {
                $("#navtop").css("display", "none");
            }
        });
        /* ]]> */

    </script>

</head>

<body>
<div id="header" class="header">
    <a name="id_home"></a>
        <div class="width titul">
            <div class="reg inline">
                <style>
                    #login-link{
                        z-index: 1000;
                    }
                    .bannerformmodal{
                        /*z-index: -1;*/
                        display: none;
                    }
                </style>
                <a href="login" id="login-link" class="bleft inline login login-link"><div class="">Login</div></a>
                <div class="bleft bright inline login"><a href="" data-toggle="modal" id="reg-link" class="login-link" data-target=".bannerformmodal">Register</a></div>

                    <div class="modal fade bannerformmodal" tabindex="-1" role="dialog" aria-labelledby="bannerformmodal" aria-hidden="true">
                      <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <!-- <h4 class="modal-title" id="myModalLabel">Contact Form</h4> -->
                            </div>
                            <div class="modal-body">

                                <div class="form-group">
                                  <div class="input-group">                               
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <button type="button" class="btn " style="margin-left: 30px"><a href="/payment_status"> Buy a token </a></button>

                                    <div type="button" class="btn " style="margin-left: 50px" ><a href="register"  id="login-link" class="login-link"> Already have a token</a></div>

                                    <button type="button" class="btn " style="margin-left: 50px" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" data-dismiss="modal">Close</button>
                                  </div>
                                </div>
                            </div>                                
                          </div>
                        </div>
                      </div>
                    </div>
            </div>
           {{--  <div class="modal fade packagemodel" tabindex="-1" role="dialog" aria-labelledby="packagemodel" aria-hidden="true">
                      <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <!-- <h4 class="modal-title" id="myModalLabel">Contact Form</h4> -->
                            </div>
                            <div class="modal-body">
                                    <table class="table table-bordered">
                                        <tr><th>$25</th><th>6 Month</th><th>Action</th></tr>
                                        <tr><td>$25</td><td>6 Month</td><td><form action="/makepaymentfortoken"><input type="hidden" name="amount" value="1875"><input type="text" name="name"><input type="submit" name="submit" value="submit"></form></td></tr>
                                    </table>
                                
                            </div>
                                
                          </div>
                        </div>
                      </div>
                    </div>
 --}}
            {{-- <div class="social inline fright">
                <ul id="social">
                <li class="bleft inline">
                    <ul style="margin-left: 10px">
                        <li><a href="https://www.facebook.com" target="_blank"  original-title="twitter"><img src="img/social/icon_header_face.png" alt=""></a></li>
                        <li><a href="https://plus.google.com" target="_blank"  original-title="googleplus"><img src="img/social/icon_header_g.png" alt=""></a></li>
                        <li><a href="https://twitter.com/" title="facebook" target="_blank" ><img src="img/social/icon_header_tw.png" alt=""></a></li>
                        <li><a href="http://www.linkedin.com/" target="_blank" original-title="linkedin"><img src="img/social/icon_header_in.png" alt=""></a></li>
                        <li><a href="johndoe@domainname.com" target="_self" original-title="mail"><img src="img/social/icon_header_p.png" alt=""></a></li>
                        <li><a href="johndoe@domainname.com" target="_self" original-title="mail"><img src="img/social/icon_header_ball.png" alt=""></a></li>
                    </ul>
                </li>                    
                </ul>
            </div> --}}
        </div>

        <div class="clear"></div>
        <div class="navigate">
            <div class="width">
                <div class="logo inline">
                     <a class="navbar-brand" href="index.html">
                    <img src="/wealth-assets/img/logo-bella-shop.png" width="70" height="10" alt="Awesome Image"/>
                </a>
                </div>

                <div class="navigation inline">
                    <ul class="nav inline menuleft">
                        <li class="active"><a class="bleft" href="index.html">HOME</a></li>
                        <li><a id="link_about" class="bleft" href="#">ABOUT</a></li>
                        <li><a id="link" class="bleft" href="#">PROJECTS</a></li>
                        <li><a id="link_reg" class="bleft" href="#">REGISTER</a></li>
                        <li><a id="link_contact" class="bleft" href="#">CONTACTS</a></li>
                        <li><a id="link_partners" class="bleft last" href="#">PARTNERS</a></li>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>
            <div id="header_select">
                <select id="dynamic_select">
                    <option value="" selected>Select A Page</option>
                    <option value="index.html#id_about">About</option>
                    <option value="index.html#id3">Projects</option>
                    <option value="index.html#id_reg">Registration</option>
                    <option value="index.html#id_cont">Contacts</option>
                    <option value="index.html#id_partn">Partners</option>
                </select>
            </div>
        </div>


    </div>
<div id="navtop" class="width navigate" style="position: fixed;  display: none; top: 0px; left: 0px;right: 0px;z-index: 99999999999999999;">
    <div class="navigation">
        <ul class="nav inline menuleft">
            <li class="active"><a class="bleft" id="link_home" href="#">HOME</a></li>
            <li><a id="link_about_b" class="bleft" href="#">ABOUT</a></li>
            <li><a id="link_b" class="bleft" href="#">PROJECTS</a></li>
            <li><a id="link_reg_b" class="bleft" href="#">REGISTER</a></li>
            <li><a id="link_contact_b" class="bleft" href="#">CONTACTS</a></li>
            <li><a id="link_partners_b" class="bleft last" href="#">PARTNERS</a></li>
        </ul>
    </div>
</div>

@yield('content')


<div id="footer">
    <div class="footer">
        <div class="row-fluid">
            <div class="span3">
                <h2>Feelgrowth</h2>
                <p>Feelgrowth provides you with an easy to navigate, a totally automated dashboard that displays everything in real-time to allow you to monitor your donations second by second. Other than our small $29 fee there is never any additional charges.<!--  Feelgrowth crowdfunding has ZERO PERCENT FEES and you can keep as much as 100% of the money raised! We have created a safe system with safeguards that keep your crowdfunding activities protected and this system cannot be manipulated. Team crowdfunding is a fundraising movement designed to help charities, businesses and individuals raise funds for virtually any legal endeavor of their choosing. --> mauris venenapus</p>
            </div>
            <div class="span3 foot">
                <h4>Recent Tweets</h4>
                <div class="tmargin20">
                    <img class="fleft tw" src="img/social_tweet.png" alt="">
                    <div class="mleft40">
                        <span>1234 Street </span>
                        <span>4r6yg 456hv  </span>
                        <span>Lorem ipus ex vixc ilflf nfnfoof wrfoo rcgetg  erger </span>
                        <br>
                        <a class="green">http://www.feelgrowth.in</a>
                    </div>
                    <div class="clear foot"></div>
                    <img class="fleft tw" src="img/social_tweet.png" alt="">
                    <div class=" mleft40">
                        <span>1234 Street </span>
                        <span>4r6yg 456hv  </span>
                        <span>Lorem ipus ex vixc ilflf n </span>
                        <br>
                        <a class="green">http://www.feelgrowth.in</a>
                    </div>
                </div>
            </div>
            <div class="span3 foot">
                <h4>Recent Posts</h4>
                <div class="tmargin20">
                    <div class="recents">
                        <p class="green"><a href="#">Lorem ipsus dolor sit amet</a></p>
                        <p class="green"><a href="#">Proin nibh aufu suscript</a></p>
                        <p class="green"><a href="#">Lorem ipsus dolor sit amet</a></p>
                    </div>
                    <p class="green recents"><a href="#">LQuri wfcw ertvc</a></p>
                </div>
            </div>
            <div class="span3 foot">
                <h4>Flickr Feed</h4>
                <div class="flckr">
                    <div>
                        <div class="flickr"></div>
                        <div class="flickr"></div>
                        <div class="flickr"></div>
                        <div class="flickrl"></div>

                        <div class="flickr"></div>
                        <div class="flickr"></div>
                        <div class="flickr"></div>
                        <div class="flickrl"></div>

                        <div class="flickr"></div>
                        <div class="flickr"></div>
                        <div class="flickr"></div>
                        <div class="flickrl"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_bot">
        <div class="footer_bott">
              <div class="fleft">© FGrowth. Design by Backstagesupporters.com</div>
              <div class="fright">
                <ul id="social_b" class="socialbott inline">
                    <li><a href="https://twitter.com/" target="_blank" original-title="twitter"><img src="img/social/icon_footer_face.png" alt=""></a></li>
                    <li><a href="https://plus.google.com" target="_blank" original-title="googleplus"><img src="img/social/icon_footer_g.png" alt=""></a></li>
                    <li><a href="https://www.facebook.com" title="facebook" target="_blank"><img src="img/social/icon_footer_tw.png" alt=""></a></li>
                    <li><a href="http://www.linkedin.com/" target="_blank" original-title="linkedin"><img src="img/social/icon_footer_in.png" alt=""></a></li>
                    <li><a href="http://www.linkedin.com/" target="_self" original-title="mail"><img src="img/social/icon_footer_p.png" alt=""></a></li>
                    <li><a href="http://www.linkedin.com/" target="_self" original-title="mail"><img src="img/social/icon_footer_ball.png" alt=""></a></li>
                </ul>
              </div>
            <div class="clear"></div>
        </div>
    </div>
</div>

    <script>
        /* <![CDATA[ */
        (function($) {
            $(function() {

                $('select').selectbox();

                $('#add').click(function(e) {
                    $(this).parents('div.section').append('<br /><br /><select><option>-- Выберите --</option><option>Пункт 1</option><option>Пункт 2</option><option>Пункт 3</option><option>Пункт 4</option><option>Пункт 5</option></select>');
                    $('select').selectbox();
                    e.preventDefault();
                })

                $('#add2').click(function(e) {
                    var options = '';
                    for (i = 1; i <= 5; i++) {
                        options += '<option>Option ' + i + '</option>';
                    }
                    $(this).parents('div.section').find('select').each(function() {
                        $(this).append(options);
                    })
                    $('select').trigger('refresh');
                    e.preventDefault();
                })

                $('#che').click(function(e) {
                    $(this).parents('div.section').find('option:nth-child(5)').attr('selected', true);
                    $('select').trigger('refresh');
                    e.preventDefault();
                })

            })
        })(jQuery)
        /* ]]> */

    </script>
    <script>
        /* <![CDATA[ */
        $(document).ready(function(){
            $("#toggl").click(function(){
                $("#panel").slideToggle("slow");
                $("#home-page-featured").slideToggle("slow");
                $(this).toggleClass('latest_top');
            });
        });
        /* ]]> */
        </script>
<script>
    /* <![CDATA[ */
    jQuery('#advanced-search').hide();
    jQuery('#advanced-search-button').click(function(event) {
        /* Preventing default link action */
        event.preventDefault();
        jQuery('#default-search').slideToggle('fast');
        jQuery('#advanced-search').slideToggle('fast');
        jQuery(this).toggleClass('expanded');
    });
    /* ]]> */

</script>
<script>
    /* <![CDATA[ */
    $(function() {

        var availableTags = [
            "ActionScript",
            "AppleScript",
            "Asp",
            "BASIC",
            "C",
            "C++",
            "Clojure",
            "COBOL",
            "ColdFusion",
            "Erlang",
            "Fortran",
            "Groovy",
            "Haskell",
            "Java",
            "JavaScript",
            "Lisp",
            "Perl",
            "PHP",
            "Python",
            "Ruby",
            "Scala",
            "Scheme"
        ];
        $( "#autocomplete" ).autocomplete({
            source: availableTags
        });

        // Hover states on the static widgets
        $( "#dialog-link, #icons li" ).hover(
                function() {
                    $( this ).addClass( "ui-state-hover" );
                },
                function() {
                    $( this ).removeClass( "ui-state-hover" );
                }
        );
    });
    /* ]]> */

</script>
<script>
    /* <![CDATA[ */
    function scrollToAnchor(aid){
        var aTag = $("a[name='"+ aid +"']");
        $('html,body').animate({scrollTop: aTag.offset().top},'slow');
    }
    $("#link").click(function() {
        scrollToAnchor('id3');
    });
    $("#link_about").click(function() {
        scrollToAnchor('id_about');
    });
    $("#link_reg").click(function() {
        scrollToAnchor('id_reg');
    });
    $("#link_contact").click(function() {
        scrollToAnchor('id_cont');
    });
    $("#link_home").click(function() {
        scrollToAnchor('id_home');
    });
    $("#link_partners").click(function() {
        scrollToAnchor('id_partn');
    });
    $("#link_b").click(function() {
        scrollToAnchor('id3');
    });
    $("#link_about_b").click(function() {
        scrollToAnchor('id_about');
    });
    $("#link_reg_b").click(function() {
        scrollToAnchor('id_reg');
    });
    $("#link_contact_b").click(function() {
        scrollToAnchor('id_cont');
    });
    $("#link_partners_b").click(function() {
        scrollToAnchor('id_partn');
    });
    /* ]]> */


</script>
<script>
    /* <![CDATA[ */
    function hexFromRGB(r, g, b) {
        var hex = [
            r.toString( 16 ),
            g.toString( 16 ),
            b.toString( 16 )
        ];
        $.each( hex, function( nr, val ) {
            if ( val.length === 1 ) {
                hex[ nr ] = "0" + val;
            }
        });
        return hex.join( "" ).toUpperCase();
    }
    function refreshSwatch() {
        var red = $( "#project1" ).slider( "value" ),
                green = $( "#project2" ).slider( "value" ),
                blue = $( "#project3" ).slider( "value" ),
                blues = $( "#project4" ).slider( "value" ),
                hex = hexFromRGB( red, green, blue);
        $( "#swatch" ).css( "background-color", "#" + hex );

    }
    $(function() {
        $( "#project1, #project2, #project3, #project4, #project5, #project6, #project7, #project8, #project9, #project10, #project11, #project12" ).slider({
            orientation: "horizontal",
            range: "min",
            disabled: true,
            max: 100,
            slide: refreshSwatch,
            change: refreshSwatch
        });

        $.each($('.project_value'), function(k,v){
            var value = 100 * Number($(v).text())/Number($($(v).attr('href')).attr('value'));
            value = Math.floor(value);
            $($(v).attr('href')).slider("value", value);
            $($(v).attr('href')).find(".ui-slider-handle").text(value+'%');
        });

    });
    /* ]]> */

</script>

<script src="js/jquery.selectbox.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/jquery-uii.js"></script>
<script src="js/contact.js"></script>
<script src="js/jquery.colorbox.js"></script>
<script src="js/jquery.placeholder.js"></script>
<script src="js/jquery.tweet.js"></script>
<script src="js/jflickrfeed.min.js"></script>
<script src="js/scripts.js"></script>

<!--[if IE 8]>
<script src="js/css3-mediaqueries.js"></script>
<![endif]-->
<script>
    /* <![CDATA[ */
    $(document).ready(function(){
        var slider = $(".slider").slider({
            change: function () {
/*                var value = $(this).slider("option", "value");
                $(this).find(".ui-slider-handle").text(value+'%');*/
            },
            slide: function () {
/*                var value = $(this).slider("option", "value");
                $(this).find(".ui-slider-handle").text(value+"%");*/
            }
        });
/*        slider.each(function(index){
            var el = slider.get(index);
            var value = $(el).slider("value");
            $(el).slider("value", value);
        });*/
    });
    /* ]]> */
</script>
<script>
    /* <![CDATA[ */
    $(document).ready(function(){
        //Examples of how to assign the Colorbox event to elements
        $(".group1").colorbox({rel:'nofollow',scalePhotos:'false'});

    });
    /* ]]> */

</script>
<script>
    /* <![CDATA[ */
    $(function() {
        $('input, textarea').placeholder();
    });
    /* ]]> */

</script>
<script>
    /* <![CDATA[ */
    $(function(){
        // bind change event to select
        $('#dynamic_select').bind('change', function () {
            var url = $(this).val(); // get selected value
            if (url) { // require a URL
                window.location = url; // redirect
            }
            return false;
        });
    });
    /* ]]> */
</script>

</body>
</html>