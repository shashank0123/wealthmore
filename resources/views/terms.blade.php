<html>
<head>
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>

<body>
	<div class="container" style="width: 80%; margin: 1% 10%">
<section class="terms" style="text-align: center;color: #505050; font-family: verdana; line-height: 2">
	<button class="btn btn-primary"><a href="/register" style="color: #fff">Go Back</a></button>
	<h1><u>WealthIndia Terms & Conditions</u></h1>
	@lang('terms.content');
	<br><br>                                    
	<button class="btn btn-primary"><a href="/register" style="color: #fff">Go Back</a></button>
	<br><br>
</section>

</div>
</body>
</html>