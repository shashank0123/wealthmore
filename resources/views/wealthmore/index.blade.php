@extends('layouts.wealth-more')

<?php
use App\Product;
use App\MainCategory;
use App\Testimonial;
use App\Banner;
use App\Review;
//use DB; 
$count=0;
?>
<style>
	.products-images { width: 210px !important; height: 270px !important }
</style>

@section('content')
<!-- CONTENT AREA -->
<div class="content-area">

    <!-- PAGE -->
    <section class="page-section no-padding slider">
        <div class="container full-width">

            <div class="main-slider">
                <div class="owl-carousel" id="main-slider">

                    <!-- Slide 3 -->
                    @foreach($banners as $banner)
                    <div class="item slide3 dark">
                        <img class="slide-img" src="{{ URL::to('/') }}/assetsss/images/AdminProduct/banner/{{ $banner->image_url }}" alt=""/ style="width: 100%; height: 300px">
                        <div class="caption">
                            <div class="container">
                                <div class="div-table">
                                    <div class="div-cell">
                                        <div class="caption-content">
                                                        {{-- <h2 class="caption-title">New Arrivals On Sale</h2>
                                                        <h3 class="caption-subtitle"><span>Summer Collection</span></h3>
                                                        <p class="caption-text">
                                                            <a class="btn btn-theme" href="#">Shop this item Now</a>
                                                        </p> --}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                <!-- /Slide 3 -->


                            </div>
                        </div>

                    </div>
                </section>
                <!-- /PAGE -->
                <?php
                $small_banner = Banner::where('image_type','medium')->where('status','Active')->first();
                ?>
                <!-- PAGE -->
                <section class="page-section">
                    <div class="container">
                        <div class="row">
                            @if($small_banner != null)
                            <div class="col-md-8">
                                <div class="thumbnail no-border no-padding thumbnail-banner size-1x3-b alt-font">
                                    <div class="media">
                                        <a class="media-link" href="#">
                                            <div class="img-bg" style="background-image: url('{{ URL::to('/') }}/assetsss/images/AdminProduct/banner/{{ $small_banner->image_url }}')"></div>
                                            <div class="caption text-right">
                                                <div class="caption-wrapper div-table">
                                                    <div class="caption-inner div-cell">
                                                        {{-- <h2 class="caption-title"><span>Winter Fashion</span></h2>
                                                        <h3 class="caption-sub-title"><span>Collection Ready</span></h3>
                                                        <span class="btn btn-theme btn-theme-sm">Shop Now</span> --}}
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            @endif

                            <?php $last_product = Product::orderBy('created_at','DESC')->first();?>
                            @if($last_product != null)
                            <div class="col-md-4">
                                <div class="thumbnail no-border no-padding thumbnail-banner size-1x1-b alt-font">
                                    <div class="media">
                                        <a class="media-link" href="/productdetail/{{$last_product->id}}">
                                            <div class="img-bg" style="background-image: url('{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $last_product->image1 }}')"></div>
                                            <div class="caption text-left">
                                                <div class="caption-wrapper div-table">
                                                    <div class="caption-inner div-cell">
                                                        <h2 class="caption-title"><span>{{$last_product->name}}</span></h2>
                                                        {{-- <h3 class="caption-sub-title"><span>Winter 2015</span></h3> --}}
                                                        <span class="btn btn-theme btn-theme-sm">Shop Now</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </section>
                <!-- /PAGE -->

                <!-- PAGE -->
                <section class="page-section">
                    <div class="container">

                        <div class="tabs">
                            <ul id="tabs" class="nav nav-justified-off"><!--
                                --><li class=""><a href="#featured" data-toggle="tab">Featured</a></li><!--
                                --><li class="active"><a href="#new-arrival" data-toggle="tab">New Arrival</a></li><!--
                            --><li class=""><a href="#trending" data-toggle="tab">Trending</a></li>
                        </ul>
                    </div>

                    <div class="tab-content">

                        <!-- tab 1 -->
                        <div class="tab-pane fade" id="featured">
                            <div class="row">

                                @if(isset($products))
                                @foreach($products as $row)
                                <?php

                                $category = Product::where('products.id',$row->id)->leftjoin('main_categories','main_categories.id','=','products.category_id')->select('main_categories.Mcategory_name')->first();

                                ?>

                                <?php $discount=(($row->mrp - $row->sell_price)*100)/$row->mrp;

                                $rate = Review::where('product_id',$row->id)->avg('rating');
                                $final_rate = floor($rate);
                                if($final_rate == 0){
                                    $final_rate = -1;
                                }
                                else { $final_rate = $final_rate - 5; }
                                ?>

                                <div class="col-md-3 col-sm-6">
                                    <div class="thumbnail no-border no-padding">
                                        <div class="media">

                                            <a class="media-link" data-gal="prettyPhoto" href="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $row->image1 }}">
                                                <img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $row->image1 }}" alt="" class="products-images"/>
                                                <span class="icon-view"><strong><i class="fa fa-eye"></i></strong></span>
                                            </a>
                                        </div>
                                        <div class="caption text-center">
                                            <h4 class="caption-title"><a href="/productdetail/{{ $row->id }}">{{$row->name}}</a></h4>
                                            @if ($row->jbv) <h5>JBV : {{$row->jbv}}</h5>@endif
                                            @if ($row->rvv)<h5>RBV : {{$row->rvv}}</h5> @endif
                                            <div class="rating">
                                                @for($i=0 ; $i<5 ; $i++)
                                                @if($final_rate>=0)
                                                <span class="star active"></span>
                                                @else
                                                <span class="star"></span>
                                                @endif
                                                <?php $final_rate++; ?>
                                                @endfor
                                            </div>
                                            <div class="price"><ins>Rs. {{$row->sell_price}}</ins> {{-- <del>$425.00</del> --}}</div>
                                            <div class="buttons">
                                                    <a class="btn btn-theme btn-theme-transparent btn-wish-list" href="#" onclick="addWishList({{ $row->id }})"><i class="fa fa-heart"></i></a><!--
                                                    <a class="btn btn-theme btn-theme-transparent btn-icon-left" href="#"  onclick="addCart({{$row->id}}><i class="fa fa-shopping-cart"></i>Add to Cart</a>-->

                                                    <a class="btn btn-theme btn-theme-transparent btn-icon-left" href="/productdetail/{{$row->id}}">{{-- <i class="fa fa-shopping-cart"></i> --}}View Detail</a>

                                                    <!--
                                                -->{{-- <a class="btn btn-theme btn-theme-transparent btn-compare" href="#"><i class="fa fa-exchange"></i></a> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                @endif

                            </div>
                        </div>

                        <?php
                        $featuredproducts = Product::where('trending','yes')->where('status','Active')->orderBy('created_at','desc')->limit(4)->get();
                        ?>
                        <!-- tab 2 -->
                        <div class="tab-pane fade active in" id="new-arrival">
                            <div class="row">
                                @if(isset($featuredproducts))
                                @foreach($featuredproducts as $col)
                                <?php
                                $featurecategory = Product::where('products.id',$col->id)->leftjoin('main_categories','main_categories.id','=','products.category_id')->select('main_categories.Mcategory_name')->first();

                                ?>
                                <?php $discount=(($col->mrp - $col->sell_price)*100)/$col->mrp;

                                $rate = Review::where('product_id',$col->id)->avg('rating');
                                $final_rate = floor($rate);
                                if($final_rate == 0){
                                    $final_rate = -1;
                                }
                                else { $final_rate = $final_rate - 5; }
                                ?>
                                <div class="col-md-3 col-sm-6">
                                    <div class="thumbnail no-border no-padding">
                                        <div class="media">
                                            <a class="media-link" data-gal="prettyPhoto" href="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $col->image1 }} ">
                                                <img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $col->image1 }} " alt="" class="products-images"/>
                                                <span class="icon-view"><strong><i class="fa fa-eye"></i></strong></span>
                                            </a>
                                        </div>
                                        <div class="caption text-center">
                                            <h4 class="caption-title"><a href="/productdetail/{{ $col->id }}">{{$col->name}}</a></h4>
                                            @if ($col->jbv) <h5>JBV : {{$col->jbv}}</h5>@endif
                                            @if ($col->rvv)<h5>RBV : {{$col->rvv}}</h5> @endif
                                            <div class="rating">
                                                @for($i=0 ; $i<5 ; $i++)
                                                @if($final_rate>=0)
                                                <span class="star active"></span>
                                                @else
                                                <span class="star"></span>
                                                @endif
                                                <?php $final_rate++; ?>
                                                @endfor
                                            </div>
                                            <div class="price"><ins>Rs. {{$col->sell_price}}</ins> {{-- <del>$425.00</del> --}}</div>
                                            <div class="buttons">
                                                    <a class="btn btn-theme btn-theme-transparent btn-wish-list" href="#" onclick="addWishList({{ $col->id }})"><i class="fa fa-heart"></i></a><!--
                                                    -->
                                                    {{-- <a class="btn btn-theme btn-theme-transparent btn-icon-left" href="#" onclick="addCart({{$col->id}})"><i class="fa fa-shopping-cart"></i>Add to Cart</a> --}}
                                                   <a class="btn btn-theme btn-theme-transparent btn-icon-left" href="/productdetail/{{$col->id}}">{{-- <i class="fa fa-shopping-cart"></i> --}}View Detail</a>
                                                    <!--
                                                -->{{-- <a class="btn btn-theme btn-theme-transparent btn-compare" href="#"><i class="fa fa-exchange"></i></a> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                @endif                               
                                
                            </div>
                        </div>


                        <?php
                        $trendproducts = Product::where('trending','yes')->where('status','Active')->orderBy('name','asc')->limit(4)->get();

                        ?>
                        <!-- tab 3 -->
                        <div class="tab-pane fade" id="trending">
                            <div class="row">

                                @if($trendproducts != null)

                                @foreach($trendproducts as $set)
                                <?php
                                $trendcategory = Product::where('products.id',$set->id)->leftjoin('main_categories','main_categories.id','=','products.category_id')->select('main_categories.Mcategory_name')->first();

                                ?>
                                <?php 

                                $rate = Review::where('product_id',$set->id)->avg('rating');
                                $final_rate = floor($rate);
                                if($final_rate == 0){
                                    $final_rate = -1;
                                }
                                else { $final_rate = $final_rate - 5; }
                                ?>
                                <div class="col-md-3 col-sm-6">
                                    <div class="thumbnail no-border no-padding">
                                        <div class="media">
                                            <a class="media-link" data-gal="prettyPhoto" href="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $set->image1 }} ">
                                                <img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $set->image1 }} " alt="" class="products-images"/>
                                                <span class="icon-view" ><strong><i class="fa fa-eye"></i></strong></span>
                                            </a>
                                        </div>
                                        <div class="caption text-center">
                                            <h4 class="caption-title"><a href="/productdetail/{{ $set->id }}">{{$set->name}}</a></h4>
                                            @if ($set->jbv) <h5>JBV : {{$set->jbv}}</h5>@endif
                                            @if ($set->rvv)<h5>RBV : {{$set->rvv}}</h5> @endif
                                            <div class="rating">
                                                @for($i=0 ; $i<5 ; $i++)
                                                @if($final_rate>=0)
                                                <span class="star active"></span>
                                                @else
                                                <span class="star"></span>
                                                @endif
                                                <?php $final_rate++; ?>
                                                @endfor
                                            </div>
                                            <div class="price"><ins>Rs. {{$set->sell_price}}</ins>{{--  <del>$425.00</del> --}}</div>
                                            <div class="buttons">
                                                    <a class="btn btn-theme btn-theme-transparent btn-wish-list" href="#" onclick="addWishList({{ $set->id }})"><i class="fa fa-heart"></i></a><!--
                                                    -->{{-- <a class="btn btn-theme btn-theme-transparent btn-icon-left" href="#" onclick="addCart({{$set->id}})"><i class="fa fa-shopping-cart"></i>Add to Cart</a> --}}
                                                    <a class="btn btn-theme btn-theme-transparent btn-icon-left" href="/productdetail/{{$set->id}}">{{-- <i class="fa fa-shopping-cart"></i> --}}View Detail</a>
                                                    <!--
                                                -->{{-- <a class="btn btn-theme btn-theme-transparent btn-compare" href="#"><i class="fa fa-exchange"></i></a> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                @endif
                                
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            <!-- /PAGE -->


            <?php
            $brands = Banner::where('image_type','brand')->where('status','Active')->get();
            ?>

            @if(isset($brands))
            <!-- PAGE -->
            <section class="page-section">
                <div class="container">
                    <h2 class="section-title"><span>Brand &amp; Clients</span></h2>
                    <div class="partners-carousel">
                        <div class="owl-carousel" id="partners">
                            @foreach($brands as $brand)
                            <div><a href="#"><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/banner/{{ $brand->image_url }} " alt=""/></a></div>
                            @endforeach
                            
                        </div>
                    </div>
                </div>
            </section>
            @endif
            <!-- /PAGE -->

            <!-- PAGE -->
            <section class="page-section image testimonials">
                <div class="container">
                    <h2 class="section-title section-title-lg"><span><span class="thin">What </span> Customers <span class="thin"> Say</span></span></h2>

                    <?php  $testimonials = Testimonial::where('status','Active')->get();
                    ?>
                    @if(isset($testimonials) )                    

                    <div class="testimonials-carousel">
                        <div class="owl-carousel" id="testimonials">
                            @foreach($testimonials as $testimonial)
                            <div class="testimonial">
                                <div class="testimonial-text">{{$testimonial->review}}</div>
                                <div class="testimonial-name">{{$testimonial->client_name}} from {{$testimonial->city}}</div>
                            </div>
                            @endforeach
                        </div>
                    </div>

                    @endif
                </div>
            </section>
            <!-- /PAGE -->
            <?php             
            $top_products = DB::table( 'products' )
            ->join( 'reviews', 'reviews.product_id', '=', 'products.id' )
            ->where( 'products.status', 'Active' )
            ->groupBy( 'products.id' )
            ->select( 'products.id', DB::raw( 'AVG( rating )' ) )->orderBy('AVG( rating )','DESC')
            ->get();
            ?>

            <!-- PAGE -->
            <section class="page-section">
                <div class="container">
                    <h2 class="section-title"><span>Top Rated Products</span></h2>
                    <div class="top-products-carousel">
                        <div class="owl-carousel" id="top-products-carousel">
                            @foreach($top_products as $pro)
                            <?php
                            $product = Product::where('id',$pro->id)->first();?>
                            <div class="thumbnail no-border no-padding">
                                <div class="media">
                                    <a class="media-link" data-gal="prettyPhoto" href="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }} ">
                                        <img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }} " alt="{{$product->name}}" style=" height: 200px" />
                                        <span class="icon-view"><strong><i class="fa fa-eye"></i></strong></span>
                                    </a>
                                </div>
                                <div class="caption text-center">
                                    <h4 class="caption-title"><a href="/productdetail/{{$product->id}}">{{$product->name}}</a></h4>
                                    <?php
                                    $rate = Review::where('product_id',$product->id)->avg('rating');
                                    $final_rate = floor($rate)+1;
                                    if($final_rate == 0){
                                        $final_rate = -1;
                                    }
                                    else { $final_rate = $final_rate - 5; }
                                    ?>
                                    <div class="rating">
                                       @for($i=0 ; $i<5 ; $i++)
                                       @if($final_rate>=0)
                                       <span class="star active"></span>
                                       @else
                                       <span class="star"></span>
                                       @endif
                                       <?php $final_rate++; ?>
                                       @endfor
                                   </div>
                                   <div class="price"><ins>Rs. {{$product->sell_price}}</ins> {{-- <del>$425.00</del> --}}</div>
                                   <div class="buttons">
                                            <a class="btn btn-theme btn-theme-transparent btn-wish-list" href="#"><i class="fa fa-heart" onclick="addWishList({{ $product->id }})"></i></a><!--
                                            -->{{-- <a class="btn btn-theme btn-theme-transparent" href="#"  onclick="addCart({{$product->id}})">Add to Cart</a> --}}
                                            <a class="btn btn-theme btn-theme-transparent btn-icon-left" href="/productdetail/{{$product->id}}">{{-- <i class="fa fa-shopping-cart"></i> --}}View Detail</a>
                                            <!--
                                        -->{{-- <a class="btn btn-theme btn-theme-transparent btn-compare" href="#"><i class="fa fa-exchange"></i></a> --}}
                                    </div>
                                </div>
                            </div>
                            @endforeach                          
                            

                            @foreach($products as $product)
                            <?php $flag = 0; ?>
                            @foreach($top_products as $pro)
                            @if($pro->id == $product->id){
                            <?php 
                            $flag = 1;
                            ?>
                            @endif
                            @endforeach

                            @if($flag == 0)
                            <div class="thumbnail no-border no-padding">
                                <div class="media">
                                    <a class="media-link" data-gal="prettyPhoto" href="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}">
                                        <img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }} " alt="{{$product->name}}" style=" height: 200px" />
                                        <span class="icon-view"><strong><i class="fa fa-eye"></i></strong></span>
                                    </a>
                                </div>
                                <div class="caption text-center">
                                    <h4 class="caption-title"><a href="/productdetail/{{$product->id}}">{{$product->name}}</a></h4>
                                    <?php
                                    $final_rate = -2;
                                    ?>
                                    <div class="rating">
                                       @for($i=0 ; $i<5 ; $i++)
                                       @if($final_rate>=0)
                                       <span class="star active"></span>
                                       @else
                                       <span class="star"></span>
                                       @endif
                                       <?php $final_rate++; ?>
                                       @endfor
                                   </div>
                                   <div class="price"><ins>Rs. {{$product->sell_price}}</ins> {{-- <del>$425.00</del> --}}</div>
                                   <div class="buttons">
                                            <a class="btn btn-theme btn-theme-transparent btn-wish-list" href="#"><i class="fa fa-heart" onclick="addWishList({{ $product->id }})"></i></a><!--
                                            -->{{-- <a class="btn btn-theme btn-theme-transparent" href="#"  onclick="addCart({{$product->id}})">Add to Cart</a> --}}
                                            <a class="btn btn-theme btn-theme-transparent btn-icon-left" href="/productdetail/{{$product->id}}">{{-- <i class="fa fa-shopping-cart"></i> --}}View Detail</a>
                                            <!--
                                        -->{{-- <a class="btn btn-theme btn-theme-transparent btn-compare" href="#"><i class="fa fa-exchange"></i></a> --}}
                                    </div>
                                </div>
                            </div>
                            @endif
                            @endforeach

                        </div>
                    </div>
                </div>
            </section>
            <!-- /PAGE -->

            <!-- PAGE -->
           {{--  <section class="page-section">
                <div class="container">
                    <a class="btn btn-theme btn-title-more btn-icon-left" href="#"><i class="fa fa-file-text-o"></i>See All Posts</a>
                    <h2 class="block-title"><span>Our Recent posts</span></h2>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="recent-post">
                                <div class="media">
                                    <a class="pull-left media-link" href="#">
                                        <img class="media-object" src="/wealth-assets/img/preview/blog/recent-post-1.jpg" alt="">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                    <div class="media-body">
                                        <p class="media-category"><a href="#">Shoes</a> / <a href="#">Dress</a></p>
                                        <h4 class="media-heading"><a href="#">Standard Post Comment Header Here</a></h4>
                                        Fusce gravida interdum eros a mollis. Sed non lorem varius, volutpat nisl in, laoreet ante.
                                        <div class="media-meta">
                                            6th June 2014
                                            <span class="divider">/</span><a href="#"><i class="fa fa-comment"></i>27</a>
                                            <span class="divider">/</span><a href="#"><i class="fa fa-heart"></i>18</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="recent-post">
                                <div class="media">
                                    <a class="pull-left media-link" href="#">
                                        <img class="media-object" src="/wealth-assets/img/preview/blog/recent-post-2.jpg" alt="">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                    <div class="media-body">
                                        <p class="media-category"><a href="#">Wedding</a> / <a href="#">Meeting</a></p>
                                        <h4 class="media-heading"><a href="#">Standard Post Comment Header Here</a></h4>
                                        Fusce gravida interdum eros a mollis. Sed non lorem varius, volutpat nisl in, laoreet ante.
                                        <div class="media-meta">
                                            6th June 2014
                                            <span class="divider">/</span><a href="#"><i class="fa fa-comment"></i>27</a>
                                            <span class="divider">/</span><a href="#"><i class="fa fa-heart"></i>18</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section> --}}
            <!-- /PAGE -->

            <?php
            $small_banner = Banner::where('image_type','medium')->where('status','Active')->orderBy('created_at','DESC')->first();
            ?>

            <!-- PAGE -->
            <section class="page-section">
                <div class="container">
                    <div class="row">
                        @if($small_banner != null)
                        <div class="col-md-6">
                            <div class="thumbnail no-border no-padding thumbnail-banner size-1x3">
                                <div class="media">
                                    <a class="media-link" href="#">
                                        <div class="img-bg" style="background-image: url('{{ URL::to('/') }}/assetsss/images/AdminProduct/banner/{{ $small_banner->image_url }}')"></div>
                                        <div class="caption">
                                            <div class="caption-wrapper div-table">
                                                <div class="caption-inner div-cell">
                                                    {{-- <h2 class="caption-title"><span>Lorem Ipsum</span></h2>
                                                    <h3 class="caption-sub-title"><span>Dolor Sir Amet Percpectum</span></h3>
                                                    <span class="btn btn-theme btn-theme-sm">Shop Now</span> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        @endif
                        <?php
                        $last_category = MainCategory::orderBy('created_at','DESC')->limit(2)->get();

                        ?>

                        @if($last_category != null)
                        @foreach($last_category as $last)
                        <?php
                        $last_pro = Product::where('category_id',$last->id)->orderBy('created_at','DESC')->first();
                        ?>
                        @if($last_pro != null)
                        <div class="col-md-3">
                            <div class="thumbnail no-border no-padding thumbnail-banner size-1x1">
                                <div class="media">
                                    <a class="media-link" href="/productdetail/{{$last_pro->id}}">
                                        <div class="img-bg" style="background-image: url('{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $last_pro->image1 }}')"></div>
                                        <div class="caption text-center">
                                            <div class="caption-wrapper div-table">
                                                <div class="caption-inner div-cell">
                                                    <h2 class="caption-title"><span>{{$last_pro->name}}</span></h2>
                                                    {{-- <h3 class="caption-sub-title"><span>Dolor Sir Amet Percpectum</span></h3> --}}
                                                    <span class="btn btn-theme btn-theme-sm">Shop Now</span>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        @endif
                        @endforeach
                        @endif

                    </div>
                </div>
            </section>
            <!-- /PAGE -->

            <!-- PAGE -->
            <section class="page-section">
                <div class="container">
                    <div class="row">
                        {{-- <div class="col-md-4">
                            <div class="product-list">
                                <a class="btn btn-theme btn-title-more" href="#">See All</a>
                                <h4 class="block-title"><span>Top Sellers</span></h4>                         
                                <div class="media">
                                    <a class="pull-left media-link" href="#">
                                        <img class="media-object" src="/wealth-assets/img/preview/shop/top-sellers-3.jpg" alt="">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                    <div class="media-body">
                                        <h4 class="media-heading"><a href="#">Standard Product Header</a></h4>
                                        <div class="rating">
                                                <span class="star"></span>
                                                <span class="star active"></span>
                                                <span class="star active"></span>
                                                <span class="star active"></span>
                                            <span class="star active"></span>
                                        </div>
                                        <div class="price"><ins>$400.00</ins> <del>$425.00</del></div>
                                    </div>
                                </div>
                            </div>
                        </div> --}}

                        <?php             
                        $top_products = DB::table( 'products' )
                        ->join( 'reviews', 'reviews.product_id', '=', 'products.id' )
                        ->where( 'products.status', 'Active' )
                        ->groupBy( 'products.id' )
                        ->select( 'products.id', DB::raw( 'AVG( rating )' ) )->orderBy('AVG( rating )','DESC')
                        ->limit(3)->get();

                        ?>
                        
                        <div class="col-md-6">
                            <div class="product-list">
                                <a class="btn btn-theme btn-title-more" href="#">See All</a>
                                <h4 class="block-title"><span>Top Accessories</span></h4>
                                @if($top_products != null)
                                @foreach($top_products as $top)
                                <?php $product = Product::where('id',$top->id)->first(); ?>
                                @if($product != null)
                                <div class="media">
                                    <a class="pull-left media-link" href="/productdetail/{{$product->id}}">
                                        <img class="media-object" src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}" alt="{{$product->name}}" style="width: 100px; height: 100px;">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                    <div class="media-body">
                                        <h4 class="media-heading"><a href="/productdetail/{{$product->id}}">{{$product->name}}</a></h4>
                                        <?php
                                        $rate = Review::where('product_id',$product->id)->avg('rating');
                                        $final_rate = floor($rate)+1;
                                        if($final_rate == 0){
                                            $final_rate = -1;
                                        }
                                        else { $final_rate = $final_rate - 5; }
                                        ?>
                                        <div class="rating">
                                           @for($i=0 ; $i<5 ; $i++)
                                           @if($final_rate>=0)
                                           <span class="star active"></span>
                                           @else
                                           <span class="star"></span>
                                           @endif
                                           <?php $final_rate++; ?>
                                           @endfor
                                       </div>
                                       <div class="price"><ins>{{$product->sell_price}}</ins> {{-- <del>$425.00</del> --}}</div>
                                   </div>
                                   
                               </div>
                               @endif
                               @endforeach
                               @endif                                
                           </div>
                       </div>

                       <?php
                       $new_arrival = Product::where('trending','yes')->where('status','Active')->orderBy('created_at','DESC')->limit(3)->get();
                       ?>
                       <div class="col-md-6">
                        <div class="product-list">
                            <a class="btn btn-theme btn-title-more" href="#">See All</a>
                            <h4 class="block-title"><span>Top Newest</span></h4>

                            @if($new_arrival != null)
                            @foreach($new_arrival as $new)
                            <div class="media">
                                <a class="pull-left media-link" href="/productdetail/{{$new->id}}">
                                    <img class="media-object" src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $new->image1 }}" alt="{{$new->name}}" style="width: 100px; height: 100px;">
                                    <i class="fa fa-plus"></i>
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading"><a href="/productdetail/{{$new->id}}">{{$new->name}}</a></h4>
                                    <?php
                                    $rate = Review::where('product_id',$new->id)->avg('rating');
                                    $final_rate = floor($rate);
                                    if($final_rate == 0){
                                        $final_rate = -1;
                                    }
                                    else { $final_rate = $final_rate - 5; }
                                    ?>
                                    <div class="rating">
                                       @for($i=0 ; $i<5 ; $i++)
                                       @if($final_rate>=0)
                                       <span class="star active"></span>
                                       @else
                                       <span class="star"></span>
                                       @endif
                                       <?php $final_rate++; ?>
                                       @endfor
                                   </div>
                                   <div class="price"><ins>Rs. {{$new->sell_price}}</ins> {{-- <del>$425.00</del> --}}</div>
                               </div>
                           </div>
                           @endforeach
                           @endif
                       </div>
                   </div>
               </div>
           </div>
       </section>
       <!-- /PAGE -->

       <!-- PAGE -->
       <section class="page-section no-padding-top">
        <div class="container">
            <div class="row blocks shop-info-banners">
                <div class="col-md-4">
                    <div class="block">
                        <div class="media">
                            <div class="pull-right"><i class="fa fa-gift"></i></div>
                            <div class="media-body">
                                <h4 class="media-heading">Buy 1 Get 1</h4>
                                Proin dictum elementum velit. Fusce euismod consequat ante.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="block">
                        <div class="media">
                            <div class="pull-right"><i class="fa fa-comments"></i></div>
                            <div class="media-body">
                                <h4 class="media-heading">Call to Free</h4>
                                Proin dictum elementum velit. Fusce euismod consequat ante.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="block">
                        <div class="media">
                            <div class="pull-right"><i class="fa fa-trophy"></i></div>
                            <div class="media-body">
                                <h4 class="media-heading">Money Back!</h4>
                                Proin dictum elementum velit. Fusce euismod consequat ante.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /PAGE -->

</div>
<!-- /CONTENT AREA -->


@endsection

@section('script')




<script>

  var countproduct=<?php if(session()->get('count') != null){echo session()->get('count'); } else { echo 0;} ?>;

  function addToCart(){
     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
     var pro_id = $('#hidden_id').val();
     var quant = $('#quickquantity').val();
     countproduct++;
     $.ajax({
        /* the route pointing to the post function */
        url: '/add-cart',
        type: 'POST',
        /* send the csrf-token and the input to the controller */
        data: {_token: CSRF_TOKEN, id: pro_id, quantity: quant},
        success: function (data) { 
           $("#show-total").html(countproduct);           
       }
   }); 
 }

		//modal  box of quick view
		function openview(id,name,price,mrp,description,img1,img2,img3,rate){
			$('#hidden_id').val(id);    
			$('#viewname').text(name);
			$('#viewdescription').text(description);
			$('#viewprice').text(price);
    //For main image
    $("#imggallery1").attr("data-zoom", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img1);
    $("#imggallery2").attr("src", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img1);

    //for a tag (data-zoom attribute)    
    $("#imgdz2").attr("data-zoom", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img2);
    $("#imgdz3").attr("data-zoom", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img3);
    $("#imgdz1").attr("data-zoom", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img1);

    //for a tag (data-images attribute)
    $("#imgdz1").attr("data-images", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img1);
    $("#imgdz2").attr("data-images", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img2);
    $("#imgdz3").attr("data-images", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img3);

    //for image tag (src attribute)
    $("#img1").attr("src", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img1);
    $("#img2").attr("src", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img2);
    $("#img3").attr("src", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img3);
    $("#imgt1").attr("src", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img1);
    $("#imgt2").attr("src", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img2);
    $("#imgt3").attr("src", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img3);
    var discount = ((mrp-price)*100)/mrp;
    $('#viewdiscount').text(discount.toFixed(2));
    $('#viewrate').val(rate);

}



//adding items to cart
function addCart(id){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
	countproduct++;
	$.ajax({
		/* the route pointing to the post function */
		url: '/add-to-cart',
		type: 'POST',
		/* send the csrf-token and the input to the controller */
		data: {_token: CSRF_TOKEN, id: id},
		success: function (data) { 
			window.location.href = '';
			$("#show-total").html(countproduct);           
		}
	}); 
}

//adding items to Wishlist
function addWishList(id,count){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

	$.ajax({
		/* the route pointing to the post function */
		url: '/add-to-wishlist',
		type: 'POST',
		/* send the csrf-token and the input to the controller */
		data: {_token: CSRF_TOKEN, id: id},
		success: function (data) { 
			window.location.href = '';
			$("#ajaxdata").text(count);
		}
	}); 
}
</script>
@endsection
