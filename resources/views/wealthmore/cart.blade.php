
<?php
use App\Product;
use App\Review;
$bill = 0;
$i=0;
?>

@extends('layouts.wealth-more')

@section('content')

<!-- CONTENT AREA -->
<div class="content-area">

    <!-- BREADCRUMBS -->
    <section class="page-section breadcrumbs">
        <div class="container">
            <div class="page-header">
                <h1>Shopping Cart</h1>
            </div>
        </div>
    </section>
    <!-- /BREADCRUMBS -->

    <!-- PAGE -->
    <section class="page-section color">
        <div class="container">
            <h3 class="block-title alt"><i class="fa fa-angle-down"></i> Orders</h3>
            <div class="row orders">
                <div class="col-md-8">
                    @if(session()->get('cart') != null)
                    <?php
                    $ids = array();
                    $cat_id = array();                  
                    $quantities = array();


                    foreach(session()->get('cart') as $data)
                    {
                        $ids[$i] = $data->product_id;                               
                        $quantities[$i] = $data->quantity;
                        $i++;
                    }                       
                    ?>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Image</th>                                
                                <th>Product Name</th>                            
                                <th>JBV</th>
                                <th>RBV</th>
                                <th>Quantity</th>
                                <th>MRP</th>
                                <th>DP</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>

                            @for($j=0 ; $j<$i ; $j++ )
                            <?php
                            $product = Product::where('id',$ids[$j])->first(); 
                            if($product != null)
                            {
                                $cat_id[$j] = $product->category_id;
                            }
                            else
                            {
                                $cat_id[$j] = 0;
                            }
                            ?>
                            <tr id="cart{{$product->id}}">
                                <td class="image"><a class="media-link" href="/productdetail/{{$product->id}}"><i class="fa fa-plus"></i><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }} " alt="{{$product->name}}" style="width: 100px; height: 120px" /></a></td>
                                <td class="description">
                                    <h4><a href="/productdetail/{{$product->id}}">{{$product->name}}</a></h4>
                                    <?php echo htmlspecialchars_decode($product->short_descriptions); ?>
                                </td>
                                <td class="total">
                                    <h4>{{$product->jbv}}</h4>
                                </td>
                                <td class="total">
                                    <h4>{{$product->rvv}}</h4>
                                </td>
                                <td class="total" style="font-weight: normal;">
                                    <i class="fa fa-plus" style="cursor: pointer;" onclick="increaseQty({{$product->id}})"></i>
                                    <input type="text" value="{{$quantities[$j]}}" id="increase_qty{{$product->id}}" style="width: 50px; text-align: center;">
                                    <i class="fa fa-minus"  onclick="decreaseQty({{$product->id}})" style="cursor: pointer;"></i>
                                </td>
                                <td class="total">
                                    <h4>Rs. {{$product->mrp}}</h4>
                                </td>
                                <td class="total">
                                    <h4>Rs. {{$product->sell_price}}</h4>
                                </td>
                                <?php
                                $total = $quantities[$j]*$product->sell_price;
                                $bill =$bill +  $total;
                                ?>
                                <td class="total" ><sapn id="total-amt{{$product->id}}"> Rs. {{$total}} </sapn><a style="cursor: pointer;" onclick="deleteCartProduct({{$ids[$j]}})"><i class="fa fa-close"></i></a></td>
                            </tr>
                            @endfor

                        </tbody>
                    </table>
                    @else

                    <div class="sattement">
                        <h2>Nothing in Cart</h2>
                    </div>
                    @endif
                </div>
                <div class="col-md-4">
                    <h3 class="block-title"><span>Shopping cart</span></h3>
                    <div class="shopping-cart">
                        <style>
                            table tr td { padding: 20px }
                        </style>
                        <table>
                            <tr>
                                <td>Sub-total:</td>
                                <td>Rs. <span id="total_price">{{$bill}}</span></td>
                            </tr>
{{--                                         <tr>
                                            <td>Shipping:</td>
                                            <td>$25</td>
                                        </tr>
                                        --}}                                        <tfoot>                                            
                                            <tr>
                                                <td>Total:</td>
                                                <td>Rs. <span id="total-price">{{$bill}}</span></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>                
                        <div class="overflowed">
                            <a class="btn btn-theme btn-theme-dark" href="/en">Home Page</a>
                            <a class="btn btn-theme pull-right" href="/checkout">Checkout</a>
                        </div>
                    </div>
                </section>
                <!-- /PAGE -->

                <!-- PAGE -->
                <section class="page-section">
                    <div class="container">
                        <div class="row blocks shop-info-banners">
                            <div class="col-md-4">
                                <div class="block">
                                    <div class="media">
                                        <div class="pull-right"><i class="fa fa-gift"></i></div>
                                        <div class="media-body">
                                            <h4 class="media-heading">Buy 1 Get 1</h4>
                                            Proin dictum elementum velit. Fusce euismod consequat ante.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="block">
                                    <div class="media">
                                        <div class="pull-right"><i class="fa fa-comments"></i></div>
                                        <div class="media-body">
                                            <h4 class="media-heading">Call to Free</h4>
                                            Proin dictum elementum velit. Fusce euismod consequat ante.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="block">
                                    <div class="media">
                                        <div class="pull-right"><i class="fa fa-trophy"></i></div>
                                        <div class="media-body">
                                            <h4 class="media-heading">Money Back!</h4>
                                            Proin dictum elementum velit. Fusce euismod consequat ante.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- /PAGE -->

            </div>
            <!-- /CONTENT AREA -->



            <script>



		//modal  box of quick view
		function openview(id,name,price,mrp,description,img1,img2,img3,rate){
			$('#hidden_id').val(id);    
			$('#viewname').text(name);
			$('#viewdescription').text(description);
			$('#viewprice').text(price);
    //For main image
    $("#imggallery1").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#imggallery2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);

    //for a tag (data-zoom attribute)    
    $("#imgdz2").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#imgdz3").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
    $("#imgdz1").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);

    //for a tag (data-images attribute)
    $("#imgdz1").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#imgdz2").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#imgdz3").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);

    //for image tag (src attribute)
    $("#img1").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#img2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#img3").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
    $("#imgt1").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#imgt2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#imgt3").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
    var discount = ((mrp-price)*100)/mrp;
    $('#viewdiscount').text(discount.toFixed(2));
    $('#viewrate').val(rate);

}



function increaseQty(id){
    var pro_quant = $('#increase_qty'+id).val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');    
    var cat_id = id;
    countproduct++;
    $.ajax({
        /* the route pointing to the post function */
        url: '/add-cart',
        type: 'POST',
        /* send the csrf-token and the input to the controller */
        dataType: 'JSON',
        data: {_token: CSRF_TOKEN, id: id, quantity: pro_quant},
        success: function (data) { 
            // window.location.href = '/productdetail/'+cat_id;
            $('#increase_qty'+id).empty();
            $('#increase_qty'+id).val(data.quant);
            $('#total-amt'+id).empty();
            $('#total-amt'+id).text('Rs. '+data.total);
            $('#total_price').empty();
            $('#total_price').text(data.subTotal);
            $('#total-price').empty();
            $('#total-price').text(+data.subTotal);

            // $("#ajaxdata").html(data);
        }
    }); 
}


function decreaseQty(id){
    var pro_quant = $('#increase_qty'+id).val();
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');    
    var cat_id = id;
    countproduct++;
    $.ajax({
        /* the route pointing to the post function */
        url: '/decrease-quantity',
        type: 'POST',
        /* send the csrf-token and the input to the controller */
        dataType: 'JSON',
        data: {_token: CSRF_TOKEN, id: id, quantity: pro_quant},
        success: function (data) { 
            // window.location.href = '/productdetail/'+cat_id;
            $('#increase_qty'+id).empty();
            $('#increase_qty'+id).val(data.quant);
            $('#total-amt'+id).empty();
            $('#total-amt'+id).text('Rs. '+data.total);
            $('#total_price').empty();
            $('#total_price').text(data.subTotal);
            $('#total-price').empty();
            $('#total-price').text(+data.subTotal);

            // $("#ajaxdata").html(data);
        }
    }); 
}


var countproduct=<?php if(session()->get('count') != null){echo session()->get('count'); } else { echo 0;} ?>;

function addToCart(){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
	var pro_id = $('#hidden_id').val();
	var quant = $('#quickquantity').val();
	countproduct++;
	$.ajax({
		/* the route pointing to the post function */
		url: '/add-cart',
		type: 'POST',
		/* send the csrf-token and the input to the controller */
		data: {_token: CSRF_TOKEN, id: pro_id, quantity: quant},
		success: function (data) { 
			window.location.href = '/cart'; 
			$("#show-total").html(countproduct);           
		}
	}); 
}



var count=<?php echo session()->get('count'); ?>;
function deleteCartProduct(id){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
	count--;
	$('#cart'+id).hide();

	$.ajax({
		/* the route pointing to the post function */
		url: '/cart/delete-product/id',
		type: 'POST',
		datatype: 'JSON',
		/* send the csrf-token and the input to the controller */
		data: {_token: CSRF_TOKEN, id: id},
		success: function (data) { 
			// alert('data deleted successfully');
            window.location.href ='/cart' 
            $("#show-total").html(count);
            $("#total_price").empty();		
            $("#total_price").html(data.bill);	
            $("#total-price").empty();      
            $("#total-price").html(data.bill);	
            $("#grand_total").html(data.bill);		

            

					// $('#ajaxTable').load(document.URL + ' #ajaxTable');
					// $('#refresh-div').load(document.URL + ' #refresh-div');	
				}
			}); 
}

function increment(id,quant){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

			// $('#refresh-div').load(document.URL + ' #refresh-div');
			// $('#total-bill').load(document.URL + ' #total-bill');
			$.ajax({
				/* the route pointing to the post function */
				url: '/cart/increase/id/quant',
				type: 'POST',
				datatype: 'JSON',
				/* send the csrf-token and the input to the controller */
				data: {_token: CSRF_TOKEN, id: id, quant: quant},
				success: function (data) { 
					// alert(data);
					$('#quantity').html(data.quant);
					$('#refresh-div').load(document.URL + ' #refresh-div');
				}
			});
		}

		function decrement(id,quant){
			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
			// $('#refresh-div').load(document.URL + ' #refresh-div');
			// $('#total-bill').load(document.URL + ' #total-bill');
			$.ajax({
				/* the route pointing to the post function */
				url: '/cart/decrease/id/quant',
				type: 'POST',
				
				data: {_token: CSRF_TOKEN, id: id, quant: quant},
				success: function (data) { 
					// alert('decremented');
				// 	alert(data);
               $('#refresh-div').load(document.URL + ' #refresh-div');
           }
       });
		}

		//adding items to Wishlist
		function addWishList(id,count){
			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

			$.ajax({
				/* the route pointing to the post function */
				url: '/add-to-wishlist',
				type: 'POST',
				/* send the csrf-token and the input to the controller */
				data: {_token: CSRF_TOKEN, id: id},
				success: function (data) { 
					window.location.href = '/cart';
					$("#ajaxdata").text(count);
				}
			}); 
		}
	</script>

	@endsection