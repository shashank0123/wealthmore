<?php
use App\Product;
use App\MainCategory;
use App\Banner;
use App\Testimonial;
use App\Review;
$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
$total = 0;
?>

<div class="row products grid" id="productCat">
    <?php $flag = 0;?>
    @if($searched_products != null)
    @foreach($searched_products as $product)
    <?php $flag++; ?>
    <div class="col-md-4 col-sm-6">
        <div class="thumbnail no-border no-padding">
            <div class="media">
                <a class="media-link" href="/productdetail/{{$product->id}}">
                    <img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}" alt="{{$product->image}}" style="height: 350px;"  />
                    <span class="icon-view">
                        <strong><i class="fa fa-eye"></i></strong>
                    </span>
                </a>
            </div>
            <div class="caption text-center">
                <h4 class="caption-title"><a href="/productdetail/{{$product->id}}">{{$product->name}}</a></h4>
                
                <h5>JBV :  {{$product->jbv}}</h5>
                <h5>RBV :  {{$product->rvv}}</h5>

                <?php
                $rate = Review::where('product_id',$product->id)->avg('rating');
                $final_rate = floor($rate);
                if($final_rate == 0){
                    $final_rate = 4;
                }
                ?>
                <div class="rating">
                    @for($i=0 ; $i<5 ; $i++)
                    @if($final_rate>0)
                    <span class="star active"></span>
                    @else
                    <span class="star"></span>
                    @endif
                    <?php $final_rate--; ?>
                    @endfor
                </div>
                <div class="price"><ins>{{$product->sell_price}}</ins> {{-- <del>$425.00</del> --}}</div>
                <div class="buttons">
                    <a class="btn btn-theme btn-theme-transparent btn-wish-list" href="#" onclick="addWishList({{ $product->id }})"><i class="fa fa-heart"></i></a>
                    {{-- <a class="btn btn-theme btn-theme-transparent btn-icon-left" href="#" onclick="addCart({{$product->id}})"><i class="fa fa-shopping-cart"></i>Add to Cart</a> --}}
                    <a class="btn btn-theme btn-theme-transparent btn-icon-left" href="/productdetail/{{$product->id}}">{{-- <i class="fa fa-shopping-cart"></i> --}}View Detail</a>
                    {{-- --><a class="btn btn-theme btn-theme-transparent btn-compare" href="#"><i class="fa fa-exchange"></i></a> --}}
                </div>
            </div>
        </div>
    </div> 
    @endforeach
    @endif

    @if($flag == 0)
    <div class="content" style="text-align: center; margin-top: 100px">
        <h2 style="color: #000">No Products Available</h2>
    </div>

    @endif
</div>