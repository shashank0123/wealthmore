<?php
use App\MainCategory;
use App\Review;
use App\Product;
use App\Banner;
$count = Review::where('product_id',$getproduct->id)->count();
$bill = 0;
$i=0;

if($count>0){
	$rate = Review::where('product_id',$getproduct->id)->avg('rating');
	$final_rate = floor($rate);
}
else
	$final_rate = 0;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{$getproduct->page_title}}</title>

   <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/wealth-assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="shortcut icon" href="/wealth-assets/img/favicon.png">

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <!-- CSS Global -->
    <link href="/wealth-assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/wealth-assets/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="/wealth-assets/plugins/fontawesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="/wealth-assets/plugins/prettyphoto/css/prettyPhoto.css" rel="stylesheet">
    <link href="/wealth-assets/plugins/owl-carousel2/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="/wealth-assets/plugins/owl-carousel2/assets/owl.theme.default.min.css" rel="stylesheet">
    <link href="/wealth-assets/plugins/animate/animate.min.css" rel="stylesheet">
    <link href="/wealth-assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
        <link href="/wealth-assets/plugins/countdown/jquery.countdown.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="/wealth-assets/css/theme.css" rel="stylesheet">
    <link href="/wealth-assets/css/theme-green-1.css" rel="stylesheet" id="theme-config-link">

    <!-- Head Libs -->
    <script src="/wealth-assets/plugins/modernizr.custom.js"></script>

    <![endif]-->
    <style>
        #themeConfig { display: none !important; }
        .top-bar { background-color: #394263 !important;}
        .sign-color { color: #000 !important; font-weight: bold; }
        .sf-menu li.active { background-color: #394263; }
        .fixed-css { height: 600px }
    </style>
</head>
<body id="home" class="wide">
    <!-- PRELOADER -->
    <div id="preloader">
        <div id="preloader-status">
            <div class="spinner">
                <div class="rect1"></div>
                <div class="rect2"></div>
                <div class="rect3"></div>
                <div class="rect4"></div>
                <div class="rect5"></div>
            </div>
            <div id="preloader-title">Loading</div>
        </div>
    </div>
    <!-- /PRELOADER -->

    <!-- WRAPPER -->
    <div class="wrapper">
        <input type="hidden" name="cat_id" id="cat_id" value="{{$id}}">

        <!-- Popup: Shopping cart items -->
        <div class="modal fade popup-cart" id="popup-cart" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="container">
                    <div class="cart-items">
                        <div class="cart-items-inner" id=showminicart>
                            @if(session()->get('cart') != null)
                            <?php $i=0; ?>
                            @foreach($cartproducts as $product)
                            <div class="media">
                                <a class="pull-left" href="/productdetail/{{$product->id}}"><img class="media-object item-image" src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}" alt="{{$product->name}}" style="width: 70px; height: 70px"></a>
                                <p class="pull-right item-price">Rs. {{$product->sell_price}}</p>
                                <div class="media-body">
                                    <h4 class="media-heading item-title"><a href="/productdetail/{{$product->id}}">{{$quantity[$i]}}x {{$product->name}}</a></h4>
                                    <p class="item-desc">Lorem ipsum dolor</p>
                                </div>
                            </div>
                            @endforeach
                            @endif
                            <div class="media">
                                <p class="pull-right item-price" id="bill"></p>
                                <div class="media-body">
                                    <h4 class="media-heading item-title summary">Subtotal</h4>
                                </div>
                            </div>
                            <div class="media">
                                <div class="media-body">
                                    <div>
                                        <a href="#" class="btn btn-theme btn-theme-dark" data-dismiss="modal">Close</a>
                                        <a href="<?php if(!empty($member->id)) { echo '/cart';} else { echo '/en/login'; } ?>" class="btn btn-theme btn-theme-transparent btn-call-checkout">Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Popup: Shopping cart items -->

        <!-- Header top bar -->
        <div class="top-bar">
            <div class="container">
                <div class="top-bar-left">
                    <ul class="list-inline">
                        @if(empty($member->id))
                        <li class="icon-user"><a href="/en/login"><img src="/wealth-assets/img/icon-1.png" alt=""/> <span>Login</span></a></li>
                        <li class="icon-form"><a href="/register"><img src="/wealth-assets/img/icon-2.png" alt=""/> <span>Not a Member? <span class="sign-color">Sign Up</span></span></a></li>
                        @endif
                        <li><a href="mailto:info@backstagesupporters.com"><i class="fa fa-envelope"></i> <span>info@backstagesupporters.com</span></a></li>
                    </ul>
                </div>
                <div class="top-bar-right">
                    <ul class="list-inline">

                       @if(!empty($member->id))                           
                       <li class="hidden-xs"><a href="/en/member">My Account</a></li>
                       <li class="hidden-xs"><a href="/wishlist">My Wishlist</a></li>
                       @endif
                       <li class="hidden-xs"><a href="/about">About</a></li>
                       <li class="hidden-xs"><a href="/contact-us">Contact</a></li>
                       <li class="hidden-xs"><a href="/faq">FAQ</a></li>
                       @if(!empty($member->id))
                       <li class="hidden-xs"><a href="/en/logout">Logout</a></li>
                       @endif

                   </ul>
               </div>
           </div>
       </div>
       <!-- /Header top bar -->

       <!-- HEADER -->
       <header class="header fixed">
        <div class="header-wrapper">
            <div class="container">

                <!-- Logo -->
                <div class="logo">
                    <a href="/"><img src="/wealth-assets/img/logo-bella-shop.png" alt="WealthMore" style="width: 235px; height: 45px; top: -10px; position: absolute;" /></a>
                </div>
                <!-- /Logo -->

                <!-- Header search -->
                <div class="header-search">
                    <input class="form-control" type="text" placeholder="What are you looking?"/>
                    <button><i class="fa fa-search"></i></button>
                </div>
                <!-- /Header search -->

                <!-- Header shopping cart -->
                <div class="header-cart">
                    <div class="cart-wrapper">
                        <a href="/wishlist" class="btn btn-theme-transparent hidden-xs hidden-sm"><i class="fa fa-heart"></i></a>
                        {{-- <a href="compare-products.html" class="btn btn-theme-transparent hidden-xs hidden-sm"><i class="fa fa-exchange"></i></a> --}}
                        <a href="#" class="btn btn-theme-transparent" data-toggle="modal" data-target="#popup-cart" onclick="showCart()"><i class="fa fa-shopping-cart"></i> 
                            <?php
                            $a=0;
                            if (session()->get('cart')!=null)
                                foreach(session()->get('cart') as $data)
                                {                               
                                    $a++;
                                }                   
                                $show=session()->get('count');
                                ?>

                                @if(session()->get('count') != null)
                                <span class="hidden-xs" id="show-total">{{$show}}</span>
                                @else
                                <span class="hidden-xs" id="show-total">0</span>
                                @endif
                            </span><span>item(s) </span> <i class="fa fa-angle-down"></i></a>
                            <!-- Mobile menu toggle button -->
                            <a href="#" class="menu-toggle btn btn-theme-transparent"><i class="fa fa-bars"></i></a>
                            <!-- /Mobile menu toggle button -->
                        </div>
                    </div>
                    <!-- Header shopping cart -->

                </div>
            </div>
            <div class="navigation-wrapper">
                <div class="container">
                    <!-- Navigation -->
                    <nav class="navigation closed clearfix">
                        <a href="#" class="menu-toggle-close btn"><i class="fa fa-times"></i></a>
                        <ul class="nav sf-menu">
                            <li class="active"><a href="/en/shop">Home</a></li>

                            @foreach($maincategory as $row)                               

                            <li class="megamenu"><a href="/products/{{$row->id}}">{{$row->Mcategory_name}}</a>
                                <ul>
                                    <li class="row">
                                        <?php 
                                        $category = MainCategory::where('category_id',$row->id)->where('status','Active')->get();
                                        ?>
                                        @foreach($category as $rows)
                                        <div class="col-md-3">
                                            <h4 class="block-title"><a href="/products/{{$rows->id}}"><span>{{$rows->Mcategory_name}}</span></a></h4>
                                            <ul>
                                                <?php
                                                $subcategory = MainCategory::where('category_id',$rows->id)->where('status','Active')->get();
                                                ?>
                                                @foreach($subcategory as $col)
                                                <li><a href="/products/{{$col->id}}">{{$col->Mcategory_name}}</a></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endforeach                                   


                                    </li>
                                </ul>
                            </li>
                            @endforeach

                        </ul>
                    </nav>
                    <!-- /Navigation -->
                </div>
            </div>
        </header>
        <!-- /HEADER -->



        <!-- CONTENT AREA -->
        <div class="content-area">

            <!-- PAGE -->
            <section class="page-section">
                <div class="container">

                    <div class="row product-single">
                        <div class="col-md-6">
                            <div class="badges">
                            {{-- <div class="hot">hot</div>
                            <div class="new">new</div> --}}
                        </div>
                        <div class="owl-carousel img-carousel">
                            <div class="item">
                                <a class="btn btn-theme btn-theme-transparent btn-zoom" href="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image1 }}" data-gal="prettyPhoto"><i class="fa fa-plus"></i></a>
                                <a href="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image1 }}" data-gal="prettyPhoto"><img class="img-responsive fixed-css" src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image1 }}" alt=""/></a></div>
                                <div class="item">
                                    <a class="btn btn-theme btn-theme-transparent btn-zoom" href="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image2 }}" data-gal="prettyPhoto"><i class="fa fa-plus"></i></a>
                                    <a href="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image2 }}" data-gal="prettyPhoto"><img class="img-responsive fixed-css" src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image2 }}" alt=""/></a></div>
                                    <div class="item">
                                        <a class="btn btn-theme btn-theme-transparent btn-zoom" href="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image3 }}" data-gal="prettyPhoto"><i class="fa fa-plus"></i></a>
                                        <a href="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image3 }}" data-gal="prettyPhoto"><img class="img-responsive fixed-css" src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image3 }}" alt=""/></a></div>
                                        <div class="item">
                                            <a class="btn btn-theme btn-theme-transparent btn-zoom" href="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image2 }}" data-gal="prettyPhoto"><i class="fa fa-plus"></i></a>
                                            <a href="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image2 }}" data-gal="prettyPhoto"><img class="img-responsive fixed-css" src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image2 }}" alt=""/></a></div>
                                        </div>
                                        <div class="row product-thumbnails">
                                            <div class="col-xs-2 col-sm-2 col-md-3"><a href="#" onclick="jQuery('.img-carousel').trigger('to.owl.carousel', [0, 300]);"><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image1 }}" alt="" style=" height: 120px;"/></a></div>
                                            <div class="col-xs-2 col-sm-2 col-md-3"><a href="#" onclick="jQuery('.img-carousel').trigger('to.owl.carousel', [1, 300]);"><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image2 }}" alt="" style=" height: 120px;"/></a></div>
                                            <div class="col-xs-2 col-sm-2 col-md-3"><a href="#" onclick="jQuery('.img-carousel').trigger('to.owl.carousel', [2, 300]);"><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image3 }}" alt="" style=" height: 120px;"/></a></div>
                                            <div class="col-xs-2 col-sm-2 col-md-3"><a href="#" onclick="jQuery('.img-carousel').trigger('to.owl.carousel', [3, 300]);"><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image2 }}" alt="" style=" height: 120px;"/></a></div>
                                        </div>
                                    </div>
                                    <?php 
                                    $rate = Review::where('product_id',$getproduct->id)->avg('rating');
                                    $final_rate = floor($rate);
                                    if($final_rate == 0){
                                        $final_rate = -1;
                                    }
                                    else { $final_rate = $final_rate - 5; }
                                    ?>
                                    <div class="col-md-6">
                                        {{-- <div class="back-to-category">
                                            <span class="link"><i class="fa fa-angle-left"></i> Back to <a href="category.html">Category</a></span>
                                            <div class="pull-right">
                                                <a class="btn btn-theme btn-theme-transparent btn-previous" href="#"><i class="fa fa-angle-left"></i></a>
                                                <a class="btn btn-theme btn-theme-transparent btn-next" href="#"><i class="fa fa-angle-right"></i></a>
                                            </div>
                                        </div> --}}
                                        <h2 class="product-title">{{$getproduct->name}}</h2>
                                        <div class="product-rating clearfix">
                                            <div class="rating">
                                                @for($i=0 ; $i<5 ; $i++)
                                                @if($final_rate>=0)
                                                <span class="star active"></span>
                                                @else
                                                <span class="star"></span>
                                                @endif
                                                <?php $final_rate++; ?>
                                                @endfor
                                            </div>
                                            <a class="reviews" href="#">{{$count_review}} reviews</a>{{--  | <a class="add-review" href="#">Add Your Review</a> --}}
                                        </div>
                                        {{-- <div class="product-availability">Availability: <strong>In stock</strong> 21 Item(s)</div> --}}
                                        <hr class="page-divider small"/>

                                        <div class="product-price">MRP - Rs. {{$getproduct->mrp}}</div>
                                        <div style="font-size: 32px !important">DP &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Rs. {{$getproduct->sell_price}}</div>
                                        <hr class="page-divider"/>

                                        <div class="product-text">
                                            <p>{{$getproduct->short_descriptions}}</p>
                                        </div>
                                        <hr class="page-divider"/>
                                        <div class="product-text">
                                            <h4> <b>Purchase Value </b>(Select One)</h4>
                                            <input type="hidden" id="purchase-value" name="purchaseValue">
                                            <p><input type="radio" value="jbv" name="purchase_value" onchange="handler1('jbv')"> &nbsp;&nbsp;&nbsp;JBV :  {{$getproduct->jbv}}</p>
                                            <p><input type="radio" value="rbv" name="purchase_value" onchange="handler2('rbv')"> &nbsp;&nbsp;&nbsp;RBV  :  {{$getproduct->rvv}}</p>
                                        </div>
                                        <hr class="page-divider"/>
                                        <form action="#" class="row variable">
                                {{-- <div class="col-sm-6">
                                    <div class="form-group selectpicker-wrapper">
                                        <label for="exampleSelect1">Size</label>
                                        <select
                                        id="exampleSelect1"
                                        class="selectpicker input-price" data-live-search="true" data-width="100%"
                                        data-toggle="tooltip" title="Select">
                                        <option>Select Your Size</option>
                                        <option>Size 1</option>
                                        <option>Size 2</option>
                                    </select>
                                </div>
                            </div> --}}
                            {{-- <div class="col-sm-6">
                                <div class="form-group selectpicker-wrapper">
                                    <label for="exampleSelect2">Color</label>
                                    <select
                                    id="exampleSelect2"
                                    class="selectpicker input-price" data-live-search="true" data-width="100%"
                                    data-toggle="tooltip" title="Select">
                                    <option>Select Your Color</option>
                                    <option>Color 1</option>
                                    <option>Color 2</option>
                                </select>
                            </div>
                        </div> --}}
                    </form>
                    <hr class="page-divider small"/>


                    <div class="buttons">
                        <div class="quantity">
                            <button class="btn" onclick="decrement({{$getproduct->id}})"><i class="fa fa-minus"></i></button>
                            <input class="form-control qty" id="qty" type="number" step="1" min="1" name="quantity" value="1" title="Qty" style="width: 100px; text-align: center;">
                            <button class="btn" onclick="increment({{$getproduct->id}})"><i class="fa fa-plus"></i></button>
                        </div>
                        <button class="btn btn-theme btn-cart btn-icon-left" type="submit" onclick="addToCart({{$getproduct->id}})"><i class="fa fa-shopping-cart" ></i>Add to cart</button>
                        <button class="btn btn-theme btn-wish-list" onclick = "addWishList({{$getproduct->id}})"><i class="fa fa-heart"></i></button>
                        {{-- <button class="btn btn-theme btn-compare"><i class="fa fa-exchange"></i></button> --}}
                    </div>

                    <hr class="page-divider small"/>
                    <?php
                    $category = Product::where('products.id',$id)->leftjoin('main_categories','main_categories.id','=','products.category_id')->select('main_categories.Mcategory_name')->first();
                    ?>
                    <table>
                        <tr>
                            <td class="title">Category:</td>
                            <td>{{$category->Mcategory_name}}</td>
                        </tr>
                        {{-- <tr>
                            <td class="title">Product Code:</td>
                            <td>PS08</td>
                        </tr> --}}
                        <tr>
                            <td class="title">Tags:</td>
                            <td>{{$getproduct->page_keywords}}</td>
                        </tr>
                    </table>
                    <hr class="page-divider small"/>

                    <ul class="social-icons list-inline">
                        <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#" class="pinterest"><i class="fa fa-youtube"></i></a></li>
                    </ul>

                </div>
            </div>
        </div>
    </section>
    <!-- /PAGE -->

    <!-- PAGE -->
    <section class="page-section md-padding">
        <div class="container">
            <div class="row blocks shop-info-banners">
                <div class="col-md-4">
                    <div class="block">
                        <div class="media">
                            <div class="pull-right"><i class="fa fa-gift"></i></div>
                            <div class="media-body">
                                <h4 class="media-heading">Buy 1 Get 1</h4>
                                Proin dictum elementum velit. Fusce euismod consequat ante.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="block">
                        <div class="media">
                            <div class="pull-right"><i class="fa fa-comments"></i></div>
                            <div class="media-body">
                                <h4 class="media-heading">Call to Free</h4>
                                Proin dictum elementum velit. Fusce euismod consequat ante.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="block">
                        <div class="media">
                            <div class="pull-right"><i class="fa fa-trophy"></i></div>
                            <div class="media-body">
                                <h4 class="media-heading">Money Back!</h4>
                                Proin dictum elementum velit. Fusce euismod consequat ante.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /PAGE -->

    <!-- PAGE -->
    <section class="page-section">
        <div class="container">
            <div class="tabs-wrapper content-tabs">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#item-description" data-toggle="tab">Item Description</a></li>
                    <li><a href="#reviews" data-toggle="tab">Reviews ({{$count_review}})</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="item-description">
                        <p><?php echo htmlspecialchars_decode($getproduct->long_descriptions) ?></p>
                    </div>
                    <div class="tab-pane fade" id="reviews">

                        <div class="comments">
                            @foreach($reviews as $review)
                            <div class="media comment">
                                <a href="#" class="pull-left comment-avatar">
                                    <img alt="" src="/wealth-assets/img/preview/avatars/avatar-1.jpg" class="media-object">
                                </a>
                                <div class="media-body">
                                    <p class="comment-meta"><span class="comment-author"><a href="#">{{$review->name}}</a> <span class="comment-date"> {{$review->created_at}} <i class="fa fa-flag"></i></span></span></p>
                                    <p class="comment-text">{{$review->review}}</p>
                                </div>
                            </div>
                            @endforeach                         
                        </div>
                        <div class="comments-form">
                            <h4 class="block-title">Add a Review</h4>
                            <form method="post" action="/product-detail/{{ $getproduct->id }}" name="commentform" id="commentform">
                                <div class="form-group"><input type="text" placeholder="Your name and surname" class="form-control" title="comments-form-name" name="name"></div>
                                <div class="form-group"><input type="text" placeholder="Your email adress" class="form-control" title="comments-form-email" name="email"></div>
                                <div class="col-sm-4 col-xs-12">
                                    <label class="controls">
                                        <span class="lbl">Rating <span class="required">*</span></span>
                                        <div class="form-group">

                                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

                                            <script>
                                                $(function() {
                                                    $("div.star > s").on("click", function(e) {

                                                                    // remove all active classes first, needed if user clicks multiple times
                                                                    $(this).closest('div').find('.active').removeClass('active');

                                                                    $(e.target).parentsUntil("div").addClass('active'); // all elements up from the clicked one excluding self
                                                                    $(e.target).addClass('active');  // the element user has clicked on


                                                                    var numStars = $(e.target).parentsUntil("div").length+1;
                                                                    $('.show-result input').val(numStars );
                                                                });
                                                });
                                            </script>

                                            <div class="star"><s><s><s><s><s></s></s></s></s></s></div>
                                            <div class="show-result">
                                                <input type="hidden" name="rating" id="rating" >
                                            </div>

                                        </div>
                                    </label>
                                </div>
                                <div class="form-group"><textarea placeholder="Your message" class="form-control" title="comments-form-comments" name="comments-form-comments" rows="6"></textarea></div>
                                <div class="form-group"><button type="submit" class="btn btn-theme btn-theme-transparent btn-icon-left" id="submit"><i class="fa fa-comment"></i> Review</button></div>
                            </form>
                        </div>
                        <!-- // -->

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /PAGE -->

    <!-- PAGE -->
    <section class="page-section">
        <div class="container">
            <h2 class="section-title section-title-lg"><span>Related Products</span></h2>
            <div class="featured-products-carousel">
                <div class="owl-carousel" id="featured-products-carousel">                             

                    @if($products != null)
                    @foreach($products as $relatedproduct)
                    <?php
                    $category = Maincategory::where('id',$relatedproduct->category_id)->first();
                    ?>
                    <?php $discount=(($relatedproduct->mrp - $relatedproduct->sell_price)*100)/$relatedproduct->mrp;

                    $rate = Review::where('product_id',$relatedproduct->id)->avg('rating');
                    $final_rate = floor($rate);
                    if($final_rate == 0){
                        $final_rate = -1;
                    }
                    else { $final_rate = $final_rate - 5; }
                    ?>
                    <div class="thumbnail no-border no-padding">
                        <div class="media">
                            <a class="media-link" href="/productdetail/{{$relatedproduct->id}}">
                                <img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $relatedproduct->image1 }}" alt="{{$relatedproduct->name}}" style="height: 250px; width: 200px" />
                                <span class="icon-view">
                                    <strong><i class="fa fa-eye"></i></strong>
                                </span>
                            </a>
                        </div>
                        <div class="caption text-center">
                            <h4 class="caption-title">{{$relatedproduct->name}}</h4>
                            
                            <h5>JBV :  {{$relatedproduct->jbv}}</h5>
                            <h5>RBV :  {{$relatedproduct->rvv}}</h5>
                            <div class="rating">
                                @for($i=0 ; $i<5 ; $i++)
                                @if($final_rate>=0)
                                <span class="star active"></span>
                                @else
                                <span class="star"></span>
                                @endif
                                <?php $final_rate++; ?>
                                @endfor
                            </div>
                            <div class="price"><ins>Rs. {{$relatedproduct->sell_price}}</ins> {{-- <del>$425.00</del> --}}</div>
                            <div class="buttons">
                                <a class="btn btn-theme btn-theme-transparent btn-wish-list" href="#" onclick = "addWishList({{$relatedproduct->id}})"><i class="fa fa-heart"></i></a>
                                {{-- <a class="btn btn-theme btn-theme-transparent btn-icon-left" href="#" onclick="addCart({{$relatedproduct->id}})"><i class="fa fa-shopping-cart"></i>Add to Cart</a> --}}
                                <a class="btn btn-theme btn-theme-transparent btn-icon-left" href="/productdetail/{{$relatedproduct->id}}">{{-- <i class="fa fa-shopping-cart"></i> --}}View Detail</a>
                                {{-- <a class="btn btn-theme btn-theme-transparent btn-compare" href="#"><i class="fa fa-exchange"></i></a> --}}
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif
                </div>
            </div>
            <hr class="page-divider half"/>
            {{-- <a class="btn btn-theme btn-view-more-block" href="#" style="max-width: 100%;">View More</a> --}}
        </div>
    </section>
    <!-- /PAGE -->

    <?php
    $brands = Banner::where('image_type','brand')->where('status','Active')->get();
    ?>

    @if(isset($brands))
    <!-- PAGE -->
    <section class="page-section">
        <div class="container">
            <h2 class="section-title"><span>Brand &amp; Clients</span></h2>
            <div class="partners-carousel">
                <div class="owl-carousel" id="partners">
                    @foreach($brands as $brand)
                    <div><a href="#"><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/banner/{{ $brand->image_url }} " alt=""/></a></div>
                    @endforeach

                </div>
            </div>
        </div>
    </section>
    @endif
    <!-- /PAGE -->

</div>
<!-- /CONTENT AREA -->

<!-- FOOTER -->
<footer class="footer">
    <div class="footer-widgets">
        <div class="container">
            <div class="row">

                <div class="col-md-3">
                    <div class="widget">
                        <h4 class="widget-title">About Us</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sollicitudin ultrices suscipit. Sed commodo vel mauris vel dapibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <ul class="social-icons">
                            <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" class="instagram"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="widget">
                        <h4 class="widget-title">News Letter</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <form action="#">
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="Enter Your Mail and Get $10 Cash"/>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-theme btn-theme-transparent">Subscribe</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="widget widget-categories">
                        <h4 class="widget-title">Information</h4>
                        <ul>
                            <li><a href="/about">About Us</a></li>
                            <li><a href="#">Delivery Information</a></li>
                            <li><a href="/contact-us">Contact Us</a></li>
                            <li><a href="/tnc">Terms and Conditions</a></li>
                            <li><a href="/privacypolicy">Private Policy</a></li>
                        </ul>
                    </div>
                </div>
                <?php
                $subcat = array();
                $subid = array();
                $sub_i = 0;
                $maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
                foreach($maincategory as $main){                            
                    $category = MainCategory::where('category_id',$main->id)->where('status','Active')->get();

                    if($category != null){
                        foreach($category as $cat){
                            $subcategory = MainCategory::where('category_id',$cat->id)->where('status','Active')->get();
                            if($subcategory != null){
                                foreach($subcategory as $sub){
                                    $subcat[$sub_i] = $sub->Mcategory_name;
                                    $subid[$sub_i] = $sub->id;
                                    $sub_i++;
                                }
                            }
                        }
                    }
                }
                ?>
                <div class="col-md-3">
                    <div class="widget widget-tag-cloud">
                        <h4 class="widget-title">Item Tags</h4>
                        <ul>
                            @if($subcat != null)
                            @for($j=0 ; $j<10 ; $j++)                                    
                            <li><a href="/products/{{$subid[$j]}}">{{$subcat[$j]}}</a></li>
                            @endfor
                            @endif                                    
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="footer-meta">
        <div class="container">
            <div class="row">

                <div class="col-sm-6">
                    <div class="copyright">Copyright 2019 Backstage Supporters  |   All Rights Reserved   |   Designed By Me</div>
                </div>
                <div class="col-sm-6">
                    <div class="payments">
                        <ul>
                            <li><img src="/wealth-assets/img/preview/payments/visa.jpg" alt=""/></li>
                            <li><img src="/wealth-assets/img/preview/payments/mastercard.jpg" alt=""/></li>
                            <li><img src="/wealth-assets/img/preview/payments/paypal.jpg" alt=""/></li>
                            <li><img src="/wealth-assets/img/preview/payments/american-express.jpg" alt=""/></li>
                            <li><img src="/wealth-assets/img/preview/payments/visa-electron.jpg" alt=""/></li>
                            <li><img src="/wealth-assets/img/preview/payments/maestro.jpg" alt=""/></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
</footer>
<!-- /FOOTER -->

<div id="to-top" class="to-top"><i class="fa fa-angle-up"></i></div>

</div>
<!-- /WRAPPER -->

<!-- JS Global -->
    <script src="/wealth-assets/plugins/jquery/jquery-1.11.1.min.js"></script>
    <script src="/wealth-assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="/wealth-assets/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
    <script src="/wealth-assets/plugins/superfish/js/superfish.min.js"></script>
    <script src="/wealth-assets/plugins/prettyphoto/js/jquery.prettyPhoto.js"></script>
    <script src="/wealth-assets/plugins/owl-carousel2/owl.carousel.min.js"></script>
    <script src="/wealth-assets/plugins/jquery.sticky.min.js"></script>
    <script src="/wealth-assets/plugins/jquery.easing.min.js"></script>
    <script src="/wealth-assets/plugins/jquery.smoothscroll.min.js"></script>
    <script src="/wealth-assets/plugins/smooth-scrollbar.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.js"></script>

    
    <!-- JS Page Level -->
    <script src="/wealth-assets/plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="/wealth-assets/plugins/countdown/jquery.plugin.min.js"></script>
    <script src="/wealth-assets/plugins/countdown/jquery.countdown.min.js"></script>
    <script src="/wealth-assets/js/theme.js"></script>

    <!--[if (gte IE 9)|!(IE)]><!-->
    <script src="/wealth-assets/plugins/jquery.cookie.js"></script>
    <script src="/wealth-assets/js/theme-config.js"></script>
    <!--<![endif]-->

<!-- Popup: Shopping cart items -->
<div class="modal fade popup-cart" id="popup-cart" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="container">
            <div class="cart-items">
                <div class="cart-items-inner">
                    <div class="media">
                        <a class="pull-left" href="#"><img class="media-object item-image" src="/wealth-assets/img/preview/shop/order-1s.jpg" alt=""></a>
                        <p class="pull-right item-price">Rs. 450.00</p>
                        <div class="media-body">
                            <h4 class="media-heading item-title"><a href="#">1x Standard Product</a></h4>
                            <p class="item-desc">Lorem ipsum dolor</p>
                        </div>
                    </div>
                    <div class="media">
                        <p class="pull-right item-price">$450.00</p>
                        <div class="media-body">
                            <h4 class="media-heading item-title summary">Subtotal</h4>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-body">
                            <div>
                                            <a href="#" class="btn btn-theme btn-theme-dark" data-dismiss="modal">Close</a><!--
                                        --><a href="<?php if(!empty($member->id)){ echo '/cart'; } else { echo '/en/login';} ?>" class="btn btn-theme btn-theme-transparent btn-call-checkout">Cart</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Popup: Shopping cart items -->



        <script>
           var countproduct=<?php if(session()->get('count') != null){echo session()->get('count'); } else { echo 0;} ?>;

           function addQuickCart(){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var pro_id = $('#hidden_id').val();
            var quant = $('#quickquantity').val();
            var cat_id = $('#cat_id').val();
            countproduct++;
            $.ajax({
             /* the route pointing to the post function */
             url: '/add-cart',
             type: 'POST',
             /* send the csrf-token and the input to the controller */
             data: {_token: CSRF_TOKEN, id: pro_id, quantity: quant},
             success: function (data) { 
              window.location.href = '/productdetail/'+cat_id;
              $("#show-total").html(countproduct);           
          }
      }); 
        }

        function openview(id,name,price,mrp,description,img1,img2,img3,rate){         
            $('#hidden_id').val(id);
            $('#viewname').text(name);
            $('#viewdescription').text(description);
            $('#viewprice').text(price);
            $('#rating').text(rate);
    //For main image
    $("#imggallery1").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#imggallery2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);

    //for a tag (data-zoom attribute)    
    $("#imgdz2").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#imgdz3").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
    $("#imgdz1").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);

    //for a tag (data-images attribute)
    $("#imgdz1").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#imgdz2").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#imgdz3").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);

    //for image tag (src attribute)
    $("#img1").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#img2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#img3").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
    $("#imgt1").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#imgt2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#imgt3").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
    var discount = ((mrp-price)*100)/mrp;
    $('#viewdiscount').text(discount.toFixed(2));
    $('#viewrate').val(rate);

} 


function deleteProduct(id){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');			

	$('#listhide'+id).hide();
	$.ajax({
		/* the route pointing to the post function */
		url: '/cart/delete-product/id',
		type: 'POST',
		/* send the csrf-token and the input to the controller */				
		data: {_token: CSRF_TOKEN, id: id},
		success: function (data) { 
			$("#show-total").html(data.showcount);			
			$("#bill").html(data.bill);			
		}
	}); 
}

function handler1(value) { 
    var valu = value;
    alert(valu); 
    $('#purchase-value').val(valu);
}

function handler2(value) { 
    var valu = value;
    alert(valu); 
    
    $('#purchase-value').val(valu);
}


//adding items to cart
function addToCart(id){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var pro_quant = $('#qty').val();
    var purchase = $('#purchase-value').val();
    var cat_id = $('#cat_id').val();

    if(pro_quant != ""  && purchase != ""){
        alert(pro_quant+","+purchase);

    countproduct++;
    $.ajax({
        /* the route pointing to the post function */
        url: '/add-cart',
        type: 'POST',
        /* send the csrf-token and the input to the controller */
        dataType: 'JSON',
        data: {_token: CSRF_TOKEN, id: id, quantity: pro_quant,money_type : purchase},
        success: function (data) { 
            window.location.href = '/productdetail/'+cat_id;
            $('#show-total').html(countproduct);
            // $("#ajaxdata").html(data);
        }
    });
    }
    else{
        Swal('Purchase Value & Quantity both are mendatory to add product to the cart');
    }
     
}


//adding items to cart
function addCart(id){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
	var pro_quant = $('#qty').val();
	var cat_id = $('#cat_id').val();
	countproduct++;
	$.ajax({
		/* the route pointing to the post function */
		url: '/add-cart',
		type: 'POST',
		/* send the csrf-token and the input to the controller */
		dataType: 'JSON',
		data: {_token: CSRF_TOKEN, id: id, quantity: pro_quant},
		success: function (data) { 
			window.location.href = '/productdetail/'+cat_id;
			$('#show-total').html(countproduct);
			// $("#ajaxdata").html(data);
		}
	}); 
}


function showCart(){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
	$.ajax({
		/* the route pointing to the post function */
		url: '/cart/show',
		type: 'POST',
		/* send the csrf-token and the input to the controller */					
		data: {_token: CSRF_TOKEN},
		success: function (data) {			
			$('#showminicart').html(data);
			$("#bill").text(data.bill);			
		}
	}); 
}

function increment(id){
	var quant = $('#qty').val();
    // alert(quant);
    
        quant++;
    $('#qty').empty();
    $('#qty').val(quant);
    
}

function decrement(id){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    var quant = $('#qty').val();
    // alert(quant);
    if(quant>0){
        quant--;
    $('#qty').empty();
    $('#qty').val(quant);
    }
   
}


//adding items to Wishlist
function addWishList(id){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
	var cat_id = $('#cat_id').val();
	$.ajax({
		/* the route pointing to the post function */
		url: '/add-to-wishlist',
		type: 'POST',
		/* send the csrf-token and the input to the controller */
		dataType: 'JSON',
		data: {_token: CSRF_TOKEN, id: id},
		success: function (data) { 
			window.location.href = '/productdetail/'+cat_id;
			$("#ajaxdata").html(data);
		}
	}); 
}
</script>



</body>
    </html>