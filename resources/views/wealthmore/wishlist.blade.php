<?php
use App\Product;
use App\MainCategory;
use App\Review;
?>




@extends('layouts.wealth-more')
@section('content')
<!-- CONTENT AREA -->
<div class="content-area">

    <!-- BREADCRUMBS -->
    <section class="page-section breadcrumbs">
        <div class="container">
            <div class="page-header">
                <h1>Wishlist</h1>
            </div>
                        {{-- <ul class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Shop</a></li>
                            <li class="active">Shopping Cart</li>
                        </ul> --}}
                    </div>
                </section>
                <!-- /BREADCRUMBS -->

                <!-- PAGE -->
                <section class="page-section color no-padding-bottom">
                    <div class="container">

                        <div class="row wishlist">
                            <div class="col-md-9">

                                @if(session()->get('wishlist') != null)

                                <?php
                                $i=0;
                                $ids = array();                 
                                $quantities = array();

                                foreach(session()->get('wishlist') as $data)
                                {
                                    $ids[$i] = $data->product_id;                               
                                    $quantities[$i] = $data->quantity;
                                    $i++;
                                }                       
                                ?>

                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th>Product Name</th>
                                            <th>Price</th>                                      
                                            <th>JBV</th>
                                            <th>RBV</th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @for($j=0 ; $j<$i ; $j++)
                                        <?php
                                        $product = Product::where('id',$ids[$j])->first();
                                        $category = MainCategory::where('id',$product->category_id)->first();
                                        
                                        ?>
                                        <tr id="hide{{$product->id}}">
                                            <td class="image"><a class="media-link" href="#"><i class="fa fa-plus"></i><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}" alt="{{$product->name}}" style="width: 100px; height: 100px" /></a></td>
                                            <td class="description">
                                                <h4><a href="/productdetail/{{$product->id}}">{{$product->name}}</a></h4>
                                                {{$category->Mcategory_name}}
                                            </td>
                                            
                                            <td class="price">Rs. {{$product->sell_price}}</td>

                                            <td class="price">
                                                {{$product->jbv}}
                                            </td>
                                            <td class="price">
                                                {{$product->rvv}}
                                            </td>
                                            <td class="add">
                                                {{-- <a class="btn btn-theme btn-theme-dark btn-icon-left" href="#" onclick="addCart({{$product->id}})"><i class="fa fa-shopping-cart"></i> Add to cart</a> --}}
                                                <a class="btn btn-theme btn-theme-transparent btn-icon-left" href="/productdetail/{{$product->id}}">{{-- <i class="fa fa-shopping-cart"></i> --}}View Detail</a>
                                            </td>
                                            <td class="total"><a href="#" onclick="deleteWishlistProduct({{$product->id}})"><i class="fa fa-close"></i></a></td>
                                        </tr>
                                        @endfor
                                        
                                        
                                    </tbody>
                                </table>

                                @else
                                <div class="container" id="no-result">
                                    <h2>Nothing in Your Wishlist</h2>
                                </div>

                                @endif
                                <a class="btn btn-theme btn-theme-transparent btn-icon-left btn-continue-shopping" href="/en/shop"><i class="fa fa-shopping-cart"></i>Continue shopping</a>
                            </div>
                            <div class="col-md-3">
                                <h3 class="block-title"><span>Login</span></h3>
                                <form action="#" class="form-sign-in">
                                    <div class="row">
                                        <div class="col-md-12 hello-text-wrap">
                                            <span class="hello-text text-thin">Hello, welcome to your account</span>
                                        </div>
                                        <div class="col-md-12">
                                            <a class="btn btn-theme btn-block btn-icon-left facebook" href="#"><i class="fa fa-facebook"></i>Sign in with Facebook</a>
                                        </div>
                                        <div class="col-md-12">
                                            <a class="btn btn-theme btn-block btn-icon-left twitter" href="#"><i class="fa fa-twitter"></i>Sign in with Twitter</a>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group"><input class="form-control" type="text" placeholder="User name or email"></div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group"><input class="form-control" type="password" placeholder="Your password"></div>
                                        </div>
                                        <div class="col-md-12 col-lg-6">
                                            <div class="checkbox">
                                                <label><input type="checkbox"> Remember me</label>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-6 text-right-lg">
                                            <a class="forgot-password" href="#">forgot password?</a>
                                        </div>
                                        <div class="col-md-12">
                                            <a class="btn btn-theme btn-block btn-theme-dark" href="#">Login</a>
                                        </div>
                                        <div class="col-md-12">
                                            <a class="btn btn-theme btn-block btn-theme-transparent" href="#">Create account</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- /PAGE -->

                <!-- PAGE -->
                <section class="page-section">
                    <div class="container">
                        <div class="row blocks shop-info-banners">
                            <div class="col-md-4">
                                <div class="block">
                                    <div class="media">
                                        <div class="pull-right"><i class="fa fa-gift"></i></div>
                                        <div class="media-body">
                                            <h4 class="media-heading">Buy 1 Get 1</h4>
                                            Proin dictum elementum velit. Fusce euismod consequat ante.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="block">
                                    <div class="media">
                                        <div class="pull-right"><i class="fa fa-comments"></i></div>
                                        <div class="media-body">
                                            <h4 class="media-heading">Call to Free</h4>
                                            Proin dictum elementum velit. Fusce euismod consequat ante.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="block">
                                    <div class="media">
                                        <div class="pull-right"><i class="fa fa-trophy"></i></div>
                                        <div class="media-body">
                                            <h4 class="media-heading">Money Back!</h4>
                                            Proin dictum elementum velit. Fusce euismod consequat ante.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- /PAGE -->

            </div>
            <!-- /CONTENT AREA -->
            @endsection

            @section('script')
            <script>

var countproduct=<?php if(session()->get('count') != null){echo session()->get('count'); } else { echo 0;} ?>;


//adding items to cart
function addCart(id){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    countproduct++;
    $.ajax({
        /* the route pointing to the post function */
        url: '/add-to-cart',
        type: 'POST',
        /* send the csrf-token and the input to the controller */
        data: {_token: CSRF_TOKEN, id: id},
        success: function (data) { 
            window.location.href = '/wishlist';
            $("#show-total").html(countproduct);           
        }
    }); 
}

              function deleteWishlistProduct(id){
                 var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                 $('#hide'+id).hide();
                 $.ajax({
                    /* the route pointing to the post function */
                    url: '/wishlist/delete-product/id',
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */
                    data: {_token: CSRF_TOKEN, id: id},
                    success: function (data) { 
					// alert('data deleted successfully');
					// $('#refresh-wishlist').load(document.URL + ' #refresh-wishlist');	
				}
			}); 
             }

             function clearWishList(){
			// alert('enter to confirm');
			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

			$.ajax({
				/* the route pointing to the post function */
				url: '/wishlist/delete-all',
				type: 'POST',
				/* send the csrf-token and the input to the controller */				
				success: function (data) { 
					// alert('data deleted successfully');
					$('#no-result').show();
					$('#refresh-wishlist').load(document.URL + ' #refresh-wishlist');	
				}
			});
		}

		function addAll(){
			// alert('enter to confirm');
			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

			$.ajax({
				/* the route pointing to the post function */
				url: '/wishlist/add-all',
				type: 'POST',
				/* send the csrf-token and the input to the controller */				
				success: function (data) { 
					alert('data added successfully');
					$('#no-result').show();
					$('#refresh-wishlist').load(document.URL + ' #refresh-wishlist');	
				}
			});
		}
	</script>
	@endsection