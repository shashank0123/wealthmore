<?php
$postdata =$_POST;
use App\MainCategory;
use App\Product;
use App\OrderItem;

$maincategory = MainCategory::where('category_id',0)->get();

$cartproducts = array();
$quantity = array();
$i = 0;
if(session()->get('cart') != null){
	foreach(session()->get('cart') as $cart){
		$cartproducts[$i] = Product::where('id',$cart->product_id)->first();
		$quantity[$i] = $cart->quantity;
		$i++;
	}
}
$count=1;
?>
@extends('layouts.wealth-more')

@section('content')
<style>
	.success-top{ text-align: center;margin: 50px 50px; }
	.success-top h1 { text-align: center; color: #37BC9B; font-weight: bold;font-size:  72px;}
	.success-top p { text-align: center; font-weight: bold;font-size:  18px;}
	.success-bottom { border: 1px solid #ddd; background-color: #efe; margin-bottom: 50px }
	.success-bottom .table { margin: 20px 0px; border: none; }
	#table-cont { width: 90% }
	.table>tbody>tr>td { padding: 20px 10px; }
	.table tr td{ padding: 20px 10px; }
	.success-bottom h4 { margin-left: 50px; text-align: center; font-weight: bold; color: #aaa; }
	
</style>

<div class="container">
	<div class="row">		
		<div class="success-top">
			<h1>Thank You</h1>
			<p><strong>Payment Received</strong></p>
			<h6><strong>Transaction Id : </strong>{{$postdata['txnid']}}</h6>
		</div>
		<div class="success-bottom "><br>
			<h4>Payment History</h4><br>
			<div class="container" id="table-cont">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>Order Detail </th>
							<th>Status</th>
							<th>Payment Method</th>
							<th>Transaction Id</th>
							<th>Total Payable Amount</th>
							<th>Date</th>

						</tr>
					</thead>

					<tbody>
						@foreach($transaction_details as $detail)

						<?php $products = OrderItem::where('created_at',$detail->created_at)->get(); ?>
						<tr>
							<td><?php echo $count++.". " ?>
								
								@foreach($products as $product)
								<?php $name=Product::where('id',$product->item_id)->first();
								?>
								<?php echo $name->name.". " ?>
								@endforeach		

							</td>
							<td>{{$detail->status}}</td>
							<td><?php echo $detail->payment_method;?></td>
							<td>{{$detail->transaction_id}}</td>
							<td>Rs. {{$detail->price}}</td>
							<td>Rs. {{$detail->created_at}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>		
			</div>	
		</div>
	</div>
</div>

@endsection

