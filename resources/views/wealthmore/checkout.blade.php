<?php
use App\Product;
use App\Address;
use App\Country;
use App\State;
use App\City;
if(!empty($member->id)){
	$address = Address::where('user_id',$member->id)->first();
}


$i=0;
$total = 0;
?>

@extends('layouts.wealth-more')

@section('content')

<style>
	.page-large-title { padding: 20px 0; color: #37bc9b;}
</style>

<!-- BREADCRUMBS -->
<section class="page-section breadcrumbs">
	<div class="container">
		<div class="page-header">
			<h1>Checkout</h1>
		</div>

	</div>
</section>
<!-- /BREADCRUMBS -->

<section class="page-section color">
	<div class="container">
		

		<form action="<?php if(!empty($member->id)){ echo '/checkout-page/'.$member->id ;} else {echo '/en/login'; } ?>" method="POST" class="checkout woocommerce-checkout" id="myForm">
		<div class="row">
			{{csrf_field()}}
			<div class="col-md-8 col-xs-12 section-last">			
				<div class="woocommerce-billing-fields">
					<h2 class="section-title small-spacing">Billing Infomation</h2>
					<div class="clear"></div>
					<div class="row">
						<div class="col-md-12">
							<label for="address">Address</label>
							<div class="form-group"><input class="form-control" name="address" id="address" type="text" placeholder="Address" value="<?php if(!empty($address)){echo $address->address;}?>"></div>
						</div>

						<?php $country = Country::all();?>
						
						<?php $states=State::all();?>
						<div class="col-md-12">
							<div class="form-group">
								<label for="state">State</label>
								<select class="form-control" name="state_id" id="state_id" required="required">
									@if(empty($address->state_id))
									<option>Select State</option> 
									@else
									@foreach($states as $state)
									<option value="{{$state->id}}"<?php if($address->state_id == $state->id){echo 'selected';} ?>> {{$state->state}} </option>
									@endforeach
									@endif 
									{{-- <option value="<?php if(!empty($address)){ echo $address->state_id;} else { echo "" ;}?>"><?php if(!empty($address)){ $state=State::where('id',$address->state_id)->first();
								echo $state->state;} else { echo "";} ?></option> --}}
							</select>
							</div>
						</div>
						<?php $cities=City::all();?>
						<div class="col-md-12">
							<div class="form-group">
								<label for="city">City</label>
								<select class="form-control" name="city_id" id="city_id" required="required">
									@if(empty($address->city_id))
									<option>Select City</option> 
									@else
									@foreach($cities as $city)
									<option value="{{$city->id}}"<?php if($address->city_id == $city->id){echo 'selected';} ?>> {{$city->city}} </option>
									@endforeach
									@endif 
									{{-- <option value="<?php if(!empty($address)){ echo $address->city_id;} else { echo "" ;}?>"><?php if(!empty($address)){ $city=City::where('id',$address->city_id)->first();
								echo $city->city;} else { echo "";} ?></option> --}}
							</select>
							</div>
						</div>

						<div class="col-md-12">
							<label for="pin_code">PIN Code</label>
							<div class="form-group"><input class="form-control" name="pin_code" id="pin_code" type="text" placeholder="PIN Code" value="<?php if(!empty($address)){echo $address->pin_code;}?>"></div>
						</div>

						<div class="col-md-12">
							<label for="phone-number">Mobile Number</label>
							<div class="form-group"><input class="form-control" name="phone" id="phone-number" type="text" placeholder="Contact Number" value="<?php if(!empty($address)){echo $address->phone;}?>"></div>
						</div>												
					</div><!-- .row -->
				</div><!-- .woocommerce-billing-fields --><br>
				
<input type="button" name="reset" class="btn btn-alert " value="Change Address" onclick="resetForm()">
				

			</div><!-- .col -->
			<div class="col-md-4 col-xs-12 section-last">
				<h3 class="block-title alt"><i class="fa fa-angle-down"></i> Payments options</h3>
				@if(session()->get('cart') != null)

							<?php

							$ids = array();
							$cat_id = array();					
							$quantities = array();


							foreach(session()->get('cart') as $data)
							{
								$ids[$i] = $data->product_id;								
								$quantities[$i] = $data->quantity;
								$i++;
							}						
							?>
							@for($j=0 ; $j<$i ; $j++ )
							<?php
							$product = Product::where('id',$ids[$j])->first(); 
							$cost = $product->sell_price * $quantities[$j];
							$total = $total + $cost;

							?>

							
							@endfor
							@endif	

							<input type="hidden" value="{{$total}}" name="total_bill">
                <div class="panel-group payments-options" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel radio panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapseOne">
                                    <span class="dot"></span> Direct Bank Transfer
                                </a>
                            </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
                            <div class="panel-body">
                                <div class="alert alert-success" role="alert">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sollicitudin ultrices suscipit. Sed commodo vel mauris vel dapibus.</div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapseTwo">
                                    <span class="dot"></span> Cheque Payment
                                </a>
                            </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                            <div class="panel-body">
                                Please send your cheque to Store Name, Store Street, Store Town, Store State / County, Store Postcode.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapseThree">
                                    <span class="dot"></span> Credit Card
                                </a>
                                <span class="overflowed pull-right">
                                    <img src="/wealth-assets/img/preview/payments/mastercard-2.jpg" alt=""/>
                                    <img src="/wealth-assets/img/preview/payments/visa-2.jpg" alt=""/>
                                    <img src="/wealth-assets/img/preview/payments/american-express-2.jpg" alt=""/>
                                    <img src="/wealth-assets/img/preview/payments/discovery-2.jpg" alt=""/>
                                    <img src="/wealth-assets/img/preview/payments/eheck-2.jpg" alt=""/>
                                </span>
                            </h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3"></div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading4">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                    <span class="dot"></span> PayUMoney
                                </a>
                                <span class="overflowed pull-right"><img src="/wealth-assets/img/preview/payments/paypal-2.jpg" alt=""/></span>
                            </h4>
                        </div>
                        <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4"></div>
                    </div>
                </div>

                <div class="overflowed">
                    <a class="btn btn-theme btn-theme-dark" href="/en/shop">Home Page</a>
                    <button class="btn btn-theme pull-right" href="/checkout-page/{{$member->id}}">Place Your Order</button>
                </div>
				</div><!-- .col -->
			</div><!-- .row -->
		</form><!-- .woocommerce-checkout -->



	</div>
</section>

@endsection

@section('script')
<script>
	$(document).ready(function() {
			$('#country_id').on('change', function() {
				var countryID = $(this).val();
				if(countryID) {
					$.ajax({
						url: '/state/'+countryID,
						type: "GET",
						data : {"_token":"{{ csrf_token() }}",country_id:countryID},
						dataType: "json",
						success:function(data) {                       
							if(data){

								$('#state_id').empty();
								$('#state_id').focus;
								$('#state_id').append('<option value="">-- Select state_id --</option>'); 
								$.each(data, function(key, value){
									$('select[name="state_id"]').append('<option value="'+ value.id +'">' + value.state+ '</option>');
								});
							}else{
								$('#state_id').empty();
							}
						}
					});
				}else{
					$('#state_id').empty();
				}
			});
		});



		$(document).ready(function() {
			$('#state_id').on('change', function() {
				var stateID = $(this).val();
				if(stateID) {
					$.ajax({
						url: '/city/'+stateID,
						type: "GET",
						data : {"_token":"{{ csrf_token() }}",state_id:stateID},
						dataType: "json",
						success:function(data) {                       
							if(data){

								$('#city_id').empty();
								$('#city_id').focus;
								$('#city_id').append('<option value="">-- Select City --</option>'); 
								$.each(data, function(key, value){
									$('select[name="city_id"]').append('<option value="'+ value.id +'">' + value.city+ '</option>');
								});
							}else{
								$('#city_id').empty();
							}
						}
					});
				}else{
					$('#city_id').empty();
				}
			});
		});

		function resetForm(){
			$('input[type=text]').val("");
			$('input[type=number]').val("");
			// $('#state_id').empty();
			// $('#city_id').empty();
			// $('#country_id').empty();
			// document.getElementById('myForm').reset();
		}

</script>
@endsection