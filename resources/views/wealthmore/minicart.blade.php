<?php
use App\Review;
$total = 0;
?>

<div class="cart-items-inner" id=showminicart>
	@if(session()->get('cart') != null)
	<?php $i=0;?>
	@foreach($cartproducts as $product)
	
	<div class="media" id="listhide{{$product->id}}">
		<a class="pull-left" href="/productdetail/{{$product->id}}"><img class="media-object item-image" src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}" alt="{{$product->name}}" style="width: 70px; height: 70px"></a>
		<p class="pull-right item-price">Rs. {{$quantity[$i]*$product->sell_price}}</p>
		<div class="media-body">
			<h4 class="media-heading item-title"><a href="/productdetail/{{$product->id}}">{{$quantity[$i]}}x {{$product->name}}</a></h4>
			{{-- <p class="item-desc">Lorem ipsum dolor</p> --}}
		</div>
	</div>
	<?php $i++ ?>
	@endforeach
	@endif
	<div class="media">
		<p class="pull-right item-price" id="bill">Rs. {{$bill}}</p>
		<div class="media-body">
			<h4 class="media-heading item-title summary">Subtotal</h4>
		</div>
	</div>
	<div class="media">
		<div class="media-body">
			<div>
				<a href="#" class="btn btn-theme btn-theme-dark" data-dismiss="modal">Close</a>
				<a href="<?php if(!empty($member->id)){ echo '/cart';} else{ echo '/en/login'; }?>" class="btn btn-theme btn-theme-transparent btn-call-checkout">Cart</a>
			</div>
		</div>
	</div>
</div>

	
	