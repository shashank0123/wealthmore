@extends('layouts.wealth-more')

<?php
use App\Product;
use App\MainCategory;
use App\Banner;
use App\Testimonial;
use App\Review;
$maincategory = MainCategory::where('category_id',0)->get();

?>

@section('content')


<!-- CONTENT AREA -->
<div class="content-area">

    <!-- BREADCRUMBS -->
    <section class="page-section breadcrumbs">
        <div class="container">
            <div class="page-header">
                <h1>Category Products</h1>
            </div>
                        {{-- <ul class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Shop</a></li>
                            <li class="active">Category Grid View Page With Left Sidebar</li>
                        </ul> --}}
                    </div>
                </section>
                <!-- /BREADCRUMBS -->

                <!-- PAGE WITH SIDEBAR -->
                <section class="page-section with-sidebar">
                    <div class="container">
                        <div class="row">
                            <!-- SIDEBAR -->
                            <aside class="col-md-3 sidebar" id="sidebar">
                                <!-- widget search -->
                                <div class="widget">
                                    {{-- <div class="widget-search">
                                        <form action="/searchproduct" method="GET" class="searchform">
                                            <input class="form-control" name="product_keyword" type="text" placeholder="Search">
                                            <button type="submit" name="search"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div> --}}
                                    <!-- /widget search -->
                                    <!-- widget shop categories -->
                                    <div class="widget shop-categories">
                                        <h4 class="widget-title">Categories</h4>
                                        <div class="widget-content" style="max-height: 300px; overflow: auto">
                                            <ul>


                                                <?php $count=Product::where('status','Active')->count();
                                                $i=0; ?>
                                                <li id="cat0" value="0">
                                                    <span class="arrow">{{$count}}</span>
                                                    <a style="cursor: pointer;">All</a>                                        
                                                </li>

                                                @foreach($maincategory as $row)

                                                <li id="cat{{$row->id}}" value="{{$row->id}}">
                                                    {{-- <span class="arrow">{{$total[$i++]}}</span> --}}
                                                    <a style="cursor: pointer;">{{$row->Mcategory_name}}</a>                                        
                                                </li>
                                                @endforeach



                                            {{-- <li>
                                                <span class="arrow"><i class="fa fa-angle-down"></i></span>
                                                <a href="#">Man</a>
                                                <ul class="children">
                                                    <li>
                                                        <a href="#">Sweaters & Knits
                                                            <span class="count">12</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Jackets & Coats
                                                            <span class="count">12</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Denim
                                                            <span class="count">12</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Pants
                                                            <span class="count">12</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Shorts
                                                            <span class="count">12</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li> --}}
                                            
                                            {{-- <li><a href="#">Sale Off</a></li> --}}
                                        </ul>
                                    </div>
                                </div>
                                <!-- /widget shop categories -->
                                <!-- widget product color -->
                                {{-- <div class="widget widget-colors">
                                    <h4 class="widget-title">Colors</h4>
                                    <div class="widget-content">
                                        <ul>
                                            <li><a href="#"><span style="background-color: #ffffff"></span></a></li>
                                            <li><a href="#"><span style="background-color: #161618"></span></a></li>
                                            <li><a href="#"><span style="background-color: #e74c3c"></span></a></li>
                                            <li><a href="#"><span style="background-color: #783ce7"></span></a></li>
                                            <li><a href="#"><span style="background-color: #3498db"></span></a></li>
                                            <li><a href="#"><span style="background-color: #00a847"></span></a></li>
                                            <li><a href="#"><span style="background-color: #3ce7d9"></span></a></li>
                                            <li><a href="#"><span style="background-color: #fa17bc"></span></a></li>
                                            <li><a href="#"><span style="background-color: #a87e00"></span></a></li>
                                        </ul>
                                    </div>
                                </div> --}}
                                <!-- /widget product color -->
                                <!-- widget price filter -->
                                <div class="widget widget-filter-price">
                                    <h4 class="widget-title">Price</h4>
                                    <div class="widget-content">
                                        <div id="slider-range"></div>
                                        <input type="text" id="amount" />
                                        <button class="btn btn-theme">Filter</button>
                                    </div>
                                </div>
                                <!-- /widget price filter -->
                                <!-- widget tabs -->
                                <div class="widget widget-tabs">
                                    <div class="widget-content">
                                        <ul id="tabs" class="nav nav-justified">
                                            <li><a href="#tab-s1" data-toggle="tab">Top</a></li>
                                            {{-- <li class="active"><a href="#tab-s2" data-toggle="tab">Sale Off</a></li> --}}
                                            {{-- <li><a href="#tab-s3" data-toggle="tab">Deals</a></li> --}}
                                        </ul>
                                        <div class="tab-content">
                                            <!-- tab 1 -->
                                            <?php
                                            $top = Product::orderBy('updated_at','DESC')->first();
                                            ?>
                                            <div class="tab-pane fade in active" id="tab-s1">
                                                <div class="product-list">
                                                    <div class="media">
                                                        <a class="pull-left media-link" href="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $top->image1 }} ">
                                                            <img class="media-object" src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $top->image1 }} " alt="{{$top->name}}" style="height: 300px">
                                                            <i class="fa fa-plus"></i>
                                                        </a>
                                                        <div class="media-body">
                                                            <h4 class="media-heading"><a href="/productdetail/{{$top->id}}">{{$top->name}}</a></h4>

                                                            <?php
                                                            $rate = Review::where('product_id',$top->id)->avg('rating');
                                                            $final_rate = floor($rate)+1;
                                                            if($final_rate == 0){
                                                                $final_rate = 4;
                                                            }
                                                            ?>
                                                            <div class="rating">
                                                             @for($i=0 ; $i<5 ; $i++)
                                                             @if($final_rate>0)
                                                             <span class="star active"></span>
                                                             @else
                                                             <span class="star"></span>
                                                             @endif
                                                             <?php $final_rate--; ?>
                                                             @endfor
                                                         </div>
                                                     </div>
                                                     <div class="price"><ins>Rs. {{$top->sell_price}}</ins> {{-- <del>$425.00</del> --}}</div>
                                                 </div>
                                             </div>


                                         </div>
                                     </div>

                                     <!-- tab 2 -->
                                        {{-- <div class="tab-pane fade in active" id="tab-s2">
                                            <div class="product-list">
                                                <div class="media">
                                                    <a class="pull-left media-link" href="#">
                                                        <img class="media-object" src="/wealth-assets/img/preview/shop/top-sellers-4.jpg" alt="">
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                    <div class="media-body">
                                                        <h4 class="media-heading"><a href="#">Standard Product Header</a></h4>
                                                        <div class="rating">
                                                                <span class="star"></span><!--
                                                                --><span class="star active"></span><!--
                                                                --><span class="star active"></span><!--
                                                                --><span class="star active"></span><!--
                                                            --><span class="star active"></span>
                                                        </div>
                                                        <div class="price"><ins>$400.00</ins> <del>$425.00</del></div>
                                                    </div>
                                                </div>
                                                <div class="media">
                                                    <a class="pull-left media-link" href="#">
                                                        <img class="media-object" src="/wealth-assets/img/preview/shop/top-sellers-5.jpg" alt="">
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                    <div class="media-body">
                                                        <h4 class="media-heading"><a href="#">Standard Product Header</a></h4>
                                                        <div class="rating">
                                                                <span class="star"></span><!--
                                                                --><span class="star active"></span><!--
                                                                --><span class="star active"></span><!--
                                                                --><span class="star active"></span><!--
                                                            --><span class="star active"></span>
                                                        </div>
                                                        <div class="price"><ins>$400.00</ins> <del>$425.00</del></div>
                                                    </div>
                                                </div>
                                                <div class="media">
                                                    <a class="pull-left media-link" href="#">
                                                        <img class="media-object" src="/wealth-assets/img/preview/shop/top-sellers-6.jpg" alt="">
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                    <div class="media-body">
                                                        <h4 class="media-heading"><a href="#">Standard Product Header</a></h4>
                                                        <div class="rating">
                                                                <span class="star"></span><!--
                                                                --><span class="star active"></span><!--
                                                                --><span class="star active"></span><!--
                                                                --><span class="star active"></span><!--
                                                            --><span class="star active"></span>
                                                        </div>
                                                        <div class="price"><ins>$400.00</ins> <del>$425.00</del></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> --}}

                                        <!-- tab 3 -->
                                        {{-- <div class="tab-pane fade" id="tab-s3">
                                            <div class="product-list">
                                                <div class="media">
                                                    <a class="pull-left media-link" href="#">
                                                        <img class="media-object" src="/wealth-assets/img/preview/shop/top-sellers-7.jpg" alt="">
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                    <div class="media-body">
                                                        <h4 class="media-heading"><a href="#">Standard Product Header</a></h4>
                                                        <div class="rating">
                                                                <span class="star"></span><!--
                                                                --><span class="star active"></span><!--
                                                                --><span class="star active"></span><!--
                                                                --><span class="star active"></span><!--
                                                            --><span class="star active"></span>
                                                        </div>
                                                        <div class="price"><ins>$400.00</ins> <del>$425.00</del></div>
                                                    </div>
                                                </div>
                                                <div class="media">
                                                    <a class="pull-left media-link" href="#">
                                                        <img class="media-object" src="/wealth-assets/img/preview/shop/top-sellers-8.jpg" alt="">
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                    <div class="media-body">
                                                        <h4 class="media-heading"><a href="#">Standard Product Header</a></h4>
                                                        <div class="rating">
                                                                <span class="star"></span><!--
                                                                --><span class="star active"></span><!--
                                                                --><span class="star active"></span><!--
                                                                --><span class="star active"></span><!--
                                                            --><span class="star active"></span>
                                                        </div>
                                                        <div class="price"><ins>$400.00</ins> <del>$425.00</del></div>
                                                    </div>
                                                </div>
                                                <div class="media">
                                                    <a class="pull-left media-link" href="#">
                                                        <img class="media-object" src="/wealth-assets/img/preview/shop/top-sellers-9.jpg" alt="">
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                    <div class="media-body">
                                                        <h4 class="media-heading"><a href="#">Standard Product Header</a></h4>
                                                        <div class="rating">
                                                                <span class="star"></span><!--
                                                                --><span class="star active"></span><!--
                                                                --><span class="star active"></span><!--
                                                                --><span class="star active"></span><!--
                                                            --><span class="star active"></span>
                                                        </div>
                                                        <div class="price"><ins>$400.00</ins> <del>$425.00</del></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> --}}
                                    </div>
                                    {{-- <a class="btn btn-theme btn-theme-dark btn-theme-sm btn-block" href="#">More Products</a> --}}
                                </div>

                                <!-- /widget tabs -->
                                <!-- widget tag cloud -->
                            {{-- <div class="widget widget-tag-cloud">
                                <a class="btn btn-theme btn-title-more" href="#">See All</a>
                                <h4 class="widget-title"><span>Tags</span></h4>
                                <ul>
                                    <li><a href="#">Fashion</a></li>
                                    <li><a href="#">Jeans</a></li>
                                    <li><a href="#">Top Sellers</a></li>
                                    <li><a href="#">E commerce</a></li>
                                    <li><a href="#">Hot Deals</a></li>
                                    <li><a href="#">Supplier</a></li>
                                    <li><a href="#">Shop</a></li>
                                    <li><a href="#">Theme</a></li>
                                    <li><a href="#">Website</a></li>
                                    <li><a href="#">Isamercan</a></li>
                                    <li><a href="#">Themeforest</a></li>
                                </ul>
                            </div> --}}
                            <!-- /widget tag cloud -->

                            <?php             
                            $top_products = DB::table( 'products' )
                            ->join( 'reviews', 'reviews.product_id', '=', 'products.id' )
                            ->where( 'products.status', 'Active' )
                            ->groupBy( 'products.id' )
                            ->select( 'products.id', DB::raw( 'AVG( rating )' ) )->orderBy('AVG( rating )','DESC')
                            ->limit(3)->get();
                            ?>

                            <!-- widget products carousel -->
                            <div class="widget">
                                <a class="btn btn-theme btn-title-more" href="#">See All</a>
                                <h4 class="widget-title"><span>Top products</span></h4>
                                <div class="sidebar-products-carousel">
                                    <div class="owl-carousel" id="sidebar-products-carousel">

                                        @if($top_products != null)
                                        @foreach($top_products as $product)
                                        <?php
                                        $top = Product::where('id',$product->id)->first();
                                        ?>
                                        <div class="thumbnail no-border no-padding">
                                            <div class="media">
                                                <a class="media-link" href="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $top->image1 }}">
                                                    <img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $top->image1 }}" alt=""/>
                                                    <span class="icon-view">
                                                        <strong><i class="fa fa-eye"></i></strong>
                                                    </span>
                                                </a>
                                            </div>
                                            <div class="caption text-center">
                                                <h4 class="caption-title">{{$top->name}}</h4>

                                                <?php 
                                                $rate = Review::where('product_id',$top->id)->avg('rating');
                                                $count_reviews = Review::where('product_id',$top->id)->count();
                                                $final_rate = floor($rate);
                                                if($final_rate == 0){
                                                    $final_rate = 4;
                                                }
                                                ?>

                                                <div class="rating">
                                                    @for($i=0 ; $i<5 ; $i++)
                                                    @if($final_rate>0)
                                                    <span class="star active"></span>
                                                    @else
                                                    <span class="star"></span>
                                                    @endif
                                                    <?php $final_rate--; ?>
                                                    @endfor
                                                </div>
                                                <div class="price"><ins>Rs. {{$top->sell_price}}</ins>{{--  <del>$425.00</del> --}}</div>
                                                <div class="buttons">
                                                    <a class="btn btn-theme btn-theme-transparent btn-wish-list" href="#"><i class="fa fa-heart"></i></a>
                                                    {{-- <a class="btn btn-theme btn-theme-transparent btn-icon-left" href="#" onclick="addCart({{$top->id}})"><i class="fa fa-shopping-cart"></i>Add to Cart</a> --}}
                                                    <a class="btn btn-theme btn-theme-transparent btn-icon-left" href="/productdetail/{{$top->id}}">{{-- <i class="fa fa-shopping-cart"></i> --}}View Detail</a>
                                                    {{-- <a class="btn btn-theme btn-theme-transparent btn-compare" href="#"><i class="fa fa-exchange"></i></a> --}}
                                                </div>
                                            </div>
                                        </div>
                                        
                                        @endforeach
                                        @endif

                                    </div>
                                </div>
                            </div>
                            <!-- /widget products carousel -->

                            <?php
                            $hot_deal = Product::where('status','Active')->orderBy('created_at','DEsc')->orderBy('sell_price','DESC')->limit(3)->get();
                            ?>
                            <!-- widget shop hot deals -->
                            <div class="widget widget-shop-deals">
                                <a class="btn btn-theme btn-title-more" href="#">See All</a>
                                <h4 class="widget-title"><span>Hot Deals</span></h4>
                                <div class="hot-deals-carousel">
                                    <div class="owl-carousel" id="hot-deals-carousel">
                                        @foreach($hot_deal as $deal)
                                        <div class="thumbnail thumbnail-hot-deal no-border no-padding">
                                            <div class="media">
                                                <a class="media-link" href="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $deal->image1 }}" alt="{{$deal->name}}" style="height: 350px"">
                                                <img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $deal->image1 }}" alt="{{$deal->name}}" style="height: 350px" />
                                                <span class="icon-view"><strong><i class="fa fa-eye"></i></strong></span>
                                                <div class="countdown-wrapper">
                                                    <div id="dealCountdown1" class="defaultCountdown clearfix"></div>
                                                </div>
                                            </a>
                                        </div>

                                        <?php 
                                        $rate = Review::where('product_id',$deal->id)->avg('rating');
                                        $count_reviews = Review::where('product_id',$deal->id)->count();
                                        $final_rate = floor($rate);
                                        if($final_rate == 0){
                                            $final_rate = 4;
                                        }
                                        ?>
                                        <div class="caption text-center">
                                            <h4 class="caption-title">{{$deal->name}}</h4>
                                            <div class="rating">
                                                @for($i=0 ; $i<5 ; $i++)
                                                @if($final_rate>0)
                                                <span class="star active"></span>
                                                @else
                                                <span class="star"></span>
                                                @endif
                                                <?php $final_rate--; ?>
                                                @endfor
                                            </div>
                                            <span class="reviews">{{$count_reviews}} reviews</span>
                                            <div class="price"><ins>Rs. {{$deal->sell_price}}</ins>{{--  <del>$425.00</del> --}}</div>
                                            <div class="caption-text">{{$deal->short_descriptions}}</div>
                                        </div>
                                    </div>
                                    @endforeach                                       

                                </div>
                            </div>
                        </div>
                        <!-- /widget shop hot deals -->
                    </aside>
                    <!-- /SIDEBAR -->
                    <!-- CONTENT -->
                    <div class="col-md-9 content" id="content">

                        <div class="main-slider sub">
                            <div class="owl-carousel" id="main-slider">

                                <!-- Slide 1 -->
                                {{-- <div class="item slide1 sub">
                                    <img class="slide-img" src="/wealth-assets/img/preview/slider/slide-1-sub.jpg" alt=""/>
                                    <div class="caption">
                                        <div class="container">
                                            <div class="div-table">
                                                <div class="div-cell">
                                                    <div class="caption-content">
                                                        <h2 class="caption-title"><span>Winter Fashion</span></h2>
                                                        <h3 class="caption-subtitle"><span>Collection Ready</span></h3>
                                                        <p class="caption-text">
                                                            <a class="btn btn-theme" href="#">Shop Now</a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> --}}
                                <!-- /Slide 1 -->

                                <!-- Slide 2 -->
                                {{-- <div class="item slide2 sub">
                                    <img class="slide-img" src="/wealth-assets/img/preview/slider/slide-1-sub.jpg" alt=""/>
                                    <div class="caption">
                                        <div class="container">
                                            <div class="div-table">
                                                <div class="div-cell">
                                                    <div class="caption-content">
                                                        <h2 class="caption-title"><span>Winter Fashion</span></h2>
                                                        <h3 class="caption-subtitle"><span>Collection Ready</span></h3>
                                                        <p class="caption-text">
                                                            <a class="btn btn-theme" href="#">Shop Now</a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> --}}
                                <!-- /Slide 2 -->

                            </div>
                        </div>


                        <!-- shop-sorting -->
                        <div class="shop-sorting">
                            <div class="row">
                                <div class="col-sm-8">
                                    {{-- <form class="form-inline" action=""> --}}
                                        <div class="form-group selectpicker-wrapper">
                                            <label class="form-group">Sort By : </label>
                                            <select class="selectpicker input-price" data-live-search="true" data-width="100%" data-toggle="tooltip" title="Select" id="sort">
                                                <option value="name">Name</option>
                                                {{-- <option value="rating">Rating</option> --}}
                                                <option value="new">New Arrivals</option>
                                                <option value="low-high">Price: low to high</option>
                                                <option value="high-low" selected>Price: high to low</option>
                                            </select>
                                        </div>
                                                {{-- <div class="form-group selectpicker-wrapper">
                                                    <select
                                                        class="selectpicker input-price" data-live-search="true" data-width="100%"
                                                        data-toggle="tooltip" title="Select">
                                                        <option>Select Manifacturers</option>
                                                        <option>Select Manifacturers</option>
                                                        <option>Select Manifacturers</option>
                                                    </select>
                                                </div> --}}
                                            {{-- </form> --}}
                                        </div>
                                        <div class="col-sm-4 text-right-sm">
                                            <a class="btn btn-theme btn-theme-transparent btn-theme-sm" href="#"><img src="/wealth-assets/img/icon-list.png" alt=""/></a>
                                            <a class="btn btn-theme btn-theme-transparent btn-theme-sm" href="#"><img src="/wealth-assets/img/icon-grid.png" alt=""/></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- /shop-sorting -->
                                <?php $flag = 0; ?>
                                <!-- Products grid -->
                                <div class="row products grid" id="productCat">
                                    @if($searched_products != null)
                                    @foreach($searched_products as $product)
                                    <?php $flag++; ?> 
                                    <div class="col-md-4 col-sm-6">
                                        <div class="thumbnail no-border no-padding">
                                            <div class="media">
                                                <a class="media-link" href="/productdetail/{{$product->id}}">
                                                    <img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}" alt="{{$product->image}}" style="height: 350px;"  />
                                                    <span class="icon-view">
                                                        <strong><i class="fa fa-eye"></i></strong>
                                                    </span>
                                                </a>
                                            </div>
                                            <div class="caption text-center">
                                                <h4 class="caption-title"><a href="/productdetail/{{$product->id}}">{{$product->name}}</a></h4>
                                                
                                                <h5>JBV :  {{$product->jbv}}</h5>
                                                <h5>RBV :  {{$product->rvv}}</h5>

                                                <?php
                                                $rate = Review::where('product_id',$product->id)->avg('rating');
                                                $final_rate = floor($rate);
                                                if($final_rate == 0){
                                                    $final_rate = 4;
                                                }
                                                ?>
                                                <div class="rating">
                                                    @for($i=0 ; $i<5 ; $i++)
                                                    @if($final_rate>0)
                                                    <span class="star active"></span>
                                                    @else
                                                    <span class="star"></span>
                                                    @endif
                                                    <?php $final_rate--; ?>
                                                    @endfor
                                                </div>
                                                <div class="price"><ins>{{$product->sell_price}}</ins> {{-- <del>$425.00</del> --}}</div>
                                                <div class="buttons">
                                                    <a class="btn btn-theme btn-theme-transparent btn-wish-list" href="#" onclick="addWishList({{ $product->id }})"><i class="fa fa-heart"></i></a>
                                                    {{-- <a class="btn btn-theme btn-theme-transparent btn-icon-left" href="#" onclick="addCart({{$product->id}})"><i class="fa fa-shopping-cart"></i>Add to Cart</a> --}}
                                                    <a class="btn btn-theme btn-theme-transparent btn-icon-left" href="/productdetail/{{$product->id}}">{{-- <i class="fa fa-shopping-cart"></i> --}}View Detail</a>
                                                    {{-- --><a class="btn btn-theme btn-theme-transparent btn-compare" href="#"><i class="fa fa-exchange"></i></a> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    @endforeach                                    
                                    @endif


                                    @if($flag == 0)
                                    <?php $flag = 0; ?>
                                    <div class="content" style="text-align: center;">
                                        <h2 style="color: #0000">No Products Available</h2>
                                    </div>
                                    @endif

                                </div>
                                <!-- /Products grid -->

                                <!-- Pagination -->
                            {{-- <div class="pagination-wrapper">
                                <ul class="pagination">
                                    <li class="disabled"><a href="#"><i class="fa fa-angle-double-left"></i> Previous</a></li>
                                    <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">Next <i class="fa fa-angle-double-right"></i></a></li>
                                </ul>
                            </div> --}}
                            <!-- /Pagination -->

                        </div>
                        <!-- /CONTENT -->

                    </div>
                </div>
            </section>
            <!-- /PAGE WITH SIDEBAR -->

            <!-- PAGE -->
            <section class="page-section no-padding-top">
                <div class="container">
                    <div class="row blocks shop-info-banners">
                        <div class="col-md-4">
                            <div class="block">
                                <div class="media">
                                    <div class="pull-right"><i class="fa fa-gift"></i></div>
                                    <div class="media-body">
                                        <h4 class="media-heading">Buy 1 Get 1</h4>
                                        Proin dictum elementum velit. Fusce euismod consequat ante.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="block">
                                <div class="media">
                                    <div class="pull-right"><i class="fa fa-comments"></i></div>
                                    <div class="media-body">
                                        <h4 class="media-heading">Call to Free</h4>
                                        Proin dictum elementum velit. Fusce euismod consequat ante.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="block">
                                <div class="media">
                                    <div class="pull-right"><i class="fa fa-trophy"></i></div>
                                    <div class="media-body">
                                        <h4 class="media-heading">Money Back!</h4>
                                        Proin dictum elementum velit. Fusce euismod consequat ante.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /PAGE -->

        </div>
        <!-- /CONTENT AREA -->


        @endsection 

        @section('script')


        <script>
          var countproduct=<?php if(session()->get('count') != null){echo session()->get('count'); } else { echo 0;} ?>;

          function addToCart(){
             var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
             var pro_id = $('#hidden_id').val();
             var quant = $('#quickquantity').val();
             countproduct++;
             $.ajax({
                /* the route pointing to the post function */
                url: '/add-cart',
                type: 'POST',
                /* send the csrf-token and the input to the controller */
                data: {_token: CSRF_TOKEN, id: pro_id, quantity: quant},
                success: function (data) { 
                   $("#show-total").html(countproduct);           
               }
           }); 
         }

         function openview(id,name,price,mrp,description,img1,img2,img3,rate){
             $('#hidden_id').val(id);
             $('#viewname').text(name);

             $('#viewdescription').text(description);
             $('#viewprice').text(price);
    //For main image
    $("#imggallery1").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#imggallery2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);

    //for a tag (data-zoom attribute)    
    $("#imgdz2").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#imgdz3").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
    $("#imgdz1").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);

    //for a tag (data-images attribute)
    $("#imgdz1").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#imgdz2").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#imgdz3").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);

    //for image tag (src attribute)
    $("#img1").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#img2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#img3").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
    $("#imgt1").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#imgt2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#imgt3").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
    var discount = ((mrp-price)*100)/mrp;
    $('#viewdiscount').text(discount.toFixed(2));
    $('#viewrate').val(rate);

}

function searchFilter(id){  
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        /* the route pointing to the post function */
        url: '/wishlist/delete-all',
        type: 'POST',
        /* send the csrf-token and the input to the controller */
        dataType: 'JSON',

        success: function (data) { 
           $('#no-result').show();
           $('#refresh-wishlist').load(document.URL + ' #refresh-wishlist');  
       }
   });
}

function searchAll(){       
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        /* the route pointing to the post function */
        url: '/search-all',
        type: 'POST',
        /* send the csrf-token and the input to the controller */
        dataType: 'JSON',
        success: function (data) { 
            $('#refresh-wishlist').load(document.URL + ' #refresh-wishlist');   
        }
    });
} 

//adding items to cart
function addCart(id){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var cat_id = $('#cat_id').val();
    countproduct++;
    $.ajax({
        /* the route pointing to the post function */
        url: '/category-wise/cart',
        type: 'POST',
        /* send the csrf-token and the input to the controller */
        dataType: 'JSON',
        data: {_token: CSRF_TOKEN, id: id},
        success: function (data) { 
            window.location.href = '/products/'+cat_id;
            $("#ajaxdata").html(data);
            $("#show-total").html(countproduct); 
        }
    }); 
}

//adding items to Wishlist
function addWishList(id){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var cat_id = $('#cat_id').val();
    $.ajax({
        /* the route pointing to the post function */
        url: '/category-wise/wishlist',
        type: 'POST',
        /* send the csrf-token and the input to the controller */
        dataType: 'JSON',
        data: {_token: CSRF_TOKEN, id: id},
        success: function (data) { 
            window.location.href = '/products/'+cat_id;
            $("#ajaxdata").html(data);
        }
    }); 
}

$('#cat0').click(function(){        

    var cat = $('#cat0').val();
    $.ajax({
        type: 'get',
        data_type: 'html',
        url: '/search-product/productsCat',
        data:{ cat_id:cat},

        success:function(response){
            $('#productCat').html(response);
            console.log(response);
        }
    });
});


@foreach($maincategory as $row)
$('#cat{{$row->id}}').click(function(){
    var cat = $('#cat{{$row->id}}').val();

    $.ajax({
        type: 'get',
        data_type: 'html',
        url: '/search-product/productsCat',
        data:{ cat_id:cat},

        success:function(response){             
            $('#productCat').html(response);
            console.log(response);
        }
    });
});
@endforeach

$('#sort').on('change',(function(){
    var sort = $('#sort').val();
    $.ajax({
        type: 'get',
        data_type: 'html',
        url: '/search-product/sort',
        data:{ sort_type:sort},

        success:function(response){             
            $('#productCat').html(response);
            console.log(response);
        }
    });
}));

function priceFilter(){
    var min = $('#min-price').val();
    var max = $('#max-price').val();
    $.ajax({
        type: 'get',
        data_type: 'html',
        url: '/search-product/price-filter',
        data:{ max_value:max , min_value:min },

        success:function(response){             
            $('#productCat').html(response);
            console.log(response);
        }
    });
}


</script>

@endsection