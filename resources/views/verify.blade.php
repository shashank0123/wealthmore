@extends('back.app')

@section('title')
Login - {{ config('app.name') }}
@stop

@section('content')
<style>
  body{
    background:url(../assets/img/login.jpg) no-repeat center center fixed !important; }
  .card{margin-top:0% auto; max-width:40%;margin-left: 17%;margin-top: 20%; background-color: #fff;border-radius: 4%;}
  .pink-text {  font-size: 24px; }
  .pull-right {  margin-bottom: 15px !important; }
  @media screen and (max-width: 991px)  {
    .card { max-width:70%;margin-left: 15%;margin-top: 20% !important }
  }
  @media screen and (max-width: 580px)  {
    .card { max-width:90%;margin-left: 5%; }
  }
  
</style>
<div class="container-fluid center"> 

  <div class="card bordered z-depth-2">
    <div class="card-header">
      <div class="brand-logo" style="text-align: center;">
        <img src="{{ asset('/wealth-assets/img/logo-bella-shop.png') }}" width="100">
      </div>
    </div>
    <div class="container">
    <form class="form-floating action-form" method="post" action="/" >
      <div class="card-content">        
        
        <input type="number" name="otp" class="form-control" id="otp">
        
      </div>
    
      <div class="card-action clearfix">
        <div class="pull-right">
          <button type="submit" class="btn btn-link black-text">
            <span class="btn-preloader">
              <i class="md md-cached md-spin"></i>
            </span>
            <span>Verify</span>
          </button>
        </div>
      </div>
    </form>
    </div>
  </div>
</div>

@stop