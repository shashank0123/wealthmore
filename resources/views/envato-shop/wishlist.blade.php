<?php

use App\Wishlist;
use App\Product;

$id=0;
if(!empty($member->id))
$id = $member->id;
elseif(!empty($user->id))
$id = $user->id;

$wishlists = Wishlist::where('user_id',$id)->get();

?>

@extends('layouts/wealth-shop')

@section('content')

<!-- Main Content - start -->
<main>
    <section class="container">
        <ul class="b-crumbs">
            <li>
                <a href="index.html">
                    Home
                </a>
            </li>
            <li>
                <span>Wishlist</span>
            </li>
        </ul>
        <h1 class="main-ttl"><span>Wishlist</span></h1>
        <!-- Catalog Items | Full - start -->
        <div class="section-cont section-full">

            <div class="prod-items section-items">

                @if(!empty($wishlists))
                @foreach($wishlists as $wishlist)
                <?php
                $product = Product::where('id',$wishlist->product_id)->first();
                ?>
                <div class="prod-i" id="hide-product{{$product->id}}">
                    <div class="prod-i-top">
                        <a href="/product-detailw/{{$product->slug_name}}" class="prod-i-img"><!-- NO SPACE --><img src="/assetsss/images/AdminProduct/{{$product->image1}}" alt="Sunt temporibus velit"><!-- NO SPACE --></a>
                        <p class="prod-i-info">
                            <a onclick="removeProduct({{$product->id}})" class="prod-i-favorites"><span>Remove from Wishlist</span><i class="fa fa-remove"></i></a>
                            <a href="#" class="qview-btn prod-i-qview"><span>Quick View</span><i class="fa fa-search"></i></a>                           
                        </p>

                        <p class="prod-i-info" style="margin-top: 50px">
                            <input type="hidden" id="purchase-value{{$product->id}}" name="purchaseValue">
                            <input type="radio" name="purchase_value" value="{{$product->jbv}}"  onchange="handler1('jbv',{{$product->id}})"> <b>JBV : {{$product->jbv}}</b>                          <br>
                            <input type="radio" name="purchase_value"  onchange="handler2('rbv',{{$product->id}})" value="{{$product->rvv}}"> <b>RBV : {{$product->rvv}}</b>        <br><br> 
                        </p>


                        <a class="prod-i-buy" style="margin-top: 50px" onclick="@if($product->availability == 'yes') addToCart({{$product->id}}) @else showModel(); @endif">Add to cart</a>                      
                    </div>
                    <h3>
                        <a href="/product-detailw/{{$product->slug_name}}">{{strtoupper($product->name)}}</a>
                    </h3>
                    <p class="prod-i-price">
                        <b>Rs. {{$product->sell_price}}</b>
                    </p>
                </div>

                @endforeach
                @endif

                
            
            </div>

        </div>
        <!-- Catalog Items | Full - end -->

        <!-- Quick View Product - start -->
        <div class="qview-modal">
            <div class="prod-wrap">
                <a href="product.html">
                    <h1 class="main-ttl">
                        <span>Reprehenderit adipisci</span>
                    </h1>
                </a>
                <div class="prod-slider-wrap">
                    <div class="prod-slider">
                        <ul class="prod-slider-car">
                            <li>
                                <a data-fancybox-group="popup-product" class="fancy-img" href="http://placehold.it/500x525">
                                    <img src="http://placehold.it/500x525" alt="">
                                </a>
                            </li>
                            <li>
                                <a data-fancybox-group="popup-product" class="fancy-img" href="http://placehold.it/500x591">
                                    <img src="http://placehold.it/500x591" alt="">
                                </a>
                            </li>
                            <li>
                                <a data-fancybox-group="popup-product" class="fancy-img" href="http://placehold.it/500x525">
                                    <img src="http://placehold.it/500x525" alt="">
                                </a>
                            </li>
                            <li>
                                <a data-fancybox-group="popup-product" class="fancy-img" href="http://placehold.it/500x722">
                                    <img src="http://placehold.it/500x722" alt="">
                                </a>
                            </li>
                            <li>
                                <a data-fancybox-group="popup-product" class="fancy-img" href="http://placehold.it/500x722">
                                    <img src="http://placehold.it/500x722" alt="">
                                </a>
                            </li>
                            <li>
                                <a data-fancybox-group="popup-product" class="fancy-img" href="http://placehold.it/500x722">
                                    <img src="http://placehold.it/500x722" alt="">
                                </a>
                            </li>
                            <li>
                                <a data-fancybox-group="popup-product" class="fancy-img" href="http://placehold.it/500x722">
                                    <img src="http://placehold.it/500x722" alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="prod-thumbs">
                        <ul class="prod-thumbs-car">
                            <li>
                                <a data-slide-index="0" href="#">
                                    <img src="http://placehold.it/500x525" alt="">
                                </a>
                            </li>
                            <li>
                                <a data-slide-index="1" href="#">
                                    <img src="http://placehold.it/500x591" alt="">
                                </a>
                            </li>
                            <li>
                                <a data-slide-index="2" href="#">
                                    <img src="http://placehold.it/500x525" alt="">
                                </a>
                            </li>
                            <li>
                                <a data-slide-index="3" href="#">
                                    <img src="http://placehold.it/500x722" alt="">
                                </a>
                            </li>
                            <li>
                                <a data-slide-index="4" href="#">
                                    <img src="http://placehold.it/500x722" alt="">
                                </a>
                            </li>
                            <li>
                                <a data-slide-index="5" href="#">
                                    <img src="http://placehold.it/500x722" alt="">
                                </a>
                            </li>
                            <li>
                                <a data-slide-index="6" href="#">
                                    <img src="http://placehold.it/500x722" alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="prod-cont">
                    <p class="prod-actions">
                        <a href="#" class="prod-favorites"><i class="fa fa-heart"></i> Add to Wishlist</a>
                        <a href="#" class="prod-compare"><i class="fa fa-bar-chart"></i> Compare</a>
                    </p>
                    <div class="prod-skuwrap">
                        <p class="prod-skuttl">Color</p>
                        <ul class="prod-skucolor">
                            <li class="active">
                                <img src="img/color/blue.jpg" alt="">
                            </li>
                            <li>
                                <img src="img/color/red.jpg" alt="">
                            </li>
                            <li>
                                <img src="img/color/green.jpg" alt="">
                            </li>
                            <li>
                                <img src="img/color/yellow.jpg" alt="">
                            </li>
                            <li>
                                <img src="img/color/purple.jpg" alt="">
                            </li>
                        </ul>
                        <p class="prod-skuttl">Sizes</p>
                        <div class="offer-props-select">
                            <p>XL</p>
                            <ul>
                                <li><a href="#">XS</a></li>
                                <li><a href="#">S</a></li>
                                <li><a href="#">M</a></li>
                                <li class="active"><a href="#">XL</a></li>
                                <li><a href="#">L</a></li>
                                <li><a href="#">4XL</a></li>
                                <li><a href="#">XXL</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="prod-info">
                        <p class="prod-price">
                            <b class="item_current_price">$238</b>
                        </p>
                        <p class="prod-qnt">
                            <input type="text" value="1">
                            <a href="#" class="prod-plus"><i class="fa fa-angle-up"></i></a>
                            <a href="#" class="prod-minus"><i class="fa fa-angle-down"></i></a>
                        </p>
                        <p class="prod-addwrap">
                            <a href="#" class="prod-add">Add to cart</a>
                        </p>
                    </div>
                    <ul class="prod-i-props">
                        <li>
                            <b>SKU</b> 05464207
                        </li>
                        <li>
                            <b>Manufacturer</b> Mayoral
                        </li>
                        <li>
                            <b>Material</b> Cotton
                        </li>
                        <li>
                            <b>Pattern Type</b> Print
                        </li>
                        <li>
                            <b>Wash</b> Colored
                        </li>
                        <li>
                            <b>Style</b> Cute
                        </li>
                        <li>
                            <b>Color</b> Blue, Red
                        </li>
                        <li><a href="#" class="prod-showprops">All Features</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Quick View Product - end -->
    </section>
</main>
<!-- Main Content - end -->


@endsection


@section('script')
<script>

function showModel(){
    Swal('Product is out of stock right now');
}

function handler1(value,id) { 
        var valu = value;
        // alert(valu); 
        $('#purchase-value'+id).val(valu);
    }

    function handler2(value,id) { 
        var valu = value;
        // alert(valu); 

        $('#purchase-value'+id).val(valu);
    }


//adding items to cart
function addToCart(id){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var pro_quant = 1;
    var pro_color = "";
    var pro_size = "";
    var purchase = $('#purchase-value'+id).val();

    if(pro_quant != ""  && purchase != ""){
        alert(pro_quant+","+purchase+","+pro_color+","+pro_size);

        $.ajax({
            /* the route pointing to the post function */
            url: '/add-to-cartw',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            dataType: 'JSON',
            data: {_token: CSRF_TOKEN, id: id, quantity: pro_quant,money_type : purchase,color: pro_color, size: pro_size},
            success: function (data) { 
                window.location.href = '/wishlistw/';
               
            // $("#ajaxdata").html(data);
        }
    });
    }
    else{
        Swal('Purchase Value & Quantity both are mendatory to add product to the cart');
    }

}

//Remove item from wishlist
function removeProduct(id){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var user_id = <?php echo $id; ?>;
    var product_id = id;

    
        // alert(user_id+" , "+product_id);

        $.ajax({
            /* the route pointing to the post function */
            url: '/delete-from-wishlist',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            dataType: 'JSON',
            data: {_token: CSRF_TOKEN, user_id: user_id, product_id: product_id},
            success: function (data) { 
                // alert(data.message);
                $('#hide-product'+product_id).hide();
               
            // $("#ajaxdata").html(data);
        }
    });
    
}


</script>
@endsection