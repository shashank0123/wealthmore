@extends('layouts/wealth-shop')

@section('content')
<style>
     <style type="text/css">
  .textpolicy{ background-color: white; margin-top: 20px; margin-bottom: 100px; text-align: justify; }
  .textpolicy p{font-weight: 100px; padding: 0px 10px  0px 10px ; font-size: 15px;
    line-height: 1.5; font-family: "Lato", sans-serif;     color: #595959; letter-spacing: 0.5px; text-align: justify;}
 .textpolicy h4{ text-align: center; margin-top: 20px; margin-bottom: 10px; font-size: 28px; font-weight:600 }
.textpolicy h5{padding: 0px 30px  0px 30px ; font-size: 20px ; font-weight: 600; }
ol {list-style-type:decimal; margin-left:  20px}
ol li { line-height: 1.8; color: #595959; font-size: 15px}

  
</style>
</style>

<!-- Main Content - start -->
<main>
    <section class="container">

        <ul class="b-crumbs">
            <li>
                <a href="index.html">
                    Home
                </a>
            </li>
            <li>
                <span>Terms & Conditions</span>
            </li>
        </ul>        

<div class="container-fluid" style="background-color:#F1F3F6;">
  <div class="row">
    
 <div class="col-sm-2"></div> 
 <div  class="col-sm-8 textpolicy">
   
   <h4> PRIVACY POLICY</h4>
   <hr>
   <p>We value the trust you place in us. That's why we insist upon the highest standards for secure transactions and customer information privacy. Please read the following statement to learn about our information gathering and dissemination practices.</p>
   <br><br>
   <h5>Note:-</h5>
   <p>We value the trust you place in us. That's why we insist upon the highest standards for secure transactions and customer information privacy. Please read the following statement to learn about our information gathering and dissemination practices.</p>
<br>
   <h5>1. Collection of Personally Identifiable Information and other Information</h5><br>
   <p>When you use our Website, we collect and store your personal information which is provided by you from time to time. Our primary goal in doing so is to provide you a safe, efficient, smooth and customized experience. This allows us to provide services and features that most likely meet your needs, and to customize our Website to make your experience safer and easier. More importantly, while doing so we collect personal information from you that we consider necessary for achieving this purpose.
<br><br>
In general, you can browse the Website without telling us who you are or revealing any personal information about yourself. Once you give us your personal information, you are not anonymous to us. Where possible, we indicate which fields are required and which fields are optional. You always have the option to not provide information by choosing not to use a particular service or feature on the Website. We may automatically track certain information about you based upon your behaviour on our Website. We use this information to do internal research on our users' demographics, interests, and behaviour to better understand, protect and serve our users. This information is compiled and analysed on an aggregated basis. This information may include the URL that you just came from (whether this URL is on our Website or not), which URL you next go to (whether this URL is on our Website or not), your computer browser information, and your IP address.
<br><br>
We use data collection devices such as "cookies" on certain pages of the Website to help analyse our web page flow, measure promotional effectiveness, and promote trust and safety. "Cookies" are small files placed on your hard drive that assist us in providing our services. We offer certain features that are only available through the use of a "cookie". We also use cookies to allow you to enter your password less frequently during a session. Cookies can also help us provide information that is targeted to your interests. Most cookies are "session cookies," meaning that they are automatically deleted from your hard drive at the end of a session. You are always free to decline our cookies if your browser permits, although in that case you may not be able to use certain features on the Website and you may be required to re-enter your password more frequently during a session.
<br><br>
Additionally, you may encounter "cookies" or other similar devices on certain pages of the Website that are placed by third parties. We do not control the use of cookies by third parties.
<br><br>
If you choose to buy on the Website, we collect information about your buying behaviour.
<br><br>
If you transact with us, we collect some additional information, such as a billing address, a credit / debit card number and a credit / debit card expiration date and/ or other payment instrument details and tracking information from cheques or money orders.
<br><br>
If you choose to post messages on our message boards, chat rooms or other message areas or leave feedback, we will collect that information you provide to us. We retain this information as necessary to resolve disputes, provide customer support and troubleshoot problems as permitted by law.
<br><br>
If you send us personal correspondence, such as emails or letters, or if other users or third parties send us correspondence about your activities or postings on the Website, we may collect such information into a file specific to you.
<br><br>
We collect personally identifiable information (email address, name, phone number, credit card / debit card / other payment instrument details, etc.) from you when you set up a free account with us. While you can browse some sections of our Website without being a registered member, certain activities (such as placing an order) do require registration. We do use your contact information to send you offers based on your previous orders and your interests.</p><br><br>
 
<h5>2. Links to Other Sites</h5>
<br>
<p>Our Website links to other websites that may collect personally identifiable information about you. Flipkart.com is not responsible for the privacy practices or the content of those linked websites.</p>
<br><br>

<h5>3. Security Precautions</h5>
<br>

<p>Our Website has stringent security measures in place to protect the loss, misuse, and alteration of the information under our control. Whenever you change or access your account information, we offer the use of a secure server. Once your information is in our possession we adhere to strict security guidelines, protecting it against unauthorized access.</p>
<br><br>

<h5>4. Choice/Opt-Out</h5>
<br>

<p>We provide all users with the opportunity to opt-out of receiving non-essential (promotional, marketing-related) communications from us on behalf of our partners, and from us in general, after setting up an account.</p>
<br><br>
<p>
If you want to remove your contact information from all flipkart.com lists and newsletters, please visit unsubscribe</p><br>












 </div> 


 <div class="col-sm-2"></div>      
</div>
</div>
        
    </section>
</main>
<!-- Main Content - end -->

@endsection

@section('script')

@endsection