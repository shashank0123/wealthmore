<?php
use App\MainCategory;
use App\Product;
$main_categories = MainCategory::where('category_id',0)->where('status','Active')->get();

$cate_ids = array();
$count_cat=0;


if(!empty($main_categories)){
    foreach($main_categories as $main){
        $cate_ids[$count_cat++] = $main->id;
        $categories = MainCategory::where('category_id',$main->id)->get();
        if(!empty($categories)){
            foreach($categories as $cat){
                $cate_ids[$count_cat++] = $cat->id;
                $sub_categories = MainCategory::where('category_id',$cat->id)->get();
                if(!empty($sub_categories)){
                    foreach($sub_categories as $sub){
                        $cate_ids[$count_cat++] = $sub->id;
                    }
                }
            }            
        }
    }
}
?>

@extends('layouts/wealth-shop')

@section('content')

<!-- Main Content - start -->
<main>

    <input type="hidden" name="u_id" value="@if(!empty($member->id)){{$member->id}}@elseif(!empty($user->id)){{$user->id}}@else{{0}}@endif" id="u_id">

    <section class="container">


        <!-- Slider -->
        <div class="fr-slider-wrap">
            <div class="fr-slider">
                <ul class="slides">
                    @if(!empty($banners))
                    @foreach($banners as $banner)
                    <li>
                        <img src="{{URL::to('/assetsss/images/AdminProduct/banner')}}/{{$banner->image_url}}" alt="">
                        {{-- <div class="fr-slider-cont">
                            <h3>MEGA SALE -30%</h3>
                            <p>Winter collection for women's. <br>We all have choices for you. Check it out!</p>
                            <p class="fr-slider-more-wrap">
                                <a class="fr-slider-more" href="#">View collection</a>
                            </p>
                        </div> --}}
                    </li>
                    @endforeach
                    @endif                    
                </ul>
            </div>
        </div>


        <!-- Popular Products -->
        <div class="fr-pop-wrap">

            <h3 class="component-ttl"><span>Popular products</span></h3>

            <ul class="fr-pop-tabs sections-show">
                <li><a data-frpoptab-num="1" data-frpoptab="#all" href="#" class="active">All Categories</a></li>
                @if(!empty($main_categories))
                <?php $i = 0;?>
                @foreach($main_categories as $main)
                <?php
                $i++;
                ?>
                @if($i<=5)
                <li><a data-frpoptab-num="{{$main->id}}" data-frpoptab="#{{$main->id}}" href="#">{{strtoupper($main->Mcategory_name)}}</a></li>
                @endif
                @endforeach
                @endif
                {{-- <li><a data-frpoptab-num="3" data-frpoptab="#frpoptab-tab-3" href="#">Men</a></li>
                <li><a data-frpoptab-num="4" data-frpoptab="#frpoptab-tab-4" href="#">Kids</a></li>
                <li><a data-frpoptab-num="5" data-frpoptab="#frpoptab-tab-5" href="#">Shoes</a></li> --}}
            </ul>

            <div class="fr-pop-tab-cont">

                <p data-frpoptab-num="1" class="fr-pop-tab-mob active" data-frpoptab="#all">All Categories</p>
                <div class="flexslider prod-items fr-pop-tab" id="all">

                    <ul class="slides">
                        @if(!empty($cate_ids))
                        @for($j=0; $j<$count_cat ; $j++)
                        <?php

                        $product = Product::where('category_id',$cate_ids[$j])->where('status','Active')->first();      
                        ?>
                        @if(!empty($product))
                        <li class="prod-i">
                            <div class="prod-i-top">
                                <a href="/product-detailw/{{$product->slug_name}}" class="prod-i-img"><!-- NO SPACE --><img src="{{ URL::to('/assetsss/images/AdminProduct')}}/{{$product->image1}}" alt="Aspernatur excepturi rem"><!-- NO SPACE --></a>
                                <p class="prod-i-info">
                                    <a class="prod-i-favorites" onclick="addToWishlist({{$product->id}})"><span>Wishlist</span><i class="fa fa-heart"></i></a>
                                    <a href="#" onclick="openview({{ $product->id }},'{{ $product->name }}',{{$product->sell_price}},{{ $product->mrp }},'{{$product->short_descriptions}}','{{$product->image1}}','{{$product->image2}}','{{$product->image3}}','<?php if($product->availability == 'yes'){ echo "In stock"; } else { echo "Out of Stock"; } ?>','{{$product->product_color}}')" class="qview-btn prod-i-qview"><span>Quick View</span><i class="fa fa-search"></i></a>
                                    {{-- <a class="prod-i-compare" href="#"><span>Compare</span><i class="fa fa-bar-chart"></i></a> --}}
                                </p>
                                <p class="prod-i-addwrap">
                                    <a href="/product-detailw/{{$product->slug_name}}" class="prod-i-add">Go to detail</a>
                                </p>
                            </div>
                            <h3>
                                <a href="/product-detailw/{{$product->slug_name}}">{{strtoupper($product->name)}}</a>
                            </h3>
                            <p class="prod-i-price">
                                <b>Rs. {{$product->sell_price}}</b>
                            </p>
                            <div class="prod-i-skuwrapcolor">                                
                                <div class="row">
                                    <div class="col-sm-6">
                                        &nbsp;&nbsp;&nbsp;<b>JBV</b> : {{$product->jbv}}
                                    </div>
                                    <div class="col-sm-6">
                                        <b>RBV</b> : {{$product->rvv}}&nbsp;&nbsp;&nbsp;
                                    </div>
                                </div>
                                    {{-- <li class="bx_active"><img src="/new-asset/img/color/red.jpg" alt="Red"></li>
                                    <li><img src="/new-asset/img/color/blue.jpg" alt="Blue"></li> --}}                               
                                </div>
                            </li>
                            @endif
                            @endfor
                            @endif 
                        </ul>
                    </div>

                    @foreach($main_categories as $main)
                    <p data-frpoptab-num="{{$main->id}}" class="fr-pop-tab-mob" data-frpoptab="#{{$main->id}}">Kids</p>
                    <div class="flexslider prod-items fr-pop-tab" id="{{$main->id}}">
                        <?php
                        $k=0;
                        $cate_id[$k++] = $main->id;
                        $categories = MainCategory::where('category_id',$main->id)->get();
                        if(!empty($categories)){
                            foreach($categories as $cat){
                                $cate_id[$k++] = $cat->id;
                                $sub_categories = MainCategory::where('category_id',$cat->id)->get();
                                if(!empty($sub_categories)){
                                    foreach($sub_categories as $sub){
                                        $cate_id[$k++] = $sub->id;
                                    }
                                }
                            }            
                        }

                        ?>

                        @if(!empty($cate_id))
                        <ul class="slides">
                            @for($j=0 ; $j<$k ;$j++)
                            <?php
                            $product = Product::where('category_id',$cate_id[$j])->first();
                            ?>
                            @if(!empty($product))
                            <li class="prod-i">
                                <div class="prod-i-top">
                                    <a href="/product-detailw/{{$product->slug_name}}" class="prod-i-img"><!-- NO SPACE --><img src="{{url('/assetsss/images/AdminProduct')}}/{{$product->image1}}" alt="Aspernatur excepturi rem"><!-- NO SPACE --></a>
                                    <p class="prod-i-info">
                                        <a href="#" class="prod-i-favorites"><span>Wishlist</span><i class="fa fa-heart"></i></a>
                                        <a href="#" onclick="openview({{ $product->id }},'{{ $product->name }}',{{$product->sell_price}},{{ $product->mrp }},'{{$product->short_descriptions}}','{{$product->image1}}','{{$product->image2}}','{{$product->image3}}','<?php if($product->availability == 'yes'){ echo "In stock"; } else { echo "Out of Stock"; } ?>','{{$product->product_color}}')" class="qview-btn prod-i-qview"><span>Quick View</span><i class="fa fa-search"></i></a>
                                        {{-- <a class="prod-i-compare" href="#"><span>Compare</span><i class="fa fa-bar-chart"></i></a> --}}
                                    </p>
                                    <p class="prod-i-addwrap">
                                        <a href="/product-detailw/{{$product->slug_name}}" class="prod-i-add">Go to detail</a>
                                    </p>
                                </div>
                                <h3>
                                    <a href="/product-detailw/{{$product->slug_name}}">{{strtoupper($product->name)}}</a>
                                </h3>
                                <p class="prod-i-price">
                                    <b>Rs. {{$product->sell_price}}</b>
                                </p>
                                <div class="prod-i-skuwrapcolor">                               
                                    <div class="row">
                                        <div class="col-sm-6">
                                            &nbsp;&nbsp;&nbsp;<b>JBV</b> : {{$product->jbv}}
                                        </div>
                                        <div class="col-sm-6">
                                            <b>RBV</b> : {{$product->rvv}}&nbsp;&nbsp;&nbsp;
                                        </div>
                                    </div>                                   
                                </div>
                            </li>     
                            @endif                   
                            @endfor
                        </ul>
                        @endif
                    </div>
                    @endforeach
                </div><!-- .fr-pop-tab-cont -->


            </div><!-- .fr-pop-wrap -->








            <!-- Banners -->
            <div class="banners-wrap">
                <div class="banners-list">
                    @if(!empty($top_banner))
                    <div class="banner-i style_11">
                        <span class="banner-i-bg" style="background: url('/assetsss/images/AdminProduct/banner/{{$top_banner->image_url}}');"></span>
                        {{-- <div class="banner-i-cont">
                            <p class="banner-i-subttl">NEW COLLECTION</p>
                            <h3 class="banner-i-ttl">MEN'S<br>CLOTHING</h3>
                            <p class="banner-i-link"><a href="/sectionw">View More</a></p>
                        </div> --}}
                    </div>
                    @endif
                    @if(!empty($trending))
                    @foreach($trending as $product)
                    <?php $cat=MainCategory::where('id',$product->category_id)->first(); ?>
                    <div class="banner-i style_22">
                        <span class="banner-i-bg" style="background: url('/assetsss/images/AdminProduct/{{$product->image1}}');"></span>
                        <div class="banner-i-cont">
                            <p class="banner-i-subttl" style="color: #38B64A">GREAT COLLECTION</p>
                            {{-- <h3 class="banner-i-ttl">CLOTHING<br>ACCESSORIES</h3> --}}
                            <p class="banner-i-link"><a href="/categorywise-productsw/{{$cat->slug_name}}">Show more</a></p>
                        </div>
                    </div>
                    @endforeach
                    @endif                    
                </div>
            </div>

            <!-- Popular Products -->
            <div class="fr-pop-wrap">

                <h3 class="component-ttl"><span>Trending products</span></h3>

                <ul class="fr-pop-tabs sections-show" style="display:none">
                    <li><a data-frpoptab-num="1" data-frpoptab="#all" href="#" class="active" >All Categories</a></li>   
                </ul>

                <div class="fr-pop-tab-cont">
                    <div class="flexslider prod-items fr-pop-tab" id="all">

                        <ul class="slides">

                            @if(!empty($trending_post))
                            @foreach($trending_post as $product)
                            <li class="prod-i">
                                <div class="prod-i-top">
                                    <a href="/product-detailw/{{$product->slug_name}}" class="prod-i-img"><!-- NO SPACE --><img src="{{ URL::to('/assetsss/images/AdminProduct')}}/{{$product->image1}}" alt="Aspernatur excepturi rem"><!-- NO SPACE --></a>
                                    <p class="prod-i-info">
                                        <a class="prod-i-favorites" onclick="addToWishlist({{$product->id}})"><span>Wishlist</span><i class="fa fa-heart"></i></a>
                                        <a href="#" onclick="openview({{ $product->id }},'{{ $product->name }}',{{$product->sell_price}},{{ $product->mrp }},'{{$product->short_descriptions}}','{{$product->image1}}','{{$product->image2}}','{{$product->image3}}','<?php if($product->availability == 'yes'){ echo "In stock"; } else { echo "Out of Stock"; } ?>','{{$product->product_color}}')" class="qview-btn prod-i-qview"><span>Quick View</span><i class="fa fa-search"></i></a>
                                        {{-- <a class="prod-i-compare" href="#"><span>Compare</span><i class="fa fa-bar-chart"></i></a> --}}
                                    </p>
                                    <p class="prod-i-addwrap">
                                        <a href="/product-detailw/{{$product->slug_name}}" class="prod-i-add">Go to detail</a>
                                    </p>
                                </div>
                                <h3>
                                    <a href="/product-detailw/{{$product->slug_name}}">{{strtoupper($product->name)}}</a>
                                </h3>
                                <p class="prod-i-price">
                                    <b>Rs. {{$product->sell_price}}</b>
                                </p>
                                <div class="prod-i-skuwrapcolor">                                
                                    <div class="row">
                                        <div class="col-sm-6">
                                            &nbsp;&nbsp;&nbsp;<b>JBV</b> : {{$product->jbv}}
                                        </div>
                                        <div class="col-sm-6">
                                            <b>RBV</b> : {{$product->rvv}}&nbsp;&nbsp;&nbsp;
                                        </div>
                                    </div>

                                </div>
                            </li>

                            @endforeach
                            @endif 
                        </ul>
                    </div>

                </div><!-- .fr-pop-tab-cont -->
            </div><!-- .fr-pop-wrap -->

            <!-- Popular Products -->
            <div class="fr-pop-wrap">

                <h3 class="component-ttl"><span>Featured products</span></h3>

                <ul class="fr-pop-tabs sections-show" style="display:none">
                    <li><a data-frpoptab-num="1" data-frpoptab="#all" href="#" class="active" >All Categories</a></li>   
                </ul>

                <div class="fr-pop-tab-cont">
                    <div class="flexslider prod-items fr-pop-tab" id="all">

                        <ul class="slides">

                            @if(!empty($featured_post))
                            @foreach($featured_post as $product)
                            <li class="prod-i">
                                <div class="prod-i-top">
                                    <a href="/product-detailw/{{$product->slug_name}}" class="prod-i-img"><!-- NO SPACE --><img src="{{ URL::to('/assetsss/images/AdminProduct')}}/{{$product->image1}}" alt="Aspernatur excepturi rem"><!-- NO SPACE --></a>
                                    <p class="prod-i-info">
                                        <a class="prod-i-favorites" onclick="addToWishlist({{$product->id}})"><span>Wishlist</span><i class="fa fa-heart"></i></a>
                                        <a href="#" onclick="openview({{ $product->id }},'{{ $product->name }}',{{$product->sell_price}},{{ $product->mrp }},'{{$product->short_descriptions}}','{{$product->image1}}','{{$product->image2}}','{{$product->image3}}','<?php if($product->availability == 'yes'){ echo "In stock"; } else { echo "Out of Stock"; } ?>','{{$product->product_color}}')" class="qview-btn prod-i-qview"><span>Quick View</span><i class="fa fa-search"></i></a>
                                        {{-- <a class="prod-i-compare" href="#"><span>Compare</span><i class="fa fa-bar-chart"></i></a> --}}
                                    </p>
                                    <p class="prod-i-addwrap">
                                        <a href="/product-detailw/{{$product->slug_name}}" class="prod-i-add">Go to detail</a>
                                    </p>
                                </div>
                                <h3>
                                    <a href="/product-detailw/{{$product->slug_name}}">{{strtoupper($product->name)}}</a>
                                </h3>
                                <p class="prod-i-price">
                                    <b>Rs. {{$product->sell_price}}</b>
                                </p>
                                <div class="prod-i-skuwrapcolor">                                
                                    <div class="row">
                                        <div class="col-sm-6">
                                            &nbsp;&nbsp;&nbsp;<b>JBV</b> : {{$product->jbv}}
                                        </div>
                                        <div class="col-sm-6">
                                            <b>RBV</b> : {{$product->rvv}}&nbsp;&nbsp;&nbsp;
                                        </div>
                                    </div>

                                </div>
                            </li>

                            @endforeach
                            @endif 
                        </ul>
                    </div>

                </div><!-- .fr-pop-tab-cont -->
            </div><!-- .fr-pop-wrap -->


            <div class="banners-wrap">
                <div class="banners-list">

                    @if(!empty($featured))
                    @foreach($featured as $feature)
                    <div class="banner-i style_22">
                        <span class="banner-i-bg" style="background: url(/assetsss/images/AdminProduct/{{$feature->image1}});"></span>
                        <div class="banner-i-cont">
                            {{-- <p class="banner-i-subttl">DISCOUNT -20%</p> --}}
                            <h3 class="banner-i-ttl">{{strtoupper($feature->name)}}</h3>
                            <p class="banner-i-link"><a href="/product-detailw/{{$feature->slug_name}}">Shop now</a></p>
                        </div>
                    </div>
                    @endforeach
                    @endif
                    @if(!empty($bottom_banner))
                    <div class="banner-i style_12">
                        <span class="banner-i-bg" style="background: url(/assetsss/images/AdminProduct/banner/{{$bottom_banner->image_url}});"></span>
                        {{-- <div class="banner-i-cont">
                            <p class="banner-i-subttl">STYLISH CLOTHES</p>
                            <h3 class="banner-i-ttl">WOMEN'S COLLECTION</h3>
                            <p>A great selection of dresses, <br>blouses and women's suits</p>
                            <p class="banner-i-link"><a href="/sectionw">View More</a></p>
                        </div> --}}
                    </div>
                    @endif
                </div>
            </div>


            <!-- Special offer -->
            <div class="discounts-wrap">
                <h3 class="component-ttl"><span>Special offer</span></h3>
                <div class="flexslider discounts-list">
                    <ul class="slides">
                        @if(!empty($offers))
                        @foreach($offers as $offer)
                        <li class="discounts-i">
                            <a href="#" class="discounts-i-img">
                                <img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{$offer->image1}}" alt="{{$product->name}}">
                            </a>
                            <h3 class="discounts-i-ttl">
                                <a href="#">{{strtoupper($offer->name)}}</a>
                            </h3>
                            <p class="discounts-i-price">
                                <b>Rs. {{$offer->sell_price}}</b> <del>Rs. {{$offer->mrp}}</del>
                            </p>
                        </li>

                        @endforeach
                        @endif
                        
                    </ul>
                </div>
                <div class="discounts-info">
                    <p>Special offer!<br>Limited time only</p>
                    <a href="#">Shop now</a>
                </div>
            </div>


            <!-- Latest news -->
           {{--  <div class="posts-wrap">
                <div class="posts-list">
                    <div class="posts-i">
                        <a class="posts-i-img" href="/postw">
                            <span style="background: url(http://placehold.it/354x236)"></span>
                        </a>
                        <time class="posts-i-date" datetime="2016-11-09 00:00:00"><span>30</span> Jan</time>
                        <div class="posts-i-info">
                            <a href="/blogw" class="posts-i-ctg">Reviews</a>
                            <h3 class="posts-i-ttl">
                                <a href="/postw">Animi quaerat at</a>
                            </h3>
                        </div>
                    </div>
                    <div class="posts-i">
                        <a class="posts-i-img" href="/postw">
                            <span style="background: url(http://placehold.it/354x236)"></span>
                        </a>
                        <time class="posts-i-date" datetime="2016-11-09 00:00:00"><span>29</span> Jan</time>
                        <div class="posts-i-info">
                            <a href="/blogw" class="posts-i-ctg">Articles</a>
                            <h3 class="posts-i-ttl">
                                <a href="/postw">Ex atque commodi</a>
                            </h3>
                        </div>
                    </div>
                    <div class="posts-i">
                        <a class="posts-i-img" href="/postw">
                            <span style="background: url(http://placehold.it/354x236)"></span>
                        </a>
                        <time class="posts-i-date" datetime="2016-11-09 00:00:00"><span>25</span> Jan</time>
                        <div class="posts-i-info">
                            <a href="/blogw" class="posts-i-ctg">News</a>
                            <h3 class="posts-i-ttl">
                                <a href="/postw">Hic quod maxime deserunt</a>
                            </h3>
                        </div>
                    </div>
                </div>
            </div> --}}


            <!-- Testimonials -->
            <div class="reviews-wrap">
                <div class="reviewscar-wrap">
                    <div class="swiper-container reviewscar">
                        <div class="swiper-wrapper">
                            @if(!empty($testimonials))
                            @foreach($testimonials as $test)
                            <div class="swiper-slide">
                                <p>{{$test->review}}</p>
                            </div>
                            @endforeach
                            @endif
                            
                        </div>
                    </div>
                    <div class="swiper-container reviewscar-thumbs">
                        <div class="swiper-wrapper">
                            @if(!empty($testimonials))
                            @foreach($testimonials as $test)
                            <div class="swiper-slide">
                                <img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{$test->image_url}}" alt="{{$test->client_name}}" style="width: 120px; height: 80px">
                                <h3 class="reviewscar-ttl"><a href="/reviewsw">{{$test->client_name}}</a></h3>
                                <p class="reviewscar-post">{{$test->city}}</p>
                            </div>
                            @endforeach
                            @endif
                            
                        </div>
                    </div>
                </div>
            </div>

            <!-- Subscribe Form -->
            <div class="newsletter">
                <h3>Subscribe to us</h3>
                <p>Enter your email if you want to receive our news</p>
                <form id="newsletter">
                    <input type="email" placeholder="Your e-mail" id="news-email" type="text" class="form-control"><br>
                    <input onclick="submitNews()" name="submit" value="Subscribe" class="btn btn-success" style="width: 100px; font-size: 10px" >
                </form>
            </div>

            <!-- Quick View Product - start -->
            <div class="qview-modal">
                <div class="prod-wrap">
                    <a href="#">
                        <h1 class="main-ttl">
                            <input type="hidden" name="product_id" value="" id="hidden_id">

                            <span id="viewname"></span>
                        </h1>
                    </a>
                    <div class="prod-slider-wrap">
                        <div class="prod-slider">
                            <ul class="prod-slider-car">
                                <li>
                                    <a data-fancybox-group="popup-product" class="fancy-img" href="" id="ahref1">
                                        <img src="" alt="" id="imggallery1">
                                    </a>
                                </li>
                                <li>
                                    <a data-fancybox-group="popup-product" class="fancy-img" href="" id="ahref2" >
                                        <img src="" alt="" id="imggallery2">
                                    </a>
                                </li>
                                <li>
                                    <a data-fancybox-group="popup-product" class="fancy-img" href="" id="ahref3">
                                        <img src="" alt="" id="imggallery3">
                                    </a>
                                </li>
                                <li>
                                    <a data-fancybox-group="popup-product" class="fancy-img" href="" id="ahref4">
                                        <img src="" alt="" id="imggallery4">
                                    </a>
                                </li>
                                <li>
                                    <a data-fancybox-group="popup-product" class="fancy-img" href="" id="ahref5">
                                        <img src="" alt="" id="imggallery5">
                                    </a>
                                </li>
                                <li>
                                    <a data-fancybox-group="popup-product" class="fancy-img" href="" id="ahref6">
                                        <img src="" alt="" id="imggallery6">
                                    </a>
                                </li>
                                
                            </ul>
                        </div>
                        <div class="prod-thumbs">
                            <ul class="prod-thumbs-car">
                                <li>
                                    <a data-slide-index="0" href="#">
                                        <img src="" alt="" id="imgcol1">
                                    </a>
                                </li>
                                <li>
                                    <a data-slide-index="1" href="#">
                                        <img src="" alt=""  id="imgcol2">
                                    </a>
                                </li>
                                <li>
                                    <a data-slide-index="2" href="#">
                                        <img src="" alt=""  id="imgcol3">
                                    </a>
                                </li>
                                <li>
                                    <a data-slide-index="3" href="#">
                                        <img src="" alt=""  id="imgcol4">
                                    </a>
                                </li>
                                <li>
                                    <a data-slide-index="4" href="#">
                                        <img src="" alt=""  id="imgcol5">
                                    </a>
                                </li>
                                <li>
                                    <a data-slide-index="5" href="#">
                                        <img src="" alt=""  id="imgcol6">
                                    </a>
                                </li>
                                
                            </ul>
                        </div>
                    </div>

                    <div class="prod-cont">

                        <div style="text-align: center;">
                            <h1 style="font-size: 24px" id="availability"></h1><br>
                            
                        </div>

                        <p class="prod-actions">
                            <a href="#" class="prod-favorites"><i class="fa fa-heart"></i> Add to Wishlist</a>
                            <a href="#" class="prod-compare"><i class="fa fa-bar-chart"></i> Compare</a>
                        </p>
                        {{-- <div class="prod-skuwrap">
                            <p class="prod-skuttl">Color</p>
                            <ul class="prod-skucolor">
                                <li class="active" id="colorModel">

                                </li>
                               
                            </ul>
                            <p class="prod-skuttl">Sizes</p>
                            <div class="offer-props-select">
                                <p>XL</p>
                                <ul>
                                    <li><a href="#">XS</a></li>
                                    <li><a href="#">S</a></li>
                                    <li><a href="#">M</a></li>
                                    <li class="active"><a href="#">XL</a></li>
                                    <li><a href="#">L</a></li>
                                    <li><a href="#">4XL</a></li>
                                    <li><a href="#">XXL</a></li>
                                </ul>
                            </div>
                        </div> --}}
                        
                        <div class="prod-info">
                            <p class="prod-price">
                                <b class="item_current_price" id="viewprice"></b>
                            </p>
                            <p class="prod-qnt">
                                <input type="text" value="1">
                                <a href="#" class="prod-plus"><i class="fa fa-angle-up"></i></a>
                                <a href="#" class="prod-minus"><i class="fa fa-angle-down"></i></a>
                            </p>
                            <p class="prod-addwrap">
                                <a href="#" class="prod-add">Add to cart</a>
                            </p>
                        </div>
                        <ul class="prod-i-props">
                            <li id="viewdescription">
                                {{-- <b>SKU</b> 05464207 --}}
                            </li>
                            {{-- <li>
                                <b>Manufacturer</b> Mayoral
                            </li>
                            <li>
                                <b>Material</b> Cotton
                            </li>
                            <li>
                                <b>Pattern Type</b> Print
                            </li>
                            <li>
                                <b>Wash</b> Colored
                            </li>
                            <li>
                                <b>Style</b> Cute
                            </li>
                            <li>
                                <b>Color</b> Blue, Red
                            </li>
                            <li><a href="#" class="prod-showprops">All Features</a></li> --}}
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Quick View Product - end -->
        </section>
    </main>
    <!-- Main Content - end -->

    @endsection

    @section('script')

    <script>

       //modal  box of quick view
       function openview(id,name,price,mrp,description,img1,img2,img3,stock,color){
        $('#hidden_id').val(id);    
        $('#viewname').text(name);
        $('#viewdescription').text(description);
        $('#viewprice').text('Rs. '+price);
        $('#colorModel').text(color);
        $('#availability').text(stock);
    //For main image
    $("#imggallery1").attr("src", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img1);
    $("#imggallery2").attr("src", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img2);
    $("#imggallery3").attr("src", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img3);
    $("#imggallery4").attr("src", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img1);
    $("#imggallery5").attr("src", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img2);
    $("#imggallery6").attr("src", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img3);

    //for a tag (data-zoom attribute)    
    $("#ahref1").attr("href", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img1);
    $("#ahref2").attr("href", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img2);
    $("#ahref3").attr("href", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img3);

    //for a tag (data-images attribute)
    $("#ahref4").attr("href", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img1);
    $("#ahref5").attr("href", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img2);
    $("#ahref6").attr("href", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img3);

    //for image tag (src attribute)
    $("#imgcol1").attr("src", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img1);
    $("#imgcol2").attr("src", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img2);
    $("#imgcol3").attr("src", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img3);
    $("#imgtcol4").attr("src", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img1);
    $("#imgcol5").attr("src", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img2);
    $("#imgcol6").attr("src", "{{ URL::to('/') }}/wealth-assets/images/AdminProduct/"+img3);
    var discount = ((mrp-price)*100)/mrp;
    $('#viewdiscount').text(discount.toFixed(2));

}


//adding items to Wishlist
function addToWishlist(id,count){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var user_id = $('#u_id').val();

    if(user_id == 0){
        Swal('Please Login First.');
    } 
    else{
        $.ajax({
            /* the route pointing to the post function */
            url: '/add-to-wishlistw',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            data: {_token: CSRF_TOKEN, id: id, user_id: user_id},
            success: function (data) { 
                window.location.href = '';
                $("#ajaxdata").text(count);
            }
        }); 
    }
}

function submitNews(){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var email = $('#news-email').val();

    if(email == ""){
        Swal('First Enter your Email.');
    }
    else{

        $.ajax({
            /* the route pointing to the post function */
            url: '/post-newsletter',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            data: {_token: CSRF_TOKEN, email: email},
            success: function (data) { 
                Swal(data.message);
            }
        });
    }
}
</script>

@endsection