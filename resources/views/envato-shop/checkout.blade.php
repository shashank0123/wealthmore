<?php
use App\Product;
use App\Address;
use App\Country;
use App\State;
use App\City;
$id=0;
if(!empty($member->id)){
	$address = Address::where('user_id',$member->id)->first();
	$id=$member->id;
}


$i=0;
$total = 0;
?>



@extends('layouts/wealth-shop')


@section('content')
<div class="container wrapper">
	<div class="row cart-head">
		<div class="container">
			<div class="row">
				<p></p>
			</div>

			<div class="row">
				<p></p>
			</div>
		</div>
	</div>    
	<div class="row cart-body">
		<form class="form-horizontal" method="POST" action="<?php if(!empty($member->id)){ echo '/checkout-pagew/'.$member->id ;}  else {echo '/en/login'; } ?>" id="myForm">
			@csrf
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 ">
				<!--SHIPPING METHOD-->
				<div class="panel panel-info">
					<div class="panel-heading">Address</div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-md-12">
								<h4>Shipping Address</h4>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12"><strong>Country:</strong></div>
							<div class="col-md-12">
								<input type="text" class="form-control" name="country" value="India" id="country" required />
							</div>
							<p style="color: red; text-align: center;" id="country-error"></p>
						</div>
						<div class="form-group">
							<div class="col-md-12"><strong>Name:</strong></div>
							<div class="col-md-12">
								<input type="text" class="form-control" name="name" id="name" value="@if(!empty($user->first_name)){{strtoupper($user->first_name)}}@endif" required />
							</div>                                
							<p style="color: red; text-align: center;" id="name-error"></p>
						</div>
						<div class="form-group">
							<div class="col-md-12"><strong>Address:</strong></div>
							<div class="col-md-12">
								<input type="text" name="address" id="address" class="form-control" value="<?php if(!empty($address)){echo $address->address;}?>" readonly/>
							</div>
							<p style="color: red; text-align: center;" id="address-error"></p>
						</div>
						<?php $states=State::all();?>
						<div class="form-group">
							<div class="col-md-12"><strong>State:</strong></div>
							<div class="col-md-12">
								<select class="form-control" name="state_id" id="state_id" required="required">
									
									<option>Select State</option> 
									
									@foreach($states as $state)
									<option value="{{$state->id}}"<?php if(!empty($address->state_id) && ($address->state_id == $state->id)){echo 'selected';} ?>> {{$state->state}} </option>
									@endforeach
								</select>
							</div>
							<p style="color: red; text-align: center;" id="state-error"></p>
						</div>
						<?php $cities=City::all();?>
						<div class="form-group">
							<div class="col-md-12"><strong>City:</strong></div>
							<div class="col-md-12">
								<select class="form-control" name="city_id" id="city_id" required="required">
									
									<option>Select City</option> 
									
									@foreach($cities as $city)
									<option value="{{$city->id}}"<?php if(!empty($address->city_id) && ($address->city_id == $city->id)){echo 'selected';} ?>> {{$city->city}} </option>
									@endforeach
																		
							</select>
							</div>
							<p style="color: red; text-align: center;" id="city-error"></p>
						</div>
						<div class="form-group">
							<div class="col-md-12"><strong>Zip / Postal Code:</strong></div>
							<div class="col-md-12">
								<input type="text" name="pin_code" id="pin_code" class="form-control" value="<?php if(!empty($address)){echo $address->pin_code;}?>" readonly/>
							</div>
							<p style="color: red; text-align: center;" id="pin-error"></p>
						</div>
						<div class="form-group">
							<div class="col-md-12"><strong>Phone Number:</strong></div>
							<div class="col-md-12"><input type="text" name="phone" id="phone-number" class="form-control" value="<?php if(!empty($address)){echo $address->phone;}?>" readonly/></div>
							<p style="color: red; text-align: center;" id="phone-error"></p>
						</div>
						{{-- <div class="form-group">
							<div class="col-md-12"><strong>Email Address:</strong></div>
							<div class="col-md-12"><input type="text" name="email_address" class="form-control" value="" /></div>
						</div> --}}
					</div>
				</div>
				<input type="reset" onclick="resetForm()" class="btn btn-primary" value="Change Address">Change Address
				<!--SHIPPING METHOD END-->
				<br><br>
			</div>
			
							
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
				<!--REVIEW ORDER-->
				<div class="panel panel-info">
					<div class="panel-heading">
						Review Order <div class="pull-right"><small><a class="afix-1" href="#">Edit Cart</a></small></div>
					</div>
					@if(session()->get('cart') != null)

							<?php
							

							$ids = array();
							$cat_id = array();					
							$quantities = array();


							foreach(session()->get('cart') as $data)
							{
								$ids[$i] = $data->product_id;								
								$quantities[$i] = $data->quantity;
								$i++;
							}						
							?>
							
							@endif	
					<div class="panel-body">

						@for($j=0 ; $j<$i ; $j++ )
							<?php
							$product = Product::where('id',$ids[$j])->first(); 
							$cost = $product->sell_price * $quantities[$j];
							$total = $total + $cost;

							?>

							
							
						<div class="form-group">
							<div class="col-sm-6 col-xs-6">
								<div class="col-xs-12">{{ucfirst($product->name)}}</div>

							</div>
							<div class="col-sm-3 col-xs-3">
								<div class="col-xs-12"><small>x &nbsp;<span>{{$quantities[$j]}}</span></small></div> 
							</div>

							<div class="col-sm-3 col-xs-3 text-right">
								<h6><span>Rs. </span>{{$quantities[$j]*$product->sell_price}}</h6>
							</div>
						</div>
						<div class="form-group"><hr /></div>
						@endfor
						<input type="hidden" value="{{$total}}" name="total_bill">
						<div class="form-group">
							<div class="col-xs-12">
								<strong>Subtotal</strong>
								<div class="pull-right"><span></span><span>Nil</span></div>
							</div>
							<div class="col-xs-12">
								<small>Shipping</small>
								<div class="pull-right"><span>-</span></div>
							</div>
						</div>
						<div class="form-group"><hr /></div>
						<div class="form-group">
							<div class="col-xs-12">
								<strong>Order Total</strong>
								<div class="pull-right"><span>Rs. </span><span>{{$total}}</span></div>
							</div>
						</div>
					</div>
					
                </div>
                <div class="panel-group payments-options" id="accordion" role="tablist" aria-multiselectable="true">
                    
                    <h3>Select Payment Mode</h3>
                    
                    <div class="panel radio panel-default">
                        <div class="panel-heading" role="tab" id="heading4">
                            <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                    <span class="dot"></span> PayUMoney
                                </a>
                                <span class="overflowed pull-right"><img src="/wealth-assets/img/preview/payments/paypal-2.jpg" alt=""/></span>
                            </h4>
                        </div>
                        <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4"></div>
                    </div>
				</div>
				<!--REVIEW ORDER END-->

				<div style="text-align: right;" >
					<button onclick="goToCheckout({{$id}})" class="btn btn-success btn-block">Chek Out</button>
				</div>
			</div>

		</form>
	</div>
	<div class="row cart-footer">

	</div>
</div>

<script>
	function resetForm(){
		document.getElementById("myForm").reset();
	}

	function goToCheckout(id){
		var country = $('#country').val();
		var name = $('#name').val();
		var phone = $('#phone-number').val();
		var state = $('#state_id').val();
		var city = $('#city_id').val();
		var pin_code = $('#pin_code').val();
		var address = $('#address').val();

		if( country == ""){
			$('#country-error').html('* This field is required.');
		}
		else{
			$('#country-error').hide();
		}

		if( name == ""){
			$('#name-error').html('* This field is required.');

		}
		else{
			$('#name-error').hide();
		}

		if( address == ""){

			$('#address-error').html('* This field is required.');
		}
		else{
			$('#address-error').hide();
		}

		if( phone == ""){
			$('#phone-error').html('* This field is required.');

		}
		else{
			$('#phone-error').hide();
		}

		if( city == ""){
			$('#city-error').html('* This field is required.');

		}
		else{
			$('#city-error').hide();
		}

		if( state == ""){
			$('#state-error').html('* This field is required.');

		}
		else{
			$('#state-error').hide();
		}

		if( pin_code == ""){
			$('#pin-error').html('* This field is required.');

		}
		else{
			$('#pin-error').hide();
		}

		if(address!="" && name!="" && state!="" && city!="" && phone!="" && pin_code!="" && country!="" ){
			 window.location.href ="/checkout-pagew/"+id;
			


	}
		}
</script>
@endsection