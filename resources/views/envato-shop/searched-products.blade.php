@extends('layouts/wealth-shop')

@section('content')

<style>
	#size_filter option {   padding: 3.5% }
	#size_filter #first {  border-bottom: 1px solid #eee; padding: 3.5% }
</style>

<!-- Main Content - start -->
<main>
	<section class="container">
		<input type="hidden" id="cat_id" name="cat_id" value="{{$keyword}}"> 
		<input type="hidden" id="filter_color" name="filter_color" value=""> 
		<input type="hidden" id="filter_size" name="filter_size" value=""> 
		<input type="hidden" id="filter_min" name="filter_min" value="0"> 
		<input type="hidden" id="filter_max" name="filter_max" value="{{$max}}"> 
		<input type="hidden" id="filter_weight" name="filter_weight" value=""> 
		<input type="hidden" id="filter_sort" name="filter_sort" value="">

		<ul class="b-crumbs">
			<li>
				<a href="/hellow">
					HOME
				</a>
			</li>
			
			<li>
				<span>{{strtoupper($keyword)}}</span>
			</li>
		</ul>
		<h1 class="main-ttl"><span>Categories</span></h1>
		<!-- Catalog Sidebar - start -->
		<div class="section-sb">

			<!-- Catalog Categories - start -->

			<div class="section-sb-current" style="max-height: 300px; overflow: auto">
				<h3>{{-- <a href="/categorywise-productsw/{{$cat->slug_name}}">{{strtoupper($cat->Mcategory_name)}}<span id="section-sb-toggle" class="section-sb-toggle"><span class="section-sb-ico"></span></span></a> --}}</h3>
				<ul class="section-sb-list" id="section-sb-list" >
					@if(!empty($subcat))
					@foreach($subcat as $sub)
					<li class="categ-1">
						<a href="/categorywise-productsw/{{$sub->slug_name}}">
							<span class="categ-1-label">{{ucfirst($sub->Mcategory_name)}}</span>
						</a>
					</li>
					@endforeach
					@endif
					
					{{-- <li class="categ-1 has_child">
						<a href="catalog-list.html">
							<span class="categ-1-label">Accessories</span>
							<span class="section-sb-toggle"><span class="section-sb-ico"></span></span>
						</a>
						<ul>
							<li class="categ-2">
								<a href="catalog-list.html">
									<span class="categ-2-label">Sunglasses</span>
								</a>
							</li>
							<li class="categ-2">
								<a href="catalog-list.html">
									<span class="categ-2-label">Tech Cases</span>
								</a>
							</li>
							<li class="categ-2">
								<a href="catalog-list.html">
									<span class="categ-2-label">Jewelry</span>
								</a>
							</li>
							<li class="categ-2">
								<a href="catalog-list.html">
									<span class="categ-2-label">Stella</span>
								</a>
							</li>
						</ul>
					</li> --}}

				</ul>
			</div>
			<!-- Catalog Categories - end -->

			<!-- Filter - start -->
			<div class="section-filter">
				
				<div class="section-filter-cont">
					<h3 style="background-color: #38B64A; width: 90%; text-align: center; margin-left: 5%; color: #fff; padding: 2%">Price Filter</h3><br>
					<div class="section-filter-price" style="height: auto ; text-align: center; ">
						<div class="range-slider section-filter-price" id="price_filter" data-min="0" data-max="{{$max}}" data-from="0" data-to="{{$max-500}}" data-prefix="Rs." data-grid="false" onchange="sendPrice(this)"></div>
					<button type="button" class="btn btn-success" style="margin: 10px" onclick="filterProducts()">Filter</button>					
					</div>					
				</div>
			</div>

			@if(!empty($colors))
			<div class="section-filter">
				
				<div class="section-filter-cont">
					<h3 style="background-color: #38B64A; width: 90%; text-align: center; margin-left: 5%; color: #fff; padding: 2%">Color Filter</h3><br>
					<div>
						<select name="color" id="color_filter" class="form-control" style="width: 90%; margin: 0 5% 2% 5%"  onchange="sendColor()">
							@foreach($colors as $color)
							<option value="{{$color}}">{{ucfirst($color)}}</option>
							@endforeach
						</select>
					</div>										
				</div>
			</div>
			@endif

			@if(!empty($sizes))
			<div class="section-filter">
				
				<div class="section-filter-cont">
					<h3 style="background-color: #38B64A; width: 90%; text-align: center; margin-left: 5%; color: #fff; padding: 2%">Size Filter</h3><br>
					<div>
						<select name="color" id="size_filter" class="form-control" style="width: 90%; margin: 0 5% 2% 5%" size='6'  onchange="sendSize()">
							<option value="" id="first">Select Size</option>
							<option value="S">S</option>
							<option value="M">M</option>
							<option value="L">L</option>
							<option value="XL">XL</option>
							<option value="XXL">XXL</option>
						</select>
					</div>										
				</div>
			</div>
			@endif

			@if(!empty($weights))
			<div class="section-filter">
				
				<div class="section-filter-cont">
					<h3 style="background-color: #38B64A; width: 90%; text-align: center; margin-left: 5%; color: #fff; padding: 2%">Weight Filter</h3><br>
					<div>
						<select name="color" id="color_filter" onchange="sendWeight()">
							@foreach($weights as $weight)
							<option value="{{$weight}}">{{ucfirst($weight)}}</option>
							@endforeach
						</select>
					</div>										
				</div>
			</div>
			@endif

			<!-- Filter - end -->

		</div>
		<!-- Catalog Sidebar - end -->
		<!-- Catalog Items | List V1 - start -->
		<div class="section-cont">

			<!-- Catalog Topbar - start -->
			<div class="section-top">

				<!-- View Mode -->
				<ul class="section-mode">
					<li class="section-mode-gallery"><a title="View mode: Gallery" href="catalog-gallery.html"></a></li>
					<li class="section-mode-list active"><a title="View mode: List" href="catalog-list.html"></a></li>
					
				</ul>

				<!-- Sorting -->
				<div class="section-sortby">
					<p>Sort By</p>
					<ul>
						<li onclick="sort('latest')">
							<a>Latest</a>
						</li>
						<li onclick="sort('l-h')">
							<a>Price : Low-High</a>
						</li>
						<li onclick="sort('h-l')">
							<a>Price : High-Low</a>
						</li>
						<li onclick="sort('a-z')">
							<a>A <i class="fa fa-angle-right"></i> Z</a>
						</li>
						<li onclick="sort('z-a')">
							<a>Z <i class="fa fa-angle-right"></i> A</a>
						</li>
						
					</ul>
				</div>

				<!-- Count per page -->
				{{-- <div class="section-count">
					<p>12</p>
					<ul>
						<li><a href="#">12</a></li>
						<li><a href="#">24</a></li>
						<li><a href="#">48</a></li>
					</ul>
				</div> --}}

			</div>
			<!-- Catalog Topbar - end -->
			<div class="prod-items section-items" id="product_cat">
				<?php $i=0; ?>
				@if(!empty($searched_products))
				@foreach($searched_products as $product)
				<?php $i++; ?>
				<div class="prodlist-i">
					<a class="prodlist-i-img" href="/product-detailw/{{$product->slug_name}}"><!-- NO SPACE --><img src="/assetsss/images/AdminProduct/{{$product->image1}}" alt="Adipisci aperiam commodi"><!-- NO SPACE --></a>
					<div class="prodlist-i-cont">
						<h3><a href="/product-detailw/{{$product->slug_name}}">{{ucfirst($product->name)}}</a></h3>
						<div class="prodlist-i-txt">
												
						</div>
						<div class="prodlist-i-skuwrap">

							@if(!empty($product->color))

							<div class="prodlist-i-skuitem">
								<p class="prodlist-i-skuttl">Color</p>
								<ul class="prodlist-i-skucolor">
									<input type="hidden" name="product_color" id="product_color{{$product->id}}" value="{{$product->product_color}}">
									<li class="active">{{$product->product_color}}</li>
									
								</ul>
							</div>
							@endif

							@if(!empty($product->product_size))
							<?php  
							$sizes = explode(',',$product->product_size);
							//var_dump($sizes);
							?>
							<div class="prodlist-i-skuitem">
								<p class="prodlist-i-skuttl">Available Sizes</p>				<select name="product_size" class="form-control" id="product_size{{$product->id}}">
									@foreach($sizes as $size)
									<option value="{{$size}}">{{$size}}</option>
									@endforeach
								</select>			
							</div>
							@endif

							<div class="">
								<p class="prodlist-i-skuttl">Purchase Value</p>
								<ul>
									<input type="hidden" id="purchase-value{{$product->id}}" name="purchaseValue">
									<input type="radio" name="purchase_value{{$product->id}}" value="{{$product->jbv}}" onchange="handler1('jbv',{{$product->id}})"> JBV : {{$product->jbv}}<br>
									<input type="radio" name="purchase_value{{$product->id}}" value="{{$product->rvv}}" onchange="handler2('rbv',{{$product->id}})"> RBV : {{$product->rvv}}			
								</ul>
								<br>
							</div>
						</div>
						<div class="prodlist-i-action">
							<p class="prodlist-i-qnt">
								<input value="1" type="text" id="qty{{$product->id}}" >
								<a class="prodlist-i-plus"><i class="fa fa-angle-up" onclick="increment({{$product->id}})"></i></a>
								<a class="prodlist-i-minus"><i class="fa fa-angle-down" onclick="decrement({{$product->id}})"></i></a>
							</p>
							<p class="prodlist-i-addwrap">
								<a class="prodlist-i-add" onclick="@if($product->availability == 'yes') addToCart({{$product->id}}) @else showModel(); @endif">Add to cart</a>
							</p>
							<span class="prodlist-i-price">
								<b>Rs. {{$product->sell_price}}</b>
							</span>
						</div>
						<p class="prodlist-i-info">
							<a class="prodlist-i-favorites"  onclick="addToWishlist({{$product->id}})"><i class="fa fa-heart"></i> Add to wishlist</a>
							<a href="#" class="qview-btn prodlist-i-qview"><i class="fa fa-search"></i> Quick view</a>
							{{-- <a class="prodlist-i-compare" href="#"><i class="fa fa-bar-chart"></i> Compare</a> --}}
						</p>
					</div>

					<div class="prodlist-i-props-wrap">
						<p style="padding: 10px">{{ucfirst($product->short_descriptions)}}</p>
						{{-- <ul class="prodlist-i-props">
							<li><b>Exterior</b> Silt Pocket</li>
							<li><b>Material</b> PU</li>
							<li><b>Occasion</b> Versatile</li>
							<li><b>Shape</b> Casual Tote</li>
							<li><b>Pattern Type</b> Solid</li>
							<li><b>Style</b> American Style</li>
							<li><b>Hardness</b> Soft</li>
							<li><b>Decoration</b> None</li>
							<li><b>Closure Type</b> Zipper</li>
						</ul> --}}
					</div>
				</div>
				@endforeach
				@endif

				@if($i ==0)
				<div style="text-align: center;padding: 10%">
					<h2 style="font-size: 32px">No Product Available Right Now</h2>
				</div>
				@endif
				
			</div>

			<!-- Pagination - start -->
			{{-- <ul class="pagi">
				<li class="active"><span>1</span></li>
				<li><a href="#">2</a></li>
				<li><a href="#">3</a></li>
				<li><a href="#">4</a></li>
				<li class="pagi-next"><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
			</ul> --}}
			<!-- Pagination - end -->
		</div>

		<!-- Quick View Product - start -->
		<div class="qview-modal">
			<div class="prod-wrap">
				<a href="product.html">
					<h1 class="main-ttl">
						<span>Reprehenderit adipisci</span>
					</h1>
				</a>
				<div class="prod-slider-wrap">
					<div class="prod-slider">
						<ul class="prod-slider-car">
							<li>
								<a data-fancybox-group="popup-product" class="fancy-img" href="http://placehold.it/500x525">
									<img src="http://placehold.it/500x525" alt="">
								</a>
							</li>
							<li>
								<a data-fancybox-group="popup-product" class="fancy-img" href="http://placehold.it/500x591">
									<img src="http://placehold.it/500x591" alt="">
								</a>
							</li>
							<li>
								<a data-fancybox-group="popup-product" class="fancy-img" href="http://placehold.it/500x525">
									<img src="http://placehold.it/500x525" alt="">
								</a>
							</li>
							<li>
								<a data-fancybox-group="popup-product" class="fancy-img" href="http://placehold.it/500x722">
									<img src="http://placehold.it/500x722" alt="">
								</a>
							</li>
							<li>
								<a data-fancybox-group="popup-product" class="fancy-img" href="http://placehold.it/500x722">
									<img src="http://placehold.it/500x722" alt="">
								</a>
							</li>
							<li>
								<a data-fancybox-group="popup-product" class="fancy-img" href="http://placehold.it/500x722">
									<img src="http://placehold.it/500x722" alt="">
								</a>
							</li>
							<li>
								<a data-fancybox-group="popup-product" class="fancy-img" href="http://placehold.it/500x722">
									<img src="http://placehold.it/500x722" alt="">
								</a>
							</li>
						</ul>
					</div>
					<div class="prod-thumbs">
						<ul class="prod-thumbs-car">
							<li>
								<a data-slide-index="0" href="#">
									<img src="http://placehold.it/500x525" alt="">
								</a>
							</li>
							<li>
								<a data-slide-index="1" href="#">
									<img src="http://placehold.it/500x591" alt="">
								</a>
							</li>
							<li>
								<a data-slide-index="2" href="#">
									<img src="http://placehold.it/500x525" alt="">
								</a>
							</li>
							<li>
								<a data-slide-index="3" href="#">
									<img src="http://placehold.it/500x722" alt="">
								</a>
							</li>
							<li>
								<a data-slide-index="4" href="#">
									<img src="http://placehold.it/500x722" alt="">
								</a>
							</li>
							<li>
								<a data-slide-index="5" href="#">
									<img src="http://placehold.it/500x722" alt="">
								</a>
							</li>
							<li>
								<a data-slide-index="6" href="#">
									<img src="http://placehold.it/500x722" alt="">
								</a>
							</li>
						</ul>
					</div>
				</div>

				<div class="prod-cont">
					<p class="prod-actions">
						<a class="prod-favorites" onclick = ""><i class="fa fa-heart"></i> Add to Wishlist</a>
						<a href="#" class="prod-compare"><i class="fa fa-bar-chart"></i> Compare</a>
					</p>
					<div class="prod-skuwrap">
						<p class="prod-skuttl">Color</p>
						<ul class="prod-skucolor">
							<li class="active">
								<img src="img/color/blue.jpg" alt="">
							</li>
							<li>
								<img src="img/color/red.jpg" alt="">
							</li>
							<li>
								<img src="img/color/green.jpg" alt="">
							</li>
							<li>
								<img src="img/color/yellow.jpg" alt="">
							</li>
							<li>
								<img src="img/color/purple.jpg" alt="">
							</li>
						</ul>
						<p class="prod-skuttl">Sizes</p>
						<div class="offer-props-select">
							<p>XL</p>
							<ul>
								<li><a href="#">XS</a></li>
								<li><a href="#">S</a></li>
								<li><a href="#">M</a></li>
								<li class="active"><a href="#">XL</a></li>
								<li><a href="#">L</a></li>
								<li><a href="#">4XL</a></li>
								<li><a href="#">XXL</a></li>
							</ul>
						</div>
					</div>
					<div class="prod-info">
						<p class="prod-price">
							<b class="item_current_price">$238</b>
						</p>
						<p class="prod-qnt">
							<input type="text" value="1">
							<a href="#" class="prod-plus"><i class="fa fa-angle-up"></i></a>
							<a href="#" class="prod-minus"><i class="fa fa-angle-down"></i></a>
						</p>
						<p class="prod-addwrap">
							<a href="#" class="prod-add">Add to cart</a>
						</p>
					</div>
					<ul class="prod-i-props">
						<li>
							<b>SKU</b> 05464207
						</li>
						<li>
							<b>Manufacturer</b> Mayoral
						</li>
						<li>
							<b>Material</b> Cotton
						</li>
						<li>
							<b>Pattern Type</b> Print
						</li>
						<li>
							<b>Wash</b> Colored
						</li>
						<li>
							<b>Style</b> Cute
						</li>
						<li>
							<b>Color</b> Blue, Red
						</li>
						<li><a href="#" class="prod-showprops">All Features</a></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- Quick View Product - end -->
	</section>
</main>
<!-- Main Content - end -->
@endsection

@section('script')



<script>
	
function showModel(){
	Swal('Product is out of stock right now');
}

	function increment(id){
		var quant = $('#qty'+id).val();
		// alert(quant);

		quant++;
		$('#qty'+id).empty();
		$('#qty'+id).val(quant);

	}

	function decrement(id){
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

		var quant = $('#qty'+id).val();
		// alert(quant);
		if(quant>0){
			quant--;
			$('#qty'+id).empty();
			$('#qty'+id).val(quant);
		}   
	}

	function handler1(value,id) { 
		var valu = value;
		// alert(valu); 
		$('#purchase-value'+id).val(valu);
	}

	function handler2(value,id) { 
		var valu = value;
		// alert(valu); 

		$('#purchase-value'+id).val(valu);
	}

	var countproduct=0;

//adding items to cart
function addToCart(id){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
	var pro_quant = $('#qty'+id).val();
	var pro_color = $('#product_color'+id).val();
	var pro_size = $('#product_size'+id).val();
	var purchase = $('#purchase-value'+id).val();
	var cat_id = $('#cat_id').val();

	if(pro_quant != ""  && purchase != ""){
		// alert(pro_quant+","+purchase+","+pro_color+","+pro_size);

		countproduct++;
		$.ajax({
			/* the route pointing to the post function */
			url: '/add-to-cartw',
			type: 'POST',
			/* send the csrf-token and the input to the controller */
			dataType: 'JSON',
			data: {_token: CSRF_TOKEN, id: id, quantity: pro_quant,money_type : purchase,color: pro_color, size: pro_size},
			success: function (data) { 
				window.location.href = '/searched-productsw?product_keyword='+cat_id;
				$('#cart-count').html(countproduct);
            // $("#ajaxdata").html(data);
        }
    });
	}
	else{
		Swal('Purchase Value & Quantity both are mendatory to add product to the cart');
	}

}


//adding items to Wishlist
function addToWishlist(id,count){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var user_id = $('#u_id').val();

    if(user_id == 0){
        Swal('Please Login First.');
    } 
    else{
        $.ajax({
            /* the route pointing to the post function */
            url: '/add-to-wishlistw',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            data: {_token: CSRF_TOKEN, id: id, user_id: user_id},
            success: function (data) { 
                window.location.href = '';
                $("#ajaxdata").text(count);
            }
        }); 
    }
}




function sendColor(){
	$('#filter_color').val($('#color_filter').val());
	productFilter();
}

function sendSize(){
	$('#filter_size').val($('#size_filter').val());
	productFilter();
}

function sendWeight(){
	$('#filter_weight').val($('#weight_filter').val());
	productFilter();
}

function sort(slug){
	$('#filter_sort').val(slug);
	productFilter();
}

function sendPrice(slug){
	var min = $('.irs-from').html();
	var max = $('.irs-to').html();
	$('#filter_min').val(min);
	$('#filter_max').val(max);
}

function filterProducts(){
	productFilter();	
}

function productFilter(){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
	var color = $('#filter_color').val();
	var size = $('#filter_size').val();
	var weight = $('#filter_weight').val();
	var cat_id = $('#cat_id').val();
	var min = $('#filter_min').val();
	var max = $('#filter_max').val();
	var sort = $('#filter_sort').val();
	// alert(color+" / "+size+" / "+weight+" / "+cat_id+" / "+min+" / "+max+" / "+sort);

	$.ajax({
            /* the route pointing to the post function */
            url: '/filter-searched-products',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            data: {_token: CSRF_TOKEN, color: color, size: size, weight: weight, cat_id: cat_id, min: min, max: max, sort:sort },
            success: function (data) { 
            	$('#product_cat').html(data);
               
            }
        });
}

</script>

@endsection