@extends('layouts/wealth-shop')

@section('content')

<!-- Main Content - start -->
<main>
    <section class="container stylization maincont">


        <ul class="b-crumbs">
            <li>
                <a href="index.html">
                    Home
                </a>
            </li>
            <li>
                <span>Contacts</span>
            </li>
        </ul>
        <h1 class="main-ttl"><span>Contacts</span></h1>
        <!-- Contacts - start -->
        <br>
        <div class="iconbox-wrap">
            <div class="row iconbox-list">
                <div class="cf-xs-6 cf-sm-4 cf-lg-4 col-xs-6 col-sm-4 iconbox-i">
                    {{-- <p class="iconbox-i-img"><!-- NO SPACE --><img src="http://placehold.it/43x90" alt=""><!-- NO SPACE --></p> --}}
                    <h3 class="iconbox-i-ttl">Our Address</h3>
                    WZ-31, <br>
                    New Delhi-110045
                    <span class="iconbox-i-margin"></span>
                </div>
                <div class="cf-xs-6 cf-sm-4 cf-lg-4 col-xs-6 col-sm-4 iconbox-i">
                    {{-- <p class="iconbox-i-img"><!-- NO SPACE --><img src="http://placehold.it/47x90" alt=""><!-- NO SPACE --></p> --}}
                    <h3 class="iconbox-i-ttl">Email</h3>
                    <a href="mailto:info@wealthindia.global">info@wealthindia.global</a>
                    <span class="iconbox-i-margin"></span>
                </div>
                <div class="cf-xs-6 cf-sm-4 cf-lg-4 col-xs-6 col-sm-4 iconbox-i">
                    {{-- <p class="iconbox-i-img"><!-- NO SPACE --><img src="http://placehold.it/58x90" alt=""><!-- NO SPACE --></p> --}}
                    <h3 class="iconbox-i-ttl">Contact</h3>
                    <a href="tel:18001038581">18001038581</a>
                    <span class="iconbox-i-margin"></span>
                </div>
            </div>
        </div>

        <!-- Contacts Info - end -->
        <div class="social-wrap">
            <div class="social-list">
                <div class="social-i">
                    <a rel="nofollow" target="_blank" href="http://facebook.com/">
                        <p class="social-i-img">
                            <i class="fa fa-facebook"></i>
                        </p>
                        <p class="social-i-ttl">Facebook</p>
                    </a>
                </div>
                <div class="social-i">
                    <a rel="nofollow" target="_blank" href="http://google.com/">
                        <p class="social-i-img">
                            <i class="fa fa-google-plus"></i>
                        </p>
                        <p class="social-i-ttl">Google +</p>
                    </a>
                </div>
                <div class="social-i">
                    <a rel="nofollow" target="_blank" href="http://twitter.com/">
                        <p class="social-i-img">
                            <i class="fa fa-twitter"></i>
                        </p>
                        <p class="social-i-ttl">Twitter</p>
                    </a>
                </div>
                
                <div class="social-i">
                    <a rel="nofollow" target="_blank" href="http://instagram.com/">
                        <p class="social-i-img">
                            <i class="fa fa-instagram"></i>
                        </p>
                        <p class="social-i-ttl">Instagram</p>
                    </a>
                </div>
                <div class="social-i">
                    <a rel="nofollow" target="_blank" href="http://youtube.com/">
                        <p class="social-i-img">
                            <i class="fa fa-youtube"></i>
                        </p>
                        <p class="social-i-ttl">Youtube</p>
                    </a>
                </div>
            </div>
        </div>

        <!-- Contact Form -->
        <div class="contactform-wrap">
            <form action="#" class="form-validate" id="contact-form">
                <h3 class="component-ttl component-ttl-ct component-ttl-hasdesc"><span>Feedback</span></h3>
                {{-- <p class="component-desc component-desc-ct">Alias minima veritatis unde illo deserunt omnis facilis</p> --}}
                <p class="contactform-field contactform-text">
                    <label class="contactform-label">Name</label><!-- NO SPACE --><span class="contactform-input"><input placeholder="Name" type="text" name="name" id="contact-name"></span>
                <p id="name-error" style="text-align: center; color: red"></p> 
                </p>
                <p class="contactform-field contactform-email">
                    <label class="contactform-label">E-mail</label><!-- NO SPACE --><span class="contactform-input"><input placeholder="Your E-mail" type="text" name="email" id="contact-email"></span>
                <p id="email-error" style="text-align: center; color: red"></p>
                </p>
                <p class="contactform-field contactform-email">
                    <label class="contactform-label">Subject</label><!-- NO SPACE --><span class="contactform-input"><input placeholder="Subject" type="text" name="subject" id="contact-subject"></span>
                <p id="subject-error" style="text-align: center; color: red"></p>
                </p>
                <p class="contactform-field contactform-textarea">
                    <label class="contactform-label">Message</label><!-- NO SPACE --><span class="contactform-input"><textarea placeholder="Your message" name="message" id="contact-message"></textarea></span>
                <p id="message-error" style="text-align: center; color: red"></p>
                </p>
                <p class="contactform-submit">
                    <input value="Send" type="button" class="btn btn-success" onclick="sendContactForm()">
                </p>
            </form>
        </div>
        <br>
        <br>
        <!-- Google Maps -->
        <div class="contacts-map allstore-gmap">
            <div class="marker" data-zoom="15" data-lat="-37.81485261872975" data-lng="144.95655298233032" data-marker="img/marker.png">534-540 Little Bourke St, Melbourne VIC 3000, Australia</div>
        </div>
        <!-- Contacts - end -->

    </section>
</main>
<!-- Main Content - end -->

@endsection

@section('script')

<script>
    function sendContactForm(){
        var name = $('#contact-name').val();
        var email = $('#contact-email').val();
        var subject = $('#contact-subject').val();
        var message = $('#contact-message').val();
        if(name == ""){
            $('#name-error').text('* Name is required.');
        }
        else{
            $('#name-error').hide();

        }

        if(email == ""){
            $('#email-error').text('* Email is required.');
        }
        else{
            $('#email-error').hide();

        }

        if(subject == ""){
            $('#subject-error').text('* Subject is required.');
        }
        else{
            $('#subject-error').hide();

        }

        if(message == ""){
            $('#message-error').text('* Message is required.');
        }
        else{
            $('#message-error').hide();

        }

        if(name!="" && email !="" && subject!="" && message!=""){

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
            /* the route pointing to the post function */
            url: '/post-contact-form',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            data: {_token: CSRF_TOKEN, email: email, name:name , subject:subject, message:message},
            success: function (data) { 
                Swal(data.message);
            }
        });
        }
    } 
</script>


@endsection

