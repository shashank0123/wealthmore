<?php
use App\Product;
$bill = 0;
$id=0;
if(!empty($member->id)){
 $id=$member->id;
}

?>

@extends('layouts/wealth-shop')

@section('content')

<!-- Main Content - start -->
<main>
    <section class="container stylization maincont">


        <ul class="b-crumbs">
            <li>
                <a href="index.html">
                    Home
                </a>
            </li>
            <li>
                <span>Cart</span>
            </li>
        </ul>
        <h1 class="main-ttl"><span>Cart</span></h1>
        <!-- Cart Items - start -->
        <form action="#">
            <div class="cart-items-wrap">
            @if(session()->get('cart') != null)
                    <?php
                    $ids = array();
                    $cat_id = array();                  
                    $quantities = array();  
                    $sizes = array();  
                    $values = array();  
                    $colors = array();  
                    $i=0;

                    foreach(session()->get('cart') as $data)
                    {
                        $ids[$i] = $data->product_id;                               
                        $quantities[$i] = $data->quantity;
                        $colors[$i] = $data->color;
                        $sizes[$i] = $data->size;
                        $values[$i] = $data->purchase_type;
                        $i++;
                    } 
                     
                    ?>
                <table class="cart-items">
                    <thead>
                    <tr>

                        <td class="cart-image">Image</td>                                
                                <td class="cart-ttl">Product Name</td>                            
                                <td class="cart-price">JBV</td>
                                <td class="cart-price">RBV</td>
                                <td class="cart-quantity">Quantity</td>
                                <td class="cart-price">MRP</td>
                                <td class="cart-price">DP</td>
                                <td class="cart-summ">Total</td>
                        <td class="cart-del">&nbsp;</td>

                        {{-- <td class="cart-image">Photo</td>
                        <td class="cart-ttl">Products</td>
                        <td class="cart-price">Price</td>
                        <td class="cart-quantity">Quantity</td>
                        <td class="cart-summ">Summ</td> --}}
                    </tr>
                    </thead>
                    <tbody id="hide-cart">

                         @for($j=0 ; $j<$i ; $j++ )
                            <?php
                            $product = Product::where('id',$ids[$j])->first(); 

                            if($product != null)
                            {
                                $cat_id[$j] = $product->category_id;
                            }
                            else
                            {
                                $cat_id[$j] = 0;
                            }

                            $bill = $bill + $quantities[$j]*$product->sell_price;
                            ?>
                    <tr id="remove-list{{$product->id}}">
                        <td class="cart-image">
                            <a href="product.html">
                                <img src="/assetsss/images/AdminProduct/{{$product->image1}}" alt="{{ucfirst($product->name)}}">
                            </a>
                        </td>
                        <td class="cart-ttl">
                            <a href="product.html">{{ucfirst($product->name)}}</a>
                            @if(!empty($colors[$j]))<p>Color: {{$colors[$j]}}</p>@endif
                            @if(!empty($sizes[$j]))<p>Size: {{$sizes[$j]}}</p>@endif
                        </td>
                        <td class="cart-price">
                            @if($values[$j] == 'jbv')<b>{{$product->jbv}}</b>@else<b>-</b>@endif
                        </td>
                        <td class="cart-price">
                            @if($values[$j] == 'rbv')<b>{{$product->rvv}}</b>@else<b>-</b>@endif
                        </td>

                        <td class="cart-quantity">
                            <p class="cart-qnt">
                                <input value="{{$quantities[$j]}}" type="text" id="qty{{$product->id}}" readonly>
                                <a class="cart-plus" onclick="increment({{$product->id}})"><i class="fa fa-angle-up"></i></a>
                                <a class="cart-minus" onclick="decrement({{$product->id}})"><i class="fa fa-angle-down"></i></a>
                            </p>
                        </td>
                        <td class="cart-price">
                            <b>Rs. {{$product->mrp}}</b>
                        </td>
                        <td class="cart-price">
                            <b>Rs. {{$product->sell_price}}</b>
                       </td>
                        <td class="cart-summ">
                            <b id="total{{$product->id}}">Rs. {{$quantities[$j]*$product->sell_price}}</b>
                            <p class="cart-forone">unit price <b>$220</b></p>
                        </td>
                        <td class="cart-del">
                            <a onclick="removeSessionData({{$product->id}})" class="cart-remove"></a>
                        </td>
                    </tr>
                    @endfor
                    
                    </tbody>
                </table>
                @else

                    <div class="statement" style="text-align: center;">
                        <h2>Nothing in Cart</h2>
                    </div>
                    @endif
            </div>
            <ul class="cart-total">
                <li class="cart-summ">TOTAL: <b id="bill">Rs. {{$bill}} </b></li>
            </ul>
            <div class="cart-submit">
                {{-- <div class="cart-coupon">
                    <input placeholder="your coupon" type="text">
                    <a class="cart-coupon-btn" href="#"><img src="img/ok.png" alt="your coupon"></a>
                </div> --}}
                <a href="/checkoutw/{{$id}}" class="cart-submit-btn">Checkout</a>
                <a onclick="clearCart()" class="cart-clear">Clear cart</a>
            </div>
        </form>
        <!-- Cart Items - end -->

    </section>
</main>
<!-- Main Content - end -->

@endsection


@section('script')

<script>
    //adding items to Wishlist
function increment(id,count){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var value = $("#qty"+id).val();
        var type = 'inc';
        // alert(value);
   
        $.ajax({
            /* the route pointing to the post function */
            url: '/increament-quantity',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            data: {_token: CSRF_TOKEN, product_id: id, type : type},
            success: function (data) { 
                
                $("#qty"+id).val(++value);
                $("#total"+id).text('Rs. '+data.amount);
                $("#bill").text('Rs. '+data.bill);
            }
        }); 
    } 

     //adding items to Wishlist
function removeSessionData(id){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
          
        $.ajax({
            /* the route pointing to the post function */
            url: '/remove-cart-product',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            data: {_token: CSRF_TOKEN, product_id: id},
            success: function (data) {                 
                $("#remove-list"+id).hide();                
                $("#bill").text('Rs. '+data.bill);
            }
        }); 
    } 

    //adding items to Wishlist
function decrement(id,count){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var value = $("#qty"+id).val();
        var type = 'dec';
        // alert(value);
        if(value>1){    
        $.ajax({
            /* the route pointing to the post function */
            url: '/increament-quantity',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            data: {_token: CSRF_TOKEN, product_id: id,type: type},
            success: function (data) { 
                
                $("#qty"+id).val(--value);
                $("#total"+id).text('Rs. '+data.amount);
                $("#bill").text('Rs. '+data.bill);
            }
        }); 
    }
    }

    function clearCart(){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
         $.ajax({
            /* the route pointing to the post function */
            url: '/clear-cart',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            data: {_token: CSRF_TOKEN},
            success: function (data) { 
               $('#hide-cart').hide(); 
               $('#bill').text('Rs. 0'); 
                $("#cart-count").html('0');
               
            }
        }); 
    }
</script>


@endsection