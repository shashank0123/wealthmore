@extends('layouts/wealth-shop')

@section('content')

<!-- Main Content - start -->
<main>
    <section class="container">

<input type="hidden" name="u_id" value="@if(!empty($member->id)){{$member->id}}@elseif(!empty($user->id)){{$user->id}}@else{{0}}@endif" id="u_id">
<input type="hidden" name="product_slug" value="{{$product->slug_name}}" id="product_slug">


        <ul class="b-crumbs">
            <li>
                <a href="/hellow">
                    Home
                </a>
            </li>          
            <li>
                <span>{{ucfirst($category->Mcategory_name)}}</span>
            </li>
        </ul>
        <h1 class="main-ttl"><span>{{strtoupper($product->name)}}</span></h1>
        <!-- Single Product - start -->
        <div class="prod-wrap prod2-wrap">

            <!-- Product Images -->
            <div class="prod-slider-wrap prod2-slider-wrap">
                <div class="prod-slider">
                    <ul class="prod2-slider-car">
                        <li>
                            <a data-fancybox-group="product" class="fancy-img" href="{{URL::to('/assetsss/images/AdminProduct')}}/{{$product->image1}}">
                                <img src="{{URL::to('/assetsss/images/AdminProduct')}}/{{$product->image1}}" alt="{{$product->name}}">
                            </a>
                        </li>
                        <li>
                            <a data-fancybox-group="product" class="fancy-img" href="{{URL::to('/assetsss/images/AdminProduct')}}/{{$product->image2}}">
                                <img src="{{URL::to('/assetsss/images/AdminProduct')}}/{{$product->image2}}" alt="{{$product->name}}">
                            </a>
                        </li>
                        <li>
                            <a data-fancybox-group="product" class="fancy-img" href="{{URL::to('/assetsss/images/AdminProduct')}}/{{$product->image3}}">
                                <img src="{{URL::to('/assetsss/images/AdminProduct')}}/{{$product->image3}}" alt="">
                            </a>
                        </li>
                        {{-- <li>
                            <a data-fancybox-group="product" class="fancy-img" href="http://placehold.it/500x722">
                                <img src="http://placehold.it/500x722" alt="">
                            </a>
                        </li>
                        <li>
                            <a data-fancybox-group="product" class="fancy-img" href="http://placehold.it/500x722">
                                <img src="http://placehold.it/500x722" alt="">
                            </a>
                        </li>
                        <li>
                            <a data-fancybox-group="product" class="fancy-img" href="http://placehold.it/500x722">
                                <img src="http://placehold.it/500x722" alt="">
                            </a>
                        </li> --}}
                    </ul>
                </div>
                <div class="prod-thumbs">
                    <ul class="prod2-thumbs-car">
                        <li>
                            <a data-slide-index="0" href="#">
                                <img class="scroll_active" src="{{URL::to('/assetsss/images/AdminProduct')}}/{{$product->image1}}" alt="{{$product->name}}">
                            </a>
                        </li>
                        <li>
                            <a data-slide-index="1" href="#">
                                <img src="{{URL::to('/assetsss/images/AdminProduct')}}/{{$product->image2}}" alt="{{$product->name}}">
                            </a>
                        </li>
                        <li>
                            <a data-slide-index="2" href="#">
                                <img src="{{URL::to('/assetsss/images/AdminProduct')}}/{{$product->image3}}" alt="{{$product->name}}">
                            </a>
                        </li>
                        {{-- <li>
                            <a data-slide-index="3" href="#">
                                <img src="http://placehold.it/500x722" alt="">
                            </a>
                        </li>
                        <li>
                            <a data-slide-index="4" href="#">
                                <img src="http://placehold.it/500x722" alt="">
                            </a>
                        </li>
                        <li>
                            <a data-slide-index="5" href="#">
                                <img src="http://placehold.it/500x722" alt="">
                            </a>
                        </li> --}}
                    </ul>
                </div>
            </div>

            <!-- Product Description/Info -->
            <div class="prod-cont">
                <div class="prod-cont-inner">
                    <div class="prod-cont-txt">
                        {{$product->short_descriptions}}
                    </div>
                    <p class="prod-actions">
                        <a  onclick="addToWishlist({{$product->id}})" class="prod-favorites"><i class="fa fa-heart"></i> Wishlist</a>
                        {{-- <a href="#" class="prod-compare"><i class="fa fa-bar-chart"></i> Compare</a> --}}
                    </p>
                    <div class="prod-skuwrap">
                        @if(!empty($product->product_color))

                        <p class="prod-skuttl"><b>Color</b> : {{ucfirst($product->product_color)}}</p>
                        {{-- <p>{{ucfirst($product->product_color)}}</p> --}}
                        {{-- <select class="form-control" name="product_color" id="product_color">
                            <option value="{{$product->product_color)}}">
                                {{ucfirst($product->product_color)}}
                            </option>                            
                        </select> --}}
                        @endif
                        @if(!empty($product->product_size))
                        <p class="prod-skuttl">CLOTHING SIZES</p>
                        <div class="offer-props-select">
                            <p id="product_size">S</p>
                            <ul>
                                @foreach($product_size as $size)
                                <li><a>{{$size}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    </div>

                    

                    <div class="">
                                <p class="prodlist-i-skuttl" style="font-size: 18px">Purchase Value</p>
                                <ul>
                                    <input type="hidden" id="purchase-value{{$product->id}}" name="purchaseValue">
                                    <input type="radio" name="purchase_value{{$product->id}}" value="{{$product->jbv}}" onchange="handler1('jbv',{{$product->id}})"> JBV : {{$product->jbv}}<br>
                                    <input type="radio" name="purchase_value{{$product->id}}" value="{{$product->rvv}}" onchange="handler2('rbv',{{$product->id}})"> RBV : {{$product->rvv}}            
                                </ul>
                                <br>
                            </div>

                    <div class="prod-info">
                        <p class="prod-price">
                            <b class="item_current_price">Rs. {{$product->sell_price}}</b>
                        </p>
                        <p class="prod-qnt">
                            <input value="1" type="text" id="qty{{$product->id}}" readonly>
                            <a class="prod-plus" onclick="increment({{$product->id}})"><i class="fa fa-angle-up"></i></a>
                            <a class="prod-minus" onclick="decrement({{$product->id}})"><i class="fa fa-angle-down"></i></a>
                        </p>
                        <p class="prod-addwrap">
                            <a class="prod-add" rel="nofollow" onclick="@if($product->availability == 'yes') addToCart({{$product->id}}) @else showModel(); @endif">Add to cart</a>
                        </p>
                    </div>
                    {{-- <ul class="prod-i-props">
                        <li>
                            <b>SKU</b> 05464207
                        </li>
                        <li>
                            <b>Material</b> Nylon
                        </li>
                        <li>
                            <b>Pattern Type</b> Solid
                        </li>
                        <li>
                            <b>Wash</b> Colored
                        </li>
                        <li>
                            <b>Style</b> Sport
                        </li>
                        <li>
                            <b>Color</b> Blue
                        </li>
                        <li>
                            <b>Gender</b> Unisex
                        </li>
                        <li>
                            <b>Rain Cover</b> No
                        </li>
                        <li>
                            <b>Exterior</b> Solid Bag
                        </li>
                        <li><a href="#" class="prod-showprops">All Features</a></li>
                    </ul> --}}


                    <!-- Product Tabs -->
                    <div class="prod-tabs-wrap">
                        <ul class="prod-tabs">
                            <li><a data-prodtab-num="1" class="active" href="#" data-prodtab="#prod-tab-1">Description</a></li>
                            {{-- <li><a data-prodtab-num="2" id="prod-props" href="#" data-prodtab="#prod-tab-2">Features</a></li>
                            <li><a data-prodtab-num="3" href="#" data-prodtab="#prod-tab-3">Video</a></li>
                            <li><a data-prodtab-num="4" href="#" data-prodtab="#prod-tab-4">Articles</a></li> --}}
                            <li><a data-prodtab-num="5" href="#" data-prodtab="#prod-tab-5">Reviews</a></li>
                        </ul>
                        <div class="prod-tab-cont">

                            <p data-prodtab-num="1" class="prod-tab-mob active" data-prodtab="#prod-tab-1">Description</p>
                            <div class="prod-tab stylization" id="prod-tab-1">
                                <p><?php echo $product->long_descriptions; ?></p>
                            </div>
                            {{-- <p data-prodtab-num="2" class="prod-tab-mob" data-prodtab="#prod-tab-2">Features</p>
                            <div class="prod-tab prod-props" id="prod-tab-2">
                                <ul class="prod-props">
                                    <li><span class="prod-propttl"><span>Exterior</span></span> <span class="prod-propval">Silt Pocket</span></li>
                                    <li><span class="prod-propttl"><span>Material</span></span> <span class="prod-propval">PU</span></li>
                                    <li><span class="prod-propttl"><span>Occasion</span></span> <span class="prod-propval">Versatile</span></li>
                                    <li><span class="prod-propttl"><span>Shape</span></span> <span class="prod-propval">Casual Tote</span></li>
                                    <li><span class="prod-propttl"><span>Pattern Type</span></span> <span class="prod-propval">Solid</span></li>
                                    <li><span class="prod-propttl"><span>Style</span></span> <span class="prod-propval">American Style</span></li>
                                    <li><span class="prod-propttl"><span>Hardness</span></span> <span class="prod-propval">Soft</span></li>
                                    <li><span class="prod-propttl"><span>Decoration</span></span> <span class="prod-propval">None</span></li>
                                    <li><span class="prod-propttl"><span>Closure Type</span></span> <span class="prod-propval">Zipper</span></li>
                                </ul>
                            </div>
                            <p data-prodtab-num="3" class="prod-tab-mob" data-prodtab="#prod-tab-3">Video</p>
                            <div class="prod-tab prod-tab-video" id="prod-tab-3">
                                <iframe width="853" height="480" src="https://www.youtube.com/embed/kaOVHSkDoPY?rel=0&amp;showinfo=0" allowfullscreen></iframe>
                            </div>
                            <p data-prodtab-num="4" class="prod-tab-mob" data-prodtab="#prod-tab-4">Articles</p>
                            <div class="prod-tab prod-tab-articles" id="prod-tab-4">
                                <div class="flexslider post-rel-wrap" id="post-rel-car" data-type="type-2">
                                    <ul class="slides">
                                        <li class="posts-i">
                                            <a class="posts-i-img" href="post.html"><span style="background: url(http://placehold.it/354x236)"></span></a>
                                            <time class="posts-i-date" datetime="2017-01-01 08:18"><span>09</span> Feb</time>
                                            <div class="posts-i-info">
                                                <a class="posts-i-ctg" href="blog.html">Articles</a>
                                                <h3 class="posts-i-ttl"><a href="post.html">Adipisci corporis velit</a></h3>
                                            </div>
                                        </li>
                                        <li class="posts-i">
                                            <a class="posts-i-img" href="post.html"><span style="background: url(http://placehold.it/360x203)"></span></a>
                                            <time class="posts-i-date" datetime="2017-01-01 08:18"><span>05</span> Jan</time>
                                            <div class="posts-i-info">
                                                <a class="posts-i-ctg" href="blog.html">Reviews</a>
                                                <h3 class="posts-i-ttl"><a href="post.html">Excepturi ducimus recusandae</a></h3>
                                            </div>
                                        </li>
                                        <li class="posts-i">
                                            <a class="posts-i-img" href="post.html"><span style="background: url(http://placehold.it/360x224)"></span></a>
                                            <time class="posts-i-date" datetime="2017-01-01 08:18"><span>17</span> Apr</time>
                                            <div class="posts-i-info">
                                                <a class="posts-i-ctg" href="blog.html">Reviews</a>
                                                <h3 class="posts-i-ttl"><a href="post.html">Consequuntur minus numquam</a></h3>
                                            </div>
                                        </li>
                                        <li class="posts-i">
                                            <a class="posts-i-img" href="post.html"><span style="background: url(http://placehold.it/314x236)"></span></a>
                                            <time class="posts-i-date" datetime="2017-01-01 08:18"><span>21</span> May</time>
                                            <div class="posts-i-info">
                                                <a class="posts-i-ctg" href="blog.html">Articles</a>
                                                <h3 class="posts-i-ttl"><a href="post.html">Non ex sapiente excepturi</a></h3>
                                            </div>
                                        </li>
                                        <li class="posts-i">
                                            <a class="posts-i-img" href="post.html"><span style="background: url(http://placehold.it/318x236)"></span></a>
                                            <time class="posts-i-date" datetime="2017-01-01 08:18"><span>24</span> Jan</time>
                                            <div class="posts-i-info">
                                                <a class="posts-i-ctg" href="blog.html">Articles</a>
                                                <h3 class="posts-i-ttl"><a href="post.html">Veritatis officiis</a></h3>
                                            </div>
                                        </li>
                                        <li class="posts-i">
                                            <a class="posts-i-img" href="post.html"><span style="background: url(http://placehold.it/354x236)"></span></a>
                                            <time class="posts-i-date" datetime="2017-01-01 08:18"><span>08</span> Sep</time>
                                            <div class="posts-i-info">
                                                <a class="posts-i-ctg" href="blog.html">Reviews</a>
                                                <h3 class="posts-i-ttl"><a href="post.html">Ratione magni laudantium</a></h3>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div> --}}
                            <p data-prodtab-num="5" class="prod-tab-mob" data-prodtab="#prod-tab-5">Reviews</p>
                            <div class="prod-tab" id="prod-tab-5">
                                @if(!empty($reviews))
                                <ul class="reviews-list">
                                    @foreach($reviews as $review)
                                    <li class="reviews-i existimg">
                                        <div class="reviews-i-img">
                                            <img src="http://placehold.it/120x120" alt="Averill Sidony">
                                            <div class="reviews-i-rating">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <time datetime="2017-12-21 12:19:46" class="reviews-i-date">21 May 2017</time>
                                        </div>
                                        <div class="reviews-i-cont">
                                            <p>Numquam aliquam maiores ratione dolores ducimus, laborum hic similique delectus. Neque saepe nobis omnis laudantium itaque tempore voluptate harum error, illum nemo, reiciendis architecto, quam tenetur amet sit quisquam cum.<br>Pariatur cum tempore eius nulla impedit cumque odit quos porro iste a voluptas, optio alias voluptate minima distinctio facere aliquid quasi, vero illum tenetur sed temporibus eveniet obcaecati.</p>
                                            <span class="reviews-i-margin"></span>
                                            <h3 class="reviews-i-ttl">Averill Sidony</h3>
                                            <p class="reviews-i-showanswer"><span data-open="Show answer" data-close="Hide answer">Show answer</span> <i class="fa fa-angle-down"></i></p>
                                        </div>
                                        <div class="reviews-i-answer">
                                            <p>Thanks for your feedback!<br>
                                                Nostrum voluptate autem, eaque mollitia sed rem cum amet qui repudiandae libero quaerat veniam accusantium architecto minima impedit. Magni illo illum iure tempora vero explicabo, esse dolores rem at dolorum doloremque iusto laboriosam repellendus. <br>Numquam eius voluptatum sint modi nihil exercitationem dolorum asperiores maiores provident repellat magnam vitae, consequatur omnis expedita, accusantium voluptas odit id.</p>
                                                <span class="reviews-i-margin"></span>
                                            </div>
                                        </li>
                                        @endforeach
                                        
                                    </ul>
                                    @endif
                                    <div class="prod-comment-form">
                                        <h3>Add your review</h3>
                                        <form method="GET" action="/product-detailw/{{$product->slug_name}}">
                                            <input type="text" placeholder="Name" name="name">
                                            <input type="text" placeholder="E-mail" name="email">
                                            <input type="hidden" placeholder="Rating" name="rating" >
                                            <textarea placeholder="Your review" name="review"></textarea>
                                            <div class="prod-comment-submit">
                                                <input type="submit" value="Submit">
                                                <div class="prod-rating">
                                                    <i class="fa fa-star-o" title="5"></i><i class="fa fa-star-o" title="4"></i><i class="fa fa-star-o" title="3"></i><i class="fa fa-star-o" title="2"></i><i class="fa fa-star-o" title="1"></i>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!-- Single Product - end -->

            <!-- Related Products - start -->
            <div class="prod-related prod2-related">
                <h2><span>Related products</span></h2>
                <div class="prod-related-car" id="prod-related-car">
                    @if(!empty($related_products))
                    <ul class="slides">
                        <li class="prod-rel-wrap">
                        @foreach($related_products as $product)
                            <div class="prod-rel">
                                <a href="product.html" class="prod-rel-img">
                                    <img src="{{URL::to('/assetsss/images/AdminProduct')}}/{{$product->image1}}" alt="{{$product->name}}">
                                </a>
                                <div class="prod-rel-cont">
                                    <h3><a href="product.html">{{strtoupper($product->name)}}</a></h3>
                                    <p class="prod-rel-price">
                                        <b>Rs. {{$product->sell_price}}</b>
                                    </p>
                                    <p class="prod-rel-price">
                                        <b style="font-size: 12px">JBV : {{$product->jbv}}</b>
                                        <b style="font-size: 12px">&nbsp;/&nbsp;&nbsp;RBV : {{$product->rvv}}</b>
                                    </p>
                                    <div class="prod-rel-actions">
                                        <a title="Wishlist" class="prod-rel-favorites" onclick="addToWishlist({{$product->id}})"><i class="fa fa-heart"></i></a>
                                        {{-- <a title="Compare" href="#" class="prod-rel-compare"><i class="fa fa-bar-chart"></i></a> --}}
                                        <p class="prod-i-addwrap">
                                            <a title="Add to cart" class="prod-i-add" onclick="addToCart({{$product->id}})"><i class="fa fa-shopping-cart"></i></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </li>
                    </ul>
                    @endif
                </div>
            </div>
            <!-- Related Products - end -->

        </section>
    </main>
    <!-- Main Content - end -->

    @endsection

    @section('script')
    <script>

    //adding items to Wishlist
function addToWishlist(id,count){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var user_id = $('#u_id').val();
    var slug = $('#product_slug').val();
    if(user_id == 0){
        Swal('Please Login First.');
    } 
    else{
        $.ajax({
            /* the route pointing to the post function */
            url: '/add-to-wishlistw',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            data: {_token: CSRF_TOKEN, id: id, user_id: user_id},
            success: function (data) { 
                window.location.href = '/product-detailw/'+slug;
                $("#ajaxdata").text(count);
            }
        }); 
    }
}

function showModel(){
    Swal('Product is out of stock right now');
}

    function increment(id){
        var quant = $('#qty'+id).val();
        // alert(quant);

        quant++;
        $('#qty'+id).empty();
        $('#qty'+id).val(quant);

    }

    function decrement(id){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        var quant = $('#qty'+id).val();
        // alert(quant);
        if(quant>0){
            quant--;
            $('#qty'+id).empty();
            $('#qty'+id).val(quant);
        }   
    }

    function handler1(value,id) { 
        var valu = value;
        // alert(valu); 
        $('#purchase-value'+id).val(valu);
    }

    function handler2(value,id) { 
        var valu = value;
        // alert(valu); 

        $('#purchase-value'+id).val(valu);
    }

    var countproduct=0;

//adding items to cart
function addToCart(id){
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var pro_quant = $('#qty'+id).val();
    var pro_color = $('#product_color'+id).val();
    var pro_size = $('#product_size'+id).val();
    var purchase = $('#purchase-value'+id).val();
    var cat_id = $('#cat_id').val();
    var slug = $('#product_slug').val();

    if(pro_quant != ""  && purchase != ""){
        // alert(pro_quant+","+purchase+","+pro_color+","+pro_size);

        countproduct++;
        $.ajax({
            /* the route pointing to the post function */
            url: '/add-to-cartw',
            type: 'POST',
            /* send the csrf-token and the input to the controller */
            dataType: 'JSON',
            data: {_token: CSRF_TOKEN, id: id, quantity: pro_quant,money_type : purchase,color: pro_color, size: pro_size, purchase: purchase},
            success: function (data) { 
                window.location.href = '/product-detailw/'+slug;
                $('#cart-count').html(countproduct);
            // $("#ajaxdata").html(data);
        }
    });
    }
    else{
        Swal('Purchase Value & Quantity both are mendatory to add product to the cart');
    }

}
</script>
    @endsection