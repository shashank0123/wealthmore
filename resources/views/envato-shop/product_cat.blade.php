
<div class="prod-items section-items" id="product_cat">
				<?php $i=0; ?>
				@if(!empty($searched_products))
				@foreach($searched_products as $product)
				<?php $i++; ?>
				<div class="prodlist-i">
					<a class="prodlist-i-img" href="/product-detailw/{{$product->slug_name}}"><!-- NO SPACE --><img src="/assetsss/images/AdminProduct/{{$product->image1}}" alt="Adipisci aperiam commodi"><!-- NO SPACE --></a>
					<div class="prodlist-i-cont">
						<h3><a href="/product-detailw/{{$product->slug_name}}">{{ucfirst($product->name)}}</a></h3>
						<div class="prodlist-i-txt">
												
						</div>
						<div class="prodlist-i-skuwrap">

							@if(!empty($product->color))

							<div class="prodlist-i-skuitem">
								<p class="prodlist-i-skuttl">Color</p>
								<ul class="prodlist-i-skucolor">
									<input type="hidden" name="product_color" id="product_color{{$product->id}}" value="{{$product->product_color}}">
									<li class="active">{{$product->product_color}}</li>
									
								</ul>
							</div>
							@endif

							@if(!empty($product->product_size))
							<?php  
							$sizes = explode(',',$product->product_size);
							//var_dump($sizes);
							?>
							<div class="prodlist-i-skuitem">
								<p class="prodlist-i-skuttl">Available Sizes</p>				<select name="product_size" class="form-control" id="product_size{{$product->id}}">
									@foreach($sizes as $size)
									<option value="{{$size}}">{{$size}}</option>
									@endforeach
								</select>			
							</div>
							@endif

							<div class="">
								<p class="prodlist-i-skuttl">Purchase Value</p>
								<ul>
									<input type="hidden" id="purchase-value{{$product->id}}" name="purchaseValue">
									<input type="radio" name="purchase_value{{$product->id}}" value="{{$product->jbv}}" onchange="handler1('jbv',{{$product->id}})"> JBV : {{$product->jbv}}<br>
									<input type="radio" name="purchase_value{{$product->id}}" value="{{$product->rvv}}" onchange="handler2('rbv',{{$product->id}})"> RBV : {{$product->rvv}}			
								</ul>
								<br>
							</div>
						</div>
						<div class="prodlist-i-action">
							<p class="prodlist-i-qnt">
								<input value="1" type="text" id="qty{{$product->id}}" >
								<a class="prodlist-i-plus"><i class="fa fa-angle-up" onclick="increment({{$product->id}})"></i></a>
								<a class="prodlist-i-minus"><i class="fa fa-angle-down" onclick="decrement({{$product->id}})"></i></a>
							</p>
							<p class="prodlist-i-addwrap">
								<a class="prodlist-i-add" onclick="@if($product->availability == 'yes') addToCart({{$product->id}}) @else showModel(); @endif">Add to cart</a>
							</p>
							<span class="prodlist-i-price">
								<b>Rs. {{$product->sell_price}}</b>
							</span>
						</div>
						<p class="prodlist-i-info">
							<a class="prodlist-i-favorites"  onclick="addToWishlist({{$product->id}})"><i class="fa fa-heart"></i> Add to wishlist</a>
							<a href="#" class="qview-btn prodlist-i-qview"><i class="fa fa-search"></i> Quick view</a>
							{{-- <a class="prodlist-i-compare" href="#"><i class="fa fa-bar-chart"></i> Compare</a> --}}
						</p>
					</div>

					<div class="prodlist-i-props-wrap">
						<p style="padding: 10px">{{ucfirst($product->short_descriptions)}}</p>
						{{-- <ul class="prodlist-i-props">
							<li><b>Exterior</b> Silt Pocket</li>
							<li><b>Material</b> PU</li>
							<li><b>Occasion</b> Versatile</li>
							<li><b>Shape</b> Casual Tote</li>
							<li><b>Pattern Type</b> Solid</li>
							<li><b>Style</b> American Style</li>
							<li><b>Hardness</b> Soft</li>
							<li><b>Decoration</b> None</li>
							<li><b>Closure Type</b> Zipper</li>
						</ul> --}}
					</div>
				</div>
				@endforeach
				@endif

				@if($i ==0)
				<div style="text-align: center;padding: 10%">
					<h2 style="font-size: 32px">No Product Available Right Now</h2>
				</div>
				@endif
				
			</div>