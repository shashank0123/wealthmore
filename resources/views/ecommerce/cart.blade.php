<?php
use App\Product;
use App\Review;
$bill = 0;
$i=0;
?>

@extends('layouts.ecommerce2')

@section('content')
<style>
	.page-large-title { padding: 20px 0; color: #37bc9b;}
</style>


<div class="page-large-title">
	{{-- <div class="container">
		<h1 class="title">Shopping Cart</h1>
			<nav class="woocommerce-breadcrumb">
				<a class="home" href="#">Home</a>  / <span>Shopping Cart</span>
			</nav>
		</div><!-- .container -->
	</div> --}}<!-- .page-large-title -->
	
	<div class="section-common"  id="refresh-div">
		<div class="container">
			<div class="row">
				<div class="col-md-9 col-xs-12">
					<h2 class="section-title">Shopping Cart</h2>
					<div class="clear"></div>
					@if(session()->get('cart') != null)

					<?php

					$ids = array();
					$cat_id = array();					
					$quantities = array();


					foreach(session()->get('cart') as $data)
					{
						$ids[$i] = $data->product_id;								
						$quantities[$i] = $data->quantity;
						$i++;
					}						
					?>
					<table class="shop_table cart shop-cart-table" id="ajaxTable">
						<thead>
							<tr>
								<th class="product-name" colspan="2">Product</th>
								<th class="product-price">Price</th>
								<th class="product-quantity">Quantity</th>
								<th class="product-subtotal">Total</th>
								{{-- <th class="product-shipping">Delivery info</th> --}}
								<th class="product-remove">Delete</th>
							</tr>
						</thead>
						<tbody>
							
							@for($j=0 ; $j<$i ; $j++ )
							<?php
							$product = Product::where('id',$ids[$j])->first(); 
							if($product != null)
							{
								$cat_id[$j] = $product->category_id;
							}
							else
							{
								$cat_id[$j] = 0;
							}
							?>
							<tr class="cart_item" id="cart{{$product->id}}">
								<td class="product-thumbnail">
									<a href="/productdetail/{{$product->id}}"><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }} " alt="" class="thumb"/></a>
								</td>
								<td class="product-name">
									<div class="product-name-wrap">
										<a href="#" class="title">{{$product->name}}</a>
										<p><?php echo htmlspecialchars_decode($product->short_descriptions); ?></p>
										<dl class="variation">
											{{-- <dt class="variation-Size">Size:</dt>
											<dd class="variation-Size"><p>L</p></dd> --}}
										</dl>
									</div>
								</td>
								<td class="product-price" data-title="Price">
									<span class="amount">Rs. {{ $product->sell_price }}.00</span>
								</td>
								<td class="product-quantity" data-title="Quantity">
									<div class="quantity js__number" readonly><input type="number" id="quantity" onchange="handleChange(this);" class="js__target" min="1" value="{{$quantities[$j]}}" /><button type="button" class=" fa-plus fa" onclick="increment({{$ids[$j]}},{{$quantities[$j]}})"></button><button type="button" class=" fa-minus fa" onclick="decrement({{$ids[$j]}},{{$quantities[$j]}})"></button></div>
								</td>
								<td class="product-subtotal" data-title="Subtotal" id="totalamount">
									<?php
									$total = $quantities[$j]*$product->sell_price;
									$bill =$bill +  $total;
									?>
									<span class="amount" >Rs. {{$total}}.00</span>
								</td>
								{{-- <td class="product-shipping" data-title="Shipping">
									Free Shipping
								</td> --}}
								<td class="product-remove">
									<button class="remove" title="Remove this item" onclick="deleteCartProduct({{$ids[$j]}})"></button> 
								</td>
							</tr><!-- .cart_item-->
							@endfor						
						</tbody>
					</table><!-- .shop-cart-table -->
					@else

					<div class="sattement">
						<h2>Nothing in Cart</h2>
					</div>
					@endif
					{{-- <div class="clearfix middle-cart">
						<div class="pull-left">
							<div class="coupon">
								<input type="text" name="coupon_code" class="input-text input-text-common" id="coupon_code" placeholder="Coupon code">
								<input type="submit" class="btn-coupon button-black" name="apply_coupon" value="Apply Coupon">
							</div>
						</div>
						<div class="pull-right right-side">
							<a href="#" class="button-white-border-dark btn-update-cart">Update Cart</a>
							<a href="/checkout" class="btn-process-to-checkout button-green">Proceed to checkout</a>
						</div>
					</div><!-- .middle-cart --> --}}
				</div><!-- .col -->


				<div class="col-md-3 col-xs-12" >
					<h2 class="section-title">Cart Total</h2>
					<div class="clear"></div>
					<div class="shop-cart-total">
						<table class="shop_table shop-cart-total-table">
							<tr class="cart-subtotal">
								<th>Total</th>
								<td><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol" id="total_price">Rs. {{$bill}}.00</span></span></td>
							</tr>
							<tr class="cart-shipping">
								<th>Shipping</th>
								<td><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">Rs. </span>0.00</span></td>
							</tr>
							<tr class="cart-grand-total">
								<th>Grand Total</th>
								<td><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol" id="grand_total">Rs. {{$bill}}.00</span></span></td>
							</tr>
						</table><!-- .shop-cart-total-table -->
					</div>
					<div class="checkout" style="text-align: center;"><!-- .shop-cart-total -->
						<a href="/checkout" class="button-green button-normal">Checkout</a>
					</div>
				</div><!-- .col -->

				<div class="row">
					{{-- <div class="col-md-6 col-xs-12 section-common">


						<h2 class="section-title small-spacing">Calculate Shipping</h2>
						<div class="clear"></div>
						<form action="#">
							<div class="row">
								<div class="col-xs-12">
									<div class="form-control-row">
										<div class="select-common">
											<select class="js__select2" data-min-results="Infinity" data-placeholder="Country">
												<option value="" label="Country"></option>
												<option value="en">England</option>
												<option value="us">USA</option>
												<option value="vn">VietNam</option>
											</select>
										</div>
									</div>
								</div><!-- col -->
								<div class="col-xs-12">
									<div class="form-control-row">
										<div class="select-common">
											<select class="js__select2" data-min-results="Infinity" data-placeholder="Town / City">
												<option value="" label="Town / City"></option>
												<option value="en">New York</option>
												<option value="us">Lodon</option>
												<option value="vn">Ho Chi Minh</option>
											</select>
										</div>
									</div>
								</div><!-- col -->
								<div class="col-sm-6 col-ip-6 col-xs-12">
									<div class="form-control-row">
										<input type="text" class="input-text-common" placeholder="State">
									</div>
								</div><!-- col -->
								<div class="col-sm-6 col-ip-6 col-xs-12">
									<div class="form-control-row">
										<input type="text" class="input-text-common" placeholder="Zip / Postal Code">
									</div>
								</div><!-- col -->
								<div class="col-xs-12">
									<div class="form-control-row last">
										<input type="submit" class="button-normal button-black" value="Get a Quote">
									</div>
								</div><!-- col -->
							</div><!-- row -->
						</form>
					</div> --}}<!-- .col -->
					
					
				</div><!-- .row -->
			</div><!-- .container -->
		</div><!-- .section-common -->
	</div>
	
	<div class="container">
		{{-- //May you also like... --}}
		<div class="col-xs-12 section-common" style="height: 530px">
			<h2 class="section-title small-spacing">May you also like...</h2>
			<div class="clear"></div>
			<div class="slick-wrap">
				<div class="products products-grid slick-middle-arrow js__slickslider" data-arrows="true" data-dots="false" data-show="4" data-responsive="{'992':2,'650':1}">

					@for($k=0 ; $k<$i ; $k++)
					<?php
					$final_rate = 0;
					$allproducts = Product::where('category_id',$cat_id[$k])->orderBy('created_at','desc')->get();
					$rate = Review::where('product_id',$product->id)->avg('rating');
					if($rate != null){
						$final_rate = floor($rate);
					}
					

					?>
					@if($allproducts != null)
					@foreach($allproducts as $products)
					<div class="slick-slide">
						<div class="product-grid">
							<div class="thumb">
								<a href="/productdetail/{{$products->id}}"><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $products->image1 }} " alt="" style="width: 200px; height: 270px"></a>
								<ul class="controls">
									<li><a style="cursor: pointer;" onclick="addWishList({{ $products->id }})"><i class="fa fa-heart"></i><span>LIKE IT</span></a></li>
									
								</ul>
							</div>
							<a href="/productdetail/{{$products->id}}" class="content">
								<h2 class="title">{{$products->name}}t</h2>
								<span class="category">Men</span>
								<span class="price">
									<span class="amount">Rs. {{$products->sell_price}}</span>
								</span>
							</a>
						</div>
					</div><!-- product -->
					@endforeach
					@endif
					@endfor							
				</div><!-- .products -->
			</div><!-- .slick-wrap -->
		</div><!-- .col -->
	</div>

	@endsection


	@section('script')



	{{-- For Modal Box --}}
	<div id="quickViewPopup" class="popup js__popup" style="margin-top: 500px">
		<div class="popup-overlay js__popup_close"></div>
		<div class="popup-body">
			<button type="button" class="popup-close-btn js__popup_close"><i class="fa fa-times"></i></button>
			<div class="container">
				<div class="popup-inside">
					<div class="row">
						<input type="hidden" name="product_id" value="" id="hidden_id"> 
						<div class="col-md-12 clearfix">
							<h2 class="single-title-border-bottom">Quick view</h2>
						</div><!-- col -->
						<div class="col-md-5">
							<div class="product-detail">
								<div class="product">
									<div class="images js__gallery">
										<a href="#" class="woocommerce-main-image zoom js__zoom_popup" data-target="#zoomPopup" data-zoom="" id="imggallery1">
											<img src="" alt="" id="imggallery2"/>
										</a><!-- .woocommerce-main-image -->
										<div class="thumbnails">									
											<a href="#" class="zoom js__thumb js__active" data-images="" data-zoom="" id="imgdz1" ><img src="" alt="" id="img1"/></a>
											<a href="#" class="zoom js__thumb" data-images="" data-zoom="" id="imgdz2" ><img src="" alt="" id="img2"/></a>
											<a href="#" class="zoom js__thumb" data-images="" data-zoom="" id="imgdz3" ><img src="" alt="" id="img3" /></a>											
										</div><!-- .thumbnails -->
										<div class="hidden">										
											<img src="" alt="" id="imgt1"/>
											<img src="" alt="" id="imgt2" />
											<img src="" alt="" id="imgt3"/>
											{{-- <img src="" alt="" /> --}}
										</div><!-- load images zone -->
									</div><!-- .images -->
								</div><!-- .product -->
							</div><!-- .product-detail -->
						</div><!-- col -->
						<div class="col-md-7">
							<div class="summary">
								<h2 class="product_title" id="viewname"></h2>
								<div class="summary-top">
									<div class="rating-box">
										<ul>
											<li> 

											</li>

										</ul>
									</div>
									<a href="#">Have 25 reviews</a> {{-- <span>/</span> <a href="#">Add your review</a> --}}
								</div>
								<p class="price">Rs. <span class="amount" id="viewprice"></span></p>
								{{-- <ul class="product_meta">
									<li><span>Brand:</span> Louis Vuitton</li>
									<li><span>Available:</span> In stock</li>
									<li><span>Product code:</span> ABC 123 456</li>
								</ul> --}}
								<div class="description">
									<p id="viewdescription"></p>
								</div>
								
								<form class="cart">
									<div class="quantity js__number"><input id="quickquantity" type="number" class="js__target" value="1" /><button type="button" class="js__plus fa-plus fa"></button><button type="button" class="js__minus fa-minus fa"></button></div>
									<button type="button" class="single_add_to_cart_button" onclick="addToCart()">Add to cart</button>
								</form>
							</div><!-- .summary -->
						</div><!-- col -->
					</div><!-- .row -->
				</div><!-- .popup-inside -->
			</div><!-- .container -->
		</div><!-- .popup-body -->
	</div><!-- #quickViewPopup -->

	<div id="zoomPopup" class="popup popup-images js__popup">
		<div class="popup-overlay js__popup_close"></div>
		<div class="popup-body">
			<button type="button" class="popup-close-btn js__popup_close" onclick="reload()"><i class="fa fa-times"></i></button>
			<div class="popup-inside js__popup_images">

			</div><!-- .popup-inside -->
		</div><!-- .popup-body -->
	</div><!-- #zoomPopup -->




	<script>

		function handleChange(input) {
    if (input.value < 1) input.value = 1;
    if (input.value > 100) input.value = 100;
  }

		//modal  box of quick view
		function openview(id,name,price,mrp,description,img1,img2,img3,rate){
			$('#hidden_id').val(id);    
			$('#viewname').text(name);
			$('#viewdescription').text(description);
			$('#viewprice').text(price);
    //For main image
    $("#imggallery1").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#imggallery2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);

    //for a tag (data-zoom attribute)    
    $("#imgdz2").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#imgdz3").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
    $("#imgdz1").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);

    //for a tag (data-images attribute)
    $("#imgdz1").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#imgdz2").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#imgdz3").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);

    //for image tag (src attribute)
    $("#img1").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#img2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#img3").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
    $("#imgt1").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#imgt2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#imgt3").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
    var discount = ((mrp-price)*100)/mrp;
    $('#viewdiscount').text(discount.toFixed(2));
    $('#viewrate').val(rate);

}


var countproduct=<?php if(session()->get('count') != null){echo session()->get('count'); } else { echo 0;} ?>;

function addToCart(){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
	var pro_id = $('#hidden_id').val();
	var quant = $('#quickquantity').val();
	countproduct++;
	$.ajax({
		/* the route pointing to the post function */
		url: '/add-cart',
		type: 'POST',
		/* send the csrf-token and the input to the controller */
		data: {_token: CSRF_TOKEN, id: pro_id, quantity: quant},
		success: function (data) { 
			window.location.href = '/cart'; 
			$("#show-total").html(countproduct);           
		}
	}); 
}



var count=<?php echo session()->get('count'); ?>;
function deleteCartProduct(id){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
	count--;
	$('#cart'+id).hide();

	$.ajax({
		/* the route pointing to the post function */
		url: '/cart/delete-product/id',
		type: 'POST',
		datatype: 'JSON',
		/* send the csrf-token and the input to the controller */
		data: {_token: CSRF_TOKEN, id: id},
		success: function (data) { 
			// alert('data deleted successfully');
			$("#show-total").html(count);
			$("#total_price").empty();		
			$("#total_price").append(data.bill);		
			$("#grand_total").html(data.bill);		

					// $('#ajaxTable').load(document.URL + ' #ajaxTable');
					// $('#refresh-div').load(document.URL + ' #refresh-div');	
				}
			}); 
}

function increment(id,quant){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

			// $('#refresh-div').load(document.URL + ' #refresh-div');
			// $('#total-bill').load(document.URL + ' #total-bill');
			$.ajax({
				/* the route pointing to the post function */
				url: '/cart/increase/id/quant',
				type: 'POST',
				datatype: 'JSON',
				/* send the csrf-token and the input to the controller */
				data: {_token: CSRF_TOKEN, id: id, quant: quant},
				success: function (data) { 
					// alert(data);
					$('#quantity').html(data.quant);
					$('#refresh-div').load(document.URL + ' #refresh-div');
				}
			});
		}

		function decrement(id,quant){
			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
			// $('#refresh-div').load(document.URL + ' #refresh-div');
			// $('#total-bill').load(document.URL + ' #total-bill');
			$.ajax({
				/* the route pointing to the post function */
				url: '/cart/decrease/id/quant',
				type: 'POST',
				
				data: {_token: CSRF_TOKEN, id: id, quant: quant},
				success: function (data) { 
					// alert('decremented');
				// 	alert(data);
					$('#refresh-div').load(document.URL + ' #refresh-div');
				}
			});
		}

		//adding items to Wishlist
		function addWishList(id,count){
			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
			var userId = $('#user_id').val();
			$.ajax({
				/* the route pointing to the post function */
				url: '/add-to-wishlist',
				type: 'POST',
				/* send the csrf-token and the input to the controller */
				data: {_token: CSRF_TOKEN, id: id,user_id: userId},
				success: function (data) { 
					window.location.href = '/cart';
					$("#ajaxdata").text(count);
				}
			}); 
		}
	</script>

	@endsection