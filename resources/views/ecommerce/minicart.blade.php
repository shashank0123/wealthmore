<?php
use App\Review;
$total = 0;
?>

<div class="cart-list-content" id="showminicart">
	@if($cartproducts != null)
	<?php $i=0; $final_rate = 0; ?>

	<ul class="cart-list" style="max-height: 350px; overflow: auto">
		@foreach($cartproducts as $product)

		<li id="listhide{{$product->id}}">
			<a class="thumb" href="/productdetail/{{$product->id}}"><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }} " alt="" class="attachment-shop_thumbnail" style="width: 70px; height: 70px"></a>
			<a href="#" class="title">{{$product->name}}</a>
			<span class="quantity">
				<span class="amount">Rs. {{$product->sell_price}}.00</span> x {{$quantity[$i++]}}
			</span>
			<div class="rate">
			</div>
			<a title="Remove this item" class="mini-cart-remove" onclick="deleteProduct({{$product->id}})"><span class="fa fa-times"></span></a>						
		</li>
		@endforeach								
	</ul><!--/.cart-list -->
	@endif

	<div class="cart-list-subtotal">
		<strong class="txt fl">SubTotal:    </strong>
		<strong class="currency fr" id="bill">Rs. {{$bill}}.00</strong>
	</div><!--/.cart-list-subtotal -->
</div>