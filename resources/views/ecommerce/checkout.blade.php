<?php
use App\Product;
use App\Address;
use App\Country;
use App\State;
use App\City;
$address = Address::where('user_id',$member->id)->first();
$i=0;
$total = 0;
?>

@extends('layouts.ecommerce2')

@section('content')

<style>
	.page-large-title { padding: 20px 0; color: #37bc9b;}
	#show-address { display: none; }
</style>
<div class="page-large-title">
	<div class="container">
		<h1 class="title">Checkout</h1>			
	</div><!-- .container -->
</div><!-- .page-large-title -->	

<div class="section-common container">
	<form action="/checkout-page/{{$member->id}}" method="POST" class="checkout woocommerce-checkout" id="myForm" novalidation>
		<div class="row">
			{{csrf_field()}}
			<div class="col-md-8 col-xs-12 section-last">
				@if($address != null)
				<div class="woocommerce-billing-fields" id="hide-address">
					<h2 class="section-title small-spacing">Billing Infomation</h2>
					<div class="clear"></div>
					<div class="row">
						<div class="col-sm-12">
							<p class="form-row form-row-input">
								<label for="address">Address</label>
								<input class="input-text input-text-common" type="text" name="Address" id="address" value="{{$address->address}}">
							</p>
						</div>
						<?php 
						$country = Country::where('id',$address->country_id)->first();
						$state=State::where('id',$address->state_id)->first();
						$city=City::where('id',$address->city_id)->first();
						?>
						<div class="col-sm-12">
							<p class="form-row form-row-input">
								<label for="country">Country </label>
								<input class="input-text input-text-common" type="text" name="country" id="first-name" value="{{$country->country}}" >
							</p>
						</div>							
						<div class="col-sm-12">
							<p class="form-row form-row-input">
								<label for="state">State </label>
								<input class="input-text input-text-common" type="text" name="state" id="state" value="{{$state->state}}" >
							</p>
						</div>
						<div class="col-sm-12">
							<p class="form-row form-row-input">
								<label for="city">City <span class="required">*</span></label>
								<input class="input-text input-text-common" type="text" name="city" id="city" value="{{$city->city}}" >		
							</p>
						</div>
						<div class="col-sm-12">
							<p class="form-row form-row-input">
								<label for="pin-code">Pin CODE <span class="required">*</span></label>
								<input class="input-text input-text-common" type="number" name="pin_code" id="pin-code" value="{{$address->pin_code}}" >
							</p>
						</div>
						<div class="col-sm-12">
							<p class="form-row form-row-input">
								<label for="phone-number">Phone <span class="required">*</span></label>
								<input class="input-text input-text-common" type="number" name="phone" id="phone" value="{{$address->phone}}" >
							</p>
						</div>
					</div><!-- .row -->
				</div><!-- .woocommerce-billing-fields -->

				<div class="woocommerce-billing-fields" id="show-address">
					<h2 class="section-title small-spacing">Billing Infomation</h2>
					<div class="clear"></div>
					<div class="row">
						<div class="col-sm-12">
							<p class="form-row form-row-input">
								<label for="company-name">Address</label>
								<input class="input-text input-text-common" type="text" name="address" id="address" required="required">
							</p>
						</div>
						<div class="col-sm-12">
							<p class="form-row form-row-input">
								<label for="first-name">Country </label>
								<select class="input-text input-text-common" name="country_id" id="country_id"  required="required">
									@foreach($countries as $country)
									<option  value="{{$country->id}}"> {{$country->country}} </option>
									@endforeach
								</select>
							</p>
						</div>
						<div class="col-sm-12">
							<p class="form-row form-row-input">
								<label for="last-name">State <span class="required">*</span></label>
								<select class="input-text input-text-common" name="state_id" id="state_id" required="required"></select>
							</p>
						</div>
						<div class="col-sm-12">
							<p class="form-row form-row-input">
								<label for="email-address">City <span class="required">*</span></label>
								<select class="input-text input-text-common" name="city_id" id="city_id" required="required">
								</select>
							</p>
						</div>
						<div class="col-sm-12">
							<p class="form-row form-row-input">
								<label for="phone-number">Pin CODE <span class="required">*</span></label>
								<input class="input-text input-text-common" type="number" name="pin_code" id="phone-number" required="required">
							</p>
						</div>
						<div class="col-sm-12">
							<p class="form-row form-row-input">
								<label for="phone-number">Phone <span class="required">*</span></label>
								<input class="input-text input-text-common" type="number" name="phone" id="phone-number" required="required">
							</p>
						</div>							
					</div><!-- .row -->
				</div><!-- .woocommerce-billing-fields -->

				<div class="form-row place-order">
					<input type="button" onclick="showAddressForm()" class="button-green " name="submit"  value="Change Address">
				</div>
				

				@else
				<div class="woocommerce-billing-fields">
					<h2 class="section-title small-spacing">Billing Infomation</h2>
					<div class="clear"></div>
					<div class="row">
						<div class="col-sm-12">
							<p class="form-row form-row-input">
								<label for="company-name">Address</label>
								<input class="input-text input-text-common" type="text" name="address" id="address" required="required">
							</p>
						</div>
						<div class="col-sm-12">
							<p class="form-row form-row-input">
								<label for="first-name">Country </label>
								<select class="input-text input-text-common" name="country_id" id="country_id"  required="required">
									@foreach($countries as $country)
									<option  value="{{$country->id}}"> {{$country->country}} </option>
									@endforeach
								</select>
							</p>
						</div>
						<div class="col-sm-12">
							<p class="form-row form-row-input">
								<label for="last-name">State <span class="required">*</span></label>
								<select class="input-text input-text-common" name="state_id" id="state_id" required="required"></select>
							</p>
						</div>
						<div class="col-sm-12">
							<p class="form-row form-row-input">
								<label for="email-address">City <span class="required">*</span></label>
								<select class="input-text input-text-common" name="city_id" id="city_id" required="required">
								</select>
							</p>
						</div>
						<div class="col-sm-12">
							<p class="form-row form-row-input">
								<label for="phone-number">Pin CODE <span class="required">*</span></label>
								<input class="input-text input-text-common" type="number" name="pin_code" id="phone-number" required="required">
							</p>
						</div>
						<div class="col-sm-12">
							<p class="form-row form-row-input">
								<label for="phone-number">Phone <span class="required">*</span></label>
								<input class="input-text input-text-common" type="number" name="phone" id="phone-number" required="required">
							</p>
						</div>							
					</div><!-- .row -->
				</div><!-- .woocommerce-billing-fields -->
				@endif

				

			</div><!-- .col -->
			<div class="col-md-4 col-xs-12 section-last">
				<h2 id="order_review_heading" class="section-title small-spacing">Your Order</h2>
				<div class="clear"></div>
				<div id="order_review" class="woocommerce-checkout-review-order">
					<table class="shop_table woocommerce-checkout-review-order-table">
						<thead>
							<tr>
								<th class="product-name">Product</th>
								<th class="product-total">Total</th>
							</tr>
						</thead>
						<tbody style="width:100%; max-height:100px; overflow:width:100%" >
							@if(session()->get('cart') != null)

							<?php

							$ids = array();
							$cat_id = array();					
							$quantities = array();


							foreach(session()->get('cart') as $data)
							{
								$ids[$i] = $data->product_id;								
								$quantities[$i] = $data->quantity;
								$i++;
							}						
							?>
							@for($j=0 ; $j<$i ; $j++ )
							<?php
							$product = Product::where('id',$ids[$j])->first(); 
							$cost = $product->sell_price * $quantities[$j];
							$total = $total + $cost;

							?>

							<tr class="cart_item">
								<td class="product-name">{{$product->name}}</td>
								<td class="product-total"> <span class="amount">Rs. {{$cost}}</span> </td>
							</tr>
							@endfor
							@endif								
						</tbody>
						<tfoot>
							<tr class="cart-subtotal">
								<td>Subtotal products</td>
								<th><span class="amount">Rs. {{$total}}.00</span></th>
							</tr>
							<tr class="cart-shipping">
								<td>Shipping</td>
								<th><span class="amount">Rs. 0.00</span></th>
							</tr>
							<tr class="cart-tax">
								<td>Tax</td>
								<th><span class="amount">Rs. 0.00</span></th>
							</tr>
							<tr class="order-total">
								<td>Grand Total</td>
								<th><strong><span class="amount">Rs. {{$total}}.00</span></strong> </th>
							</tr>
						</tfoot>
					</table>
					<input type="hidden" name="total_bill" value="{{$total}}">
					<input type="hidden" name="member_id" value="{{$member->id}}">

					<div id="payment" class="woocommerce-checkout-payment">
						<ul class="wc_payment_methods">
							<li class="wc_payment_method">
								<input id="payment_method_direct" type="radio" checked="checked" name="payment_method" value="direct-bank">
								<label for="payment_method_direct">Direct Bank Transfer</label> 
								<div class="payment_box">
									<p>Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won’t be shipped until the funds have cleared in our account.</p>
								</div>
							</li>
								{{-- <li class="wc_payment_method">
									<input id="payment_method_cheque" type="radio" name="payment_method" value="cheque">
									<label for="payment_method_cheque"> Cheque Payment </label> 
									<div class="payment_box">
										<p>Please send your cheque to Store Name, Store Street, Store Town, Store State / County, Store Postcode.</p>
									</div>
								</li> --}}
								{{-- <li class="wc_payment_method">
									<input id="payment_method_cash" type="radio" name="payment_method" value="cash">
									<label for="payment_method_cash"> Cash On Delivery </label> 
									<div class="payment_box">
										<p>Pay with cash or check upon delivery.</p>
									</div>
								</li> --}}
								<li class="wc_payment_method">
									<input id="payment_method_payumoney" type="radio" class="input-radio" name="payment_method" value="payumoney">
									<label for="payment_method_payumoney">PayUMoney<img src="/assetsss/images/payumoney-banner.png" alt="" /><a href="#" class="about_payumoney" title="What is PayUMoney">What is PayUMoney?</a></label> 
									<div class="payment_box">
										<p>Pay via PayUMoney; you can pay with your credit card if you don’t have a PayUMoney account.</p>
									</div>
								</li>
							</ul>
						</div><!-- #payment -->
						
						<div class="form-row place-order">
							<input type="submit" class="button-green btn-place-order" name="submit" id="place_order" value="Place your order" data-value="Place order">
						</div>
						
					</div><!-- #order_review -->
				</div><!-- .col -->
			</div><!-- .row -->
		</form><!-- .woocommerce-checkout -->
	</div><!-- .container -->

	@endsection
	
	@section('script')
	<script>
		$(document).ready(function() {
			$('#country_id').on('change', function() {
				var countryID = $(this).val();
				if(countryID) {
					$.ajax({
						url: 'state/'+countryID,
						type: "GET",
						data : {"_token":"{{ csrf_token() }}",country_id:countryID},
						dataType: "json",
						success:function(data) {                       
							if(data){

								$('#state_id').empty();
								$('#state_id').focus;
								$('#state_id').append('<option value="">-- Select state_id --</option>'); 
								$.each(data, function(key, value){
									$('select[name="state_id"]').append('<option value="'+ value.id +'">' + value.state+ '</option>');
								});
							}else{
								$('#state_id').empty();
							}
						}
					});
				}else{
					$('#state_id').empty();
				}
			});
		});



		$(document).ready(function() {
			$('#state_id').on('change', function() {
				var stateID = $(this).val();
				if(stateID) {
					$.ajax({
						url: 'city/'+stateID,
						type: "GET",
						data : {"_token":"{{ csrf_token() }}",state_id:stateID},
						dataType: "json",
						success:function(data) {                       
							if(data){

								$('#city_id').empty();
								$('#city_id').focus;
								$('#city_id').append('<option value="">-- Select City --</option>'); 
								$.each(data, function(key, value){
									$('select[name="city_id"]').append('<option value="'+ value.id +'">' + value.city+ '</option>');
								});
							}else{
								$('#city_id').empty();
							}
						}
					});
				}else{
					$('#city_id').empty();
				}
			});
		});



		function showAddressForm(){
			$('#hide-address').hide();
			$('#show-address').show();
		}
	</script>
	@endsection