@extends('layouts/ecommerce2')
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>

@section('content')

<style>
    .about-us-area .overview-content { margin-top: 15%; text-align: center; }
     .about-us-area .overview-content span { color: #37bc9b; }
     .about-us_btn { display: block;padding: 10px; background-color:  #37bc9b; color: #fff; width: 20%; }
     .hiraola-about-us_btn-area a{ margin-left: 35% }
     .count { font-size: 48px; color:  #37bc9b; }
     .team-area { text-align: center; }
     .team-area h4 { font-size: 28px; font-weight: 600; margin-bottom: 20px }
     .count-title span { font-weight: bold;color: #777; font-size: 20px }
     a { color: #555; }
</style>
    
        <!-- Begin Hiraola's About Us Area --><br><br>
        <div class="about-us-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6 col-md-7 d-flex align-items-center">
                        <div class="overview-content">
                            <h2>Welcome To <span>Wealth India</span> Store!</h2><br>
                            <p class="short_desc">We Provide quality service and product. We believe in you and your experience. There is nothing else to say apart from the thing that after using our product, if you don't like it, return and take the money back.</p><br>
                            <div class="hiraola-about-us_btn-area" style="text-align: center;">
                                <a class="about-us_btn" href="/index">Shop Now</a><br>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-5">
                        <div class="overview-img text-center img-hover_effect">
                            <a href="#">
                                <img class="img-full" src="asset/images/about-us/1.jpg" alt="Hiraola's About Us Image">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div><br><br>
        <!-- Hiraola's About Us Area End Here -->

        <!-- Begin Hiraola's Project Countdown Area -->
        <div class="project-count-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-count text-center">
                            <div class="count-icon">
                                <span class="ion-ios-briefcase-outline"><i class='fas fa-briefcase' style='font-size:36px'></i></span><br><br>
                            </div>
                            <div class="count-title">
                                <h2 class="count">3000</h2>
                                <span>Product Sold</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-count text-center">
                            <div class="count-icon">
                                <span class="ion-ios-wineglass-outline"><i class='fas fa-wine-glass-alt' style='font-size:36px'></i></span><br><br>
                            </div>
                            <div class="count-title">
                                <h2 class="count">300</h2>
                                <span>Customers Served</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-count text-center">
                            <div class="count-icon">
                                <span class="ion-ios-lightbulb-outline"><i class='far fa-lightbulb' style='font-size:36px'></i></span><br><br>
                            </div>
                            <div class="count-title">
                                <h2 class="count">768</h2>
                                <span>Days Worked</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-count text-center">
                            <div class="count-icon">
                                <span class="ion-happy-outline"><i class='far fa-smile' style='font-size:36px'></i></span><br><br>
                            </div>
                            <div class="count-title">
                                <h2 class="count">450</h2>
                                <span>Product Served</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><br><br>
        <!-- Hiraola's Project Countdown Area End Here -->

        <!-- Begin Hiraola's Team Area -->
        <div class="team-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="section_title-2">
                            <h4>Our Team</h4>
                        </div>
                    </div>
                </div> <!-- section title end -->
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="team-member">
                            <div class="team-thumb img-hover_effect">
                                <a href="#">
                                    <img src="asset/images/about-us/team/1.jpg" alt="Our Team Member"><br><br>
                                </a>
                            </div>
                            <div class="team-content text-center">
                                <h3>Hemant Lamba</h3>
                                <p>Founder</p>
                                <a href="#">info@example.com</a>
                                <div class="hiraola-social_link">
                                    <ul>
                                        <li class="facebook">
                                            <a href="https://www.facebook.com/" data-toggle="tooltip" target="_blank" title="Facebook">
                                                <i class="fab fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li class="twitter">
                                            <a href="https://twitter.com/" data-toggle="tooltip" target="_blank" title="Twitter">
                                                <i class="fab fa-twitter-square"></i>
                                            </a>
                                        </li>
                                        <li class="youtube">
                                            <a href="https://www.youtube.com/" data-toggle="tooltip" target="_blank" title="Youtube">
                                                <i class="fab fa-youtube"></i>
                                            </a>
                                        </li>
                                        <li class="google-plus">
                                            <a href="https://www.plus.google.com/discover" data-toggle="tooltip" target="_blank" title="Google Plus">
                                                <i class="fab fa-google-plus"></i>
                                            </a>
                                        </li>
                                        <li class="instagram">
                                            <a href="https://rss.com/" data-toggle="tooltip" target="_blank" title="Instagram">
                                                <i class="fab fa-instagram"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end single team member -->
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="team-member">
                            <div class="team-thumb img-hover_effect">
                                <a href="#">
                                    <img src="asset/images/about-us/team/2.jpg" alt="Our Team Member"><br><br>
                                </a>
                            </div>
                            <style>
                                .hiraola-social_link ul { margin-left: 18% }
                                .hiraola-social_link ul li {
                                    float: left;
                                    display: block;
                                    border: 1px solid #777; 
                                    margin: 5px
                                }
                                .hiraola-social_link ul li a i{ padding: 5px }
                            </style>
                            <div class="team-content text-center" style="text-align: center;">
                                <h3>Sarah Sanchez</h3>
                                <p>Web Designer</p>
                                <a href="#">info@example.com</a><br>
                                <div class="hiraola-social_link" >
                                    <ul>
                                        <li class="facebook">
                                            <a href="https://www.facebook.com/" data-toggle="tooltip" target="_blank" title="Facebook">
                                                <i class="fab fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li class="twitter">
                                            <a href="https://twitter.com/" data-toggle="tooltip" target="_blank" title="Twitter">
                                                <i class="fab fa-twitter-square"></i>
                                            </a>
                                        </li>
                                        <li class="youtube">
                                            <a href="https://www.youtube.com/" data-toggle="tooltip" target="_blank" title="Youtube">
                                                <i class="fab fa-youtube"></i>
                                            </a>
                                        </li>
                                        <li class="google-plus">
                                            <a href="https://www.plus.google.com/discover" data-toggle="tooltip" target="_blank" title="Google Plus">
                                                <i class="fab fa-google-plus"></i>
                                            </a>
                                        </li>
                                        <li class="instagram">
                                            <a href="https://rss.com/" data-toggle="tooltip" target="_blank" title="Instagram">
                                                <i class="fab fa-instagram"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end single team member -->
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="team-member">
                            <div class="team-thumb img-hover_effect">
                                <a href="#">
                                    <img src="asset/images/about-us/team/3.jpg" alt="Our Team Member"><br><br>
                                </a>
                            </div>
                            <div class="team-content text-center">
                                <h3>Edwin Adams</h3>
                                <p>Content Writer</p>
                                <a href="javascript:void(0)">info@example.com</a>
                                <div class="hiraola-social_link">
                                    <ul>
                                        <li class="facebook">
                                            <a href="https://www.facebook.com/" data-toggle="tooltip" target="_blank" title="Facebook">
                                                <i class="fab fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li class="twitter">
                                            <a href="https://twitter.com/" data-toggle="tooltip" target="_blank" title="Twitter">
                                                <i class="fab fa-twitter-square"></i>
                                            </a>
                                        </li>
                                        <li class="youtube">
                                            <a href="https://www.youtube.com/" data-toggle="tooltip" target="_blank" title="Youtube">
                                                <i class="fab fa-youtube"></i>
                                            </a>
                                        </li>
                                        <li class="google-plus">
                                            <a href="https://www.plus.google.com/discover" data-toggle="tooltip" target="_blank" title="Google Plus">
                                                <i class="fab fa-google-plus"></i>
                                            </a>
                                        </li>
                                        <li class="instagram">
                                            <a href="https://rss.com/" data-toggle="tooltip" target="_blank" title="Instagram">
                                                <i class="fab fa-instagram"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end single team member -->
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="team-member">
                            <div class="team-thumb img-hover_effect">
                                <a href="#">
                                    <img src="asset/images/about-us/team/4.jpg" alt="Our Team Member"><br><br>
                                </a>
                            </div>
                            <div class="team-content text-center">
                                <h3>Anny Adams</h3>
                                <p>Marketing officer</p>
                                <a href="#">info@example.com</a>
                                <div class="hiraola-social_link">
                                    <ul>
                                        <li class="facebook">
                                            <a href="https://www.facebook.com/" data-toggle="tooltip" target="_blank" title="Facebook">
                                                <i class="fab fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li class="twitter">
                                            <a href="https://twitter.com/" data-toggle="tooltip" target="_blank" title="Twitter">
                                                <i class="fab fa-twitter-square"></i>
                                            </a>
                                        </li>
                                        <li class="youtube">
                                            <a href="https://www.youtube.com/" data-toggle="tooltip" target="_blank" title="Youtube">
                                                <i class="fab fa-youtube"></i>
                                            </a>
                                        </li>
                                        <li class="google-plus">
                                            <a href="https://www.plus.google.com/discover" data-toggle="tooltip" target="_blank" title="Google Plus">
                                                <i class="fab fa-google-plus"></i>
                                            </a>
                                        </li>
                                        <li class="instagram">
                                            <a href="https://rss.com/" data-toggle="tooltip" target="_blank" title="Instagram">
                                                <i class="fab fa-instagram"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end single team member -->
                </div>
            </div>
        </div><br><br><br><br>
        <!-- Hiraola's Team Area End Here -->

        <div class="section-common section-services">
        <div class="container">
            <ul class="row margin-top--30">
                <li class="col-md-4">
                    <a href="#" class="item-service">
                        <i class="fa fa-truck thumb"></i>
                        <h2 class="title">Free Shipping</h2>
                        <p>We free Shipping on all orders over Rs. 2000 on the world</p>
                    </a>
                </li><!--- .col -->
                <li class="col-md-4">
                    <a href="#" class="item-service">
                        <i class="fa thumb fa-clock-o"></i>
                        <h2 class="title">Fast Delivery</h2>
                        <p>You will receive your orders in less than 7 days</p>
                    </a>
                </li><!--- .col -->
                <li class="col-md-4">
                    <a href="#" class="item-service">
                        <i class="fa thumb fa-users"></i>
                        <h2 class="title">24/7 customer service</h2>
                        <p>We have a support department professional and friendly</p>
                    </a>
                </li><!--- .col -->
            </ul><!--- .row -->
        </div><!--- .container -->
    </div><!--- .section-services -->
<br>
        @endsection
@section('script')
        <script>
    $('.count').each(function() {
  $(this).prop('Counter', 0).animate({
    Counter: $(this).text()
  }, {
    duration: 5000,
    easing: 'swing',
    step: function(now) {
      $(this).text(Math.ceil(now));
    }
  });
});
</script>
@endsection