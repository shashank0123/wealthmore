@extends('layouts.ecommerce2')

<?php
use App\Product;
use App\MainCategory;
use App\Banner;
use App\Testimonial;
use App\Review;
$maincategory = MainCategory::where('category_id',0)->get();

?>
<style>
	.page-large-title { padding: 20px 0 !important; color: #37bc9b !important;}
</style>
@section('content')
<input type="hidden" name="cat_id" id="cat_id" value="{{$id}}">

	{{-- <div class="page-large-title">
		<div class="container">
			<h1 class="title">Kid Fashion With Right Sidebar</h1>
			<nav class="woocommerce-breadcrumb">
				<a class="home" href="#">Home</a> / <span>Kid Fashion</span>
			</nav>
		</div><!-- .container -->
	</div><!-- .page-large-title --> --}}
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-xs-12 section-common">
				{{-- <a href="#" class="light-effect adv-main-content"><img src="http://placehold.it/870x370" alt=""></a> --}}
				<div class="shop-filter">
					{{-- <form class="woocommerce-ordering shop-ordering" action="#"> --}}
						<label class="lbl-select">Default sorting:</label>
						<select class="orderby js__select2" id="sort" data-min-results="Infinity">
							<option value="name">Name</option>
							{{-- <option value="rating">Rating</option> --}}
							<option value="new">New Arrivals</option>
							<option value="low-high">Price: low to high</option>
							<option value="high-low" selected>Price: high to low</option>
						</select>
					{{-- </form> --}}
					<!-- .woocommerce-ordering -->
					{{-- <p class="woocommerce-result-count">Showing 1 - 12 from 124 products</p> --}}
					<ul class="shop-type">
						{{-- <li><a href="#"><img src="assetsss/images/icon-list.png" alt="" /></a></li> --}}
						{{-- <li><a href="#" class="active"><img src="assetsss/images/icon-grid.png" alt="" /></a></li> --}}
					</ul><!-- .shop-type -->
				</div><!-- .shop-filter -->
				<div class="products row row-inline-block text-center" id="productCat">
					@if($searched_products != null)
					@foreach($searched_products as $product)
					<div class="col-md-4 col-sm-6 col-ip-6 col-xs-12">
						<div class="product-grid">
							<div class="thumb">
								<a href="/productdetail/{{$product->id}}"><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}" style="width: 100%; height: 270px; width: 200px" alt="">									
								</a>
								<?php $discount=(($product->mrp - $product->sell_price)*100)/$product->mrp;

								$rate = Review::where('product_id',$product->id)->avg('rating');
								$final_rate = floor($rate);
								?>
								<ul class="controls">
									<li><a style="cursor: pointer;" onclick="addWishList({{ $product->id }})"><i class="fa fa-heart"></i><span>LIKE IT</span></a></li>
									{{-- <li><a href="#"><i class="fa fa-exchange"></i><span>COMPARE</span></a></li> --}}
									<li><a href="#" class="js__popup_open" onclick="openview({{ $product->id }},'{{ $product->name }}',{{$product->sell_price}},{{ $product->mrp }},'{{$product->short_descriptions}}','{{$product->image1}}','{{$product->image2}}','{{$product->image3}}',<?php echo $final_rate; ?>,'<?php if($product->availability == 'yes'){ echo "In stock"; } else { echo "Out of Stock"; } ?>')" data-target="#quickViewPopup"><i class="fa fa-arrows-alt"></i><span>QUICK VIEW</span></a></li>
								</ul>
								<button type="submit" class="add_to_cart_button" onclick="addCart({{$product->id}})">Add to cart</button>
							</div>
							<?php
							$productcat = MainCategory::where('id',$product->category_id)->first(); 
							$total++;
							?>
							<a href="/productdetail/{{$product->id}}" class="content">
								<h2 class="title">{{$product->name}}</h2>
								<span class="category">{{$productcat->Mcategory_name}}</span>
								<span class="price">
									{{-- <del><span class="amount">$40.00</span></del> --}}
									<ins><span class="amount">Rs. {{ $product->sell_price }}.00</span></ins>
								</span>
								<div class="star-rating">
									<span class="js__width" data-width="80%"><strong class="rating">4.00</strong> out of 5</span>
								</div>
							</a>
						</div>
					</div><!-- product -->
					@endforeach

					@else
					<div class="section-common container">
						<div class="row">
							<h3>No products Available</h3>
						</div>
					</div>
					@endif				
				</div><!-- products -->
				{{-- <a href="#" class="btn-ajax-loading"><i class="fa fa-refresh"></i> <span>View more (2.450 products) ...</span></a> --}}
			</div><!-- col -->
			<div class="col-md-3 col-xs-12 section-common sidebar">

				{{-- <aside class="widget widget_search">
					<form action="/searchproduct" method="GET" class="searchform">
						<input type="text" class="inp-search" name="product_keyword" placeholder="Search for something">
						<input type="submit" name="search" class="btn-search" value="Search">
					</form>
				</aside><!-- .widget_search -->
				--}}
				<aside class="widget widget_categories">
					<h2 class="widget-title section-title">Categories</h2>
					<div class="clear"></div>
					<ul>						
						<?php $count=Product::where('category_id',$id)->count();
						$i=0;  ?>
						<li  id="cat0" value="0"><a>All</a> </li>
						<?php
						$category = MainCategory::where('category_id',$id)->get();
						?>
						@foreach($category as $cat)
						
						<li id="cat{{$cat->id}}" value="{{$cat->id}}"><a>{{$cat->Mcategory_name}}</a> ({{$total[$i++]}})</li>


						@endforeach					

					</ul>
				</aside><!-- .widget_categories -->
				
				<aside class="widget widget-price-filter js__slider_price_wrap">
					<h2 class="widget-title section-title">Filter By Price</h2>
					<div class="clear"></div>
					<div class="filter-slider js__slider_price" data-output="yes" data-min="0" data-max="{{$max_price}}" data-start="100" data-end="{{$max_price/2}}" data-currency="Rs."></div>
					<form action="#" class="clearfix">
						<div class="filter-label">Price: <span>Rs. 0</span> <span>-</span> <span>Rs. {{$max_price}}</span></div>
						<input type="hidden" value="0" name="min" id="min-price" class="js__input_min" />
						<input type="hidden" value="{{$max_price}}" name="max" id="max-price" class="js__input_max" />
						<span style="display: block; cursor: pointer; background-color: #37bc9b;border-radius: 3px; color: #fff; padding: 7px 15px; width: 65px" onclick="priceFilter()" value="Filter" >Filter</span>
						{{-- <input type="submit" class="filter-submit" value="Filter" /> --}}
					</form>
				</aside><!-- .widget-price-filter -->
				
				<aside class="widget widget-color-filter">
					<h2 class="widget-title section-title">Color Options</h2>
					<div class="clear"></div>
					{{-- <form action="#" class="clearfix"> --}}
						<div class="color-option">
							<select id="color" class="orderby js__select2" style="width: 90%">
								<option>Choose Color</option>
								@foreach($color as $clr)
								<option id="{{$clr}}">{{$clr}}</option>
								@endforeach
							</select>
							{{-- <label class="lbl-variant"><input type="radio" name="radio-color-widget" class="lbl-radio"><span class="lbl-text js__background_color" data-background-color="#005baf"></span></label>
							<label class="lbl-variant"><input type="radio" name="radio-color-widget" class="lbl-radio"><span class="lbl-text js__background_color" data-background-color="#b1159e"></span></label>
							<label class="lbl-variant"><input type="radio" name="radio-color-widget" class="lbl-radio"><span class="lbl-text js__background_color" data-background-color="#f3db06"></span></label>
							<label class="lbl-variant"><input type="radio" name="radio-color-widget" class="lbl-radio"><span class="lbl-text js__background_color" data-background-color="#d90030"></span></label>
							<label class="lbl-variant"><input type="radio" name="radio-color-widget" class="lbl-radio"><span class="lbl-text js__background_color" data-background-color="#441984"></span></label>
							<label class="lbl-variant"><input type="radio" name="radio-color-widget" class="lbl-radio"><span class="lbl-text js__background_color" data-background-color="#03a9f4"></span></label>
							<label class="lbl-variant"><input type="radio" name="radio-color-widget" class="lbl-radio"><span class="lbl-text js__background_color" data-background-color="#33691e"></span></label>
							<label class="lbl-variant"><input type="radio" name="radio-color-widget" class="lbl-radio"><span class="lbl-text js__background_color" data-background-color="#ff1744"></span></label> --}}
						</div>
					{{-- </form> --}}
				</aside><!-- .widget-color-filter -->
				
				{{-- <aside class="widget widget_categories">
					<h2 class="widget-title section-title">Select a brand</h2>
					<div class="clear"></div>
					<ul>
						<li><a href="#">Adidas</a> (999)</li>
						<li><a href="#">Antony Morato</a> (216)</li>
						<li><a href="#">Armani jeans</a> (312)</li>
						<li><a href="#">Boss</a> (157)</li>
						<li><a href="#">Boom Bap</a> (101)</li>
						<li><a href="#">Chanel</a> (98)</li>
						<li><a href="#">Gucci</a> (76)</li>
						<li><a href="#">Louis Vuitton</a> (39)</li>
					</ul>
				</aside> --}}<!-- .widget_categories -->
				
				<aside class="widget widget-size-filter">
					<h2 class="widget-title section-title">Size Options</h2>
					<div class="clear"></div>
					{{-- <form action="#" class="clearfix"> --}}
						<div class="size-option">
							<select name="size" id="size" class="orderby js__select2" style="width: 90%">
								<option>Select Size</option>
								<option value="S">S</option>
								<option value="M">M</option>
								<option value="L">L</option>
								<option value="XL">XL</option>
								<option value="XXL">XXL</option>
							</select>
							{{-- <label class="lbl-variant"><input type="radio" name="radio-size-widget"  id="size" value="S" class="lbl-radio"><span class="lbl-text">S</span></label>
							<label class="lbl-variant" id="sizeM"><input type="radio" name="radio-size-widget" id="size"  value="M" class="lbl-radio"><span class="lbl-text">M</span></label>
							<label class="lbl-variant" id="sizeL"><input type="radio" name="radio-size-widget" id="size"  value="L" class="lbl-radio"><span class="lbl-text">L</span></label>
							<label class="lbl-variant" id="sizeXL"><input type="radio" name="radio-size-widget" id="size"  value="XL" class="lbl-radio"><span class="lbl-text">XL</span></label>
							<label class="lbl-variant" id="sizeXXL"><input type="radio" name="radio-size-widget" id="size"  value="XXL" class="lbl-radio"><span class="lbl-text">XXL</span></label> --}}
						</div>
					{{-- </form> --}}
				</aside><!-- .widget-size-filter -->

				{{-- <aside class="widget widget-images">
					<a href="#" class="light-effect"><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}" style="width: 250px; height: 280px;" alt="" /></a>
				</aside> --}}<!-- .widget-images -->
				
			</div><!-- col -->
			<div class="section-common section-services">
				<div class="container">
					<ul class="row margin-top--30">
						<li class="col-md-4">
							<a href="#" class="item-service">
								<i class="fa fa-truck thumb"></i>
								<h2 class="title">Free Shipping</h2>
								<p>We free Shipping on all orders over Rs. 2000 on the world</p>
							</a>
						</li><!--- .col -->
						<li class="col-md-4">
							<a href="#" class="item-service">
								<i class="fa thumb fa-clock-o"></i>
								<h2 class="title">Fast Delivery</h2>
								<p>You will receive your orders in less than 7 days</p>
							</a>
						</li><!--- .col -->
						<li class="col-md-4">
							<a href="#" class="item-service">
								<i class="fa thumb fa-users"></i>
								<h2 class="title">24/7 customer service</h2>
								<p>We have a support department professional and friendly</p>
							</a>
						</li><!--- .col -->
					</ul><!--- .row -->
				</div><!--- .container -->
			</div><!--- .section-services -->
		</div><!-- .row -->
	</div><!-- .container --><br>
	
	{{-- <div class="section-common section-subscribe fixed-wrapper">
		<div class="background js__background_image js__parallax" data-background-image="url(http://placehold.it/1920x1280"></div>
		<div class="container">
			<form action="#" class="subscribe-form">
				<h2 class="section-title">Subscribe</h2>
				<div class="clear"></div>
				<p>Get the last news & promotion program from us</p>
				<div class="inp-controls">
					<input type="email" class="inp-email" placeholder="ENTER YOUR EMAIL">
					<button type="button" class="btn-submit"><span>SUBSCRIBE</span><i class="fa fa-long-arrow-right"></i></button>
				</div>
			</form><!-- .subscribe-form -->
		</div><!-- .container -->
	</div><!-- .section-subscribe --> --}}
	
	@endsection 

	@section('script')
	{{-- For Modal Box --}}
	<div id="quickViewPopup" class="popup js__popup">
		<div class="popup-overlay js__popup_close"></div>
		<div class="popup-body">
			<button type="button" class="popup-close-btn js__popup_close"><i class="fa fa-times"></i></button>
			<div class="container">
				<div class="popup-inside">
					<div class="row">
						<input type="hidden" name="product_id" id="hidden_id" value="">
						<div class="col-md-12 clearfix">
							<h2 class="single-title-border-bottom">Quick view</h2>
						</div><!-- col -->
						<div class="col-md-5">
							<div class="product-detail">
								<div class="product">
									<div class="images js__gallery">
										<a href="#" class="woocommerce-main-image zoom js__zoom_popup" data-target="#zoomPopup" data-zoom="" id="imggallery1">
											<img src="" alt="" id="imggallery2"/>
										</a><!-- .woocommerce-main-image -->
										<div class="thumbnails">									
											<a href="#" class="zoom js__thumb js__active" data-images="" data-zoom="" id="imgdz1" ><img src="" alt="" id="img1"/></a>
											<a href="#" class="zoom js__thumb" data-images="" data-zoom="" id="imgdz2" ><img src="" alt="" id="img2"/></a>
											<a href="#" class="zoom js__thumb" data-images="" data-zoom="" id="imgdz3" ><img src="" alt="" id="img3" /></a>											
										</div><!-- .thumbnails -->
										<div class="hidden">										
											<img src="" alt="" id="imgt1"/>
											<img src="" alt="" id="imgt2" />
											<img src="" alt="" id="imgt3"/>
											{{-- <img src="" alt="" /> --}}
										</div><!-- load images zone -->
									</div><!-- .images -->
								</div><!-- .product -->
							</div><!-- .product-detail -->
						</div><!-- col -->
						<div class="col-md-7">
							<div class="summary">
								<h2 class="product_title" id="viewname"></h2>
								<div class="summary-top">
									<div class="star-rating">
										<span class="js__width" data-width="80%"><strong class="rating">4.00</strong> out of 5</span>
									</div>
									<a href="#">Have 25 reviews</a> {{-- <span>/</span> <a href="#">Add your review</a> --}}
								</div>
								<p class="price">Rs. <span class="amount" id="viewprice"></span></p>
								<ul class="product_meta">
									{{-- <li><span>Brand:</span> Louis Vuitton</li> --}}
									<li><span>Available:</span><span style="margin-left: 20%"id="availability"></span></li>
									{{-- <li><span>Product code:</span> ABC 123 456</li> --}}
								</ul>
								<div class="description">
									<p id="viewdescription"></p>
								</div>
								
								<form class="cart">
									<div class="quantity js__number"><input id="quickquantity" type="number" class="js__target" value="1" /><button type="button" class="js__plus fa-plus fa"></button><button type="button" class="js__minus fa-minus fa"></button></div>
									{{-- <button type="submit" class="single_add_to_cart_button">Add to cart</button> --}}
									<button type="submit" class="single_add_to_cart_button" onclick="addToCart()">Add to cart</a></button>
								</form>
							</div><!-- .summary -->
						</div><!-- col -->
					</div><!-- .row -->
				</div><!-- .popup-inside -->
			</div><!-- .container -->
		</div><!-- .popup-body -->
	</div><!-- #quickViewPopup -->

	<div id="zoomPopup" class="popup popup-images js__popup">
		<div class="popup-overlay js__popup_close"></div>
		<div class="popup-body">
			<button type="button" class="popup-close-btn js__popup_close"><i class="fa fa-times"></i></button>
			<div class="popup-inside js__popup_images">

			</div><!-- .popup-inside -->
		</div><!-- .popup-body -->
	</div><!-- #zoomPopup -->

	<script>
		var countproduct=<?php if(session()->get('count') != null){echo session()->get('count'); } else { echo 0;} ?>;

		function addToCart(){
			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
			var pro_id = $('#hidden_id').val();
			var quant = $('#quickquantity').val();
			countproduct++;
			$.ajax({
				/* the route pointing to the post function */
				url: '/add-cart',
				type: 'POST',
				/* send the csrf-token and the input to the controller */
				data: {_token: CSRF_TOKEN, id: pro_id, quantity: quant},
				success: function (data) { 
					$("#show-total").html(countproduct);           
				}
			}); 
		}

		function openview(id,name,price,mrp,description,img1,img2,img3,rate,stock){
			$('#hidden_id').val(id);
			$('#viewname').text(name);

			$('#viewdescription').text(description);
			$('#viewprice').text(price+".00");
			$('#availability').text(stock);
    //For main image
    $("#imggallery1").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#imggallery2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);

    //for a tag (data-zoom attribute)    
    $("#imgdz2").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#imgdz3").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
    $("#imgdz1").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);

    //for a tag (data-images attribute)
    $("#imgdz1").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#imgdz2").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#imgdz3").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);

    //for image tag (src attribute)
    $("#img1").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#img2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#img3").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
    $("#imgt1").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#imgt2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#imgt3").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
    var discount = ((mrp-price)*100)/mrp;
    $('#viewdiscount').text(discount.toFixed(2));
    $('#viewrate').val(rate);

}

function searchFilter(id){	
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

	$.ajax({
		/* the route pointing to the post function */
		url: '/wishlist/delete-all',
		type: 'POST',
		/* send the csrf-token and the input to the controller */
		dataType: 'JSON',

		success: function (data) { 
			$('#no-result').show();
			$('#refresh-wishlist').load(document.URL + ' #refresh-wishlist');	
		}
	});
}

function searchAll(){		
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

	$.ajax({
		/* the route pointing to the post function */
		url: '/search-all',
		type: 'POST',
		/* send the csrf-token and the input to the controller */
		dataType: 'JSON',
		success: function (data) { 
			$('#refresh-wishlist').load(document.URL + ' #refresh-wishlist');	
		}
	});
} 

//adding items to cart
function addCart(id){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
	var cat_id = $('#cat_id').val();
	countproduct++;
	$.ajax({
		/* the route pointing to the post function */
		url: '/category-wise/cart',
		type: 'POST',
		/* send the csrf-token and the input to the controller */
		dataType: 'JSON',
		data: {_token: CSRF_TOKEN, id: id},
		success: function (data) { 
			window.location.href = '/products/'+cat_id;
			$("#ajaxdata").html(data);
			$("#show-total").html(countproduct); 
		}
	}); 
}

//adding items to Wishlist
function addWishList(id){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
	var cat_id = $('#cat_id').val();
	var userId = $('#user_id').val();
	$.ajax({
		/* the route pointing to the post function */
		url: '/category-wise/wishlist',
		type: 'POST',
		/* send the csrf-token and the input to the controller */
		dataType: 'JSON',
		data: {_token: CSRF_TOKEN, id: id,user_id: userId},
		success: function (data) { 
			window.location.href = '/products/'+cat_id;
			$("#ajaxdata").html(data);
		}
	}); 
}

$('#cat0').click(function(){		
	var cat = $('#cat_id').val();
	$.ajax({
		type: 'get',
		data_type: 'html',
		url: '/search-product/productsCat',
		data:{ cat_id:cat},

		success:function(response){
			$('#productCat').html(response);
			console.log(response);
		}
	});
});

@foreach($category as $row)
$('#cat{{$row->id}}').click(function(){
	var cat = $('#cat{{$row->id}}').val();

	$.ajax({
		type: 'get',
		data_type: 'html',
		url: '/search-product/productsCat',
		data:{ cat_id:cat},

		success:function(response){				
			$('#productCat').html(response);
			console.log(response);
		}
	});
});
@endforeach

$('#sort').on('change',(function(){
	var sort = $('#sort').val();
	var category_id = $('#cat_id').val();
	$.ajax({
		type: 'get',
		data_type: 'html',
		url: '/search-product/sort',
		data:{ sort_type:sort, cat_id :category_id},

		success:function(response){				
			$('#productCat').html(response);
			console.log(response);
		}
	});
}));

$('#color').on('change',(function(){
	var min = $('#min-price').val();
	var max = $('#max-price').val();
	var color_code = $('#color').val();
	var category_id = $('#getID').val();
	$.ajax({
		type: 'get',
		data_type: 'html',
		url: '/search-product/colored',
		data:{ color: color_code, cat_id: category_id, min_cost: min, max_cost: max},
		success:function(response){				
			$('#productCat').html(response);
			console.log(response);
		}
	});
}));


function priceFilter(){
	var min = $('#min-price').val();
	var max = $('#max-price').val();
	var category_id = $('#getID').val();
	$.ajax({
		type: 'get',
		data_type: 'html',
		url: '/search-product/price-filter',
		data:{ max_value:max , min_value:min, cat_id: category_id},

		success:function(response){				
			$('#productCat').html(response);
			console.log(response);
		}
	});
}

$('#size').on('change',(function(){
	var size_code = $('#size').val();
	var min = $('#min-price').val();
	var max = $('#max-price').val();
	var color_code = $('#color').val();
	var category_id = $('#getID').val();

	$.ajax({
		type: 'get',
		data_type: 'html',
		url: '/search-product/size',
		data:{ size: size_code, cat_id: category_id, color: color_code, min_cost: min, max_cost: max},
		success:function(response){				
			$('#productCat').html(response);
			console.log(response);
		}
	});
})); 

</script>

@endsection