@extends('layouts/ecommerce2')

@section('content')

<style>
    .contact-main-page { margin-top: 2% }
    .contact-page-side-content { background-color: #f4f4f4; text-align: center; }
    .contact-page-side-content p { padding: 5px 30px; line-height: 1.7 }

    hr{ width: 90%; border-top: 1px solid #e0e0e0 !important;}
    h3  { font-size: 32px ; font-weight: 600; color: #444;}
    h4{ font-size: 20px ; font-weight: 600; color: #444;}
    .contact-form-content { text-align: center; }
    label span { color: red; }
    .form-input { width: 100%; height: 45px; border: 1px solid #ddd ;
        margin-bottom: 20px}
        .contact-form { margin-top: -40px; }
        .form-textbox { height: 150px; width: 100%;  border: 1px solid #ddd ; margin-bottom: 20px }
        .submit-btn { padding: 10px 50px ; color: #fff; border-radius: 3px; border: none; background-color: #555}
        .mapouter{position:relative;text-align:right;height:500px;width:100%;}
        .gmap_canvas {overflow:hidden;background:none!important;height:500px;width:100%;}
    </style>
    <!-- Begin Contact Main Page Area -->
    <div class="contact-main-page">
        <div class="container-fluid">
            <div class="mapouter">
                <div class="gmap_canvas">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d3502.6538191651343!2d77.0796095!3d28.6101604!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1560693039218!5m2!1sen!2sin" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
                    {{-- <iframe width="100%" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=university%20of%20san%20francisco&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe> --}}
                    Google Maps Generator by 
                    <a href="https://www.embedgooglemap.net">embedgooglemap.net</a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-6 col-md-12 order-2 order-lg-1">
                    <div class="contact-form-content">
                        <h3 class="contact-page-title"><br>Tell Us Your Message</h3>
                        <div class="contact-form">
                            <form id="contact-form" action="" method="POST">
                                <div class="form-group">
                                    <label>Your Name <span class="required">*</span></label><br><br>
                                    <input type="text" name="contact_name" id="contact_name" class="form-input" required="required" >
                                </div>
                                <div class="form-group">
                                    <label>Your Email <span class="required">*</span></label><br><br>
                                    <input type="email" name="contact_email" id="contact_email" class="form-input" required="required">
                                </div>
                                <div class="form-group">
                                    <label>Subject</label><br><br>
                                    <input type="text" name="contact_subject" id="contact_subject" class="form-input" required="required">
                                </div>
                                <div class="form-group form-group-2">
                                    <label>Your Message</label><br><br>
                                    <textarea name="contact_message" id="contact_message" class="form-textbox" required="required"></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="button" onclick="formSubmit()" value="submit" id="submit" class="submit-btn" name="submit">send</button>
                                </div>
                            </form>
                        </div>
                        <p class="form-messege"></p>
                    </div>
                </div>

                <div class="col-lg-1 col-md-12"></div>
                <div class="col-lg-5 offset-lg-1 col-md-12 order-1 order-lg-2">
                    <div class="contact-page-side-content">
                        <h3 class="contact-page-title"><br>Contact Us</h3><br>
                        <p class="contact-page-message">We Provide quality service and product. We believe in you and your experience. There is nothing else to say apart from the thing that after using our product, if you don't like it, return and take the money back.</p><br>
                        <div class="single-contact-block">
                            <h4><i class="fa fa-fax"></i> Address</h4><br>
                            <p>Wealth India Private Limited</p><p> A-51, 2nd Floor, New Dwarka Road, Dabri Exnt</p><p>New Delhi-110045</p>
                        </div>
                        <hr>
                        <div class="single-contact-block">
                            <h4><i class="fa fa-phone"></i> Phone</h4><br>
                            {{-- <p>Mobile: (08) 123 456 789</p> --}}
                            <p>Toll Free: 1800 419 8447</p>
                        </div>
                        <hr>
                        <div class="single-contact-block last-child">
                            <h4><i class="fa fa-envelope-o"></i> Email</h4><br>
                            <p>info@wealthindia.global</p><br>
                            {{-- <p>support@hastech.company</p> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @endsection

    @section('script')
    {{-- <script src="https://maps.google.com/maps/api/js?sensor=false&amp;libraries=geometry&amp;v=3.22&amp;key=AIzaSyChs2QWiAhnzz0a4OEhzqCXwx_qA9ST_lE"></script> --}}
    <script>
        function formSubmit(){

         var request = $.ajax({
            type: "POST",
            url: "/contact-us/form",
            data: $('#contact-form').serialize(),

        });
         request.done(function(data) {
            if (data.msg == 'Thank you .We will reply you soon'){
              console.log(data);
              Swal(data.msg);             
          }
          else
              Swal(data.msg);                  
      });

         request.fail(function(data) {
            Swal(data.msg);
        });
     }

 </script>

 @endsection