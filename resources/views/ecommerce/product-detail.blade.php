<?php
use App\MainCategory;
use App\Review;
use App\Product;
use App\Wishlist;
$count = Review::where('product_id',$getproduct->id)->count();
$bill = 0;
$count_menu = 0;
$i=0;

if($count>0)
{
	$rate = Review::where('product_id',$getproduct->id)->avg('rating');
	$final_rate = floor($rate);
}
else
	$final_rate = 0;
?>

<style>
	.fix-heart:hover { background-color: #37bc9b !important }
	.fix-heart { width: 50px; background-color: #333;text-align: center; cursor: pointer;}
	.star s:hover,
	.star s.active { color: #37bc9b; }
	.product-detail .thumbnails .zoom.js__active img { z-index: 15 !important; }
	.star-rtl s:hover,
	.star-rtl s.active { color: #37bc9b; }
	.star s, .star-rtl s { color: black; font-size: 50px; cursor: default; text-decoration: none; line-height: 50px; }
	.star { padding: 2px; }
	.star-rtl { background: #555; display: inline-block; border: 2px solid #444; }
	.star-rtl s { color: yellow; }
	.star s:hover:before, .star s.rated:before, .star s.active:before { content: "\2605";
	font-size: 32px; }
	.star s:before { content: "\2606"; font-size: 32px; }
	.star-rtl s:hover:after, .star-rtl s.rated:after, .star-rtl s.active:after { content: "\2605"; font-size: 32px; }
	.star-rtl s:after { content: "\2606"; }
	.active { color: #ffcd02  !important; }
	.fa-star { color: #ccc ; }
	.page-large-title { padding: 20px 0 !important; color: #37bc9b !important;}


	.show-result { margin: 10px; padding: 10px; color: green; font-size: 20px; }
	#response-list{ text-align: right !important }

	.main-menu-wrap .menu>li>a { font-size: 13px !important; }			
	.header .middle .logo { top: 50px !important; }
	.header .middle .logo { position: absolute; left: 0; height: 85px !important; }
	.header .middle .logo img { top: 40%; max-height: 75px;	width: auto;}
	#myBtn {
		display: none;
		position: fixed;
		bottom: 20px;
		right: 30px;
		z-index: 99;
		font-size: 18px;
		border: none;
		outline: none;
		background-color: red;
		color: white;
		cursor: pointer;
		padding: 10px 15px;
		border-radius: 50%;
	}

	#myBtn:hover {
		background-color: #555;
	}
	@media screen and (max-width: 768px)
	{
		#response-icon{ text-align: center !important; }
		#response-list{ text-align: center !important }
		.header .middle.middle-ver-2 .logo { top: 10px !important; }
		.header .middle.middle-ver-2 .right-side { top: 90px !important; }
		#hide-search {display: none;}
		#response-logo { top: 0;  }
		.fix-heart { width: 50px; background-color: #333;margin-left: -50px}
		
	}

	@media screen and (max-width: 560){
		.fix-heart { width: 50px; background-color: #333;margin-left: 0px}
	}

	@media screen and (max-width: 420px){
		.fix-heart { width: 50px; background-color: #333;margin-left: -10px}
	}


	#drop-down { display: none; }

	#myBtn { display: none; position: fixed; bottom: 20px; right: 30px; z-index: 99; font-size: 18px; border: none; outline: none; background-color: red; color: white; cursor: pointer; padding: 10px 15px; border-radius: 50%; }
	.mannual-navbar { width: 30% ; float: right; z-index: 99; font-size: 14px; line-height: 24px; background: #fff; -webkit-box-shadow: -1px 2px 9px 0 rgba(0,0,0,.85); -moz-box-shadow: -1px 2px 9px 0 rgba(0,0,0,.85); box-shadow: -1px 2px 9px 0 rgba(0,0,0,.85); padding: 44px 29px 30px; position: absolute; right: 4%; top: 200;max-height: 400px; overflow: auto}

	.more-cat { margin-left: 8%; display: none; }

	.more-sub-cat { margin-left: 8% ; display: none;}



	#submenu-navbar { display: none; display: none;
		position: absolute;
		background-color: #ff0000;
		width: 250px;
		height: 250px;
		left: 200px;
		top: 100px;
		z-index: 99;}


		.have-menu .fa {
			color: #999;    
			margin-left: 90%;
			margin-top: -23px;

		}
		.have-menu span { text-align: right;margin-top: -20px }

		.more-cat {
			display: none;
		}

		.more-sub-cat{ display: none; }
		.mannual-navbar .more-main-cat .have-menu { border-bottom: 1px solid #999 }

		.mannual-navbar .more-main-cat .have-sub-menu { border-bottom: 1px dashed #999 }

		.mannual-navbar .more-main-cat .no-sub-menu { border-top: 1px solid #999 }
		.mannual-navbar .more-main-cat { margin-left: 5% }
		.mannual-navbar ul li a:hover { color: #37bc9b; text-decoration: underline; }
		.all-more-categories { overflow: auto; }
		.mannual-navbar ul li a:hover #submenu-navbar { display: block !important; }

		.mannual-navbar ul li a {display: block;
			font-size: 15px ;
			line-height: 26px ;
			padding: 5px 0  ;
			color: #898c90 ;
			text-transform: uppercase ;
			border-bottom: 2px solid transparent }

		</style>


		<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
			<meta name="description" content="">
			<meta name="author" content="">

			<title>{{$getproduct->page_title}}</title>
			<link rel="shortcut icon" href="{{ asset('assets/img/favicon.png') }}">
			<link rel="stylesheet" href="/assetsss/styles/style.min.css">

			<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.js"></script>

			<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.css"></script>

			{{-- <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'> --}}
			<style>
				.show-result { margin: 10px; padding: 10px; color: green; font-size: 20px; }
				#response-list{ text-align: right !important }

				.main-menu-wrap .menu>li>a { font-size: 14px !important; }			
				.header .middle .logo { top: 50px !important; }
				.header .middle .logo { position: absolute; left: 0; height: 85px !important; }
				.header .middle .logo img { top: 40%; max-height: 75px;	width: auto;}

				.main-menu-wrap .menu>li { display: inline-block; position: relative; margin-left: 18px; }

				@media screen and (max-width: 768px)
				{
					#response-icon{ text-align: center !important; }
					#response-list{ text-align: center !important }
				}
			</style>
		</head>

		{{-- <body> --}}
			<body oncontextmenu="return false" onselectstart="return false" ondragstart="return false">
				@if($member->id != null)	
				<input type="hidden" name="user_id" value="{{$member->id}}" id="user_id">
				@endif

				<input type="hidden" name="cat_id" id="cat_id" value="{{$id}}">

				<button onclick="topFunction()" id="myBtn" title="Go to top"><i class='fa fa-angle-up' style='font-size:30px; color: #fff; margin-bottom: 2px; font-weight: bold;'></i></button>

				<div class="menumobile-navbar-wrapper">
					<nav class="menu-navbar menumobile-navbar js__menu_mobile">
						<div class="menumobile-close-btn js__menu_close">
							<span class="fa fa-times"></span> CLOSE
						</div>
						<div id="menu-mobile">
							<ul class="menu">
								<li class="current-menu-item"><a href="/index">Home</a></li>
								@foreach($maincategory as $row)
								<li class="menu-item-has-children"><a href="/products/{{$row->id}}">{{$row->Mcategory_name}}</a><span class="drop-down-icon js__menu_drop"></span>
									<ul class="sub-menu">
										<?php
										$category = MainCategory::where('category_id',$row->id)->where('status','Active')->get();
										?>
										@foreach($category as $rows)
										<li class="menu-item-has-children">

											<a href="/products/{{$rows->id}}">{{$rows->Mcategory_name}}</a>
											<span class="drop-down-icon js__menu_drop"></span>
											<ul class="sub-menu">
												<?php
												$subcategory = MainCategory::where('category_id',$rows->id)->where('status','Active')->get();
												?>
												@foreach($subcategory as $col)
												<li><a href="/products/{{$col->id}}">{{$col->Mcategory_name}}</a></li>
												@endforeach									
											</ul>
										</li>
										@endforeach							
									</ul>
								</li>
								@endforeach
								<li><a href="/about">About us</a></li>
								{{-- <li><a href="#">Contact us</a></li> --}}
							</ul><!--/.menu -->
						</div><!--/#menu- -navbar -->
					</nav>
				</div><!--/.menu- -navbar-wrapper -->
				<div class="mobile-sticky js__menu_sticky">
					<div class="container">
		{{-- <div class="left-side">
			<a href="/" class="logo"><img src="/assetsss/images/logo.png" alt=""></a>
		</div> --}}
		<div class="right-side">
			<button type="button" class="menumobile-toggle js__menu_toggle">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
	</div>
</div>
<div id="wrapper">
	<header class="header">
		<div class="top">
			<div class="container">
				<div class="row" id="response-icon">
					<div class="col-sm-1 col-xs-12" >
						<ul class="list-inline" style="margin-top: 5px;margin-bottom: 0">
							<li class="list-inline-item"><a href="#" class="fa fa-twitter" ></a></li>
							<li class="list-inline-item"><a href="https://www.facebook.com/M-Biz-Trading-Private-Limited-2122159934768714/" target="blank" class="fa fa-facebook"></a></li>
							{{-- <li class="list-inline-item"><a href="#" class="fa fa-google-plus"></a></li> --}}
							<li class="list-inline-item"><a href="#" class="fa fa-linkedin"></a></li>		<button type="button" class="menumobile-toggle js__menu_toggle" style="float: right; top: -10px; ">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>				
						</ul>
					</div><!--/.left-side -->

					<div class="col-sm-2 col-xs-12">
						<a href=" http://iseindia.ltd/" target="blank"><img src="/assetsss/images/s&i.png" title="Stocks & Investments"></a>
					</div>
					
					<div class="col-sm-9 col-xs-12" id="response-list">
						<ul class="menu">
							@if (isset($member->id))

							{{-- <li><a href="/en/member">Panel</a></li> --}}
							<li><a href="/en/member">My Account</a></li>
							{{-- <li><a href="/wishlist">My Wishlist</a></li> --}}
							
							@if(session()->get('cart') != null)
							{{-- <li><a href="/cart">Checkout</a></li> --}}
							@endif
							<li><a href="/en/logout">Logout</a></li>
							@else
							<li><a href="/en/login">Login</a></li>
							@endif
							<li><strong><a style="color: #f60e23">TOLL FREE&nbsp;&nbsp;: &nbsp;&nbsp;</a><a style="font-size: 20px;color: #f60e23" href="tel:1800-419-8447">1800-419-8447</a></strong></li>
						</ul>

					</div><!--/.right-side -->
				</div>
			</div><!--/.container -->
		</div><!--/.top -->
		
		@foreach($maincategory as $main)
		<?php
		$category = MainCategory::where('category_id',$main->id)->get();
		?>

		@endforeach
		<div class="container">
			<div class="middle">
				<a href="/index" class="logo"><img src="/assetsss/images/mbizlogo.png" alt="" style="width: 140px; height: 200px" id="response-logo"></a>
				<form action="/searchproduct" method="GET" class="search-form" id="hide-search">
					<div class="select-category">
						<select class="js__select2" data-min-results="Infinity" name="subcategory" style="max-width: 170px">
							<option value="">All</option>;
							@foreach($maincategory as $row)
							<?php $cat = MainCategory::where('category_id',$row->id)->get();
							?>
							@foreach($cat as $col)
							<option value="{{ $col->id }}">{{ $col->Mcategory_name }}</option>
							<?php $subcat = MainCategory::where('category_id',$col->id)->get();
							?>
							@foreach($subcat as $cols )
							<option value="{{ $cols->id }}">{{ $cols->Mcategory_name }}</option>                    
							@endforeach           
							@endforeach           
							@endforeach						
						</select>
					</div>
					<input type="text" placeholder="Search" class="inp-search" name="product_keyword" style="margin-left: 30px">
					<button type="submit" name="search" class="inp-submit"><i class="fa fa-search"></i></button>
				</form><!--/.search-form -->
				<ul class="right-side">
					<li><a href="/wishlist" class="fa fa-heart" title="Wishlist">
						<?php	
						//$show_count=session()->get('count_wishlist');
						$show_count=Wishlist::where('user_id',$member->id)->count();
						?>
						
						<span class="num" id="show-total">{{$show_count}}</span>
						
					</a></li>
					<li class="js__drop_down" >
						<a onclick="showCart()" class="fa fa-shopping-cart js__drop_down_button">
							
							<div id="total-check">
								
								<?php
								$a=0;
								if (session()->get('cart')!=null)
									foreach(session()->get('cart') as $data)
									{								
										$a++;
									}					
									$show=session()->get('count');
									?>

								</div>
								@if(session()->get('count') != null)
								<span class="num" id="show-total">{{$show}}</span>
								@else
								<span class="num" id="show-total">0</span>
								@endif
							</a>
							<div class="cart-list-container">
								<div class="cart-list-content" id="showminicart">
									@if(session()->get('cart') != null)

									<?php

									$ids = array();
									$cat_id = array();					
									$quantities = array();

									foreach(session()->get('cart') as $data)
									{
										$ids[$i] = $data->product_id;								
										$quantities[$i] = $data->quantity;
										$i++;
									}						
									?>
									<ul class="cart-list" style="height: 350px; overflow: auto">
										@for($j=0 ; $j<$i ; $j++ )
										<?php
										$product = Product::where('id',$ids[$j])->first(); 
										if($product != null)
										{
											$cat_id[$j] = $product->category_id;
										}
										else
										{
											$cat_id[$j] = 0;
										}
										?>
										<li id="listhide{{$product->id}}">
											<a class="thumb" href="#"><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }} " alt="" class="attachment-shop_thumbnail" style="width: 70px; height: 70px"></a>
											<a href="#" class="title">{{$product->name}}</a>
											<span class="quantity">
												<span class="amount">Rs. {{$product->sell_price}}.00</span> x {{$quantities[$j]}}
											</span>
											<div class="star-rating">
												<span class="js__width" data-width="100%"><strong class="rating">4.00</strong> out of 5</span>
											</div>
											<a title="Remove this item" class="mini-cart-remove" onclick="deleteProduct({{$ids[$j]}})"><span class="fa fa-times"></span></a>
											
										</li>
										@endfor								
									</ul><!--/.cart-list -->
									@endif
									
									<div class="cart-list-subtotal">
										<strong class="txt fl">SubTotal:   Rs.</strong>
										<strong class="currency fr" id="bill"></strong>
									</div><!--/.cart-list-subtotal -->
								</div><!--/.cart-list-content -->
								<a class="cart-list-bottom" href="/cart">
									<span class="fl">Checkout</span>
									<span class="fr"><i class="fa fa-long-arrow-right"></i></span>
								</a><!--/.cart-list-bottom -->
							</div><!--/.cart-list-container -->
						</li>
					</ul><!--/.right-side -->
				</div><!--/.middle -->

				<div class="bottom">
					<div class="left-side">
						{{-- <button type="button" class="menumobile-toggle js__menu_toggle">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button> --}}
						<div class="main-menu-wrap js__auto_correct_sub_menu">
							<ul class="menu">
								{{-- <li class="current-menu-item"><a href="/index"><i class='fa fa-home' style='font-size:24px'></i></a></li> --}}
								<?php $count_main_cat = 0;
								$maincat = array();
								$maincat_cnt = 0; ?>
								@foreach($maincategory as $row)
								<?php $count_main_cat++;
								if($count_main_cat>6){
									$maincat[$maincat_cnt++] = $row->id;
									continue;
								} ?>
								<li class="menu-item-has-children mega-menu-wrap"><a href="/products/{{$row->id}}">{{$row->Mcategory_name}}</a>
									<div class="mega-menu">
										<div class="row">
											<div class="col-md-12">
												<div class="row">
													<?php 
													$category = MainCategory::where('category_id',$row->id)->where('status','Active')->get();
													?>
													@foreach($category as $rows)
													<?php $count_menu = 0; ?>
													<div class="col-md-3">
														<h3 class="title"><a href="/products/{{$rows->id}}">{{$rows->Mcategory_name}}</a></h3>
														<ul class="sub-menu">
															<?php
															$subcategory = MainCategory::where('category_id',$rows->id)->where('status','Active')->limit(5)->get();
															
															?>
															@foreach($subcategory as $col)
															<li><a href="/products/{{$col->id}}">{{$col->Mcategory_name}}</a></li>
															<?php $count_menu++;  ?>
															@endforeach

														</ul>
														@if($count_menu == 5)
														<div class="row">
															<div class="col-sm-12" style="float: right;">
																<a href='#'>View More...</a>
															</div>
														</div>
														@endif
													</div><!--/col -->
													
													@endforeach

												</div><!--/.row -->
											</div><!--col -->
											<?php 
											$category = MainCategory::where('category_id',$row->id)->where('status','Active')->get();
											?>
											@foreach($category as $rows)
											<?php
											$subcategory = MainCategory::where('category_id',$rows->id)->where('status','Active')->get();
											?>											
											@endforeach
											
										</div><!--/.row -->
									</div><!--/.menu-mega -->
								</li>
								@endforeach
								<li><a href=" http://iseindia.ltd/" target="blank">Stocks & Investments</a></li>
								@if($count_main_cat>=6)
								<li class="menu-item-has-children mega-menu-wrap"><a style="cursor: pointer;">More...</a>
									<div class="mega-menu" style="width: 20%; margin-left: 80%">
										<div class="all-more-categories">
											<ul class="more-main-cat">
												@for($i = 0; $i < $maincat_cnt ; $i++)
												<?php $maincategory = MainCategory::where('id',$maincat[$i])->first(); ?>
												<li class="have-menu"><a href="/products/{{$maincategory->id}}" style="font-weight: bold;color: #898c90">{{$maincategory->Mcategory_name}}</a><i class='fa fa-chevron-down' onclick="showCat({{$maincategory->id}});"></i>
													<?php $category = MainCategory::where('category_id',$maincategory->id)->get(); ?>
													@if($category != null)

													<ul class="more-cat" id="more-cat{{$maincategory->id}}">
														@foreach($category as $cate)
														<li class="have-sub-menu"><a href="/products/{{$cate->id}}" style="color: #898c90">{{$cate->Mcategory_name}}</a> <i class='fa fa-chevron-down'onclick="showSubCat({{$cate->id}});"></i>
															<?php $subcategory = MainCategory::where('category_id',$cate->id)->get(); ?>
															@if($subcategory != null)

															<ul class="more-sub-cat" id="more-sub-cat{{$cate->id}}">
																@foreach($subcategory as $sub)
																<li class="no-sub-menu" >
																	<a href="/products/{{$sub->id}}" style="color: #898c90">{{$sub->Mcategory_name}}</a>
																</li>
																@endforeach
															</ul>

															@endif
														</li>
														@endforeach
													</ul>								
													@endif
												</li>
												@endfor
											</ul>
										</div>
									</div><!--/.menu-mega -->
								</li>
								@endif

								{{-- <li><a href="about">About us</a></li> --}}
								{{-- <li><a href="#">Contact us</a></li> --}}
							</ul><!--/.menu -->
						</div><!--/.main-menu-wrap -->
					</div><!--/.left-side -->

					
				</div><!--/.bottom -->
			</div><!--/.container -->
		</header><!--/.header -->

		<div class="page-large-title">
			<div class="container">
				<h1 class="title">{{$getproduct->name}}</h1>
					{{-- <nav class="woocommerce-breadcrumb">
						<a class="home" href="/index">Home</a>  / <span>{{$getproduct->name}}</span>
					</nav> --}}
				</div><!-- .container -->
			</div><!-- .page-large-title -->

			<div class="container">
				<div class="row">
					<div class="col-xs-12 section-common">
						<div class="row">
							<div class="col-md-6">
								<div class="product-detail product-single">
									<div class="product">
										<div class="images js__gallery">
											<a href="#" class="woocommerce-main-image zoom js__zoom_popup" data-target="#zoomPopup" data-zoom="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image1 }}">
												<img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image1 }}" alt="" />
											</a><!-- .woocommerce-main-image -->
											<div class="thumbnails">
												<a href="#" class="zoom js__thumb js__active" data-images="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image1 }}" data-zoom="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image1 }}" ><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image1 }}" alt=""  style="width: 100px; height: 140px"/></a>
												<a href="#" class="zoom js__thumb" data-images="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image2 }}" data-zoom="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image2 }}" ><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image2 }}" alt="" style="width: 100px; height: 140px"/></a>
												<a href="#" class="zoom js__thumb" data-images="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image3 }}" data-zoom="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image3 }}" ><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image3 }}" alt="" style="width: 100px; height: 140px"/></a>
												{{-- <a href="#" class="zoom js__thumb" data-images="http://placehold.it/435x590" data-zoom="http://placehold.it/448x427" ><img src="http://placehold.it/100x132" alt="" /></a> --}}
											</div><!-- .thumbnails -->
											<div class="hidden">
												<img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image1 }}" alt="" />
												<img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image2 }}" alt="" />
												<img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image3 }}" alt="" />
												<img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $getproduct->image4 }}" alt="" />
											</div><!-- load images zone -->
										</div><!-- .images -->
									</div><!-- .product -->
								</div><!-- .product-detail -->
							</div><!-- col -->
							<div class="col-md-6">
								<div class="summary summary-single">
									<h2 class="product_title">{{$getproduct->name}}</h2>
									<div class="summary-top">
										{{-- <div class="star-rating"> --}}
											<div class="rating-box col-sm-3 col-lg-3 col-md-6 col-xs-12">
												<ul>
													<li> 
														@for($i=0; $i<5 ; $i++)
														<i class="fa fa-star <?php if($final_rate>0){ echo "active"; $final_rate--;  } ?>"></i>
														@endfor						
													</li>

												</ul>
											</div>
										{{-- </div> --}}
										<div class="col-sm-9 col-lg-9 col-md-6 col-xs-12">
											<a href="#">Have {{$count_review}} reviews</a> 
										</div>{{-- <span>/</span> <a href="#">Add your review</a> --}}
									</div>
									<p class="price">Rs. <span class="amount">{{$getproduct->sell_price}}.00</span></p>
									<ul class="product_meta">
										{{-- <li><span>Brand:</span> Louis Vuitton</li> --}}
										<li><span>Availability:</span> <?php if($getproduct->availability == 'yes'){ echo "In stock"; } else { echo "Out of Stock"; } ?></li>
										{{-- <li><span>Product code:</span> ABC 123 456</li> --}}
									</ul>
									<div class="description">
										<p>{{$getproduct->short_descriptions}}</p>
									</div>
									<?php								
									$productArr = explode(",",$getproduct->product_size); 

									?>
									<ul>
										
										@if($getproduct->product_size != null)
										<span class="text">Size : </span>
										<select name="size" class="form-control" id="getSize" style="margin-left: 2%; width: 200px">
											<option>Select Size</option>
											@foreach($productArr as $arr)
											<option value="{{$arr}}">{{$arr}}</option>
											@endforeach
										</select>
										@endif
											{{-- <label class="lbl-variant"><input type="radio" name="radio-size" class="lbl-radio"><span class="lbl-text">S</span></label>
											<label class="lbl-variant"><input checked type="radio" name="radio-size" class="lbl-radio"><span class="lbl-text">M</span></label>
											<label class="lbl-variant"><input type="radio" name="radio-size" class="lbl-radio"><span class="lbl-text">L</span></label> --}}

								{{-- <li class="variant-color">
									<span class="text">Colors</span>
									<label class="lbl-variant"><input type="radio" name="radio-color" class="lbl-radio"><span class="lbl-text js__background_color" data-background-color="#fea500"></span></label>
									<label class="lbl-variant"><input type="radio" name="radio-color" class="lbl-radio"><span class="lbl-text js__background_color" data-background-color="#9fbedb"></span></label>
									<label class="lbl-variant"><input type="radio" name="radio-color" class="lbl-radio"><span class="lbl-text js__background_color" data-background-color="#f3db06"></span></label>
									<label class="lbl-variant"><input type="radio" name="radio-color" class="lbl-radio"><span class="lbl-text js__background_color" data-background-color="#00b261"></span></label>
								</li> --}}
							</ul><br>
							{{-- <form class="cart"> --}}
								<div class="row">
									<div class="col-sm-7 col-xs-10">
										<div class="quantity js__number"><input id="quantity" type="number" class="js__target" value="1" min="1" oninput="validity.valid||(value='');"/><button type="button" class="js__plus fa-plus fa"></button><button type="button" class="js__minus fa-minus fa"></button></div>

										<button onclick="<?php if($getproduct->availability == 'yes'){ echo "addCart(".$getproduct->id.")"; } else { echo "noAvailability()"; } ?>" class="single_add_to_cart_button">Add to cart</button>
									</div>
									<div class="col-sm-5 col-xs-2">
										<div class="fix-heart"  onclick="addWishList({{$getproduct->id}})">
											<i class="fa fa-heart" style="padding: 13px 0; color: #fff"></i>
										</div>
									</div>
								</div>
								



							{{-- </form> --}}
							<div class="summary-bottom">
								<div class="text-list"><strong>Share:</strong> <a href="https://www.facebook.com/M-Biz-Trading-Private-Limited-2122159934768714/" target="blank">Facebook</a><span class="split"></span>{{-- <a href="#">Twitter</a> --}}<span class="split"></span>{{-- <a href="#">Google</a><span class="split">/</span> --}}{{-- <a href="#">Linkedin</a> --}}</div>
								<div class="text-list"><strong>Tags: </strong>{{$getproduct->page_keywords}}
								</div>
							</div>
						</div><!-- .summary -->
					</div><!-- col -->
					<div class="col-xs-12 section-common">
						<div class="woocommerce-tabs js__tab">
							<ul class="tabs">
								<li><a href="#" class="js__tab_control js__active">Description</a>					</li>
								<li><a href="#" class="js__tab_control">Free Shipping</a></li>
								<li><a href="#" class="js__tab_control">Reviews ({{$count_review}})</a></li>
							</ul>
							<div class="panel entry-content js__tab_content js__active">
								<div class="text-content">
									<p><?php echo htmlspecialchars_decode($getproduct->long_descriptions) ?></p>
								</div>
							</div>
							
							<div class="panel entry-content js__tab_content">
								<div class="text-content">
									<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. </p>
								</div>
							</div>
							
							<div class="panel entry-content js__tab_content">
								<div id="comments" class="comments">
									
									<h2 class="section-title">{{$count_review}} Reviews</h2>
									<div class="clear"></div>
									<ol class="comment-list">
										<?php $count_review = 1; ?>
										@foreach($reviews as $review)
										<li class="comment">
											<div class="comment-body">
												<div class="comment-meta">
													<div class="comment-author vcard">
														<span class="avatar" style="font-weight: bold; font-size: 16px; margin-left: 5%">{{$count_review++}}</span>
														{{-- <img alt="" src="http://placehold.it/80"  class="avatar"> --}}
														<b class="fn"><a href="#" rel="external nofollow" class="url">{{$review->name}}</a></b>
													</div>
													<!-- .comment-author -->
													<div class="comment-metadata">
														<a href="#"><span class="date">{{$review->created_at}}</span> {{-- <span class="split">/</span> About 1 hour ago --}}</a>
													</div>
													<!-- .comment-metadata -->
												</div>
												<!-- .comment-meta -->
												<div class="comment-content">
													<div class="text-content">
														<p>{{$review->review}}</p>
													</div>
												</div>
												<!-- .comment-content -->
												{{-- <div class="reply"><a class="comment-reply-link" href="#">Reply</a></div> --}}
											</div>
										</li><!--- .comment-->
										@endforeach
										
										
									</ol><!--- .comment-list-->
									{{-- <a href="#" class="btn-ajax-loading"><i class="fa fa-refresh"></i> <span>View more all comments ...</span></a> --}}
									<div id="respond" class="comment-respond frm-comment">
										<h2 id="reply-title" class="comment-reply-title section-title small-spacing">Leave Your Review</h2>
										<div class="clear"></div>
										<form action="/productdetail/{{ $getproduct->id }}" method="POST"  id="commentform" class="comment-form frm-contact">
											<div class="row">
												<div class="col-sm-4 col-xs-12">
													<label class="controls">
														<span class="lbl">Full Name <span class="required">*</span></span>
														<input type="text" name="name" id="name" class="inp-text" placeholder="Your Name" required="required">
													</label>
												</div>
												<div class="col-sm-4 col-xs-12">
													<label class="controls">
														<span class="lbl">Email Address <span class="required">*</span></span>
														<input type="email" name="email" id="email" class="inp-text" placeholder="Email Address" required="required">
													</label>
												</div>
												<div class="col-sm-4 col-xs-12">
													<label class="controls">
														<span class="lbl">Rating <span class="required">*</span></span>
														<div class="form-group">

															<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

															<script>
																$(function() {
																	$("div.star > s").on("click", function(e) {

																    // remove all active classes first, needed if user clicks multiple times
																    $(this).closest('div').find('.active').removeClass('active');

																    $(e.target).parentsUntil("div").addClass('active'); // all elements up from the clicked one excluding self
																    $(e.target).addClass('active');  // the element user has clicked on


																    var numStars = $(e.target).parentsUntil("div").length+1;
																    $('.show-result input').val(numStars );
																});
																});
															</script>

															<div class="star"><s><s><s><s><s></s></s></s></s></s></div>
															<div class="show-result">
																<input type="hidden" name="rating" id="rating" >
															</div>

														</div>
													</label>
												</div>
												<div class="col-xs-12">
													<label class="controls">
														<span class="lbl">Write Message</span>
														<textarea name="review" id="review" class="inp-text inp-textarea auto-resize-textarea" placeholder="Please let us know your thoughts on this article..." required="required"></textarea>
													</label>
													<div class="controls">
														<input type="submit" name="submit" class="inp-submit" value="SEND MESSAGE">
													</div>
												</div>
											</div>
										</form>
									</div><!--- .comment-respond-->
								</div><!--- .comments-->
							</div>
						</div><!-- .woocommerce-tabs -->
					</div><!-- col -->
					<div class="col-xs-12 section-featured-product section-common upsells" style="height: 600px">
						<h2 class="section-title">Related Products</h2>
						<div class="clear"></div>
						<div class="slick-wrap">
							<div class="products products-grid slick-middle-arrow js__slickslider" data-arrows="true" data-dots="false" data-show="4" data-responsive="{'992':2,'650':1}">

								@if($products != null)
								@foreach($products as $relatedproduct)
								<?php
								$category = Maincategory::where('id',$relatedproduct->category_id)->first();
								?>
								<?php $discount=(($relatedproduct->mrp - $relatedproduct->sell_price)*100)/$relatedproduct->mrp;

								$rate = Review::where('product_id',$relatedproduct->id)->avg('rating');
								$final_rate = floor($rate);

								$productArr = explode(",",$getproduct->product_size); 

								?>
								<div class="slick-slide">
									<div class="product-grid">
										<div class="thumb">
											<a href="/productdetail/{{$relatedproduct->id}}"><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $relatedproduct->image1 }}" alt="" style="width: 220px; height: 270px"></a>
											<ul class="controls">
												<li><a style="cursor: pointer" onclick = "addWishList({{$relatedproduct->id}})"><i class="fa fa-heart"></i><span>LIKE IT</span></a></li>
												{{-- <li><a href="#"><i class="fa fa-exchange"></i><span>COMPARE</span></a></li> --}}
												<li><a href="#" class="js__popup_open" onclick="openview({{ $relatedproduct->id }},'{{ $relatedproduct->name }}',{{$relatedproduct->sell_price}},{{ $relatedproduct->mrp }},'{{$relatedproduct->short_descriptions}}','{{$relatedproduct->image1}}','{{$relatedproduct->image2}}','{{$relatedproduct->image3}}',<?php echo $final_rate; ?>, '<?php if($relatedproduct->availability == 'no'){ echo 'Out of stock'; } else { echo 'In Stock'; } ?>','<?php if($productArr != null){ foreach($productArr as $pro){ echo $pro.','; }  } ?>')" data-target="#quickViewPopup"><i class="fa fa-arrows-alt"></i><span>QUICK VIEW</span></a></li>
											</ul>
											<button type="submit" onclick="addCart({{$relatedproduct->id}})" class="add_to_cart_button" >Add to cart</button>
										</div>
										<a href="/productdetail/{{$relatedproduct->id}}" class="content">
											<h2 class="title">{{$relatedproduct->name}}</h2>
											<span class="category">{{$category->Mcategory_name}}</span>
											<span class="price">
												<span class="amount">Rs. {{$relatedproduct->sell_price}}.00</span>
											</span>
										</a>
									</div>
								</div><!-- product -->
								@endforeach
								@endif
							</div>										
						</div><!-- .slick-wrap -->		
					</div><!-- col -->
				</div><!-- .row -->
			</div><!-- col -->
		</div><!-- .row -->
	</div><!-- .container -->

	<!-- <div class="section-common section-subscribe fixed-wrapper">
		<div class="background js__background_image js__parallax" data-background-image="url(http://placehold.it/1920x1280"></div>
		<div class="container">
			<form action="#" class="subscribe-form">
				<h2 class="section-title">Subscribe</h2>
				<div class="clear"></div>
				<p>Get the last news & promotion program from us</p>
				<div class="inp-controls">
					<input type="email" class="inp-email" placeholder="ENTER YOUR EMAIL">
					<button type="button" class="btn-submit"><span>SUBSCRIBE</span><i class="fa fa-long-arrow-right"></i></button>
				</div>
			</form>
		</div>
	</div> -->
	<style>
		@media screen and (max-width: 768px){
			.footer{ text-align: center !important; }
			.footer .widget .menu li a{ text-align: center !important; } 
			.widget .title { font-size: 20px }
		}
		.widget .menu li a { color: #fff; font-size: 13px }
		.widget .menu li { padding-bottom: 8px; }

	</style>


	<div class="section-common section-services">
		<div class="container">
			<ul class="row margin-top--30">
				<li class="col-md-4">
					<a href="#" class="item-service">
						<i class="fa fa-truck thumb"></i>
						<h2 class="title">Free Shipping</h2>
						<p>We free Shipping on all orders over Rs. 2000 on the world</p>
					</a>
				</li><!--- .col -->
				<li class="col-md-4">
					<a href="#" class="item-service">
						<i class="fa thumb fa-clock-o"></i>
						<h2 class="title">Fast Delivery</h2>
						<p>You will receive your orders in less than 7 days</p>
					</a>
				</li><!--- .col -->
				<li class="col-md-4">
					<a href="#" class="item-service">
						<i class="fa thumb fa-users"></i>
						<h2 class="title">24/7 customer service</h2>
						<p>We have a support department professional and friendly</p>
					</a>
				</li><!--- .col -->
			</ul><!--- .row -->
		</div><!--- .container -->
	</div><!--- .section-services --><br>

	<footer class="footer">
		<div class="container">
			<div class="top">
				<div class="row">
					<div class="col-md-9 col-sm-12">
						<div class="row">
							<div class="col-md-4 col-sm-4">
								<div class="widget ">
									<h3 class="title" style="color: #fff">Help</h3>
									<ul class="menu">
										{{-- <li><a href="#">Track order</a></li> --}}
										<li><a href="/faq">FAQs</a></li>
										<li><a href="/privacypolicy">Privacy policy</a></li>
										<li><a href="/tnc">Terms & Conditions</a></li>
										<li><a href="/returnpolicy">Refunds</a></li>
										{{-- <li><a href="#">Support Online</a></li> --}}
									</ul>
								</div>
							</div><!-- col -->

							<div class="col-md-4 col-sm-4">
								<div class="widget ">
									<h3 class="title" style="color: #fff">Account</h3>
									<ul class="menu">
										<li><a href="/en/member">My account</a></li>
										<li><a href="/wishlist">Wishlist</a></li>
										{{-- <li><a href="">Order history</a></li> --}}
										{{-- <li><a href="#">My Favorites</a></li> --}}
										{{-- <li><a href="#">Gift Vouchers</a></li> --}}
										{{-- <li><a href="#">Specials</a></li> --}}
									</ul>
								</div>
							</div><!-- col -->

							<div class="col-md-4 col-sm-4">
								<div class="widget ">
									<h3 class="title" style="color: #fff">Quick Links</h3>
									<ul class="menu">
										{{-- <li><a href="#">Best Sellers</a></li> --}}
										{{-- <li><a href="#">Featured Products</a></li> --}}
										{{-- <li><a href="#">Hot Products</a></li> --}}
										{{-- <li><a href="#">Top Rated</a></li> --}}
										{{-- <li><a href="#">Blog</a></li> --}}
										<li><a href="/about">About Us</a></li>
										<li><a href="/contact-us">Contact Us</a></li>						
									</ul>
								</div>
							</div><!-- col -->
						</div>
						<div class="row">
							<div class="col-md-4 col-sm-6">
								<br>
								<a href="http://bizbank.in/ " target="blank"><img src="/assetsss/images/AdminProduct/bizbanklogo.jpg" style="width: 150px; height: auto;"></a>
							</div>
							<div class="col-md-4 col-sm-6">
								<h3 class="title" style="color: #fff">Bank Detail</h3>
								<p style="margin-top: 5px">M-Biz Trading Pvt Ltd<br>
									ICICI Bank<br>
									A/C - 400305500145<br>
									IFSC - ICIC0004003</p>
							</div>

							<div class="col-md-4 col-sm-6">
								<h3 class="title" style="color: #fff">GST Number</h3>
								<p style="margin-top: 5px">AA070619047507U</p>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-6">
						<div class="widget widget_text">
							<h3 class="title" style="color: #fff">Contact Us</h3>

							<p>Please feel free to contact us if you have any question.</p>
							<ul class="contact-list">
								<li><a href="mailto:info@m-biz.in"><i class="fa fa-envelope-o"></i>info@m-biz.in</a></li>
								<li><a href="tel:1800 419 8447"><i class="fa fa-phone"></i>1800 419 8447</a></li>
								<li><i class="fa fa-map-marker"></i>M-Biz Trading Private Limited<br> A-51, 2nd Floor, New Dwarka Road, Dabri Exnt, New Delhi-110045</li>
							</ul>
						</div>
					</div><!-- col -->
					{{-- <div class="col-md-3 col-sm-6">
						<div class="widget widget_subscribe">
							<h3 class="title">Newsletter Signup</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
							<form action="#" class="join-form">
								<div class="form-controls">
									<input type="email" placeholder="Enter your email" class="inp-email">
									<input type="submit" value="Join" class="inp-submit">
									<i>We respect your privac</i>
								</div>
							</form>
							<ul class="social-list" style="text-align: center;">
								<li><a href="#" class="fa fa-twitter"></a></li>
								<li><a href="#" class="fa fa-facebook"></a></li>
								<li><a href="#" class="fa fa-google-plus"></a></li>
								<li><a href="#" class="fa fa-linkedin"></a></li>
								{{-- <li><a href="#" class="fa fa-vimeo"></a></li>
								<li><a href="#" class="fa fa-pinterest-p"></a></li>
							</ul>
						</div>
					</div><!-- col --> --}}
				</div><!-- .row -->
			</div><!-- .top -->
		</div><!-- .container -->
		<div class="bottom">
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="copyright">Copyright &copy; 2019 <a href="#">M-Biz Trading Private Limited.</a> All rights reserved.</a></div>
					</div><!-- col -->

					<div class="col-sm-4">
						<ul class="social-list" style="text-align: center;">
							<li><a href="#" class="fa fa-twitter"></a></li>
							<li><a href="https://www.facebook.com/M-Biz-Trading-Private-Limited-2122159934768714/" class="fa fa-facebook" target="blank"></a></li>
							{{-- <li><a href="#" class="fa fa-google-plus"></a></li> --}}
							<li><a href="#" class="fa fa-linkedin"></a></li>
								{{-- <li><a href="#" class="fa fa-vimeo"></a></li>
								<li><a href="#" class="fa fa-pinterest-p"></a></li> --}}
							</ul>
						</div>

						<div class="col-sm-4">
							<ul class="payment-list">
								<li><a href="#"><img src="/assetsss/images/payment1.jpg" alt=""></a></li>
								<li><a href="#"><img src="/assetsss/images/payment2.jpg" alt=""></a></li>
								<li><a href="#"><img src="/assetsss/images/payment3.jpg" alt=""></a></li>
								<li><a href="#"><img src="/assetsss/images/payment4.jpg" alt=""></a></li>
								<li><a href="#"><img src="/assetsss/images/payment5.jpg" alt=""></a></li>
								<li><a href="#"><img src="/assetsss/images/payment6.jpg" alt=""></a></li>
							</ul>
						</div><!-- col -->
					</div><!-- .row -->
				</div><!-- .container -->
			</div><!-- .bottom -->
		</footer><!--/.footer -->
	</div><!--/#wrapper -->




	
	
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="/assetsss/script/html5shiv.min.js"></script>
	<script src="/assetsss/script/respond.min.js"></script>
<![endif]-->
	<!-- 
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="/assetsss/scripts/jquery.min.js"></script>
		<script src="/assetsss/scripts/main.js"></script>
		<!-- BEGIN Revolution Slider Scripts -->
		<script type="text/javascript" src="/assetsss/plugin/rev/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/jquery.themepunch.revolution.min.js"></script>
		<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->

		<script type="text/javascript" src="/assetsss/plugin/rev/js/extensions/revolution.extension.actions.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/extensions/revolution.extension.carousel.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/extensions/revolution.extension.kenburn.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/extensions/revolution.extension.layeranimation.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/extensions/revolution.extension.migration.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/extensions/revolution.extension.navigation.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/extensions/revolution.extension.parallax.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/extensions/revolution.extension.slideanims.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/extensions/revolution.extension.video.min.js"></script>

		<!-- END Revolution Slider Scripts -->
		<script src="/assetsss/scripts/jquery.inview.min.js"></script>
		<script src="/assetsss/scripts/modernizr.min.js"></script>
		<script src="/assetsss/scripts/jquery.scrollTo.min.js"></script>
		<script src="/assetsss/plugin/select2/js/select2.min.js"></script>
		<script src="/assetsss/scripts/isotope.pkgd.min.js"></script>
		<script src="/assetsss/scripts/cells-by-row.min.js"></script>
		<script src="/assetsss/scripts/packery-mode.pkgd.min.js"></script>
		<script src="/assetsss/plugin/slick/slick.min.js"></script>
		<script src="/assetsss/scripts/jquery.parallax-1.1.3.min.js"></script>
		<script src="/assetsss/scripts/nouislider.min.js"></script>
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD84ST3FIRNNVS1CEm_IE9KoR-lAIw8OPo" type="text/javascript"></script>
		<script src="/assetsss/scripts/main.min.js"></script>
		<script type="text/javascript" src="/assetsss/plugin/rev/js/jquery.revolution.min.js"></script>

		{{-- Modal pop up Script --}}

		
		{{-- For Modal Box --}}
		<div id="quickViewPopup" class="popup js__popup">
			<div class="popup-overlay js__popup_close"></div>
			<div class="popup-body">
				<button type="button" class="popup-close-btn js__popup_close"><i class="fa fa-times"></i></button>
				<div class="container">
					<div class="popup-inside">
						<div class="row">
							<input type="hidden" name="product_id" id="hidden_id" value="">
							<div class="col-md-12 clearfix">
								<h2 class="single-title-border-bottom">Quick view</h2>
							</div><!-- col -->
							<div class="col-md-5">
								<div class="product-detail">
									<div class="product">
										<div class="images js__gallery">
											<a href="#" class="woocommerce-main-image zoom js__zoom_popup" data-target="#zoomPopup" data-zoom="" id="imggallery1">
												<img src="" alt="" id="imggallery2"/>
											</a><!-- .woocommerce-main-image -->
											<div class="thumbnails">									
												<a href="#" class="zoom js__thumb js__active" data-images="" data-zoom="" id="imgdz1" ><img src="" alt="" id="img1"/></a>
												<a href="#" class="zoom js__thumb" data-images="" data-zoom="" id="imgdz2" ><img src="" alt="" id="img2"/></a>
												<a href="#" class="zoom js__thumb" data-images="" data-zoom="" id="imgdz3" ><img src="" alt="" id="img3" /></a>											
											</div><!-- .thumbnails -->
											<div class="hidden">										
												<img src="" alt="" id="imgt1"/>
												<img src="" alt="" id="imgt2" />
												<img src="" alt="" id="imgt3"/>
												{{-- <img src="" alt="" /> --}}
											</div><!-- load images zone -->
										</div><!-- .images -->
									</div><!-- .product -->
								</div><!-- .product-detail -->
							</div><!-- col -->
							<div class="col-md-7">
								<div class="summary">
									<h2 class="product_title" id="viewname"></h2>


									<p id="rating"></p>

									<div class="summary-top">
										<div class="rating-box">
											<ul>
												<li> 
													@for($i=0; $i<5 ; $i++)
													<i class="fa fa-star <?php if($final_rate>0){ echo "active"; $final_rate--;  } ?>"></i>
													@endfor						
												</li>
											</ul>
										</div><br>
										<a href="#">Have 25 reviews</a> {{-- <span>/</span> <a href="#">Add your review</a> --}}
									</div>
									<p class="price">Rs. <span class="amount" id="viewprice"></span></p>
									<ul class="product_meta">
										{{-- <li><span>Brand:</span> Louis Vuitton</li> --}}
										<li><span>Availability:</span><span style="margin-left: 20%"id="availability"></span></li>
										{{-- <li><span>Product code:</span> ABC 123 456</li> --}}
									</ul>
									<div class="description">
										<p id="viewdescription"></p>
									</div>

									<div class="description">
										<p id="productSize"></p>
									</div>

									<ul>
										
										<select>
											
										</select>
										
									</ul><br>

									{{-- <form class="cart"> --}}
										<div class="quantity js__number"><input type="number" id="quickquantity" class="js__target" value="1" onchange="handleChange(this);"/><button type="button" class="js__plus fa-plus fa"></button><button type="button" class="js__minus fa-minus fa"></button></div>
										<button type="submit" onclick="addQuickCart()" class="single_add_to_cart_button">Add to cart</button>
									{{-- </form> --}}
								</div><!-- .summary -->
							</div><!-- col -->
						</div><!-- .row -->
					</div><!-- .popup-inside -->
				</div><!-- .container -->
			</div><!-- .popup-body -->
		</div><!-- #quickViewPopup -->

		<div id="zoomPopup" class="popup popup-images js__popup">
			<div class="popup-overlay js__popup_close"></div>
			<div class="popup-body">
				<button type="button" class="popup-close-btn js__popup_close"><i class="fa fa-times"></i></button>
				<div class="popup-inside js__popup_images">

				</div><!-- .popup-inside -->
			</div><!-- .popup-body -->
		</div><!-- #zoomPopup -->

		<script>

			

			var countproduct=<?php if(session()->get('count') != null){echo session()->get('count'); } else { echo 0;} ?>;

			function addQuickCart(){
				var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
				var pro_id = $('#hidden_id').val();
				var quant = $('#quickquantity').val();
				var cat_id = $('#cat_id').val();
				countproduct++;
				$.ajax({
					/* the route pointing to the post function */
					url: '/add-cart',
					type: 'POST',
					/* send the csrf-token and the input to the controller */
					data: {_token: CSRF_TOKEN, id: pro_id, quantity: quant},
					success: function (data) { 
						window.location.href = '/productdetail/'+cat_id;
						$("#show-total").html(countproduct);           
					}
				}); 
			}

			function openview(id,name,price,mrp,description,img1,img2,img3,rate,stock,size){         
				$('#hidden_id').val(id);
				$('#viewname').text(name);
				$('#viewdescription').text(description);
				$('#viewprice').text(price+".00");
				$('#rating').text(rate);
				$('#productSize').append(size);
				$('#availability').append(stock);
				
    //For main image
    $("#imggallery1").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#imggallery2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);

    //for a tag (data-zoom attribute)    
    $("#imgdz2").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#imgdz3").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
    $("#imgdz1").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);

    //for a tag (data-images attribute)
    $("#imgdz1").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#imgdz2").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#imgdz3").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);

    //for image tag (src attribute)
    $("#img1").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#img2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#img3").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
    $("#imgt1").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#imgt2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#imgt3").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
    var discount = ((mrp-price)*100)/mrp;
    $('#viewdiscount').text(discount.toFixed(2));
    $('#viewrate').val(rate);

} 




function deleteProduct(id){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');			

	$('#listhide'+id).hide();
	$.ajax({
		/* the route pointing to the post function */
		url: '/cart/delete-product/id',
		type: 'POST',
		/* send the csrf-token and the input to the controller */				
		data: {_token: CSRF_TOKEN, id: id},
		success: function (data) { 
			$("#show-total").html(data.showcount);			
			$("#bill").html(data.bill);			
		}
	}); 
}


//adding items to cart
function addCart(id){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
	var pro_quant = $('#quantity').val();
	var cat_id = $('#cat_id').val();
	countproduct++;
	$.ajax({
		/* the route pointing to the post function */
		url: '/add-cart',
		type: 'POST',
		/* send the csrf-token and the input to the controller */
		dataType: 'JSON',
		data: {_token: CSRF_TOKEN, id: id, quantity: pro_quant},
		success: function (data) { 
			window.location.href = '/productdetail/'+cat_id;
			$('#show-total').html(countproduct);
			// $("#ajaxdata").html(data);
		}
	}); 
}


function showCart(){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
	$.ajax({
		/* the route pointing to the post function */
		url: '/cart/show',
		type: 'POST',
		/* send the csrf-token and the input to the controller */					
		data: {_token: CSRF_TOKEN},
		success: function (data) {			
			$('#showminicart').html(data);
			$("#bill").text(data.bill);			
		}
	}); 
}


function noAvailability(){
	Swal('Product Out of Stock.');
}

//adding items to Wishlist
function addWishList(id){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
	var cat_id = $('#cat_id').val();
	var userId = $('#user_id').val();
	$.ajax({
		/* the route pointing to the post function */
		url: '/add-to-wishlist',
		type: 'POST',
		/* send the csrf-token and the input to the controller */
		dataType: 'JSON',
		data: {_token: CSRF_TOKEN, id: id, user_id: userId},
		success: function (data) { 
			window.location.href = '/productdetail/'+cat_id;
			$("#ajaxdata").html(data);
		}
	}); 
}


window.onscroll = function() {scrollFunction()};

function scrollFunction() {
	if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
		document.getElementById("myBtn").style.display = "block";
	} else {
		document.getElementById("myBtn").style.display = "none";
	}
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
	document.body.scrollTop = 0;
	document.documentElement.scrollTop = 0;
}



function showDropDown(){
	$('#drop-down').toggle();
}

function showCat(id){
	$('#more-cat'+id).toggle();
}

function showSubCat(id){
	$('#more-sub-cat'+id).toggle();
}
</script>



</body>
</html>