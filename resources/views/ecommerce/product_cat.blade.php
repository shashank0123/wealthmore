<?php
use App\Product;
use App\MainCategory;
use App\Banner;
use App\Testimonial;
use App\Review;
$maincategory = MainCategory::where('category_id',0)->get();
$total = 0;
?>
<input type="hidden" name="getID" id="getID" value="{{$send_id}}">
@if($searched_products != null)
@foreach($searched_products as $product)
<div class="col-md-4 col-sm-6 col-ip-6 col-xs-12">
	<div class="product-grid">
		<div class="thumb">
			<a href="/productdetail/{{$product->id}}"><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}" style="width: 100%; height: 270px; width:200px" alt="">									
			</a>
			<?php $discount=(($product->mrp - $product->sell_price)*100)/$product->mrp;

			$rate = Review::where('product_id',$product->id)->avg('rating');
			$final_rate = floor($rate);
			?>
			<ul class="controls">
				<li><a style="cursor: pointer" onclick="addWishList({{ $product->id }})"><i class="fa fa-heart"></i><span>LIKE IT</span></a></li>
				{{-- <li><a href="#"><i class="fa fa-exchange"></i><span>COMPARE</span></a></li> --}}
				<li><a style="cursor: pointer" class="js__popup_open" onclick="openview({{ $product->id }},'{{ $product->name }}',{{$product->sell_price}},{{ $product->mrp }},'{{$product->short_descriptions}}','{{$product->image1}}','{{$product->image2}}','{{$product->image3}}',<?php echo $final_rate; ?>,'<?php if($product->availability == 'yes'){ echo "In stock"; } else { echo "Out of Stock"; } ?>')" data-target="#quickViewPopup"><i class="fa fa-arrows-alt"></i><span>QUICK VIEW</span></a></li>
			</ul>
			<button type="submit" class="add_to_cart_button" onclick="addCart({{$product->id}})">Add to cart</button>
		</div>
		<?php
		$productcat = MainCategory::where('id',$product->category_id)->first(); 
		?>
		<a href="/productdetail/{{$product->id}}" class="content">
			<h2 class="title">{{$product->name}}</h2>
			<span class="category">{{$productcat->Mcategory_name}}</span>
			<span class="price">
				{{-- <del><span class="amount">$40.00</span></del> --}}
				<ins><span class="amount">Rs. {{ $product->sell_price }}.00</span></ins>
			</span>
			<div class="star-rating">
				<span class="js__width" data-width="80%"><strong class="rating">4.00</strong> out of 5</span>
			</div>
		</a>
	</div>
</div><!-- product -->
@endforeach
@else
<div class="message">
	<h1>No Products Available</h1>
</div>
@endif