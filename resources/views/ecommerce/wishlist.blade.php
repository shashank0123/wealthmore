<?php
use App\Product;
use App\MainCategory;
use App\Wishlist;
use App\Review;
?>

<style>
	.active { color: #ffcd02  !important; }
	.fa-star { color: #ccc ; }
	.page-large-title { padding: 20px 0 !important; color: #37bc9b !important;}
</style>

@extends('layouts.ecommerce2')

@section('content')


<div class="page-large-title" id="reload-div">
	<div class="container">
		<h1 class="title">Wishlist</h1>
			{{-- <nav class="woocommerce-breadcrumb">
				<a class="home" href="#">Home</a> / <a href="#">Pages</a> / <span>Wishlist</span>
			</nav> --}}
		</div><!-- .container -->
	</div><!-- .page-large-title -->\

	<?php
	$wishlistItem = Wishlist::where('user_id',$member->id)->get();	
					
	?>
	
	@if($wishlistItem != null)

	<input type="hidden" neme="member_id" id="member_id" value="{{$member->id}}">

	<div class="section-common"  id="refresh-wishlist">
		<div class="container">
			<div class="products row row-inline-block text-center">
				@foreach($wishlistItem as $item)
				<?php
				$product = Product::where('id',$item->product_id)->first();
				$category = MainCategory::where('id',$product->category_id)->first();
				?>
				<div class="col-md-3 col-sm-6 col-ip-6 col-xs-12" id="hide{{$item->product_id}}">
					<div class="product-grid wishlist-type">
						<div class="thumb">
							<a href="/productdetail/{{$product->id}}"><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }} " alt="" style="width: 250px; height: 270px">
								{{-- <ul class="attribute-list">
									<li><span class="label-coral">New</span></li>
									<li><span class="label-red">Sale</span></li>
								</ul> --}}
							</a>
						</div>
						<a href="/productdetail/{{$product->id}}" class="content">
							<h2 class="title">{{$product->name}}</h2>
							<span class="category">{{$category->Mcategory_name}}</span>
							<span class="price">
								{{-- <del><span class="amount">$40.00</span></del> --}}
								<ins><span class="amount">Rs. {{$product->sell_price}}.00</span></ins>
							</span>
							<div class="rating-box">
								

							</ul>
						</div>
					</a>
					<a onclick="deleteWishlistProduct({{$product->id}})" class="remove-item-wishlist fa fa-times"></a>
				</div>
			</div><!-- product -->		
			@endforeach
		</div><!-- products -->
		<div class="wishlist-control">
			{{-- <input type="button" onclick="addAll()" value="Add all to cart" class="btn-add-all-to-cart button-green"> --}}
			<input type="button" onclick="clearWishList()" value="Clear List" class="btn-clear-list button-black">
		</div><!-- .wishlist-control -->
	</div><!-- .container -->
</div><!-- section wishlist -->

@else
<div class="container" id="no-result">
	<h2>Nothing in Your Wishlist</h2>
</div>

@endif

<div class="section-common section-services">
		<div class="container">
			<ul class="row margin-top--30">
				<li class="col-md-4">
					<a href="#" class="item-service">
						<i class="fa fa-truck thumb"></i>
						<h2 class="title">Free Shipping</h2>
						<p>We free Shipping on all orders over Rs. 2000 on the world</p>
					</a>
				</li><!--- .col -->
				<li class="col-md-4">
					<a href="#" class="item-service">
						<i class="fa thumb fa-clock-o"></i>
						<h2 class="title">Fast Delivery</h2>
						<p>You will receive your orders in less than 7 days</p>
					</a>
				</li><!--- .col -->
				<li class="col-md-4">
					<a href="#" class="item-service">
						<i class="fa thumb fa-users"></i>
						<h2 class="title">24/7 customer service</h2>
						<p>We have a support department professional and friendly</p>
					</a>
				</li><!--- .col -->
			</ul><!--- .row -->
		</div><!--- .container -->
	</div><!--- .section-services --><br>

	{{-- <div class="section-common section-subscribe fixed-wrapper">
		<div class="background js__background_image js__parallax" data-background-image="url(http://placehold.it/1920x1280"></div>
		<div class="container">
			<form action="#" class="subscribe-form">
				<h2 class="section-title">Subscribe</h2>
				<div class="clear"></div>
				<p>Get the last news & promotion program from us</p>
				<div class="inp-controls">
					<input type="email" class="inp-email" placeholder="ENTER YOUR EMAIL">
					<button type="button" class="btn-submit"><span>SUBSCRIBE</span><i class="fa fa-long-arrow-right"></i></button>
				</div>
			</form><!-- .subscribe-form -->
		</div><!-- .container -->
	</div><!-- .section-subscribe --> --}}
	@endsection

	@section('script')
	<script>

		function deleteWishlistProduct(id){
			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
			var userId = $('#user_id').val();
			
			$.ajax({
				/* the route pointing to the post function */
				url: '/wishlist/delete-product/id',
				type: 'POST',
				/* send the csrf-token and the input to the controller */
				data: {_token: CSRF_TOKEN, id: id, user_id: userId},
				success: function (data) { 
					// $('#hide'+id).hide();
					window.location.href = "/wishlist";
					// alert('data deleted successfully');
					// $('#refresh-wishlist').load(document.URL + ' #refresh-wishlist');	
				}
			}); 
		}

		function clearWishList(){
			// alert('enter to confirm');
			var userId = $('#member_id').val();
			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

			$.ajax({
				/* the route pointing to the post function */
				url: '/wishlist/delete-all',
				type: 'POST',
				data: {_token: CSRF_TOKEN, user_id: userId},
				/* send the csrf-token and the input to the controller */				
				success: function (data) { 
					// alert('data deleted successfully');
					$('#no-result').show();
					window.location.href = "/wishlist";
				}
			});
		}

		function addAll(){
			// alert('enter to confirm');
			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

			$.ajax({
				/* the route pointing to the post function */
				url: '/wishlist/add-all',
				type: 'POST',
				/* send the csrf-token and the input to the controller */				
				success: function (data) { 
					alert('data added successfully');
					$('#no-result').show();
					$('#refresh-wishlist').load(document.URL + ' #refresh-wishlist');	
				}
			});
		}
	</script>
	@endsection