@extends('layouts.ecommerce')

<?php
use App\Product;
use App\MainCategory;
use App\Testimonial;
use App\Review;

$count=0;
?>
<style>
	.quotes-wrap .slick-dots { position: absolute;top: 50% !important; }
	.slick-list { height: 300px !important; }
	.slick-slider { height: 200px !important; }
	.slick-wrap { margin: 0 -15px; height: 300px; }
	.active { color: #ffcd02 !important; }
	.fa-star { color: #ccc ; }
	.item-adv-large .thumb { display: block; width: 280px; height: 350px}
	
	#visible-div { display: none; }
	@media screen and (max-width: 768px){		
		#visible-div { display: block; }
		.tp-bullet { display: none; }
		#hidden-div{ display: none; }

	}
</style>

@section('content')

<div id="rev_slider_3_1_wrapper" class="rev_slider_wrapper rev_no_pag fullwidthbanner-container" data-alias="amaza">
	<!-- START REVOLUTION SLIDER 5.1.6 fullwidth mode -->
	<div id="rev_slider_3_1" class="rev_slider fullwidthabanner" data-version="5.6.1">
		<ul>	
			<!-- SLIDE  -->
			@foreach($banners as $banner)
			<li data-index="rs-5" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="" >
				<!-- MAIN IMAGE -->
				<img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/banner/{{ $banner->image_url }} "  alt="" height="450" data-bgposition="center center" data-bgfit="contain" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
				<!-- LAYERS -->
			</li>
			@endforeach

			<!-- LAYER NR. 1 -->
				{{-- <div class="tp-caption   tp-resizeme" id="slide-5-layer-2" data-x="262" data-y="15" data-width="['none','none','none','none']" data-height="['none','none','none','none']" data-transform_idle="o:1;" data-transform_in="x:50px;opacity:0;s:400;e:Power4.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="500" data-responsive_offset="on">

					<img src="/assetsss/images/AdminProduct/banner/banner11.png" alt="" width="228" height="467" data-ww="228px" data-hh="467px" data-no-retina> 
				</div> --}}

				<!-- LAYER NR. 2 -->
				{{-- <div class="tp-caption   tp-resizeme" id="slide-5-layer-1" data-x="75" data-y="13" data-width="['none','none','none','none']" data-height="['none','none','none','none']" data-transform_idle="o:1;" data-transform_in="x:-50px;opacity:0;s:200;e:Linear.easeNone;" data-transform_out="x:-50px;opacity:0;s:300;s:300;" data-start="500" data-responsive_offset="on">

					<img src="/assetsss/images/AdminProduct/banner/banner22.png" alt="" width="228" height="467" data-ww="228px" data-hh="467px" data-no-retina> 
				</div> --}}

				<!-- LAYER NR. 3 -->
				{{-- <div class="tp-caption tp-resizeme title-border-bottom shop-deal" id="slide-5-layer-3" data-x="653" data-y="80" data-width="['auto']" data-height="['auto']" data-transform_idle="o:1;" data-transform_in="y:-50px;opacity:0;s:500;e:Linear.easeNone;" data-transform_out="opacity:0;s:300;s:300;" data-start="870" data-splitin="none" data-splitout="none" data-responsive_offset="on">SHOP DEALS</div>

				<!-- LAYER NR. 4 -->
				<div class="tp-caption tp-resizeme big-sale-title" id="slide-5-layer-4" data-x="653" data-y="129" data-height="['auto']" data-transform_idle="o:1;" data-transform_in="x:50px;opacity:0;s:300;e:Power2.easeInOut;" data-transform_out="x:-50px;opacity:0;s:300;s:300;" data-start="970" data-splitin="none" data-splitout="none" data-responsive_offset="on">BIG SALE THIS</div>

				<!-- LAYER NR. 5 -->
				<div class="tp-caption tp-resizeme big-sale-title-red" id="slide-5-layer-5" data-x="653" data-y="200" data-width="['auto']" data-height="['auto']" data-transform_idle="o:1;" data-transform_in="x:-50px;opacity:0;s:500;e:Power2.easeInOut;" data-transform_out="x:50px;opacity:0;s:300;s:300;" data-start="970" data-splitin="none" data-splitout="none" data-responsive_offset="on">WEEK</div>

				<!-- LAYER NR. 6 -->
				<div class="tp-caption   tp-resizeme p-text" id="slide-5-layer-6" data-x="653" data-y="270" data-width="['auto']" data-height="['auto']" data-transform_idle="o:1;" data-transform_in="y:50px;opacity:0;s:300;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="870" data-splitin="none" data-splitout="none" data-responsive_offset="on">Buy 1 get 3 and more ...</div>

				<!-- LAYER NR. 7 -->
				<div class="tp-caption rev-btn  tp-resizeme btn-check-it-out" id="slide-5-layer-7" data-x="658" data-y="308" data-width="['auto']" data-height="['auto']" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:0;e:Linear.easeNone;" data-style_hover="c:rgba(74, 74, 74, 1.00);bg:rgba(255, 205, 2, 1.00);" data-transform_in="opacity:0;s:500;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="870" data-splitin="none" data-splitout="none" data-responsive_offset="on">CHECK IT OUT</div> --}}

				<!-- SLIDE  -->
			{{-- <li data-index="rs-6" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb=""  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
				<!-- MAIN IMAGE -->


				<img class="js__background_color rev-slidebg" src="/assetsss/images/AdminProduct/banner/bg2.jpg" data-background-color='#333333' alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-no-retina>
				<!-- LAYERS -->

				<!-- LAYER NR. 1 -->
				<div class="tp-caption   tp-resizeme" id="slide-6-layer-2" data-x=""  data-y="143" data-width="['none','none','none','none']" data-height="['none','none','none','none']" data-transform_idle="o:1;" data-transform_in="s:100;e:Power2.easeInOut;" data-transform_out="s:300;s:300;" data-start="500" data-responsive_offset="on">

					
					<img src="/assetsss/images/AdminProduct/banner/banner22.png " alt="" width="220" height="450" style="margin-left: 20%; margin-left: 50px !important;margin-top: -100px !important" data-no-retina> 
				</div>

				<!-- LAYER NR. 2 -->
				<div class="tp-caption   tp-resizeme" id="slide-6-layer-3" data-x="32" data-y="25" data-width="['none','none','none','none']" data-height="['none','none','none','none']" data-transform_idle="o:1;" data-transform_in="x:left;s:600;e:Power0.easeIn;" data-transform_out="opacity:0;s:300;s:300;" data-start="500" data-responsive_offset="on">

					{{-- <img src="/assetsss/images/AdminProduct/banner/banner11.png }} " alt="" width="220" height="450"  data-no-retina>  --}}
				{{-- </div> --}}

				<!-- LAYER NR. 3 -->
				{{-- <div class="tp-caption   tp-resizeme shop-deal-2 title-border-bottom-2"  id="slide-6-layer-4" data-x="center" data-hoffset="264" data-y="108" data-width="['auto']" data-height="['auto']" data-transform_idle="o:1;" data-transform_in="y:-50px;opacity:0;s:500;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="800" data-splitin="none" data-splitout="none" data-responsive_offset="on">SHOP DEALS</div>

				<!-- LAYER NR. 4 -->
				<div class="tp-caption   tp-resizeme big-sale-title-2" id="slide-6-layer-5" data-x="center" data-hoffset="264" data-y="164" data-width="['auto']" data-height="['auto']" data-transform_idle="o:1;" data-transform_in="x:-50px;opacity:0;s:500;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="800" data-splitin="none" data-splitout="none" data-responsive_offset="on">BIG SALE THIS WEEK</div>

				<!-- LAYER NR. 5 -->
				<div class="tp-caption   tp-resizeme p-text-2" id="slide-6-layer-6" data-x="center" data-hoffset="264" data-y="235" data-width="['auto']" data-height="['auto']" data-transform_idle="o:1;" data-transform_in="x:50px;opacity:0;s:500;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="800" data-splitin="none" data-splitout="none" data-responsive_offset="on">buy 1 get 3 and more</div>

				<!-- LAYER NR. 6 -->
				<div class="tp-caption rev-btn  tp-resizeme btn-check-it-out-2" id="slide-6-layer-8" data-x="center" data-hoffset="264" data-y="289" data-width="['auto']" data-height="['auto']" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:0;e:Linear.easeNone;" data-style_hover="c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(0, 0, 0, 1.00);" data-transform_in="y:50px;opacity:0;s:500;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="800" data-splitin="none" data-splitout="none" data-responsive_offset="on">CHECK IT OUT</div>
			</li> --}} 
			<!-- SLIDE  -->
			{{-- <li data-index="rs-7" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb=""  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
				<!-- MAIN IMAGE -->



				<img src="/assetsss/images/AdminProduct/banner/bg3.jpg"  alt=""  width="1920" height="604" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
				<!-- LAYERS -->

				<!-- LAYER NR. 1 -->


				

				<div class="tp-caption   tp-resizeme" id="slide-7-layer-10" data-x="right" data-hoffset="80" data-y="23" data-width="['none','none','none','none']" data-height="['none','none','none','none']" data-transform_idle="o:1;" data-transform_in="x:50px;opacity:0;s:500;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="1580" data-responsive_offset="on"></div>

				<!-- LAYER NR. 2 -->
				<div class="tp-caption   tp-resizeme  title-border-bottom-2 shop-deal-2" id="slide-7-layer-4"  data-x="center" data-hoffset="" data-y="108" data-width="['auto']" data-height="['auto']" data-transform_idle="o:1;"  data-transform_in="y:-50px;opacity:0;s:500;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="800" data-splitin="none" data-splitout="none" data-responsive_offset="on">SHOP DEALS</div>

				<!-- LAYER NR. 3 -->
				<div class="tp-caption   tp-resizeme big-sale-title-2" id="slide-7-layer-5" data-x="center" data-hoffset="" data-y="164" data-width="['auto']" data-height="['auto']" data-transform_idle="o:1;" data-transform_in="opacity:0;s:500;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="800" data-splitin="none" data-splitout="none" data-responsive_offset="on">BIG SALE THIS WEEK</div>

				<!-- LAYER NR. 4 -->
				<div class="tp-caption   tp-resizeme p-text-2" id="slide-7-layer-6" data-x="center" data-hoffset="" data-y="235" data-width="['auto']" data-height="['auto']" data-transform_idle="o:1;" data-transform_in="y:50px;opacity:0;s:500;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="800" data-splitin="none" data-splitout="none" data-responsive_offset="on">buy 1 get 3 and more </div>

				<!-- LAYER NR. 5 -->
				<div class="tp-caption rev-btn  tp-resizeme btn-check-it-out-2" id="slide-7-layer-8" data-x="center" data-hoffset="" data-y="289" data-width="['auto']" data-height="['auto']" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:0;e:Linear.easeNone;" data-style_hover="c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(0, 0, 0, 1.00);" data-transform_in="y:50px;opacity:0;s:500;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="800" data-splitin="none" data-splitout="none" data-responsive_offset="on">CHECK IT OUT</div>

				<!-- LAYER NR. 6 -->
				<div class="tp-caption   tp-resizeme" id="slide-7-layer-9" data-x="59" data-y="26" data-width="['none','none','none','none']" data-height="['none','none','none','none']" data-transform_idle="o:1;" data-transform_in="x:-50px;opacity:0;s:500;e:Power2.easeInOut;" data-transform_out="opacity:0;s:300;s:300;" data-start="1580" data-responsive_offset="on"><img src="/assetsss/images/AdminProduct/banner/banner33.png" style="margin-left: -30px !important" alt="" width="200" height="450" data-ww="210px" data-hh="450px" data-no-retina></div>
			</li> --}}
		</ul>
		<div class="tp-bannertimer tp-bottom"></div>
	</div>
</div><!-- END REVOLUTION SLIDER -->

	{{-- <div class="section-adv section-common">
		<div class="container">
			<div class="row margin-top--30">
				<div class="col-md-4">
					<a href="#" class="item-adv">
						<img class="thumb" src="/assetsss/images/adv-large-1.png" alt="" style="width: 155px; height: 155px">
						<div class="content">
							<h2 class="title rose-color">Women’s Store</h2>
							<p>Lorem ipsum dolor sit amet elite</p>
							<span class="btn-common btn-border-rose">SHOP NOW</span>
						</div>
					</a><!--- .item-adv -->
				</div><!--- .col -->
				<div class="col-md-4">
					<a href="#" class="item-adv">
						<img class="thumb" src="/assetsss/images/adv-large-2.png" alt="" style="width: 155px; height: 155px">
						<div class="content">
							<h2 class="title magento-color">Men’s Store</h2>
							<p>Lorem ipsum dolor sit amet elite</p>
							<span class="btn-common btn-border-magento">SHOP NOW</span>
						</div>
					</a><!--- .item-adv -->
				</div><!--- .col -->
				<div class="col-md-4">
					<a href="#" class="item-adv">
						<img class="thumb" src="/assetsss/images/adv-kid-menu.png" alt="" style="width: 140px; height: 160px">
						<div class="content">
							<h2 class="title yellow-color">KID’s Store</h2>
							<p>Lorem ipsum dolor sit amet elite</p>
							<span class="btn-common btn-border-yellow">SHOP NOW</span>
						</div>
					</a><!--- .item-adv -->
				</div><!--- .col -->
			</div><!--- .row -->
		</div><!--- .container -->
	</div><!--- .section-adv --> --}}
	
	<div class="section-featured-product section-common">
		<div class="container tab-product-wrap js__tab">
			<ul class="tab-controls">
				<li><a href="#arrival" class="js__tab_control js__active">New Arrival</a></li>
				<li><a href="#featured" class="js__tab_control">Featured</a></li>
				<li><a href="#trending" class="js__tab_control">Trending</a></li>
			</ul><!--- .tab-controls -->
			<div class="tab-contents">
				
				<div class="tab-content js__tab_content js__active" id="arrival" >
					<div class="slick-wrap">
						<div class="products products-grid slick-middle-arrow js__slickslider" data-arrows="true" data-dots="false" data-show="4" data-responsive="{'992':2,'650':1}">
							@if($products != null)

							@for($count=$pro_cnt-1 ; $count>=0 ; $count--)
							<?php
							$product = Product::where('id',$products[$count])->first();
							

							?>

							@if($product != null)

							<?php
							$category = Product::where('products.id',$products[$count])->leftjoin('main_categories','main_categories.id','=','products.category_id')->select('main_categories.Mcategory_name')->first();
							

							?>

							<?php $discount=(($product->mrp - $product->sell_price)*100)/$product->mrp;

							$rate = Review::where('product_id',$product->id)->avg('rating');
							$final_rate = floor($rate);

							?>
							<div class="slick-slide">
								<div class="product-grid">
									<div class="thumb">
										<a href="/productdetail/{{ $product->id }}"><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }} " style="height: 230px; width: 190px" /></a>
										<ul class="controls">
											<li><a style="cursor: pointer;" onclick="addWishList({{ $product->id }})"><i class="fa fa-heart"></i><span>LIKE IT</span></a></li>
											{{-- <li><a href="#"><i class="fa fa-exchange"></i><span>COMPARE</span></a></li> --}}
											<li><a href="#" class="js__popup_open" onclick="openview({{ $product->id }},'{{ $product->name }}',{{$product->sell_price}},{{ $product->mrp }},'{{$product->short_descriptions}}','{{$product->image1}}','{{$product->image2}}','{{$product->image3}}',<?php echo $final_rate; ?>,'<?php if($product->availability == 'yes'){ echo "In stock"; } else { echo "Out of Stock"; } ?>')" data-target="#quickViewPopup"><i class="fa fa-arproductss-alt"></i><span>QUICK VIEW</span></a></li>
										</ul>
										<button type="submit" onclick="<?php if($product->availability == 'yes'){ echo "addCart(".$product->id.")"; } else { echo "noAvailability()"; } ?>" class="add_to_cart_button" >Add to cart</button>
									</div>
									<a href="/productdetail/{{ $product->id }}" class="content">
										<h2 class="title">{{$product->name}}</h2>
										<span class="category">{{$category->Mcategory_name}}</span>
										<span class="price">
											{{-- <del><span class="amount">Rs. {{$product->mrp}}</span></del> --}}
											<ins><span class="amount">Rs. {{$product->sell_price}}.00</span></ins>
										</span>
									</a>
								</div>
							</div>
							@endif
							@endfor
							@endif
							<!-- product -->
						</div>
					</div>
				</div><!--- .tab-content -->
				
				<?php
				$featuredproducts = Product::where('trending','yes')->where('status','Active')->orderBy('created_at','desc')->get();
				?>

				<div class="tab-content js__tab_content" id="featured">
					<div class="slick-wrap">
						<div class="products products-grid slick-middle-arrow js__slickslider" data-arrows="true" data-dots="false" data-show="4" data-responsive="{'992':2,'650':1}">
							@if(isset($featuredproducts))
							@foreach($featuredproducts as $col)
							<?php
							$featurecategory = Product::where('products.id',$col->id)->leftjoin('main_categories','main_categories.id','=','products.category_id')->select('main_categories.Mcategory_name')->first();
							
							?>
							<?php $discount=(($col->mrp - $col->sell_price)*100)/$col->mrp;

							$rate = Review::where('product_id',$col->id)->avg('rating');
							$final_rate = floor($rate);
							?>
							<div class="slick-slide">
								<div class="product-grid">
									<div class="thumb">
										<a href="/productdetail/{{ $col->id }}"><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $col->image1 }} " style="height: 230px; width: 190px" />
											{{-- <ul class="attribute-list">
												<li><span class="label-coral">New</span></li>
												<li><span class="label-red">Sale</span></li>
											</ul> --}}
										</a>
										<ul class="controls">
											<li><a style="cursor: pointer;" onclick="addWishList({{ $col->id }})"><i class="fa fa-heart"></i><span>LIKE IT</span></a></li>
											{{-- <li><a href="#"><i class="fa fa-exchange"></i><span>COMPARE</span></a></li> --}}
											<li><a href="#" class="js__popup_open" onclick="openview({{ $col->id }},'{{ $col->name }}',{{$col->sell_price}},{{ $col->mrp }},'{{$col->short_descriptions}}','{{$col->image1}}','{{$col->image2}}','{{$col->image3}}',<?php echo $final_rate; ?>,'<?php if($col->availability == 'yes'){ echo "In stock"; } else { echo "Out of Stock"; } ?>')" data-target="#quickViewPopup"><i class="fa fa-arrows-alt"></i><span>QUICK VIEW</span></a></li>
										</ul>
										<button type="submit" class="add_to_cart_button"  onclick="<?php if($col->availability == 'yes'){ echo "addCart(".$col->id.")"; } else { echo "noAvailability()"; } ?>">Add to cart</a></button>
									</div>
									<a href="/productdetail/{{ $col->id }}" class="content">
										<h2 class="title">{{$col->name}}</h2>
										<span class="category">{{$featurecategory->Mcategory_name}}</span>
										<span class="price">
											{{-- <del><span class="amount">Rs. {{$product->mrp}}</span></del> --}}
											<ins><span class="amount">Rs. {{$col->sell_price}}.00</span></ins>
										</span>
									</a>
								</div>
							</div>{{-- product --}}
							@endforeach
							@endif
							
						</div>{{-- .products --}}
					</div>{{-- .slick-wrap --}}
				</div>{{-- - .tab-content  --}}

				<?php
				$trendproducts = Product::where('trending','yes')->where('status','Active')->orderBy('name','asc')->get();
				?>

				<div class="tab-content js__tab_content" id="trending">
					<div class="slick-wrap">
						<div class="products products-grid slick-middle-arrow js__slickslider" data-arrows="true" data-dots="false" data-show="4" data-responsive="{'992':2,'650':1}">

							@if(isset($trendproducts))

							@foreach($trendproducts as $set)
							<?php
							$trendcategory = Product::where('products.id',$set->id)->leftjoin('main_categories','main_categories.id','=','products.category_id')->select('main_categories.Mcategory_name')->first();
							
							?>
							<?php $discount=(($set->mrp - $set->sell_price)*100)/$set->mrp;

							$rate = Review::where('product_id',$set->id)->avg('rating');
							$final_rate = floor($rate);
							?>
							<div class="slick-slide">
								<div class="product-grid">
									<div class="thumb">
										<a href="/productdetail/{{ $set->id }}"><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $set->image1 }} " style="height: 230px; width: 190px" />
											{{-- <ul class="attribute-list">
												<li><span class="label-coral">New</span></li>
												<li><span class="label-red">Sale</span></li>
											</ul> --}}
										</a>
										<ul class="controls">
											<li><a style="cursor: pointer;" onclick="addWishList({{ $set->id }})"><i class="fa fa-heart"></i><span>LIKE IT</span></a></li>
											{{-- <li><a href="#"><i class="fa fa-exchange"></i><span>COMPARE</span></a></li> --}}
											<li><a href="#" class="js__popup_open" onclick="openview({{ $set->id }},'{{ $set->name }}',{{$set->sell_price}},{{ $set->mrp }},'{{$set->short_descriptions}}','{{$set->image1}}','{{$set->image2}}','{{$set->image3}}',<?php echo $final_rate; ?>,'<?php if($set->availability == 'yes'){ echo "In stock"; } else { echo "Out of Stock"; } ?>')" data-target="#quickViewPopup"><i class="fa fa-arrows-alt"></i><span>QUICK VIEW</span></a></li>
										</ul>
										<button type="submit"  onclick="<?php if($set->availability == 'yes'){ echo "addCart(".$set->id.")"; } else { echo "noAvailability()"; } ?>" class="add_to_cart_button" >Add to cart</button>
									</div>
									<a href="/productdetail/{{ $set->id }}" class="content">
										<h2 class="title">{{$set->name}}</h2>
										<span class="category">{{$trendcategory->Mcategory_name}}</span>
										<span class="price">
											{{-- <del><span class="amount">Rs. {{$product->mrp}}</span></del> --}}
											<ins><span class="amount">Rs. {{$set->sell_price}}.00</span></ins>
										</span>
									</a>
								</div>
							</div>{{-- product --}}
							@endforeach
							@endif
							
						</div>{{-- .products --}}
					</div>{{-- .slick-wrap --}}
				</div>{{-- - .tab-content  --}}
			</div><!--- .tab-contents -->
		</div><!--- .tab-product-wrap -->
	</div><!--- .section-featured-product -->
	<?php	
	
	$featured = Product::where('trending','yes')->where('status','Active')->orderBy('created_at',"desc")->limit(1)->first();	
	//$newarrival_id = $featured->id -1;
	$newarrival = Product::where('trending','yes')->where('status','Active')->where('id','!=',$featured->id)->orderBy('created_at','DESC')->limit(1)->first();

	//$newarrival = Product::where('id',$newarrival_id)->where('status','Active')->first();
	?>
	<div class="section-adv-large section-common" id="hidden-div">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					@if(isset($newarrival))
					<a href="/productdetail/{{$newarrival->id}}" class="item-adv-large">					
						<img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $newarrival->image1 }}" alt="" class="thumb" >				
						<div class="content">
							<h3 class="title-border-bottom">New Arrival</h3>
							<div class="text">
								<h2 class="title">RS {{$newarrival->sell_price}}</h2>
								<h4 class="desc">{{$newarrival->name}}</h4>
							</div>
							<!--<span class="btn-border-dark btn-common">SHOP NOW</span>-->
						</div>
					</a><!--- .item-adv-large -->
					@endif
				</div><!--- .col -->
				<div class="col-md-6" >
					@if(isset($featured))
					<a href="/productdetail/{{ $featured->id }}" class="item-adv-large">				
						<img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $featured->image1 }}" alt="" class="thumb">					
						<div class="content">
							<h3 class="title-border-bottom">Featured</h3>
							<div class="text">
								<h2 class="title">RS {{$featured->sell_price}}</h2>
								<h4 class="desc">{{$featured->name}}</h4>
							</div>
							{{-- <span class="btn-border-dark btn-common">SHOP NOW</span> --}}
						</div>
					</a><!--- .item-adv-large -->
					@endif
				</div><!--- .col -->
			</div><!--- .row -->
		</div><!--- .container -->
	</div><!--- .section-adv-large -->

	<div class="section-adv-large section-common" id="visible-div">
		<div class="container">
			<div class="row">
				<div class="col-md-6">

					@if(isset($newarrival))
					<img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $newarrival->image1 }}" alt="" class="thumb" id="banner-response">
					<a href="/productdetail/{{$newarrival->id}}" class="item-adv-large">
						
						<div class="content">
							<h3 class="title-border-bottom">New Arrival</h3>
							<div class="text">
								<h2 class="title">RS {{$newarrival->sell_price}}</h2>
								<h4 class="desc">{{$newarrival->name}}</h4>
							</div>
							<!--<span class="btn-border-dark btn-common">SHOP NOW</span>-->
						</div>
					</a><!--- .item-adv-large -->
					@endif
				</div><!--- .col -->
				<div class="col-md-6" >
					@if(isset($featured))
					<img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $featured->image1 }}" alt="" class="thumb" id="banner-response">
					<a href="/productdetail/{{ $featured->id }}" class="item-adv-large">
						
						<div class="content">
							<h3 class="title-border-bottom">Featured</h3>
							<div class="text">
								<h2 class="title">RS {{$featured->sell_price}}</h2>
								<h4 class="desc">{{$featured->name}}</h4>
							</div>
							{{-- <span class="btn-border-dark btn-common">SHOP NOW</span> --}}
						</div>
					</a><!--- .item-adv-large -->
					@endif
				</div><!--- .col -->
			</div><!--- .row -->
		</div><!--- .container -->
	</div><!--- .section-adv-large -->
	
	<div class="section-isotope-product section-common tab-product-wrap">
		<div class="container js__filter_isotope">
			<h2 class="section-title">All Products</h2>
			<ul class="tab-controls">
				<li><a style="cursor: pointer;" onclick="getAllProducts()" class="js__filter_control js__active" data-filter="*">All</a></li>
				{{-- <li><a href="#" class="js__filter_control" data-filter=".women">Women</a></li>
				<li><a href="#" class="js__filter_control" data-filter=".men">Men</a></li>
				<li><a href="#" class="js__filter_control" data-filter=".kid">Kid</a></li> --}}
			</ul>
			<div class="clear"></div>
			<div class="products products-grid row row-inline-block js__isotope_items">
				<?php $allproducts = Product::where('status','Active')->where('trending','yes')->inRandomOrder()->limit(12)->get();
				//$allproducts = Product::where('status','Active')->inRandomOrder()->limit(12)->get();
				?>
				@if(isset($allproducts))
				@foreach($allproducts as $product)
				<?php
				$productcategory = Product::where('products.id',$product->id)->leftjoin('main_categories','main_categories.id','=','products.category_id')->select('main_categories.Mcategory_name')->first();
				?>
				<?php $discount=(($product->mrp - $product->sell_price)*100)/$product->mrp;

				$rate = Review::where('product_id',$product->id)->avg('rating');
				$final_rate = floor($rate);
				?>
				<div class="col-md-3 col-sm-6 col-ip-6 col-xs-12 js__isotope_item men">
					<div class="product-grid">
						<div class="thumb">
							<a href="/productdetail/{{ $product->id }}"><img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $product->image1 }}" alt="" style="width: 200px; height: 250px">
								{{-- <ul class="attribute-list">
									<li><span class="label-coral">New</span></li>
									<li><span class="label-red">Sale</span></li>
								</ul> --}}
							</a>

							<ul class="controls">
								<li><a style="cursor: pointer;" onclick="addWishList({{ $product->id }})"><i class="fa fa-heart"></i><span>LIKE IT</span></a></li>
								{{-- <li><a href="#"><i class="fa fa-exchange"></i><span>COMPARE</span></a></li> --}}
								<li><a href="#" class="js__popup_open" onclick="openview({{ $product->id }},'{{ $product->name }}',{{$product->sell_price}},{{ $product->mrp }},'{{$product->short_descriptions}}','{{$product->image1}}','{{$product->image2}}','{{$product->image3}}',<?php echo $final_rate; ?>,'<?php if($product->availability == 'yes'){ echo "In stock"; } else { echo "Out of Stock"; } ?>')" data-target="#quickViewPopup"><i class="fa fa-arrows-alt"></i><span>QUICK VIEW</span></a></li>
							</ul>
							<button type="submit"  onclick="<?php if($product->availability == 'yes'){ echo "addCart(".$product->id.")"; } else { echo "noAvailability()"; } ?>" class="add_to_cart_button" >Add to cart</button>
						</div>
						<a href="/productdetail/{{ $product->id }}" class="content">
							<h2 class="title">{{$product->name}}</h2>
							<span class="category">{{$productcategory->Mcategory_name}}</span>
							<span class="price">
								{{-- <del><span class="amount">Rs. {{$product->mrp}}</span></del> --}}
								<ins><span class="amount">Rs. {{$product->sell_price}}.00</span></ins>
							</span>
						</a>
					</div>
				</div><!-- product -->
				@endforeach				
				@endif
			</div><!-- .products -->
			{{-- <a href="#" class="btn-ajax-loading"><i class="fa fa-refresh"></i> <span>View more (2.450 products) ...</span></a> --}}
		</div><!--- .container -->
	</div><!--- .section-isotope-product -->
	
	<div class="section-common section-services">
		<div class="container">
			<ul class="row margin-top--30">
				<li class="col-md-4">
					<a href="#" class="item-service">
						<i class="fa fa-truck thumb"></i>
						<h2 class="title">Free Shipping</h2>
						<p>We free Shipping on all orders over Rs. 2000 on the world</p>
					</a>
				</li><!--- .col -->
				<li class="col-md-4">
					<a href="#" class="item-service">
						<i class="fa thumb fa-clock-o"></i>
						<h2 class="title">Fast Delivery</h2>
						<p>You will receive your orders in less than 7 days</p>
					</a>
				</li><!--- .col -->
				<li class="col-md-4">
					<a href="#" class="item-service">
						<i class="fa thumb fa-users"></i>
						<h2 class="title">24/7 customer service</h2>
						<p>We have a support department professional and friendly</p>
					</a>
				</li><!--- .col -->
			</ul><!--- .row -->
		</div><!--- .container -->
	</div><!--- .section-services -->
	
	<style>
		@media screen and (max-width: 428px){			
			#response-test { margin-top: -70px }
			.quotes-wrap .slick-dots button { margin-top: 95px; }
			.item-quote .thumb { width: 70px; height: 70px; }

			.author .from , .author .location{ font-size: 12px !important }
		}
		.test-text { font-size: 14px !important }
	}
</style>
<div class="section-common">
	<div class="quotes-wrap slick-wrap">
		<div class="background js__background_image js__parallax" data-background-image="url(assetsss/images/bg-tes-1.jpg)" style="height: 300px !important"></div>
		<div class="container">
			<ul class="js__slickslider" data-show="1" data-arrows="false" data-dots="true" id="response-test">
				<?php  $testimonials = Testimonial::all();
				?>
				@if(isset($testimonials) )
				@foreach($testimonials as $testimonial)
				<li>
					<?php						
					$final_rate = floor($testimonial->rating);

					?>
					<div class="item-quote">
						<img src="{{ URL::to('/') }}/assetsss/images/AdminProduct/{{ $testimonial->image_url }}" alt="" class="thumb">
						<div class="rating-box">
							<ul>
								<li> 
									@for($i=0; $i<5 ; $i++)
									<i class="fa fa-star <?php if($final_rate>0){ echo "active"; $final_rate--;  } ?>"></i>
									@endfor						
								</li>
							</ul>
						</div><br>
						<p class="test-text">{{$testimonial->review}}</p>
						<div class="author">
							<span class="name">{{$testimonial->client_name}}</span>
							<span class="from">from </span>
							<span class="location">{{$testimonial->city}}</span>
						</div>
					</div><!--- .item-quote -->
				</li><!--- slide -->
				@endforeach		
				@endif			
			</ul><!--- slides -->
		</div><!--- .container -->
	</div><!--- .quotes-wrap -->
</div><!--- section quotes --><br>

{{-- Place for blogs and banners(Code is in sample.blade.php) --}}

{{-- //Modal Box for Products --}}
@endsection

@section('script')


{{-- For Modal Box --}}
<div id="quickViewPopup" class="popup js__popup">
	<div class="popup-overlay js__popup_close"></div>
	<div class="popup-body">
		<button type="button" onclick="closeModal()" class="popup-close-btn js__popup_close"><i class="fa fa-times"></i></button>
		<div class="container">
			<div class="popup-inside">
				<div class="row">
					<input type="hidden" name="product_id" value="" id="hidden_id"> 
					<div class="col-md-12 clearfix">
						<h2 class="single-title-border-bottom">Quick view</h2>
					</div><!-- col -->
					<div class="col-md-5">
						<div class="product-detail">
							<div class="product">
								<div class="images js__gallery">
									<a href="#" class="woocommerce-main-image zoom js__zoom_popup" data-target="#zoomPopup" data-zoom="" id="imggallery1">
										<img src="" alt="" id="imggallery2"/>
									</a><!-- .woocommerce-main-image -->
									<div class="thumbnails">									
										<a href="#" class="zoom js__thumb js__active" data-images="" data-zoom="" id="imgdz1" ><img src="" alt="" id="img1"/></a>
										<a href="#" class="zoom js__thumb" data-images="" data-zoom="" id="imgdz2" ><img src="" alt="" id="img2"/></a>
										<a href="#" class="zoom js__thumb" data-images="" data-zoom="" id="imgdz3" ><img src="" alt="" id="img3" /></a>											
									</div><!-- .thumbnails -->
									<div class="hidden">										
										<img src="" alt="" id="imgt1"/>
										<img src="" alt="" id="imgt2" />
										<img src="" alt="" id="imgt3"/>
										{{-- <img src="" alt="" /> --}}
									</div><!-- load images zone -->
								</div><!-- .images -->
							</div><!-- .product -->
						</div><!-- .product-detail -->
					</div><!-- col -->
					<div class="col-md-7">
						<div class="summary">
							<h2 class="product_title" id="viewname"></h2>
							<div class="summary-top">
								<div class="rating-box">
									<ul>
										<li> 
											@for($i=0; $i<5 ; $i++)
											<i class="fa fa-star <?php if($final_rate>0){ echo "active"; $final_rate--;  } ?>"></i>
											@endfor						
										</li>

									</ul>
								</div>
								<a href="#">Have 25 reviews</a> {{-- <span>/</span> <a href="#">Add your review</a> --}}
							</div>
							<p class="price">Rs. <span class="amount" id="viewprice"></span></p>
							<ul class="product_meta">
								{{-- <li><span>Brand:</span> Louis Vuitton</li> --}}
								<li><span>Available:</span><span style="margin-left: 20%"id="availability"></span></li>
								{{-- <li><span>Product code:</span> ABC 123 456</li> --}}
							</ul>
							<div class="description">
								<p id="viewdescription"></p>
							</div>

							<form class="cart">
								<div class="quantity js__number"><input id="quickquantity" type="number" class="js__target" value="1" /><button type="button" class="js__plus fa-plus fa"></button><button type="button" class="js__minus fa-minus fa"></button></div>
								<button type="submit" class="single_add_to_cart_button" onclick="addToCart()">Add to cart</button>
							</form>
						</div><!-- .summary -->
					</div><!-- col -->
				</div><!-- .row -->
			</div><!-- .popup-inside -->
		</div><!-- .container -->
	</div><!-- .popup-body -->
</div><!-- #quickViewPopup -->

<div id="zoomPopup" class="popup popup-images js__popup">
	<div class="popup-overlay js__popup_close"></div>
	<div class="popup-body">
		<button type="button" class="popup-close-btn js__popup_close"><i class="fa fa-times"></i></button>
		<div class="popup-inside js__popup_images">

		</div><!-- .popup-inside -->
	</div><!-- .popup-body -->
</div><!-- #zoomPopup -->

<script>


	function noAvailability(){
		Swal('Product Out of Stock.');
	}

	var countproduct=<?php if(session()->get('count') != null){echo session()->get('count'); } else { echo 0;} ?>;

	function addToCart(){
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
		var pro_id = $('#hidden_id').val();
		var quant = $('#quickquantity').val();
		countproduct++;
		$.ajax({
			/* the route pointing to the post function */
			url: '/add-cart',
			type: 'POST',
			/* send the csrf-token and the input to the controller */
			data: {_token: CSRF_TOKEN, id: pro_id, quantity: quant},
			success: function (data) { 
				$("#show-total").html(countproduct);           
			}
		}); 
	}

		//modal  box of quick view
		function openview(id,name,price,mrp,description,img1,img2,img3,rate,stock){
			// alert(img1+"<br>"+img2+"<br>"+img3);
			//var discount = ((mrp-price)*100)/mrp;
			//$('#viewdiscount').text(discount.toFixed(2));
			//$('#viewrate').val(rate);
			$('#hidden_id').val(id);    
			$('#viewname').text(name);
			$('#viewdescription').text(description);
			$('#viewprice').text(price+".00");
			$('#availability').text(stock);

    //For main image
    $("#imggallery1").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#imggallery2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);

    //for a tag (data-zoom attribute)    
    $("#imgdz2").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#imgdz3").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
    $("#imgdz1").attr("data-zoom", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);

    //for a tag (data-images attribute)
    $("#imgdz1").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#imgdz2").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#imgdz3").attr("data-images", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);

    //for image tag (src attribute)
    $("#img1").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#img2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#img3").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
    $("#imgt1").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img1);
    $("#imgt2").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img2);
    $("#imgt3").attr("src", "{{ URL::to('/') }}/assetsss/images/AdminProduct/"+img3);
    

}



//adding items to cart
function addCart(id){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
	countproduct++;
	$.ajax({
		/* the route pointing to the post function */
		url: '/add-to-cart',
		type: 'POST',
		/* send the csrf-token and the input to the controller */
		data: {_token: CSRF_TOKEN, id: id},
		success: function (data) { 
			window.location.href = '/en';
			$("#show-total").html(countproduct);           
		}
	}); 
}

//adding items to Wishlist
function addWishList(id,count){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
	var userId = $('#user_id').val();
	$.ajax({
		/* the route pointing to the post function */
		url: '/add-to-wishlist',
		type: 'POST',
		/* send the csrf-token and the input to the controller */
		data: {_token: CSRF_TOKEN, id: id, user_id: userId},
		success: function (data) { 
			window.location.href = '/en';
			$("#ajaxdata").text(count);
		}
	}); 
}

function getAllProducts(){
	window.location.href = "/searchproduct";
}

function closeModal(){
	$('#quickViewPopup').load();
}
</script>
@endsection