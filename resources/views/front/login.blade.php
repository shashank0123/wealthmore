@extends('front.loginlayout')

@section('title')
Login - {{ config('app.name') }}
@stop

@section('content')
<style>
  body{
    background:url(../assets/img/login.png) no-repeat center center fixed !important;
  }
  [type="text"].form-control { color: #000; }
  .pink-text {  font-size: 24px;  }
  .card{margin-top:0% auto; max-width:40%;margin-left: 28%;margin-top: 10%; background-color: #fff;border-radius: 4%;}
  .pull-right button { margin-bottom: 20px !important }

  .card.bordered .card-header {
    border-bottom: 1px solid #000 !important;
}
label {text-transform: uppercase !important;}
  #form label { color: #000; }

  @media screen and (max-width: 991px) {
    .card { max-width:70%;margin-left: 15%;margin-top: 20% !important }
  }
  @media screen and (max-width: 580px) {
    .card { max-width:90%;margin-left: 5%; }
  }
  #login-head { width: 100% !important; background-color: #009933 }
</style>

<div class="clearfix"></div>
                  
<div class="center">
  <div  class="col-lg-12 col-md-12 col-sm-12" style="background: #009933; position: absolute; top: 0px;">
                        <div style="background-color: #009933;padding-left: 10px; padding-right: 10px; padding-top: 5px;
    padding-bottom: 0;" class="pull-right" >
                            <ul>
                                <li style="list-style: none">
                                    <a href="#" style="color: #fff; font-size: 14px;text-transform: uppercase !important;">Help Line:  18001038581</a>
                                </li>
                                
                            </ul>
                        </div>
                    </div>

  <div class="card bordered z-depth-2" style="background-color: #fff">
    <div class="card-header" style="background-color: #fff">
      <div class="brand-logo" style="text-align: center;">
        <a href="/"><img src="{{ asset('/wealth-assets/img/logo-bella-shop1.png') }}" width="auto"; height="60px"></a>
      </div>
    </div>

    <div class="container m-b-30" style="max-width: 100%;">
    <form class="form-floating action-form" http-type="post" id="form" data-url="{{ route('login.post', ['lang' => \App::getLocale()]) }}">
      <div class="card-content">
        <div class="m-b-30">
          <div class="card-title strong" style="color: #000; font-weight: bold;text-transform: uppercase !important; font-size: 17px">@lang('login.title')</div>
          <!-- <p><a href="{{ route('login', ['lang' => 'en']) }}">English</a> | <a href="{{ route('login', ['lang' => 'chs']) }}">简体中文</a> | <a href="{{ route('login', ['lang' => 'cht']) }}">繁體中文</a></p> -->
          <p class="card-title-desc" style="color: red !important"> @lang('login.subtitle')</p>
        </div>
        <div class="form-group">
          <label for="username">@lang('login.username')</label>
          <input type="text" style="text-transform: uppercase;" name="username" class="form-control" id="username" required="">
        </div>
        <div class="form-group">
          <label for="password">@lang('login.password')</label>
          <input type="password" name="password" class="form-control" id="password" required="">
        </div>
        <div class="form-group">
          <div class="checkbox">
          <label><input type="checkbox" name="remember"> @lang('login.remember') </label>
          

          </div>
        </div>

        <div class="card-action clearfix">

          <div class="pull-left">
            <a href="/register" class="btn btn-link black-text">

              <span style="color: #00f; font-weight: bold;">Register Here</span>
            </a>
          </div>


          <div class="pull-right">
            <button type="submit" class="btn btn-link black-text">
              <span class="btn-preloader">
                <i class="md md-cached md-spin"></i>
              </span>
              <span style="color: #f00; font-weight: bold;">@lang('login.title')</span>
            </button>
          </div>
          <div class="row" style="text-align: center; margin-top: 30px">
          <label ><a href="/passwordReset" id="forget" style="color: #000;font-weight: bold;"> @lang('login.forgotPassword') ?</a> </label><br><br>
        </div>
        </div>
        
      </form>
    </div>
  </div>
</div>

@stop
