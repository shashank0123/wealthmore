@extends('front.app')

@section('title')
  @lang('incomedirect.title') | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">@lang('breadcrumbs.front')</a></li>
    <li><a href="{{ route('home', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.dashboard')</a></li>
S    <li class="active">@lang('breadcrumbs.directincome')</li>
  </ul>
@stop

@section('content')
  <main>
    {{-- @include('front.include.sidebar') --}}
    <div class="main-container container">
      {{-- @include('front.include.header') --}}
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section class="tables-data">
          <div class="page-header">
            <h1><i class="md md-group-add"></i> @lang('incomedirect.title')</h1>
            <p class="lead">@lang('incomedirect.subtitle')</p>
          </div>

          <div class="card">
            <div>
              <div class="datatables">
                 <table class="table table-full table-full-small dt-responsive display nowrap table-grid" cellspacing="0" width="100%" role="grid" data-url="{{ route('bonus.pairingList', ['lang' => \App::getLocale()]) }}">
                    <thead>
                      <tr>
                        <th data-id="created_at">@lang('misc.create')</th>
                        <th data-id="amount_cash">@lang('misc.cash')</th>
                        <th data-id="admin_charge">@lang('misc.admin')</th>
                        <th data-id="tds_charge">@lang('misc.tds')</th>
                        <th data-id="repurchase">@lang('misc.repurchase')</th>
                        {{-- <th data-id="amount_promotion">@lang('misc.promotion')</th> --}}
                        <th data-id="total">@lang('misc.total')</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </main>
@stop
