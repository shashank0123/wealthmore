@extends('front.app')
@section('title')
  @lang('registerHistory.title') | {{ config('app.name') }}
@stop

@section('breadcrumb')
  <ul class="breadcrumb">
    <li><a href="#">@lang('breadcrumbs.front')</a></li>
    <li><a href="{{ route('home', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.dashboard')</a></li>
    <li><a href="{{ route('member.register', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.register')</a></li>
    <li class="active">@lang('breadcrumbs.registerHistory')</li>
  </ul>
@stop
<style>
   li,a,input,p,h2,h1,h3,h4,h5,h6,span,div,td,th { text-transform: uppercase !important; }
</style>
@section('content')
  <main>
    <div class="main-container">
      <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
        <section class="tables-data">
          <div class="page-header">
            <h1 style="font-weight: bold; font-size: 24px;"><i class="md md-group-add"></i> {{ $side }} Member</h1>
            <p class="lead">@lang('registerHistory.subTitle')</p>
          </div>

          <div class="card">
            <div>
              <div class="">
               <table class="table table-full table-full-small dt-responsive display nowrap" cellspacing="0" width="100%" role="grid" data-url="{{ route('member.registerleftHistoryList') }}">
                  <thead>
                    <tr>
                      <th>@lang('registerHistory.sr')</th>
                      <th>@lang('registerHistory.join')</th>
                      <th>@lang('registerHistory.username')</th>
                      <th>@lang('registerHistory.member_id')</th>
                      <th>Status</th>
                      <th>Refered By</th>
                      <th>Mobile</th>
                      <th>@lang('registerHistory.package')</th>
                    </tr>
                  </thead>
                  <tbody class="text-center">
                      <?php $i = 1;?>
                    @if (isset($members))
                    <tr>
                      <td>{{$i++}}</td>
                      <td>{{$members->created_at}}</td>
                      <td>{{$members->username}}</td>
                      <td>HH100{{$members->id}}</td>
                      <td>@if ($members->package_id == 1) {{'Inactive'}} @else {{ 'Active' }} @endif</td>
                      <td>{{$members->register_by}}</td>
                      <td>{{$members->phone}}</td>
                      <td>{{$members->package_amount}}</td>
                    </tr>
                    @endif
                    
                    @foreach ($data as $datas)
                    
                    <tr>
                        <td>{{$i++}}</td>
                      <td>{{$datas->created_at}}</td>
                      <td>{{$datas->username}}</td>
                      <td>HH100{{$datas->id}}</td>
                      <td>@if ($datas->package_id == 1) {{'Inactive'}} @else {{ 'Active' }} @endif</td>
                      <td>{{$datas->register_by}}</td>
                      <td>{{$datas->phone}}</td>
                      <td>{{$datas->package_amount}}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </main>
@stop
