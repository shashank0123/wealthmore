<?php 
use App\Models\Member;
$name = Member::where('id',$member->id)->first();
?>
@extends('front.app')

@section('content')
<?php


?>
<!-- Page content -->
<div id="page-content">
    <!-- Dashboard Header -->
    <!-- For an image header add the class 'content-header-media' and an image as in the following example -->
    <div class="content-header content-header-media">
        <div class="header-section">
            <div class="row">
                <!-- Main Title (hidden on small devices for the statistics to fit) -->
                <div class="col-md-4 col-lg-6 hidden-xs hidden-sm">
                    <h1>Welcome <strong>{{strtoupper($name->username)}}</strong><br><small>You Look Awesome!</small></h1>
                </div>
                <!-- END Main Title -->

                <!-- Top Stats -->
                <div class="col-md-8 col-lg-6">
                    <div class="row text-center">
                        <div class="col-xs-4 col-sm-3">
                            <h2 class="animation-hatch">
                                RS<strong>{{$sum}}</strong><br>
                                <small><i class="fa fa-thumbs-o-up"></i> Earning</small>
                            </h2>
                        </div>
                        <div class="col-xs-4 col-sm-3">
                            <h2 class="animation-hatch">
                                <strong>{{$total_children}}</strong><br>
                                <small><i class="fa fa-heart-o"></i> Members</small>
                            </h2>
                        </div>
                        {{-- <div class="col-xs-4 col-sm-3">
                            <h2 class="animation-hatch">
                                <strong>101</strong><br>
                                <small><i class="fa fa-calendar-o"></i> Events</small>
                            </h2>
                        </div> --}}
                        <!-- We hide the last stat to fit the other 3 on small devices -->
                        {{-- <div class="col-sm-3 hidden-xs">
                            <h2 class="animation-hatch">
                                <strong>27&deg; C</strong><br>
                                <small><i class="fa fa-map-marker"></i> Sydney</small>
                            </h2>
                        </div> --}}
                    </div>
                </div>
                <!-- END Top Stats -->
            </div>
        </div>
        <!-- For best results use an image with a resolution of 2560x248 pixels (You can also use a blurred image with ratio 10:1 - eg: 1000x100 pixels - it will adjust and look great!) -->
        <img src="/img/placeholders/headers/dashboard_header.jpg" alt="header image" class="animation-pulseSlow">
    </div>
    <!-- END Dashboard Header -->

    <!-- Mini Top Stats Row -->

    <div class="row">
        <div class="col-sm-6">
            <!-- Widget -->
            <a href="page_widgets_stats.html" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background animation-fadeIn">
                        <i class="gi gi-crown"></i>
                    </div>
                    <div class="pull-right">
                        <!-- Jquery Sparkline (initialized in js/pages/index.js), for more examples you can check out http://omnipotent.net/jquery.sparkline/#s-about -->
                        <span id="mini-chart-brand"></span>
                    </div>
                    <h3 class="widget-content animation-pullDown visible-lg">
                        My <strong>Rank</strong>
                        <small>Popularity over time</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
    </div>
    {{-- <div class="row">
        <div class="col-sm-6 col-lg-3">
            <!-- Widget -->
            <a href="page_ready_article.php" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-autumn animation-fadeIn">
                        <i class="fa fa-file-text"></i>
                    </div>
                    <h3 class="widget-content text-right animation-pullDown">
                        New <strong>Article</strong><br>
                        <small>Mountain Trip</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
        <div class="col-sm-6 col-lg-3">
            <!-- Widget -->
            <a href="page_comp_charts.php" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-spring animation-fadeIn">
                        <i class="gi gi-usd"></i>
                    </div>
                    <h3 class="widget-content text-right animation-pullDown">
                        + <strong>250%</strong><br>
                        <small>Sales Today</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
        <div class="col-sm-6 col-lg-3">
            <!-- Widget -->
            <a href="page_ready_inbox.php" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-fire animation-fadeIn">
                        <i class="gi gi-envelope"></i>
                    </div>
                    <h3 class="widget-content text-right animation-pullDown">
                        5 <strong>Messages</strong>
                        <small>Support Center</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
        <div class="col-sm-6 col-lg-3">
            <!-- Widget -->
            <a href="page_comp_gallery.php" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-amethyst animation-fadeIn">
                        <i class="gi gi-picture"></i>
                    </div>
                    <h3 class="widget-content text-right animation-pullDown">
                        +30 <strong>Photos</strong>
                        <small>Gallery</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
        <div class="col-sm-6">
            <!-- Widget -->
            <a href="page_comp_charts.php" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background animation-fadeIn">
                        <i class="gi gi-wallet"></i>
                    </div>
                    <div class="pull-right">
                        <!-- Jquery Sparkline (initialized in js/pages/index.js), for more examples you can check out http://omnipotent.net/jquery.sparkline/#s-about -->
                        <span id="mini-chart-sales"></span>
                    </div>
                    <h3 class="widget-content animation-pullDown visible-lg">
                        Latest <strong>Sales</strong>
                        <small>Per hour</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
        <div class="col-sm-6">
            <!-- Widget -->
            <a href="page_widgets_stats.php" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background animation-fadeIn">
                        <i class="gi gi-crown"></i>
                    </div>
                    <div class="pull-right">
                        <!-- Jquery Sparkline (initialized in js/pages/index.js), for more examples you can check out http://omnipotent.net/jquery.sparkline/#s-about -->
                        <span id="mini-chart-brand"></span>
                    </div>
                    <h3 class="widget-content animation-pullDown visible-lg">
                        Our <strong>Brand</strong>
                        <small>Popularity over time</small>
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
    </div> --}}
    <!-- END Mini Top Stats Row -->

    <!-- Widgets Row -->
    <div class="row">

        <div class="col-md-6">
            <!-- Your Plan Widget -->
            <div class="widget">
                <div class="widget-extra themed-background-dark">
                    <div class="widget-options">
                        <div class="btn-group btn-group-xs">
                            <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="Edit Widget"><i class="fa fa-pencil"></i></a>
                            <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="Quick Settings"><i class="fa fa-cog"></i></a>
                        </div>
                    </div>
                    <h3 class="widget-content-light">
                        Your <strong>VIP Plan</strong>
                        <small><a href="#"><strong>Upgrade</strong></a></small>
                    </h3>
                </div>
                <div class="widget-extra-full">
                    <div class="row text-center">
                        <div class="col-xs-6 col-lg-3">
                            <h3>
                                <strong>{{ (integer)$package->package_amount}}</strong> <small>{{-- /50 --}}</small><br>
                                <small><i class="fa fa-folder-open-o"></i> Package</small>
                            </h3>
                        </div>
                        <div class="col-xs-6 col-lg-3">
                            <h3>
                                <strong>{{-- 25 --}}</strong> <small>{{explode(' ',$member->created_at)[0]}}</small><br>
                                <small><i class="fa fa-hdd-o"></i> Joining Date</small>
                            </h3>
                        </div>
                        <div class="col-xs-6 col-lg-3">
                            <h3>
                                <strong>{{$package->package_name}}</strong> <small>{{-- /1k --}}</small><br>
                                <small><i class="fa fa-building-o"></i> Children</small>
                            </h3>
                        </div>
                        <div class="col-xs-6 col-lg-3">
                            <h3>
                                <strong>{{$sum}}</strong> <small></small><br>
                                <small><i class="fa fa-envelope-o"></i> Earnings</small>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Your Plan Widget -->


            <!-- Charts Widget -->
           {{--  <div class="widget">
                <div class="widget-advanced widget-advanced-alt">
                    <!-- Widget Header -->
                    <div class="widget-header text-center themed-background">
                        <h3 class="widget-content-light text-left pull-left animation-pullDown">
                            <strong>Sales</strong> &amp; <strong>Earnings</strong><br>
                            <small>Last Year</small>
                        </h3>
                        <!-- Flot Charts (initialized in js/pages/index.js), for more examples you can check out http://www.flotcharts.org/ -->
                        <div id="dash-widget-chart" class="chart"></div>
                    </div>
                    <!-- END Widget Header -->

                    <!-- Widget Main -->
                    <div class="widget-main">
                        <div class="row text-center">
                            <div class="col-xs-4">
                                <h3 class="animation-hatch"><strong>7.500</strong><br><small>Products</small></h3>
                            </div>
                            <div class="col-xs-4">
                                <h3 class="animation-hatch"><strong>10.970</strong><br><small>Sales</small></h3>
                            </div>
                            <div class="col-xs-4">
                                <h3 class="animation-hatch">$<strong>31.230</strong><br><small>Earnings</small></h3>
                            </div>
                        </div>
                    </div>
                    <!-- END Widget Main -->
                </div>
            </div> --}}
            <!-- END Charts Widget -->



            



            <!-- Weather Widget -->
            {{-- <div class="widget">
                <div class="widget-advanced widget-advanced-alt">
                    <!-- Widget Header -->
                    <div class="widget-header text-left">
                        <!-- For best results use an image with at least 150 pixels in height (with the width relative to how big your widget will be!) - Here I'm using a 1200x150 pixels image -->
                        <img src="/img/placeholders/headers/widget5_header.jpg" alt="background" class="widget-background animation-pulseSlow">
                        <h3 class="widget-content widget-content-image widget-content-light clearfix">
                            <span class="widget-icon pull-right">
                                <i class="fa fa-sun-o animation-pulse"></i>
                            </span>
                            Weather <strong>Station</strong><br>
                            <small><i class="fa fa-location-arrow"></i> The Mountain</small>
                        </h3>
                    </div>
                    <!-- END Widget Header -->

                    <!-- Widget Main -->
                    <div class="widget-main">
                        <div class="row text-center">
                            <div class="col-xs-6 col-lg-3">
                                <h3>
                                    <strong>10&deg;</strong> <small>C</small><br>
                                    <small>Sunny</small>
                                </h3>
                            </div>
                            <div class="col-xs-6 col-lg-3">
                                <h3>
                                    <strong>80</strong> <small>%</small><br>
                                    <small>Humidity</small>
                                </h3>
                            </div>
                            <div class="col-xs-6 col-lg-3">
                                <h3>
                                    <strong>60</strong> <small>km/h</small><br>
                                    <small>Wind</small>
                                </h3>
                            </div>
                            <div class="col-xs-6 col-lg-3">
                                <h3>
                                    <strong>5</strong> <small>km</small><br>
                                    <small>Visibility</small>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!-- END Widget Main -->
                </div>
            </div> --}}
            <!-- END Weather Widget-->

            <!-- Advanced Gallery Widget -->
            {{-- <div class="widget">
                <div class="widget-advanced">
                    <!-- Widget Header -->
                    <div class="widget-header text-center themed-background-dark">
                        <h3 class="widget-content-light clearfix">
                            Awesome <strong>Gallery</strong><br>
                            <small>4 Photos</small>
                        </h3>
                    </div>
                    <!-- END Widget Header -->

                    <!-- Widget Main -->
                    <div class="widget-main">
                        <a href="page_comp_gallery.php" class="widget-image-container">
                            <span class="widget-icon themed-background"><i class="gi gi-picture"></i></span>
                        </a>
                        <div class="gallery gallery-widget" data-toggle="lightbox-gallery">
                            <div class="row">
                                <div class="col-xs-6 col-sm-3">
                                    <a href="/img/placeholders/photos/photo15.jpg" class="gallery-link" title="Image Info">
                                        <img src="/img/placeholders/photos/photo15.jpg" alt="image">
                                    </a>
                                </div>
                                <div class="col-xs-6 col-sm-3">
                                    <a href="/img/placeholders/photos/photo5.jpg" class="gallery-link" title="Image Info">
                                        <img src="/img/placeholders/photos/photo5.jpg" alt="image">
                                    </a>
                                </div>
                                <div class="col-xs-6 col-sm-3">
                                    <a href="/img/placeholders/photos/photo6.jpg" class="gallery-link" title="Image Info">
                                        <img src="/img/placeholders/photos/photo6.jpg" alt="image">
                                    </a>
                                </div>
                                <div class="col-xs-6 col-sm-3">
                                    <a href="/img/placeholders/photos/photo13.jpg" class="gallery-link" title="Image Info">
                                        <img src="/img/placeholders/photos/photo13.jpg" alt="image">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Widget Main -->
                </div>
            </div> --}}
            <!-- END Advanced Gallery Widget -->
        </div>
        <div class="col-md-6">
            <!-- Timeline Widget -->
            <div class="widget">
                <div class="widget-extra themed-background-dark">
                    <div class="widget-options">
                        <div class="btn-group btn-group-xs">
                            <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="Edit Widget"><i class="fa fa-pencil"></i></a>
                            <a href="javascript:void(0)" class="btn btn-default" data-toggle="tooltip" title="Quick Settings"><i class="fa fa-cog"></i></a>
                        </div>
                    </div>
                    <h3 class="widget-content-light">
                        Latest <strong>News</strong>
                        <small><a href="page_ready_timeline.php"><strong>View all</strong></a></small>
                    </h3>
                </div>
                <div class="widget-extra">
                    <!-- Timeline Content -->
                    {{-- <div class="timeline">
                        <ul class="timeline-list">
                            <li class="active">
                                <div class="timeline-icon"><i class="gi gi-airplane"></i></div>
                                <div class="timeline-time"><small>just now</small></div>
                                <div class="timeline-content">
                                    <p class="push-bit"><a href="page_ready_user_profile.php"><strong>Jordan Carter</strong></a></p>
                                    <p class="push-bit">The trip was an amazing and a life changing experience!!</p>
                                    <p class="push-bit"><a href="page_ready_article.php" class="btn btn-xs btn-primary"><i class="fa fa-file"></i> Read the article</a></p>
                                    <div class="row push">
                                        <div class="col-sm-6 col-md-4">
                                            <a href="/img/placeholders/photos/photo1.jpg" data-toggle="lightbox-image">
                                                <img src="/img/placeholders/photos/photo1.jpg" alt="image">
                                            </a>
                                        </div>
                                        <div class="col-sm-6 col-md-4">
                                            <a href="/img/placeholders/photos/photo22.jpg" data-toggle="lightbox-image">
                                                <img src="/img/placeholders/photos/photo22.jpg" alt="image">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="active">
                                <div class="timeline-icon themed-background-fire themed-border-fire"><i class="fa fa-file-text"></i></div>
                                <div class="timeline-time"><small>5 min ago</small></div>
                                <div class="timeline-content">
                                    <p class="push-bit"><a href="page_ready_user_profile.php"><strong>Administrator</strong></a></p>
                                    <strong>Free courses</strong> for all our customers at A1 Conference Room - 9:00 <strong>am</strong> tomorrow!
                                </div>
                            </li>
                            <li class="active">
                                <div class="timeline-icon"><i class="gi gi-drink"></i></div>
                                <div class="timeline-time"><small>3 hours ago</small></div>
                                <div class="timeline-content">
                                    <p class="push-bit"><a href="page_ready_user_profile.php"><strong>Ella Winter</strong></a></p>
                                    <p class="push-bit"><strong>Happy Hour!</strong> Free drinks at <a href="javascript:void(0)">Cafe-Bar</a> all day long!</p>
                                    <div id="gmap-timeline" class="gmap"></div>
                                </div>
                            </li>
                            <li class="active">
                                <div class="timeline-icon"><i class="fa fa-cutlery"></i></div>
                                <div class="timeline-time"><small>yesterday</small></div>
                                <div class="timeline-content">
                                    <p class="push-bit"><a href="page_ready_user_profile.php"><strong>Patricia Woods</strong></a></p>
                                    <p class="push-bit">Today I had the lunch of my life! It was delicious!</p>
                                    <div class="row push">
                                        <div class="col-sm-6 col-md-4">
                                            <a href="/img/placeholders/photos/photo23.jpg" data-toggle="lightbox-image">
                                                <img src="/img/placeholders/photos/photo23.jpg" alt="image">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="active">
                                <div class="timeline-icon themed-background-fire themed-border-fire"><i class="fa fa-smile-o"></i></div>
                                <div class="timeline-time"><small>2 days ago</small></div>
                                <div class="timeline-content">
                                    <p class="push-bit"><a href="page_ready_user_profile.php"><strong>Administrator</strong></a></p>
                                    To thank you all for your support we would like to let you know that you will receive free feature updates for life! You are awesome!
                                </div>
                            </li>
                            <li class="active">
                                <div class="timeline-icon"><i class="fa fa-pencil"></i></div>
                                <div class="timeline-time"><small>1 week ago</small></div>
                                <div class="timeline-content">
                                    <p class="push-bit"><a href="page_ready_user_profile.php"><strong>Nicole Ward</strong></a></p>
                                    <p class="push-bit">Consectetur adipiscing elit. Maecenas ultrices, justo vel imperdiet gravida, urna ligula hendrerit nibh, ac cursus nibh sapien in purus. Mauris tincidunt tincidunt turpis in porta. Integer fermentum tincidunt auctor. Vestibulum ullamcorper, odio sed rhoncus imperdiet, enim elit sollicitudin orci, eget dictum leo mi nec lectus. Nam commodo turpis id lectus scelerisque vulputate.</p>
                                    Integer sed dolor erat. Fusce erat ipsum, varius vel euismod sed, tristique et lectus? Etiam egestas fringilla enim, id convallis lectus laoreet at. Fusce purus nisi, gravida sed consectetur ut, interdum quis nisi. Quisque egestas nisl id lectus facilisis scelerisque? Proin rhoncus dui at ligula vestibulum ut facilisis ante sodales! Suspendisse potenti. Aliquam tincidunt sollicitudin sem nec ultrices. Sed at mi velit. Ut egestas tempor est, in cursus enim venenatis eget! Nulla quis ligula ipsum.
                                </div>
                            </li>
                            <li class="text-center">
                                <a href="javascript:void(0)" class="btn btn-xs btn-default">View more..</a>
                            </li>
                        </ul>
                    </div> --}}
                    <!-- END Timeline Content -->
                </div>
            </div>
            <!-- END Timeline Widget -->
        </div>
    </div>
    <!-- END Widgets Row -->

<style>

#jbv1,#rbv1,#business1,#jbv-rewards,#rbv-rewards,#bonus,#repjbv,#reprbv,#repjbvday,#reprbvday,#totaljbv,#totalrbv{display: none;}
.heading { background-color: #394263; border: 1px solid #fff; margin-bottom: 1px; }
.heading h4, .heading h5 { color: #fff; padding:1px 5%  }
thead { background-color: #394263; }
thead th { color: #fff; font-size: 16px !important;}
table{ margin: 0 1%; width: 98% !important }
td { border-right: 1px solid #000; }
.bgcolorfff { background-color: #fff; color: #000 !important; }

</style>
    {{-- Tables --}}

            {{-- JBV Qualification  --}}
            <div class="row">
                <div class="heading" style="margin-left: 1%; margin-right: 1%">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6"><h4>JBV Qualification</h4></div>
                        <div class="col-sm-6 col-xs-6" style="text-align: right;"><h5 onclick="showDiv('jbv1')">Click here to view >></h5></div>
                    </div>
                    
                </div>
                <div id="jbv1">
                    <table class="table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Slab1</th>
                                <th>Slab2</th>
                                <th>Slab3</th>
                                <th>Slab4</th>
                                <th>Slab5</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Required JBV</td>
                                <td>5000</td>
                                <td>10000</td>
                                <td>20000</td>
                                <td>40000</td>
                                <td>2500</td>
                            </tr>
                            <tr>
                                <td>Qualified</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            

            {{-- JBV Qualification --}}
            <div class="row">
                <div class="heading" style="margin-left: 1%; margin-right: 1%">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6"><h4>RBV Qualification</h4></div>
                        <div class="col-sm-6 col-xs-6" style="text-align: right;"><h5 onclick="showDiv('rbv1')">Click here to view >></h5></div>
                    </div>
                    
                </div>
                <div id="rbv1">
                    <table class="table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Slab1</th>
                                <th>Slab2</th>
                                <th>Slab3</th>
                                <th>Slab4</th>
                                <th>Slab5</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Required RBV</td>
                                <td>5000</td>
                                <td>10000</td>
                                <td>20000</td>
                                <td>40000</td>
                                <td>2500</td>
                            </tr>
                            <tr>
                                <td>Qualified</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            

            {{-- Business Qualification --}}
            <div class="row">
                <div class="heading" style="margin-left: 1%; margin-right: 1%">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6"><h4>Business Qualification</h4></div>
                        <div class="col-sm-6 col-xs-6" style="text-align: right;"><h5 onclick="showDiv('business1')">Click here to view >></h5></div>
                    </div>
                    
                </div>
                <div id="business1">
                    <table class="table">
                        <thead>
                            <tr>
                                <th style="background-color: #394263">Income Type</th>
                                <th class="bgcolorfff">Performance Bonus</th>
                                <th class="bgcolorfff">LDB/SLDB/MLDB/Royalty</th>
                                <th class="bgcolorfff">Retailer Bonus</th>
                                <th class="bgcolorfff">Premium Retailer Bonus</th>
                                <th class="bgcolorfff">Remaining Days</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="background-color: #394263; color: #fff; font-size: 16px">Required Self Purchase</td>
                                <td>250.00</td>
                                <td>1000.00</td>
                                <td>5000.00</td>
                                <td>10000.00</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td style="background-color: #394263; color: #fff; font-size: 16px">Qualified</td>
                                <td>No</td>
                                <td>No</td>
                                <td>No</td>
                                <td>No</td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            {{-- JBV Reward --}}
            <div class="row">
                <div class="heading" style="margin-left: 1%; margin-right: 1%">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6"><h4>JBV Reward</h4></div>
                        <div class="col-sm-6 col-xs-6" style="text-align: right;"><h5 onclick="showDiv('jbv-rewards')">Click here to view >></h5></div>
                    </div>
                    
                </div>
                <div id="jbv-rewards">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>ReqPoints</th>
                                <th>ActPoints</th>
                                <th>DaysLeft</th>
                                <th>RewardAmt</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                               
                                <td>00</td>
                                <td>00</td>
                                <td>00</td>
                                <td>00</td>
                                
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
            </div>

            {{-- RBV Rewards --}}
            <div class="row">
                <div class="heading" style="margin-left: 1%; margin-right: 1%">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6"><h4>RBV Reward</h4></div>
                        <div class="col-sm-6 col-xs-6" style="text-align: right;"><h5 onclick="showDiv('rbv-rewards')">Click here to view >></h5></div>
                    </div>
                    
                </div>
                <div id="rbv-rewards">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>ReqPoints</th>
                                <th>ActPoints</th>
                                <th>DaysLeft</th>
                                <th>RewardAmt</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                               
                                <td>00</td>
                                <td>00</td>
                                <td>00</td>
                                <td>00</td>
                                
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
            </div>

            {{-- Bonus --}}
            <div class="row">
                <div class="heading" style="margin-left: 1%; margin-right: 1%">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6"><h4>Bonus</h4></div>
                        <div class="col-sm-6 col-xs-6" style="text-align: right;"><h5 onclick="showDiv('bonus')">Click here to view >></h5></div>
                    </div>
                    
                </div>
                <div id="bonus">
                    <table class="table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Main Leg CF Points</th>
                                <th>Other Leg CF Points</th>
                                <th>Main Leg Points</th>
                                <th>Other Leg Points</th>
                                <th>From Date</th>
                                <th>To Date</th>                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr>                               
                                <td>Current Month</td>
                                <td>00</td>
                                <td>00</td>
                                <td>00</td>
                                <td>00</td>
                                <td>00</td>
                                <td>00</td>                                
                            </tr>                            
                        </tbody>
                    </table>
                </div>
            </div>

            {{-- Repurchase JBV --}}
            <div class="row">
                <div class="heading" style="margin-left: 1%; margin-right: 1%">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6"><h4>Repurchase JBV</h4></div>
                        <div class="col-sm-6 col-xs-6" style="text-align: right;"><h5 onclick="showDiv('repjbv')">Click here to view >></h5></div>
                    </div>
                    
                </div>
                <div id="repjbv">
                    <table class="table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Total Own JBVs</th>
                                <th>Total Main Leg JBVs</th>
                                <th>Total Other Leg JBVs</th>                          
                            </tr>
                        </thead>
                        <tbody>
                            <tr>                               
                                <td>Current Month</td>
                                <td>00</td>
                                <td>00</td>
                                <td>00</td>                                          
                            </tr>                            
                        </tbody>
                    </table>
                </div>
            </div>

            {{-- Repurchase JBV of the day --}}
            <div class="row">
                <div class="heading" style="margin-left: 1%; margin-right: 1%">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6"><h4>Repurchase JBV of the day</h4></div>
                        <div class="col-sm-6 col-xs-6" style="text-align: right;"><h5 onclick="showDiv('repjbvday')">Click here to view >></h5></div>
                    </div>
                    
                </div>
                <div id="repjbvday">
                    <table class="table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Total Own JBVs</th>
                                <th>Total Main Leg JBVs</th>
                                <th>Total Other Leg JBVs</th>                          
                            </tr>
                        </thead>
                        <tbody>
                            <tr>                               
                                <td>Repurchase JBV Today</td>
                                <td>00</td>
                                <td>00</td>
                                <td>00</td>                                          
                            </tr>                            
                        </tbody>
                    </table>
                </div>
            </div>

            {{-- Total JBV --}}
            <div class="row">
                <div class="heading" style="margin-left: 1%; margin-right: 1%">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6"><h4>Total JBV</h4></div>
                        <div class="col-sm-6 col-xs-6" style="text-align: right;"><h5 onclick="showDiv('totaljbv')">Click here to view >></h5></div>
                    </div>
                    
                </div>
                <div id="totaljbv">
                    <table class="table">
                        <thead>
                            <tr>
                                <th></th>
                                
                                <th>Total Main Leg JBVs</th>
                                <th>Total Other Leg JBVs</th>                          
                            </tr>
                        </thead>
                        <tbody>
                            <tr>                               
                                <td>Current JBV</td>
                            
                                <td>00</td>
                                <td>00</td>                                          
                            </tr>                            
                        </tbody>
                    </table>
                </div>
            </div>

            {{-- Repurchase RBV --}}
            <div class="row">
                <div class="heading" style="margin-left: 1%; margin-right: 1%">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6"><h4>Repurchase RBV</h4></div>
                        <div class="col-sm-6 col-xs-6" style="text-align: right;"><h5 onclick="showDiv('reprbv')">Click here to view >></h5></div>
                    </div>
                    
                </div>
                <div id="reprbv">
                    <table class="table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Total Own RBVs</th>
                                <th>Total Main Leg RBVs</th>
                                <th>Total Other Leg RBVs</th>                          
                            </tr>
                        </thead>
                        <tbody>
                            <tr>                               
                                <td>Current Month</td>
                                <td>00</td>
                                <td>00</td>
                                <td>00</td>                                          
                            </tr>                            
                        </tbody>
                    </table>
                </div>
            </div>

            {{-- Repurchase RBV of the day --}}
            <div class="row">
                <div class="heading" style="margin-left: 1%; margin-right: 1%">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6"><h4>Repurchase RBV Of The Day</h4></div>
                        <div class="col-sm-6 col-xs-6" style="text-align: right;"><h5 onclick="showDiv('reprbvday')">Click here to view >></h5></div>
                    </div>
                    
                </div>
                <div id="reprbvday">
                    <table class="table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Total Own RBVs</th>
                                <th>Total Main Leg RBVs</th>
                                <th>Total Other Leg RBVs</th>                          
                            </tr>
                        </thead>
                        <tbody>
                            <tr>                               
                                <td>Repurchase RBV Today</td>
                                <td>00</td>
                                <td>00</td>
                                <td>00</td>                                          
                            </tr>                            
                        </tbody>
                    </table>
                </div>
            </div>

            {{-- Total RBV --}}
            <div class="row">
                <div class="heading" style="margin-left: 1%; margin-right: 1%">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6"><h4>Total RBV</h4></div>
                        <div class="col-sm-6 col-xs-6" style="text-align: right;"><h5 onclick="showDiv('totalrbv')">Click here to view >></h5></div>
                    </div>
                    
                </div>
                <div id="totalrbv">
                    <table class="table">
                        <thead>
                            <tr>
                                <th></th>
                                
                                <th>Total Main Leg RBVs</th>
                                <th>Total Other Leg RBVs</th>                          
                            </tr>
                        </thead>
                        <tbody>
                            <tr>                               
                                <td>Current RBV</td>
                            
                                <td>00</td>
                                <td>00</td>                                          
                            </tr>                            
                        </tbody>
                    </table>
                </div>
            </div>
            {{-- End Tables --}}

</div>
<!-- END Page Content -->
<script >
    function showDiv(data){
        if(data == 'jbv1'){
            $('#jbv1').toggle();
        }
        if(data == 'rbv1'){
            $('#rbv1').toggle();
        }
        if(data == 'business1'){
            $('#business1').toggle();
        }
        if(data == 'jbv-rewards'){
            $('#jbv-rewards').toggle();
        }
        if(data == 'rbv-rewards'){
            $('#rbv-rewards').toggle();
        }if(data == 'bonus'){
            $('#bonus').toggle();
        }if(data == 'repjbv'){
            $('#repjbv').toggle();
        }if(data == 'reprbv'){
            $('#reprbv').toggle();
        }if(data == 'repjbvday'){
            $('#repjbvday').toggle();
        }if(data == 'reprbvday'){
            $('#reprbvday').toggle();
        }if(data == 'totaljbv'){
            $('#totaljbv').toggle();
        }if(data == 'totalrbv'){
            $('#totalrbv').toggle();
        }
    }
</script>
@endsection