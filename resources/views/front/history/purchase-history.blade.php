<?php 
use App\Product;
use App\Order;
use App\OrderItem;
$count = 1; 
$user_order = Order::where('user_id',$user->id)->orderBy('created_at','DESC')->get();
?>
<script src="jquery-3.4.1.min.js"></script>
@extends('front.app')

@section('title')
@lang('register.title') | {{ config('app.name') }}
@stop
<style>
  #showChoosen { display: none; }
  #showAll tr { font-size: 12px }
  #heading th { font-size: 14px }
</style>
@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">@lang('breadcrumbs.front')</a></li>
  <li><a href="{{ route('home', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.dashboard')</a></li>
  <li><a href="{{ route('network.binary', ['lang' => \App::getLocale()]) }}">@lang('breadcrumbs.hierarchy')</a></li>
  <li class="active">@lang('breadcrumbs.register')</li>
</ul>
@stop


@section('content')
<main>
  {{-- @include('front.include.sidebar') --}}
  <div class="main-container container">
    {{-- @include('front.include.header') --}}
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section>
        <input type="hidden" id="user_id" value="{{$member->id}}">
        <div class="page-header">
          <div class="row">
            <div class="col-md-6">
              <h1><i class="fa fa-cart-arrow-down"></i> @lang('sidebar.purchaseHistoryTitle')</h1>
              {{--  <p class="lead">@lang('register.notice')</p> --}}
            </div>
            <div class="col-md-6">
              <form action="{{-- /search/order --}}" method="POST" class="searchform" >
                <input type="text" class="form-set" name="product_keyword" style="font-size: 16px;" placeholder="Search for something">
                <input type="button" name="search" onclick="searchOrder();" class="btn btn-primary" style="padding: 1px 5px" value="Search">
              </form>
            </div>
          </div>          
        </div>

        <div class="row m-b-40">
          <div class="col-md-12">
            <table class="table">

              <thead id="heading">
                <tr>
                  <th>S. No.</th>
                  <th>Order Detail </th>
                  <th>JBV</th>
                  <th>RBV</th>
                  <th>Payment Method</th>
                  <th>Transaction Id</th>
                  <th>Total Payable Amount</th>
                  <th>Order Status</th>
                  <th>Action</th>
                  <th>Date</th>
                </tr>
              </thead>

              <tbody id="showAll">
                @if($user_order != null)
                @foreach($user_order as $order)
                <?php $products = OrderItem::where('created_at',$order->created_at)->get(); ?>

                <tr>
                  <td><?php echo $count++.". " ?></td>
                    @foreach($products as $product)
                  <td style="width: 20%">

                    <?php $name=Product::where('id',$product->item_id)->first();

                    ?>
                    <?php echo $name->name.". " ?>

                  </td>
                  <td>{{$name->jbv}}</td>
                  <td>{{$name->rbv}}</td>

                    @endforeach 
                  <td>{{$order->payment_method}}</td>
                  <td>{{$order->transaction_id}}</td>
                  <td>Rs. {{$order->price}}</td>
                  <td>{{$order->status}}</td>
                  <td style="width: 20%">{{-- <form method="POST"> --}}<input type="button" name="search" onclick="returnOrder({{$order->id}},'{{$order->transaction_id}}','return');" class="btn btn-primary" style="padding: 1px 5px" value="Return">{{-- </form> --}}
                    {{-- <form method="POST"> --}}<input type="button" name="search" onclick="cancelOrder({{$order->id}},'{{$order->transaction_id}}','cancel');" class="btn btn-primary" style="padding: 1px 5px" value="Cancel">{{-- </form> --}}</td>
                    <td>{{$order->created_at}}</td>
                  </tr>
                  @endforeach
                  @endif
                </tbody>
              </table>
            </div>           
          </div>
        </section>
      </div>
    </div>
  </main>


  <script>
    function searchOrder(){
      alert('hii');
    }

    function returnOrder(id,txnID,action){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      var userID = $('#user_id').val();
      var request = $.ajax({
        url: '/en/action-order',
        type: 'GET',
        dataType: 'JSON',
        data: {_token: CSRF_TOKEN, orderId: id, txnId: txnID, orderAction: action, userId: userID},    
       }); 

      request.done(function(data) {
        if (data.msg == 'Your request to return the order is sent'){          
          Swal(data.msg);
          setTimeout(function(){ window.location.href = "/en/purchase-history"; }, 3000);
        }
        else
          Swal('failure');                  
      });

      request.fail(function(data) {
        Swal('Something Went Wrong');        
      });
    }

    function cancelOrder(id,txnID,action){
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      var userID = $('#user_id').val();
      var request = $.ajax({
        url: '/en/action-order',
        type: 'GET',
        dataType: 'JSON',
        data: {_token: CSRF_TOKEN, orderId: id, txnId: txnID, orderAction: action, userId: userID},    
       }); 

      request.done(function(data) {
        if (data.msg == 'Your request to cancel the order is sent'){          
          Swal(data.msg);
          setTimeout(function(){ window.location.href = "/en/purchase-history"; }, 3000);
        }
        else
          Swal('failure');                  
      });

      request.fail(function(data) {
        Swal('Something Went Wrong');        
      });
    }
  </script>
  @endsection
