<?php 
use App\Models\MemberDetail;
use App\Models\Member;
if (isset($member)){
  $profile = MemberDetail::where('member_id',$member->id)->first();
  $name = Member::where('id',$member->id)->first();
}
else{
    $profile = [];
}
?>
<style>
    #clickBro { display: none; }
    #file { display: none; }
</style>
<!-- Main Sidebar -->
<div id="sidebar" style="min-height: 750px !important; height: 100% !important">
    <!-- Wrapper for scrolling functionality -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <div class="sidebar-content">
            <!-- Brand -->
            <a href="index.html" class="sidebar-brand">
                <i class="gi gi-flash"></i><span class="sidebar-nav-mini-hide"><strong>Wealthmore</strong>India</span>
            </a>
            <!-- END Brand -->
            <form method="post" action="/upload-image" enctype="multipart/form-data" id="upload_image">
                <!-- User Info -->
                <div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
                    <div class="sidebar-user-avatar">

                      <input type="hidden" name="member_id" id="uid" value="<?php if (isset($member)) { echo $member->id;}?>">
                      @if($profile->profimage != null)
                      <span id="uploaded_image">
                        <img src="/assetsss/images/AdminProduct/{{$profile->profimage}}" width="150px" height="150px">
                    </span>
                    @else
                    <span id="uploaded_image"></span>
                    @endif
                    <br>
                    {{-- <img src="{{ asset('images/product/fashion-12.jpg') }}" style="width:100px;height:100px;border-radius:50%;margin: auto;display: block; margin-bottom:15px"> --}}
                    

                </div>
                <div class="sidebar-user-name">@if (isset($name->username)){{strtoupper($name->username)}}@endif</div>
                <div class="sidebar-user-links">
                    <a href="#" data-toggle="tooltip" data-placement="bottom" title="Profile"><i class="gi gi-user"></i></a>
                    <a href="#" data-toggle="tooltip" data-placement="bottom" title="Messages"><i class="gi gi-envelope"></i></a>
                    <!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
                    <a href="javascript:void(0)" class="enable-tooltip" data-placement="bottom" title="Settings" onclick="$('#modal-user-settings').modal('show');"><i class="gi gi-cogwheel"></i></a>
                    <a href="/logout" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit"></i></a>
                </div>
                <label for="file" style="cursor: pointer;margin-left: 0px; margin-top: 5px; font-size: 13px;text-decoration: underline; font-weight: normal;">C<span style="text-transform: lowercase !important;">hange</span> P<span style="text-transform: lowercase !important;">icture</span></label>
                <input type="file" name="file" id="file" onchange='submitMe()'/>
                <input type="submit" name="submit" id="clickBro" />
            </form>
        </div>
        <!-- END User Info -->

        <!-- Theme Colors -->
        <!-- Change Color Theme functionality can be found in js/app.js - templateOptions() -->
        {{-- <ul class="sidebar-section sidebar-themes clearfix sidebar-nav-mini-hide"> --}}
                                <!-- You can also add the default color theme
                                <li class="active">
                                    <a href="javascript:void(0)" class="themed-background-dark-default themed-border-default" data-theme="default" data-toggle="tooltip" title="Default Blue"></a>
                                </li>
                            -->
                            {{-- <li>
                                <a href="javascript:void(0)" class="themed-background-dark-night themed-border-night" data-theme="css/themes/night.css" data-toggle="tooltip" title="Night"></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="themed-background-dark-amethyst themed-border-amethyst" data-theme="css/themes/amethyst.css" data-toggle="tooltip" title="Amethyst"></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="themed-background-dark-modern themed-border-modern" data-theme="css/themes/modern.css" data-toggle="tooltip" title="Modern"></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="themed-background-dark-autumn themed-border-autumn" data-theme="css/themes/autumn.css" data-toggle="tooltip" title="Autumn"></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="themed-background-dark-flatie themed-border-flatie" data-theme="css/themes/flatie.css" data-toggle="tooltip" title="Flatie"></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="themed-background-dark-spring themed-border-spring" data-theme="css/themes/spring.css" data-toggle="tooltip" title="Spring"></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="themed-background-dark-fancy themed-border-fancy" data-theme="css/themes/fancy.css" data-toggle="tooltip" title="Fancy"></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="themed-background-dark-fire themed-border-fire" data-theme="css/themes/fire.css" data-toggle="tooltip" title="Fire"></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="themed-background-dark-coral themed-border-coral" data-theme="css/themes/coral.css" data-toggle="tooltip" title="Coral"></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="themed-background-dark-lake themed-border-lake" data-theme="css/themes/lake.css" data-toggle="tooltip" title="Lake"></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="themed-background-dark-forest themed-border-forest" data-theme="css/themes/forest.css" data-toggle="tooltip" title="Forest"></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="themed-background-dark-waterlily themed-border-waterlily" data-theme="css/themes/waterlily.css" data-toggle="tooltip" title="Waterlily"></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="themed-background-dark-emerald themed-border-emerald" data-theme="css/themes/emerald.css" data-toggle="tooltip" title="Emerald"></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="themed-background-dark-blackberry themed-border-blackberry" data-theme="css/themes/blackberry.css" data-toggle="tooltip" title="Blackberry"></a>
                            </li>
                        </ul> --}}
                        <!-- END Theme Colors -->

                        <!-- Sidebar Navigation -->
                        <ul class="sidebar-nav">
                            <li>
                                <a href="{{ route('home', ['lang' => \App::getLocale()]) }}" class=" active"><i class="gi gi-stopwatch sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">@lang('sidebar.home')</span></a>
                            </li>
                                {{-- <li>
                                    <a href="index2.html"><i class="gi gi-leaf sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard 2</span></a>
                                </li> --}}
                                {{-- <li>
                                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-shopping_cart sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">eCommerce</span></a>
                                    <ul>
                                        <li>
                                            <a href="page_ecom_dashboard.html">Dashboard</a>
                                        </li>
                                        <li>
                                            <a href="page_ecom_orders.html">Orders</a>
                                        </li>
                                        <li>
                                            <a href="page_ecom_order_view.html">Order View</a>
                                        </li>
                                        <li>
                                            <a href="page_ecom_products.html">Products</a>
                                        </li>
                                        <li>
                                            <a href="page_ecom_product_edit.html">Product Edit</a>
                                        </li>
                                        <li>
                                            <a href="page_ecom_customer_view.html">Customer View</a>
                                        </li>
                                    </ul>
                                </li> --}}
                                {{-- <li class="sidebar-header">
                                    <span class="sidebar-header-options clearfix"><a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a><a href="javascript:void(0)" data-toggle="tooltip" title="Create the most amazing pages with the widget kit!"><i class="gi gi-lightbulb"></i></a></span>
                                    <span class="sidebar-header-title">Widget Kit</span>
                                </li> --}}
                                {{-- <li>
                                    <a href="page_widgets_stats.html"><i class="gi gi-charts sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Statistics</span></a>
                                </li>
                                <li>
                                    <a href="page_widgets_social.html"><i class="gi gi-share_alt sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Social</span></a>
                                </li>
                                <li>
                                    <a href="page_widgets_media.html"><i class="gi gi-film sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Media</span></a>
                                </li>
                                <li>
                                    <a href="page_widgets_links.html"><i class="gi gi-link sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Links</span></a>
                                </li>
                                <li class="sidebar-header">
                                    <span class="sidebar-header-options clearfix"><a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a></span>
                                    <span class="sidebar-header-title">Menu</span>
                                </li> --}}
                                <li>
                                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-certificate sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">@lang('sidebar.registerTitle')</span></a>
                                    <ul>
                                        <li>
                                            <a href="{{ route('member.register', ['lang' => \App::getLocale()]) }}">@lang('sidebar.registerLink1')</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('member.registerHistory', ['lang' => \App::getLocale()]) }}">@lang('sidebar.registerLink2')</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('member.upgrade', ['lang' => \App::getLocale()]) }}">@lang('sidebar.registerLink3')</a>
                                        </li>
                                        {{-- <li>
                                            <a href="page_ui_buttons_dropdowns.html">Buttons &amp; Dropdowns</a>
                                        </li>
                                        <li>
                                            <a href="page_ui_navigation_more.html">Navigation &amp; More</a>
                                        </li>
                                        <li>
                                            <a href="page_ui_horizontal_menu.html">Horizontal Menu</a>
                                        </li>
                                        <li>
                                            <a href="page_ui_progress_loading.html">Progress &amp; Loading</a>
                                        </li>
                                        <li>
                                            <a href="page_ui_preloader.html">Page Preloader</a>
                                        </li>
                                        <li>
                                            <a href="page_ui_color_themes.html">Color Themes</a>
                                        </li> --}}
                                    </ul>
                                </li>
                                <li>
                                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-notes_2 sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">@lang('sidebar.settingsTitle')</span></a>
                                    <ul>
                                        <li>
                                            <a href="{{ route('settings.account', ['lang' => \App::getLocale()]) }}">@lang('sidebar.settingsLink1')</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('settings.bank', ['lang' => \App::getLocale()]) }}">@lang('sidebar.settingsLink2')</a>
                                        </li>
                                        {{-- <li>
                                            <a href="{{ route('settings.password', ['lang' => \App::getLocale()]) }}">@lang('sidebar.resetpassword')</a>
                                        </li> --}}
                                        {{-- <li>
                                            <a href="page_forms_wizard.html">Wizard</a>
                                        </li> --}}
                                    </ul>
                                </li>
                                <li>
                                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-table sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">@lang('sidebar.networkTitle')</span></a>
                                    <ul>
                                        <li>
                                            <a href="{{ route('network.binary', ['lang' => \App::getLocale()]) }}">@lang('sidebar.networkLink1')</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('network.direct', ['lang' => \App::getLocale()]) }}">@lang('sidebar.directlink')</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('network.downline', ['lang' => \App::getLocale()]) }}">@lang('sidebar.downlinelink')</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('network.iddownline', ['lang' => \App::getLocale()]) }}">@lang('sidebar.iddownlinelink')</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('network.directbusinessreport', ['lang' => \App::getLocale()]) }}">@lang('sidebar.directbusinessreportlink')</a>
                                        </li>

                                        <li>
                                            <a href="{{ route('network.jbvreferal', ['lang' => \App::getLocale()]) }}">@lang('sidebar.jbvreferallink')</a>
                                        </li>
                                        {{-- <li>
                                            <a href="/en/member/left-history">@lang('sidebar.leftLink')</a>
                                        </li>
                                        <li>
                                            <a href="/en/member/right-history">@lang('sidebar.rightLink')</a>
                                        </li> --}}
                                    </ul>
                                </li>

                                <li>
                                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-table sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">@lang('sidebar.commissionTitle')</span></a>
                                    <ul>
                                        <li>
                                            <a href="{{ route('commission.performancebonus', ['lang' => \App::getLocale()]) }}">@lang('sidebar.performancebonus')</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('commission.leadershipbonus', ['lang' => \App::getLocale()]) }}">@lang('sidebar.leadershipbonus')</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('commission.rewards', ['lang' => \App::getLocale()]) }}">@lang('sidebar.rewardslink')</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('commission.directsponsorcommission', ['lang' => \App::getLocale()]) }}">@lang('sidebar.directsponsorcommission')</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('commission.directbusinessreport', ['lang' => \App::getLocale()]) }}">@lang('sidebar.directbusinessreportlink')</a>
                                        </li>

                                        <li>
                                            <a href="{{ route('commission.companyturnover', ['lang' => \App::getLocale()]) }}">@lang('sidebar.companyturnover')</a>
                                        </li>
                                        <li>
                                            <a href="/en/transaction/withdraw">Withdrawal</a>
                                        </li>
                                        <li>
                                            <a href="/en/transaction/statement">Withdrawal History</a>
                                        </li>

                                        <li>
                                            <a href="{{ route('commission.levelincome', ['lang' => \App::getLocale()]) }}">@lang('sidebar.levelincome')</a>
                                        </li>
                                        {{-- <li>
                                            <a href="/en/member/left-history">@lang('sidebar.leftLink')</a>
                                        </li>
                                        <li>
                                            <a href="/en/member/right-history">@lang('sidebar.rightLink')</a>
                                        </li> --}}
                                    </ul>
                                </li>

                                <li>
                                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-table sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">@lang('sidebar.wallettitle')</span></a>
                                    <ul>
                                        <li>
                                            <a href="{{ route('wallet.commissionwallet', ['lang' => \App::getLocale()]) }}">@lang('sidebar.commissionwallet')</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('wallet.repurchasewallet', ['lang' => \App::getLocale()]) }}">@lang('sidebar.repurchasewallet')</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('wallet.weekly', ['lang' => \App::getLocale()]) }}">@lang('sidebar.weekly')</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('wallet.payoutwallet', ['lang' => \App::getLocale()]) }}">@lang('sidebar.payoutwallet')</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('wallet.fortnightly', ['lang' => \App::getLocale()]) }}">@lang('sidebar.fortnightlylink')</a>
                                        </li>

                                        <li>
                                            <a href="{{ route('wallet.mmtransfer', ['lang' => \App::getLocale()]) }}">@lang('sidebar.mmtransfer')</a>
                                        </li>
                                        

                                        <li>
                                            <a href="{{ route('wallet.extrawallet', ['lang' => \App::getLocale()]) }}">@lang('sidebar.extrawallet')</a>
                                        </li>
                                        {{-- <li>
                                            <a href="/en/member/left-history">@lang('sidebar.leftLink')</a>
                                        </li>
                                        <li>
                                            <a href="/en/member/right-history">@lang('sidebar.rightLink')</a>
                                        </li> --}}
                                    </ul>
                                </li>
                                

                                <?php
                                $announcement = \Cache::remember('announcement.new', 60, function () {
                                  return \App\Models\Announcement::whereRaw('Date(created_at) = CURDATE()')->first();
                              });
                              ?>
                              <li>
                                <a href="{{ route('announcement.list', ['lang' => \App::getLocale()]) }}"><i class="gi gi-leaf sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">@lang('sidebar.announcementTitle')</span></a>
                            </li> 

                            <li>
                                <a href="/en/shop"><i class="gi gi-leaf sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">@lang('sidebar.shopping')</span></a>
                            </li>

                            <li>
                                <a href="{{ route('purchase-history.list', ['lang' => \App::getLocale()]) }}"><i class="gi gi-leaf sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">@lang('sidebar.purchaseHistoryTitle')</span></a>
                            </li> 

                            <li>
                                <a href="{{ route('logout', ['lang' => \App::getLocale()]) }}"><i class="gi gi-leaf sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">@lang('sidebar.logout')</span></a>
                            </li> 
                                {{-- <li>
                                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-show_big_thumbnails sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Page Layouts</span></a>
                                    <ul>
                                        <li>
                                            <a href="page_layout_static.html">Static</a>
                                        </li>
                                        <li>
                                            <a href="page_layout_static_fixed_footer.html">Static + Fixed Footer</a>
                                        </li>
                                        <li>
                                            <a href="page_layout_fixed_top.html">Fixed Top Header</a>
                                        </li>
                                        <li>
                                            <a href="page_layout_fixed_top_footer.html">Fixed Top Header + Footer</a>
                                        </li>
                                        <li>
                                            <a href="page_layout_fixed_bottom.html">Fixed Bottom Header</a>
                                        </li>
                                        <li>
                                            <a href="page_layout_fixed_bottom_footer.html">Fixed Bottom Header + Footer</a>
                                        </li>
                                        <li>
                                            <a href="page_layout_static_main_sidebar_mini.html">Mini Main Sidebar</a>
                                        </li>
                                        <li>
                                            <a href="page_layout_static_main_sidebar_partial.html">Partial Main Sidebar</a>
                                        </li>
                                        <li>
                                            <a href="page_layout_static_main_sidebar_visible.html">Visible Main Sidebar</a>
                                        </li>
                                        <li>
                                            <a href="page_layout_static_alternative_sidebar_partial.html">Partial Alternative Sidebar</a>
                                        </li>
                                        <li>
                                            <a href="page_layout_static_alternative_sidebar_visible.html">Visible Alternative Sidebar</a>
                                        </li>
                                        <li>
                                            <a href="page_layout_static_no_sidebars.html">No Sidebars</a>
                                        </li>
                                        <li>
                                            <a href="page_layout_static_both_partial.html">Both Sidebars Partial</a>
                                        </li>
                                        <li>
                                            <a href="page_layout_static_animated.html">Animated Sidebar Transitions</a>
                                        </li>
                                    </ul>
                                </li> --}}
                                {{-- <li class="sidebar-header">
                                    <span class="sidebar-header-options clearfix"><a href="javascript:void(0)" data-toggle="tooltip" title="Quick Settings"><i class="gi gi-settings"></i></a></span>
                                    <span class="sidebar-header-title">Develop Kit</span>
                                </li> --}}
                                {{-- <li>
                                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-brush sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Ready Pages</span></a>
                                    <ul>
                                        <li>
                                            <a href="#" class="sidebar-nav-submenu"><i class="fa fa-angle-left sidebar-nav-indicator"></i>Errors</a>
                                            <ul>
                                                <li>
                                                    <a href="page_ready_400.html">400</a>
                                                </li>
                                                <li>
                                                    <a href="page_ready_401.html">401</a>
                                                </li>
                                                <li>
                                                    <a href="page_ready_403.html">403</a>
                                                </li>
                                                <li>
                                                    <a href="page_ready_404.html">404</a>
                                                </li>
                                                <li>
                                                    <a href="page_ready_500.html">500</a>
                                                </li>
                                                <li>
                                                    <a href="page_ready_503.html">503</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#" class="sidebar-nav-submenu"><i class="fa fa-angle-left sidebar-nav-indicator"></i>Get Started</a>
                                            <ul>
                                                <li>
                                                    <a href="page_ready_blank.html">Blank</a>
                                                </li>
                                                <li>
                                                    <a href="page_ready_blank_alt.html">Blank Alternative</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="page_ready_search_results.html">Search Results (4)</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_article.html">Article</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_user_profile.html">User Profile</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_contacts.html">Contacts</a>
                                        </li>
                                        <li>
                                            <a href="#" class="sidebar-nav-submenu"><i class="fa fa-angle-left sidebar-nav-indicator"></i>e-Learning</a>
                                            <ul>
                                                <li>
                                                    <a href="page_ready_elearning_courses.html">Courses</a>
                                                </li>
                                                <li>
                                                    <a href="page_ready_elearning_course_lessons.html">Course - Lessons</a>
                                                </li>
                                                <li>
                                                    <a href="page_ready_elearning_course_lesson.html">Course - Lesson Page</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#" class="sidebar-nav-submenu"><i class="fa fa-angle-left sidebar-nav-indicator"></i>Message Center</a>
                                            <ul>
                                                <li>
                                                    <a href="page_ready_inbox.html">Inbox</a>
                                                </li>
                                                <li>
                                                    <a href="page_ready_inbox_compose.html">Compose Message</a>
                                                </li>
                                                <li>
                                                    <a href="page_ready_inbox_message.html">View Message</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="page_ready_chat.html">Chat</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_timeline.html">Timeline</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_files.html">Files</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_tickets.html">Tickets</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_bug_tracker.html">Bug Tracker</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_tasks.html">Tasks</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_faq.html">FAQ</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_pricing_tables.html">Pricing Tables</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_invoice.html">Invoice</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_forum.html">Forum (3)</a>
                                        </li>
                                        <li>
                                            <a href="page_ready_coming_soon.html">Coming Soon</a>
                                        </li>
                                        <li>
                                            <a href="#" class="sidebar-nav-submenu"><i class="fa fa-angle-left sidebar-nav-indicator"></i>Login, Register &amp; Lock</a>
                                            <ul>
                                                <li>
                                                    <a href="login.html">Login</a>
                                                </li>
                                                <li>
                                                    <a href="login_full.html">Login (Full Background)</a>
                                                </li>
                                                <li>
                                                    <a href="login_alt.html">Login 2</a>
                                                </li>
                                                <li>
                                                    <a href="login.html#reminder">Password Reminder</a>
                                                </li>
                                                <li>
                                                    <a href="login_alt.html#reminder">Password Reminder 2</a>
                                                </li>
                                                <li>
                                                    <a href="login.html#register">Register</a>
                                                </li>
                                                <li>
                                                    <a href="login_alt.html#register">Register 2</a>
                                                </li>
                                                <li>
                                                    <a href="page_ready_lock_screen.html">Lock Screen</a>
                                                </li>
                                                <li>
                                                    <a href="page_ready_lock_screen_alt.html">Lock Screen 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li> --}}
                                {{-- <li>
                                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-wrench sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Components</span></a>
                                    <ul>
                                        <li>
                                            <a href="#" class="sidebar-nav-submenu"><i class="fa fa-angle-left sidebar-nav-indicator"></i>3 Level Menu</a>
                                            <ul>
                                                <li>
                                                    <a href="#">Link 1</a>
                                                </li>
                                                <li>
                                                    <a href="#">Link 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="page_comp_maps.html">Maps</a>
                                        </li>
                                        <li>
                                            <a href="page_comp_charts.html">Charts</a>
                                        </li>
                                        <li>
                                            <a href="page_comp_gallery.html">Gallery</a>
                                        </li>
                                        <li>
                                            <a href="page_comp_carousel.html">Carousel</a>
                                        </li>
                                        <li>
                                            <a href="page_comp_calendar.html">Calendar</a>
                                        </li>
                                        <li>
                                            <a href="page_comp_animations.html">CSS3 Animations</a>
                                        </li>
                                        <li>
                                            <a href="page_comp_syntax_highlighting.html">Syntax Highlighting</a>
                                        </li>
                                    </ul>
                                </li> --}}
                            </ul>
                            <!-- END Sidebar Navigation -->

                            <!-- Sidebar Notifications -->
                            {{-- <div class="sidebar-header sidebar-nav-mini-hide">
                                <span class="sidebar-header-options clearfix">
                                    <a href="javascript:void(0)" data-toggle="tooltip" title="Refresh"><i class="gi gi-refresh"></i></a>
                                </span>
                                <span class="sidebar-header-title">Activity</span>
                            </div>
                            <div class="sidebar-section sidebar-nav-mini-hide">
                                <div class="alert alert-success alert-alt">
                                    <small>5 min ago</small><br>
                                    <i class="fa fa-thumbs-up fa-fw"></i> You had a new sale ($10)
                                </div>
                                <div class="alert alert-info alert-alt">
                                    <small>10 min ago</small><br>
                                    <i class="fa fa-arrow-up fa-fw"></i> Upgraded to Pro plan
                                </div>
                                
                                
                            </div> --}}
                            <!-- END Sidebar Notifications -->
                        </div>
                        <!-- END Sidebar Content -->
                    </div>
                    <!-- END Wrapper for scrolling functionality -->
                </div>
                <!-- END Main Sidebar -->

                <!-- Main Container -->
                <div id="main-container">
                    <!-- Header -->
                    <!-- In the PHP version you can set the following options from inc/config file -->
                    <!--
                        Available header.navbar classes:

                        'navbar-default'            for the default light header
                        'navbar-inverse'            for an alternative dark header

                        'navbar-fixed-top'          for a top fixed header (fixed sidebars with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar())
                            'header-fixed-top'      has to be added on #page-container only if the class 'navbar-fixed-top' was added

                        'navbar-fixed-bottom'       for a bottom fixed header (fixed sidebars with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar()))
                            'header-fixed-bottom'   has to be added on #page-container only if the class 'navbar-fixed-bottom' was added
                        -->
                        <header class="navbar navbar-default">
                            <!-- Left Header Navigation -->
                            <ul class="nav navbar-nav-custom">
                                <!-- Main Sidebar Toggle Button -->
                                <li>
                                    <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
                                        <i class="fa fa-bars fa-fw"></i>
                                    </a>
                                </li>
                                <!-- END Main Sidebar Toggle Button -->

                                <!-- Template Options -->
                                <!-- Change Options functionality can be found in js/app.js - templateOptions() -->
                                <li class="dropdown">
                                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="gi gi-settings"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-custom dropdown-options">
                                        <li class="dropdown-header text-center">Header Style</li>
                                        <li>
                                            <div class="btn-group btn-group-justified btn-group-sm">
                                                <a href="javascript:void(0)" class="btn btn-primary" id="options-header-default">Light</a>
                                                <a href="javascript:void(0)" class="btn btn-primary" id="options-header-inverse">Dark</a>
                                            </div>
                                        </li>
                                        <li class="dropdown-header text-center">Page Style</li>
                                        <li>
                                            <div class="btn-group btn-group-justified btn-group-sm">
                                                <a href="javascript:void(0)" class="btn btn-primary" id="options-main-style">Default</a>
                                                <a href="javascript:void(0)" class="btn btn-primary" id="options-main-style-alt">Alternative</a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                <!-- END Template Options -->
                            </ul>
                            <!-- END Left Header Navigation -->

                            <!-- Search Form -->
                            <form action="#" method="post" class="navbar-form-custom">
                                <div class="form-group">
                                    <input type="text" id="top-search" name="top-search" class="form-control" placeholder="Search..">
                                </div>
                            </form>
                            <!-- END Search Form -->

                            <!-- Right Header Navigation -->
                            <ul class="nav navbar-nav-custom pull-right">
                                <!-- Alternative Sidebar Toggle Button -->
                                <li>
                                    <!-- If you do not want the main sidebar to open when the alternative sidebar is closed, just remove the second parameter: App.sidebar('toggle-sidebar-alt'); -->
                                    <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar-alt', 'toggle-other');this.blur();">
                                        <i class="gi gi-share_alt"></i>
                                        <span class="label label-primary label-indicator animation-floating">4</span>
                                    </a>
                                </li>
                                <!-- END Alternative Sidebar Toggle Button -->

                                <!-- User Dropdown -->
                                <li class="dropdown">
                                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                        @if($profile->profimage != null)

                                        <img src="/assetsss/images/AdminProduct/{{$profile->profimage}}"  alt="avatar"> <i class="fa fa-angle-down"></i>
                                        @else
                                        <img src="/img/placeholders/avatars/avatar2.jpg" alt="avatar"> <i class="fa fa-angle-down"></i>
                                        @endif
                                    </a>
                                    <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                                        <li class="dropdown-header text-center">Account</li>
                                        <li>
                                            <a href="#">
                                                <i class="fa fa-clock-o fa-fw pull-right"></i>
                                                <span class="badge pull-right">10</span>
                                                Updates
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-envelope-o fa-fw pull-right"></i>
                                                <span class="badge pull-right">5</span>
                                                Messages
                                            </a>
                                            <a href="page_ready_pricing_tables.html"><i class="fa fa-magnet fa-fw pull-right"></i>
                                                <span class="badge pull-right">3</span>
                                                Subscriptions
                                            </a>
                                            <a href="page_ready_faq.html"><i class="fa fa-question fa-fw pull-right"></i>
                                                <span class="badge pull-right">11</span>
                                                FAQ
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="{{ route('settings.account', ['lang' => \App::getLocale()]) }}">
                                                <i class="fa fa-user fa-fw pull-right"></i>
                                                Profile
                                            </a>
                                            <!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
                                            <a href="{{ route('settings.bank', ['lang' => \App::getLocale()]) }}" data-toggle="modal">
                                                <i class="fa fa-cog fa-fw pull-right"></i>
                                                Bank
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            {{-- <a href="page_ready_lock_screen.html"><i class="fa fa-lock fa-fw pull-right"></i> Lock Account</a> --}}
                                            <a href="login.html"><i class="fa fa-ban fa-fw pull-right"></i> Logout</a>
                                        </li>
                                    {{-- <li class="dropdown-header text-center">Activity</li>
                                    <li>
                                        <div class="alert alert-success alert-alt">
                                            <small>5 min ago</small><br>
                                            <i class="fa fa-thumbs-up fa-fw"></i> You had a new sale ($10)
                                        </div>
                                        <div class="alert alert-info alert-alt">
                                            <small>10 min ago</small><br>
                                            <i class="fa fa-arrow-up fa-fw"></i> Upgraded to Pro plan
                                        </div>
                                        <div class="alert alert-warning alert-alt">
                                            <small>3 hours ago</small><br>
                                            <i class="fa fa-exclamation fa-fw"></i> Running low on space<br><strong>18GB in use</strong> 2GB left
                                        </div>
                                        <div class="alert alert-danger alert-alt">
                                            <small>Yesterday</small><br>
                                            <i class="fa fa-bug fa-fw"></i> <a href="javascript:void(0)" class="alert-link">New bug submitted</a>
                                        </div>
                                    </li> --}}
                                </ul>
                            </li>
                            <!-- END User Dropdown -->
                        </ul>
                        <!-- END Right Header Navigation -->
                    </header>
                    <!-- END Header -->
                    <script>
                        function submitMe() {
                            $('#file').click();
                            $("#clickBro").click();
                        }

                        function hide_bar() {
                           $("#sidenav-overlay").click();
                       }
                   </script>