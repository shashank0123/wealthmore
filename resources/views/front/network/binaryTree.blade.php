<style type="text/css">
@media screen and (max-width: 580px){
  #binary_box { overflow: auto; }
}

#binaryContainer {
    overflow: auto;
    width: 100%;
    height: 1500px;
    white-space: nowrap
}

#binaryNetwork {
    /*width: 1500px*/
}

@media only screen and (max-width:480px) {
    #binaryNetwork {
        width: 600px
    }
}

#binaryNetwork .loader {
    display: none
}

#binaryNetwork ul {
    position: relative;
    padding-top: 20px;
    transition: all .5s
}

#binaryNetwork li {
    position: relative;
    float: left;
    padding: 20px 0px 0 5px;
    list-style-type: none;
    transition: all .5s;
    text-align: center
}

#binaryNetwork li::before {
    position: absolute;
    top: 0;
    right: 50%;
    width: 50%;
    height: 20px;
    content: '';
    border-top: 1px solid #ccc
}

#binaryNetwork li::after {
    position: absolute;
    top: 0;
    right: 50%;
    right: auto;
    left: 50%;
    width: 50%;
    height: 20px;
    content: '';
    border-top: 1px solid #ccc;
    border-left: 1px solid #ccc
}

#binaryNetwork li:only-child {
    padding-top: 0
}

#binaryNetwork li:only-child::after,
#binaryNetwork li:only-child::before {
    display: none
}

#binaryNetwork li:first-child::before {
    border: 0 none
}

#binaryNetwork li:last-child::after {
    border: 0 none
}

#binaryNetwork li:last-child::before {
    border-right: 1px solid #ccc;
    border-radius: 0 5px 0 0
}

#binaryNetwork li:first-child::after {
    border-radius: 5px 0 0 0
}

#binaryNetwork ul ul::before {
    position: absolute;
    top: 0;
    left: 50%;
    width: 0;
    height: 20px;
    content: '';
    border-left: 1px solid #ccc
}

#binaryNetwork li a {
    font-size: 14px;
    display: inline-block;
    padding: 5px 10px;
    transition: all .5s;
    text-decoration: none;
    color: #000;
    border: 1px solid #ccc;
    border-radius: 5px
}

@media only screen and (max-width:480px) {
    #binaryNetwork li a {
        font-size: 8px
    }
}

#binaryNetwork li a.new {
    color: #fff;
    background: #333
}

#binaryNetwork li a:hover {
    color: #fff;
    border: 1px solid #c1134e;
    background: #e91e63
}

#binaryNetwork li a:hover+ul li a {
    color: #fff;
    border: 1px solid #c1134e;
    background: #e91e63
}

#binaryNetwork li a:hover+ul li::after,
#binaryNetwork li a:hover+ul li::before {
    border-color: #c1134e
}

#binaryNetwork li a:hover+ul ul::before,
#binaryNetwork li a:hover+ul::before {
    border-color: #c1134e
}


</style>

<div id="binary_box">
<?php
  use App\Models\Package;
  $packages = Package::all();
  $packagelist = array(); 
  foreach ($packages as $key => $value) {
    $packagelist[$value->id] = $value->package_color;
  }
  use App\Repositories\MemberRepository;
  $repo = new MemberRepository;

  function renderTree ($member, $level, $html, $packagelist) {
    if ($level >= 1) return;
    $repo = new MemberRepository;
    $html = '';
    if ($children = $repo->findChildren($member)) {
      $html .= '<ul>';
      if (count($children) <= 0) {
        $html .= '<li>
            <a href="' . route('member.register', ['lang' => \App::getLocale()]) . '?u=' . $member->username . '&p=left" class="new"><i class="md md-person-add"></i> ' . \Lang::get('binary.register') . '</a>
          </li>
          <li>
            <a href="' . route('member.register', ['lang' => \App::getLocale()]) . '?u=' . $member->username . '&p=right" class="new"><i class="md md-person-add"></i> ' . \Lang::get('binary.register') . '</a>
          </li>';
      } else {
        foreach ($children as $child) {
          if (count($children) < 2 && $child->position == 'right') {
            $html .= '<li>
              <a href="' . route('member.register', ['lang' => \App::getLocale()]) . '?u=' . $member->username . '&p=left" class="new"><i class="md md-person-add"></i> ' . \Lang::get('binary.register') . '</a>
              </li>';
          }

          if ($level < 0) {
            $html .= '<li>
              <a style="background:'.$packagelist[$child->package_id].'"  href="#" data-toggle="modal" data-target="#showModal" tabindex="0" role="button" data-id="' . $child->username . '"><i class="md md-accessibility"></i> ' . $child->username . ' (' . strtoupper($child->position[0]) . ')</a>';
          } else {
            $html .= '<li>
              <a style="background:'.$packagelist[$child->package_id].'"  href="#" tabindex="0" role="button" data-toggle="modal" data-target="#showModal" data-id="' . $child->username . '"><i class="md md-accessibility"></i>' . $child->username . ' (' . strtoupper($child->position[0]) . ')</a><a href="#" data-username="' . $child->username . '" data-more="true" title="More"><span class="md md-add"></span></a>';
          }

          $next = $level + 1;
          $html .= renderTree($child, $next, $html, $packagelist);
          $html .= '</li>';

          if (count($children) < 2 && $child->position == 'left') {
            $html .= '<li>
              <a style="background:'.$packagelist[$child->package_id].'"  href="' . route('member.register', ['lang' => \App::getLocale()]) . '?u=' . $member->username . '&p=right" class="new"><i class="md md-person-add"></i> ' . \Lang::get('binary.register') . '</a>
              </li>';
          }
        }
      }
      $html .= '</ul>';
    }
    return $html;
  }
?>

<style>
  @media screen and (max-width: 580px){

  .binary-tree-responsive { overflow: auto !important; }
  
  }
</style>
<div class="binary-tree-responsive">
<ul id="mainTree" style="overflow: auto !important;">
  <li>
    <a style="background:@if(isset($packagelist[$model->package_id])){{ $packagelist[$model->package_id] }}@endif "  href="#" tabindex="0" role="button" data-toggle="modal" data-target="#showModal" data-id="{{ $model->username }}"><i class="md md-accessibility"></i>{{$model->username}}</a>
    @if ($children = $repo->findChildren($model)) <!-- render 4 levels -->
      <ul>
        @if (count($children) <= 0)
          <li>
            <a href="{{ route('member.register', ['lang' => \App::getLocale()]) . '?u=' . $model->username . '&p=left' }}" class="new"><i class="md md-person-add"></i> {{ \Lang::get('binary.register') }}</a>
          </li>
          <li>
            <a href="{{ route('member.register', ['lang' => \App::getLocale()]) . '?u=' . $model->username . '&p=right' }}" class="new"><i class="md md-person-add"></i> {{ \Lang::get('binary.register') }}</a>
          </li>
        @else
          @foreach ($children as $child)
            @if (count($children) < 2 && $child->position == 'right')
              <li>
                <a href="{{ route('member.register', ['lang' => \App::getLocale()]) . '?u=' . $model->username . '&p=left' }}" class="new"><i class="md md-person-add"></i> {{ \Lang::get('binary.register') }}</a>
              </li>
            @endif
            <li>
              <a style="background:{{ $packagelist[$child->package_id] }} " href="#" tabindex="0" role="button" data-toggle="modal" data-target="#showModal" data-id="{{ $child->username }}"><i class="md md-accessibility"></i> {{$child->username}} ({{ strtoupper($child->position[0]) }})</a>
              {!! renderTree($child, 0, '', $packagelist) !!}
            </li>
            @if (count($children) < 2 && $child->position == 'left')
              <li>
                <a href="{{ route('member.register', ['lang' => \App::getLocale()]) . '?u=' . $model->username . '&p=right' }}" class="new"><i class="md md-person-add"></i> {{ \Lang::get('binary.register') }}</a>
              </li>
            @endif
          @endforeach
        @endif
      </ul>
    @endif
  </li>
</ul>
</div>
</div>
