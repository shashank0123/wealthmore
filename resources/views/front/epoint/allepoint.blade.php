<?php 
use App\Models\Epoint;
$epoint =  Epoint::where('member_id', $member->id)->first();
use App\Models\TransferEpoint;
$epointtransfer = TransferEpoint::where('receiver_id', $member->id)->get();
?>
@extends('front.app')

@section('title')
  All Epoints | {{ config('app.name') }}
@stop

@section('breadcrumb')
<ul class="breadcrumb">
  <li><a href="#">Front Page</a></li>
  <li><a href="{{ route('admin.home') }}">Dashboard</a></li>
  <li class="active">All Epoints</li>
</ul>
@stop

@section('content')
<main>
  {{-- @include('front.include.sidebar') --}}
  <div class="main-container container">
    {{-- @include('front.include.header') --}}
    <div class="main-content" autoscroll="true" bs-affix-target="" init-ripples="" style="">
      <section>
        <div class="page-header">
          <h1><!-- <i class="md md-person-add"></i> --> All Epoints</h1>
        </div>
     </section>
     <div class="well white" style="margin: 20px">
    <table class="table table-hover">
    <thead>
      <tr>
        <th>Sent By</th>
        <th>Epoints</th>
        <th>Date</th>
      </tr>
    </thead>
    <tbody>
      @php
        $total =0;
      @endphp
      @foreach($epointtransfer as $epointl) 
      <tr>
        <td>{{$epointl->sender_id}}</td>
        <td>{{$epointl->no_of_epoint}}</td>
        <td>{{$epointl->created_at}}</td>
      </tr>
      @php
        $total = $epointl->no_of_epoint+$total;
      @endphp
      @endforeach
      <tr>
        <td><STRONG>Total</STRONG></td>
        <td></td>
        <td><strong>@if (isset($epointl->no_of_epoint)) {{$total}} @else {{ 0 }} @endif</strong></td>
      </tr>
    </tbody>
  </table>
</div>
    </div>
   </div>
</main>
@stop
