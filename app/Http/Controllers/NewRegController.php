<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controller;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Validator;
use App\Leads;
use App\Payment;
use App\Models\Member;
use App\Models\MemberDetail;
use App\RegistrationSponsorId;
use App\MainCategory;
use App\Proof;
use Mail;
use StdClass;
use App\Repositories\MemberRepository;
use App\Repositories\PackageRepository;
use App\Repositories\SharesRepository;
use App\Repositories\BonusRepository;
use Yajra\Datatables\Facades\Datatables;
use Log;
// use PHPMailer\PHPMailer\PHPMailer;
// use PHPMailer\PHPMailer\Exception;

class NewRegController extends BaseController
{
    /**
     * The MemberRepository instance.
     *
     * @var \App\Repositories\MemberRepository
     */
    protected $MemberRepository;

    /**
     * The PackageRepository instance.
     *
     * @var \App\Repositories\PackageRepository
     */
    protected $PackageRepository;

    /**
     * The SharesRepository instance.
     *
     * @var \App\Repositories\SharesRepository
     */
    protected $SharesRepository;

    /**
     * The PackageRepository instance.
     *
     * @var \App\Repositories\PackageRepository
     */
    protected $BonusRepository;

    /**
     * Create a new MemberController instance.
     *
     * @param \App\Repositories\MemberRepository $MemberRepository
     * @return void
     */
    public function __construct(
        MemberRepository $MemberRepository,
        PackageRepository $PackageRepository,
        SharesRepository $SharesRepository,
        BonusRepository $BonusRepository
    ) {
        $this->MemberRepository = $MemberRepository;
        $this->PackageRepository = $PackageRepository;
        $this->SharesRepository = $SharesRepository;
        $this->BonusRepository = $BonusRepository;
    }
    public function Register(Request $request)
    {
        $response = new StdClass;
        $response->status = 200;
        $response->message = 'Something went wrong';
        $response->password = 0;
        $lead = new Leads;

        $data = array();
        $data['package_id'] = 1;
        $data['email']  = $request->email;
        $data['username']= $request->username;
        $data['password'] =  rand(100000, 999999);         
        $data['direct_id'] = strtolower($request->referal_id);
        $data['parent_id'] = strtolower($request->referal_id);
        $data['phone'] = $request->mobile;
        $data['dob'] = $request->dob;
        $data['register_by'] = $request->register_by;
        $data['state'] = $request->state;
        $data['city'] = $request->city;        
        $data['secret_password'] = $data['password'];
        $data['position'] = $request->placement;       

        // Log::info($data);
        if (!$package = $this->PackageRepository->findById($data['package_id'])) {
            return \Response::json([
                'type' => 'error',
                'message' => 'Package not found.'
            ]);
        }
        
        try {

            if ($check = $this->MemberRepository->findByUsername($data['username'])) {
                throw new \Exception('Username already exists.', 1);
                return false;
            }


        // var_dump($data);
        
            $user = \Sentinel::registerAndActivate([
                'email'   => $data['email'],
                'username'  =>  $data['username'],
                'password'  =>  $data['password'],
                'phone'  =>  $data['phone'],
                'permissions' =>  [
                    'user' => true,
                ]
            ]);

            if (1) { // lower member
                if (!$direct = $this->MemberRepository->findByUsername($data['direct_id'])) {
                    $user->delete();
                    throw new \Exception('Member upline not found.', 1);
                    return false;
                }

                if (!$parent = $this->MemberRepository->findByUsername($data['parent_id'])) {
                    $user->delete();
                    throw new \Exception('Member binary not found.', 1);
                    return false;
                }

                $data['parent_id'] = $this->MemberRepository->findParentId($direct->id, $data['position']);

                $member = $this->MemberRepository->saveModel(new \App\Models\Member, [
                    'username'  =>  $user->username,
                    'register_by' => $data['register_by'],
                    'secret_password' =>  $data['secret_password'],
                    'user_id'   =>  $user->id,
                    'package_id'    =>  $package->id,
                    'direct_id' =>  $direct->id,
                    'parent_id' =>  $parent->id,
                    'root_id'   =>  ($parent->position == $data['position']) ? $parent->root_id : $parent->id,
                    'level'     =>  $parent->level + 1,
                    'direct_percent'    =>  $package->direct_percent,
                    'pairing_percent'   =>  $package->pairing_percent,
                    'group_level'   =>  $package->group_level,
                    'max_pair'  =>  $package->max_pair,
                    'max_pairing_bonus' =>  $package->max_pairing_bonus,
                    'original_amount'  =>  $package->package_amount,
                    'package_amount' =>  $package->package_amount,
                    'position'  =>  $data['position']
                ]);

                $this->MemberRepository->saveModel(new \App\Models\MemberWallet, [
                    'member_id' =>  $member->id,
                    'register_point'    =>  0,
                    'purchase_point'    =>  $package->purchase_point,
                    'promotion_point'   =>  0,
                    'cash_point'    =>  0,
                    'md_point'  =>  0
                ]);

                if (env('APP_ENV') == 'local') { // local
                    $wallet = $member->wallet;
                    $this->MemberRepository->addNetwork($member);
                    // $this->SharesRepository->repurchasePackage($member, $wallet->purchase_point, $wallet);
                } else { // production
                    dispatch(new SharesAfterRegisterJob($member))->onQueue('queue-shares-register');
                    dispatch(new NetworkAfterRegisterJob($member))->onQueue('queue-network-register');
                }

                // remove cache for network
                \Cache::forget('member.' . $parent->id . '.children');

                $this->BonusRepository->calculateDirect($member, $direct);
                $this->BonusRepository->calculateOverride($member);
            }
            
            $this->MemberRepository->saveModel(new \App\Models\MemberDetail, [
                'member_id' =>  $member->id,
                'state' =>  $data['state'],

                'date_of_birth' =>  $data['dob'],
            ]);

            $this->MemberRepository->saveModel(new \App\Models\MemberShares, [
                'member_id' =>  $member->id,
                'amount'    =>  0,
                'share_limit'   =>  $package->share_limit,
                'max_share_sale'    =>  $package->max_share_sale
            ]);


            $lead->username = $request->username; 
            $lead->email = $request->email; 
            $lead->phone = $request->mobile; 
            $lead->otp      = rand(100000, 999999);
            $lead->otp_time = date('Y-m-d H:m:s') ;
            $lead->save();

            // $mobile = $lead->phone;
            // $message = "Your%20OTP%20for%20WealthIndia%20is%20$lead->otp";
            // $url = "http://smpp1.webtechsolution.co/http-tokenkeyapi.php?authentic-key=39316261636b73746167653032333637331559059857&senderid=MBIZTL&route=4&number=$mobile&message=$message"; 
            // $c = curl_init();
            // curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
            // curl_setopt($c,CURLOPT_HTTPGET ,1);
            // curl_setopt($c, CURLOPT_URL, $url);
            // $contents = curl_exec($c);
            // if (curl_errno($c)) {
            //     echo 'Curl error: ' . curl_error($c);
            // }
            // else{
            //     curl_close($c);
            // }
            if($lead){
                $response->message ="Data Inserted Successfully";
                $response->password = $data['secret_password'];
                $response->username = $request->username; 

                return response()->json($response);
                exit();  
            }

            else{
                echo " Something Went Wrong";
            }



        } catch (\Exception $e) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  $e->getMessage()
            ]);
        }
        return response()->json($response);

    // return \Response::json([
    //     'type'  => 'success',
    //     'message' => \Lang::get('message.registerSuccess'),
    //     'redirect' => route('/', ['lang' => \App::getLocale()])
    // ]);
    }

    public function verifyotp(Request $request)
    {
        // $sponsored = new RegistrationSponsorId;
        $response = new StdClass;
        $response->status = 200;
        $response->message = 'Something went wrong';

        $findUser = Leads::where('email',$request->email)->first();
        $userId = $findUser->username;
        $password = $request->get_pass;
        $email = $request->email;
        // $sponsoredId = $request->sponsoredId;
        // $sponsoredId = strtoupper($sponsoredId);

        if($findUser->verify_otp != 'verified')
        {
            if($findUser->otp == $request->otp) {                
                $response->message = 'Registered Successfully.';
                $response->user_id = $findUser->id;
                // $response->email = $email;
                $response->password = $password;
                $findUser->verify_otp ='verified';
                $findUser->update();
                // $sponsored->user_id = $findUser->id;
                // $sponsored->sponsor_id = $sponsoredId;
                // $sponsored->save();

                $data = array( 'UserID'=>$userId, 'Password'=>$password);
                Mail::send('mail', $data, function($message) use ($userId, $password)
                {   
                    $message->from('info@wealthindia.global', 'Admin');
                    $message->to($email, 'Customer')->subject('Successfull Registration');
                });


                return response()->json($response);

            }
            else {    
                $response->message = "Wrong OTP";
                return response()->json($response);
            }
        }
        else
        {
            $response->message = "Already Verified";
            return response()->json($response);
        }        
    }

    public function checkoutForm(Request $request,$id)
    {      
        $lead = Leads::where('id',$id)->first();
        return view('membership-payment',compact('lead'));
    }

    public function checkoutForm1(Request $request,$id){
        $success = "";
        if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') == 0){
    //Request hash
            $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';   
            if(strcasecmp($contentType, 'application/json') == 0){
                $data = json_decode(file_get_contents('php://input'));
                $hash=hash('sha512', $data->key.'|'.$data->txnid.'|'.$data->amount.'|'.$data->pinfo.'|'.$data->fname.'|'.$data->email.'|||||'.$data->udf5.'||||||'.$data->salt);
                $json=array();          
                $json['success'] = $hash;
                echo json_encode($json);        
            }
            exit(0);
        }

    }

    public function getSuccess(Request $request){

        $post_data = $_POST;  

        $user_email = $post_data['email'];
        $status = $post_data['txnStatus'];
        $txnid = $post_data['txnid'];


        $payment = Payment::where('txnid', $post_data['txnid'])->first();


        if($status == 'SUCCESS'){

            $lead = Leads::where('email',$user_email)->first();
            $lead->payment_status = 'success';
            $lead->update();
            if (isset($payment->user_id) && $payment->user_id){
                $user = Member::where('user_id',$payment->user_id)->first();
                $user->payment_amount = $post_data['amount'];
                $user->update();
            }
        }    

    // $data = array('Email'=>$user_email, 'TransactionID'=>$txnid, 'Payment_Status'=>$status);
    // Mail::send('mail', $data, function($message) use ($user_email, $txnid, $status)
    // {   
    //     $message->from('no-reply@abc.in', 'Mailer');
    //     $message->to('Admin@abc.in', 'Admin')->subject('Successfull Registration');
    // });



        return redirect('/en');
    }

    public function uploadAdhaarFront(Request $request){
       $member_last = MemberDetail::orderBy('created_at','DESC')->first();
       $message = 'Something went wrong';
        if (Input::hasfile('adhaar_front')) {
         $file=Input::file('adhaar_front');
         $file->move(public_path(). '/member/', time().$file->getClientOriginalName());
         $member_last->adhaar_front=time().$file->getClientOriginalName();                   
     }

     if (Input::hasfile('adhaar_back')) {
         $file=Input::file('adhaar_back');
         $file->move(public_path(). '/member/', time().$file->getClientOriginalName());
         $member_last->adhaar_back=time().$file->getClientOriginalName(); 

     $member_last->adhaar = $request->adhaar;
     $member_last->update();
     $message = 'Images uploaded successfully';
     }

     return response()->json($message);

 }

}
