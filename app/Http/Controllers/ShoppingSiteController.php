<?php

namespace App\Http\Controllers;
use App\Product;
use App\Banner;
use App\Review;
use App\User;
use App\Cart;
use App\Order;
use App\Address;
use App\OrderItem;
use App\Testimonial;
use App\MainCategory;
use App\Models\Member;
use App\Models\MemberDetail;
use App\Wishlist;

use Illuminate\Http\Request;

class ShoppingSiteController extends Controller
{
    public function getIndex(){
        $featured_post = array();
        $trending_post = array();
        $banners = Banner::where('status','active')->where('image_type','shop')->get();

        $featured = Product::where('trending','yes')->where('status','Active')->orderBy('created_at','DESC')->limit(2)->get();
        
        $products = Product::where('trending','yes')->where('status','Active')->orderBy('created_at','DESC')->limit(4)->get();
        if(!empty($products)){
            $i=0;
            foreach($products as $pro){
                $featured_post[$i++] = $pro;
            }
        }

        shuffle($featured_post);

        $trending = Product::where('status','Active')->orderBy('created_at','DESC')->limit(2)->get();
        
        $products = Product::where('status','Active')->orderBy('created_at','DESC')->limit(4)->get();

        if(!empty($products)){
            $i++;
            foreach($products as $pro){
                $trending_post[$i++] = $pro;
            }
        }

        shuffle($trending_post);

        $offers = Product::where('status','Active')->where('trending','yes')->orderBy('created_at','DESC')->get();

        $top_banner = Banner::where('image_type','medium')->where('status','Active')->orderby('created_at','DESC')->first();
        if(!empty($top_banner)){
            $bottom_banner = Banner::where('image_type','medium')->where('status','Active')->where('id','<',$top_banner->id)->orderBy('created_at','DESC')->first();
        }

        $testimonials = Testimonial::where('status','Active')->get();
        return view('envato-shop.index',compact('featured','trending','featured_post','trending_post','banners','testimonials','offers','top_banner','bottom_banner'));
    }

    public function getProductDetail($slug){
        $product = Product::where('slug_name',$slug)->first();
        $category = MainCategory::where('id',$product->category_id)->first();
        $product_color = $product->product_color;
        $product_size = array();
        if(!empty($product->product_size)){
            $product_size = explode(',',$product->product_size);
        }

        $related_products = Product::where('category_id',$product->category_id)->orderBy('created_at','DESC')->get();

        $reviews = Review::where('product_id',$product->id)->get();

        return view('envato-shop.product-detail',compact('product','category','product_color','product_size','reviews','related_products'));
    }

    public function getCategorywiseProducts( Request $request,$slug){

        $cat = MainCategory::where('slug_name',$request->slug)->first();

        $category = array();
        $searched_products = array();
        $colors = array();
        $c=0;
        $weights = array();
        $w=0;
        $sizes = array();
        $s=0;
        $i=1; $j=0;
        $category[0]=$cat->id;
        $subcat = MainCategory::where('category_id',$cat->id)->get();

        if(!empty($subcat)){
            foreach($subcat as $sub){
                $category[$i++] = $sub->id;
                $cate = MainCategory::where('category_id',$sub->id)->get();
                if(!empty($cate)){
                    foreach($cate as $cator){
                        $category[$i++] = $cator->id;
                        $lastcat = MainCategory::where('category_id',$cator->id)->get();
                        if(!empty($lastcat)){
                            foreach($lastcat as $last){
                                $category[$i++] = $last->id;
                            }
                        }
                    }
                }
            }
        }

        foreach($category as $cate){
            $products = Product::where('category_id',$cate)->where('status','Active')->get();
            if(!empty($products)){
                foreach($products as $pro){
                    $searched_products[$j++] = $pro;
                    if(!empty($pro->product_color)){
                        $colors[$c++]=$pro->product_color;
                    }
                    if(!empty($pro->product_size)){
                        $sizes[$s++]=$pro->product_size;
                    }
                    if(!empty($pro->weight)){
                        $weights[$w++]=$pro->weight;
                    }
                }
            }
        }      
        $min = Product::min('sell_price');
        $max = Product::max('sell_price');

        return view('envato-shop.categorywise-products',compact('slug','searched_products','subcat','cat','min','max','colors','sizes'));
    }

     // For Search Page
    public function getSearchProduct(Request $request)
    {
        $colors = array();
        $c=0;
        $weights = array();
        $w=0;
        $sizes = array();
        $s=0;
        $subcat = MainCategory::where('category_id',0)->get();
        $keyword = $request->product_keyword;
        $max = Product::where('status','Active')->max("sell_price");
        $min = Product::where('status','Active')->min("sell_price");

        $searched_products = Product::where('name','LIKE',"%".$keyword."%")->orWhere('short_descriptions','LIKE',"%".$keyword."%")->orWhere('long_descriptions','LIKE',"%".$keyword."%")->where('status','Active')->get();

        $products = Product::where('status','Active')->get();

        if(!empty($products)){
            foreach($products as $product){
                if(!empty($product->product_size)){
                    $sizes[$s++] = $product->product_size;
                }
                if(!empty($product->product_color)){
                    $colors[$c++] = $product->product_color;
                }
                if(!empty($product->weight)){
                    $weights[$w++] = $product->weight;
                }
            }
        }

        return view('envato-shop.searched-products',compact('searched_products','keyword','min','max','subcat','colors','sizes','weights'));
    }

    public function getCart(){
        return view('envato-shop.cart');
    }

    public function getWishlist(){
     return view('envato-shop.wishlist');
 }

 public function getCompare(){
     return view('envato-shop.compare');
 }

 public function getTnC(){
     return view('envato-shop.tnc');
 }

 public function getContact(){
     return view('envato-shop.contact');
 }

 public function getAbout(){
     return view('envato-shop.about');
 }


 public function getCheckout($id){

     return view('envato-shop.checkout',compact('user','member'));
 }

 public function getPrivacyPolicy(){
     return view('envato-shop.privacypolicy');
 }

 public function getReturnPolicy(){
     return view('envato-shop.returnpolicy');
 }

 public function checkoutForm(Request $request,$id){


    $cart = new Cart;
    $order = new Order;
    $address = new Address; 
    $check = new Address; 


    $checkSessionID = Cart::where('session_id',session()->getId())->first();

    if($checkSessionID != null){

    }
    else{
        $cart->session_id = session()->getId();
        $cart->user_id = $id;
        $cart->cart = json_encode(session()->get('cart'));
        $cart->date = date('Y-m-d H:m:s');
        $cart->save();
    }       

    $check = Address::where('user_id',$request->member_id)->first();

    if($check != null){

        $check->address = $request->address;
        $check->country_id = 1;
        $check->state_id = $request->state_id;
        $check->city_id = $request->city_id;
        $check->pin_code = $request->pin_code;
        $check->phone = $request->phone;
        $check->update();
    }
    else{
        $address->user_id = $id;
        $address->address = $request->address;
        $address->country_id = 1;
        $address->state_id = $request->state_id;
        $address->city_id = $request->city_id;
        $address->pin_code = $request->pin_code;
        $address->phone = $request->phone;
        $address->save();
    }       

    $address_id = Address::where('user_id',$id)->first();
    $order->user_id = $id;
    $order->address_id =  $address_id->id;
    $order->price =  $request->total_bill;
    $order->payment_method =  'payumoney';
    $order->date = date('Y-m-d H:m:s');
    $order->save();

    $order_id = Order::where('user_id',$id)->first();
    $i=0;
    if(session()->get('cart') != null){
        foreach(session()->get('cart') as $cart){
            $order_item = new OrderItem;
            $order_item->order_id = $order_id->id;
            $order_item->item_id = $cart->product_id;
            $price = Product::where('id',$cart->product_id)->first();
            $order_item->quantity   = $cart->quantity;
            $order_item->price = $cart->quantity * $price->sell_price;
            $order_item->save();
        }   
    }

    $checkorder = Order::where('user_id',$id)->orderBy('created_at','DESC')->first();

    $user_info = Member::where('Member.id',$id)->leftjoin('users','Member.user_id','=','users.id')->select('Member.username','users.phone','users.email')->first();
    return view('envato-shop.payment',compact('user_info','checkorder'));

}

public function checkoutForm1(Request $request,$id){
    $success = "";
    if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') == 0){
    //Request hash
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';   
        if(strcasecmp($contentType, 'application/json') == 0){
            $data = json_decode(file_get_contents('php://input'));
            $hash=hash('sha512', $data->key.'|'.$data->txnid.'|'.$data->amount.'|'.$data->pinfo.'|'.$data->fname.'|'.$data->email.'|||||'.$data->udf5.'||||||'.$data->salt);
            $json=array();          
            // $request->session()->put('success',$hash);
            $json['success'] = $hash;
            echo json_encode($json);        
        }
        exit(0);
    }

}  

public function getSuccess(Request $request){
    $postdata = $_POST;

    $user = User::where('email',$postdata['email'])->first();
    $member = Member::where('user_id',$user->id)->first();  

    $updateorder = new Order;

    $updateorder = Order::where('user_id',$member->id)->orderBy('created_at','DESC')->first();
    $updateorder->transaction_id = $postdata['txnid'];
    $updateorder->encrypted_payment_id = $postdata['encryptedPaymentId'];
    $updateorder->status = $postdata['txnStatus'];

    $updateorder->update();

    $transaction_details = Order::where('user_id',$updateorder->user_id)->whereNotNull('transaction_id')->orderBy('updated_at','DESC')->get();

    session()->forget('cart');
    return view('envato-shop.result',compact('transaction_details'));
}
}
