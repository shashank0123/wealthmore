<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\User;
use App\Models\MemberDetail;
use App\Models\Member;
use StdClass;
use Hash;


class ResetPasswordController extends Controller
{
	public function sentOTP(Request $request){
		$response = new StdClass;
		$response->status = 200;
		$response->message = 'Something went wrong';

		$name = $request->username;
		$phone = $request->phone;
		$generateOTP = rand(100000,999999);

		$member = User::where('username',$name)->first();
		$memberinfo = Member::where('username',$name)->first();
		if($memberinfo != null)
		$memberphone = MemberDetail::where('member_id',$memberinfo->id)->first();

		if($member != null){
			if($memberphone->mobile_phone == $phone){
				$member->otp = $generateOTP;
				$member->otp_date = date('Y-m-d H:m:s');
				$member->update();
				$message = "Your%20OTP%20for%20Reset%20Password%20of%20WealthIndia%20Account%20is%20$member->otp";
				$url = "http://smpp1.webtechsolution.co/http-tokenkeyapi.php?authentic-key=39316261636b73746167653032333637331559059857&senderid=MBIZTL&route=4&number=$phone&message=$message"; 
            // die;
            // $url ="http://103.247.98.91/API/SendMsg.aspx?uname=backstage023&pass=backstage023&send=MBIZTL&dest=$mobile&msg=$message";  
            // $url = "";
				$c = curl_init();
				curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
				curl_setopt($c,CURLOPT_HTTPGET ,1);

				curl_setopt($c, CURLOPT_URL, $url);
				$contents = curl_exec($c);
				if (curl_errno($c)) { 
					echo 'Curl error: ' . curl_error($c);
				}
				else{
					curl_close($c);
				}
				if($member){
					$response->message ="User Exists and updated OTP field";
					return response()->json($response);
				}

				else{
					$response->message ="Something Went Wrong";
				return response()->json($response);
				}
			}
			else{
				$response->message ="Not a Registered Number. Please Check";
				return response()->json($response);
			}
			
		}

		else{
				$response->message ="User Not Found";
				return response()->json($response);
			}
		
	}

	public function verifyOTP(Request $request){
		$response = new StdClass;
		$response->status = 200;
		$response->message = 'Something went wrong';

		$otp = $request->otp;
		$name = $request->verifiedusername;
		$phone = $request->verifiedphone;

		$member = User::where('username',$name)->first();
		if($member->otp == $otp){
			$response->message = 'Otp Verified';
			return response()->json($response);
		}
		else{
			$response->message = 'Wrong OTP. Type Again';
			return response()->json($response);
		}
	}

	public function resetPassword(Request $request){
		$response = new StdClass;
		$response->status = 200;
		$response->message = 'Something went wrong';

		$name = $request->resetusername;
		$phone = $request->resetphone;
		$new = $request->password;
		$check = $request->compare_password;

		$member = User::where('username',$name)->first();

		if($new == $check){
			$password = Hash::make($new);
			$member->password = $password;
			$member->update();
			if($member){
				$response->message = "Data inserted Successfully";
				return response()->json($response);
			}
			else{
				$response->message = "Something Went Wrong. Please Try again later";
				return response()->json($response);
			}

		}
		else{
			$response->message = "Password Not Match. Please Check";
			return response()->json($response);
		}
	}
}
