<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Member;
use App\MyEarning;
use App\Banner;
use App\Product;
use App\Update;
use App\MainCategory;
use App\State;
use App\Testimonial;
use App\Models\MemberDetail;



class StaticPagesController extends Controller
{
    public function homepage(Request $request)
    {
        $products = array();
        $i=0;
        $count = Member::count();
        $level1 = Member::where('level',1)->count();
        $level2 = Member::where('level','>',1)->count();
        $recent_joined = Member::where('package_amount','>',0)->orderBy('created_at','DESC')->limit(10)->get();
        $top_earners = MyEarning::orderBy('amount','DESC')->limit(5)->get();
        $banners = Banner::where('image_type','home')->where('status','Active')->get();
        $count_banner =1;

        $monthly_earners = MyEarning::orderBy('amount','DESC')->orderBy('created_at','DESC')->limit(5)->get();

        $cat = MainCategory::orderBy('created_at','DESC')->get();

        if(!empty($cat)){
            foreach ($cat as $c) {

        $products[$i++] = Product::where('category_id',$c->id)->where('status','Active')->where('trending','yes')->first();
            }
        }
        $business = Banner::where('image_type','business')->where('status','Active')->get();

        $testimonials = Testimonial::where('status','Active')->orderBy('created_at','DESC')->get();

        $updates = Update::where('status','Active')->orderBy('created_at','DESC')->limit(3)->get();

        $products = array_filter($products);

        shuffle($products);

        $total_products = Product::count();

        $total_members = Member::count();
    
        return view('home.index',compact('count','level1','level2','recent_joined','top_earners','banners','count_banner','monthly_earners','products','business','testimonials','updates','total_products','total_members'));
    }

    public function aboutUs(Request $request)
    {
    	return view('home.about');
    }

    public function services(Request $request)
    {
    	return view('home.service');
    }

    public function contactUs(Request $request)
    {
    	return view('home.contact');
    }

    public function faqs(Request $request)
    {
    	return view('home.faq');
    }

    public function investment(Request $request)
    {
    	return view('home.investment');
    }

    public function about(Request $request)
    {
    	return view('about');
    }

    public function contact(Request $request)
    {
    	return view('contact');
    }

    public function getTnC(){
        return view('terms');
    }

    
}
