<?php

namespace App\Http\Controllers;
use App\MainCategory;
use App\Product;
use App\Blog;
use App\Review;
use App\Testimonial;
use App\Models\Member;
use App\Models\MemberDetail;
use App\Cart;
use App\City;
use App\Banner;
use App\State;
use App\Newsletter;
use App\Country;
use App\Address;
use App\Order;
use App\OrderItem;
use App\User;
use App\Contact;
use StdClass;
use Mail;

use Illuminate\Http\Request;

class WealthMoreController extends Controller
{
	public function index()
	{
		$cartproducts = array();
		$quantity = array();
		$i = 0;
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
		$banners = Banner::where('image_type','shop')->where('status','active')->get(); 
		// echo $banners;
		// exit();
		$products = Product::where('status','Active')->orderBy('created_at','desc')->limit(4)->get();
		$blogs = Blog::where('status','active')->get();    	
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$i] = Product::where('id',$cart->product_id)->first();
				$quantity[$i] = $cart->quantity;
				$i++;
			}
		}		

		return view('wealthmore.index',compact('maincategory','products','blogs','cartproducts','quantity','banners'));
	}
	

    // For Search Page
	public function getSearchProduct(Request $request)
	{
		$subcat = $request->subcategory;
		$keyword = $request->product_keyword;
		$max_price = Product::where('status','Active')->max("sell_price");

		$cartproducts = array();
		$quantity = array();
		$cnt = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}
		else{
			$cartproducts = null;
			$quantity = 0;
		}

		$lastproduct = Product::where('status','Active')->orderBy('created_at','desc')->first();
		
		$total = array();
		$i=0;
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();

		foreach($maincategory as $row){
			$count=0;
			$count=Product::where('category_id',$row->id)->where('status','Active')->count();        	
			$category = MainCategory::where('category_id',$row->id)->get();
			if($category){
				foreach($category as $col){
					$count+=Product::where('category_id',$col->id)->where('status','Active')->count();        		
					$subcategory = MainCategory::where('category_id',$col->id)->get();
					if($subcategory){
						foreach($subcategory as $set){
							$count+=Product::where('category_id',$set->id)->where('status','Active')->count();       			
						}
					}
				}
				if($count==0)
					$total[$i++] = 0;
				else					
					$total[$i++] = $count;   
			}
		}


		if($subcat>0 and $keyword!="" )
		{
			$searched_products = Product::where('category_id',$subcat)->where('status','Active')->where('name','LIKE','%'.$keyword.'%')->orWhere('short_descriptions','LIKE','%'.$keyword.'%')->orWhere('long_descriptions','LIKE','%'.$keyword.'%')->orWhere('page_description','LIKE','%'.$keyword.'%')->orWhere('page_keywords','LIKE','%'.$keyword.'%')->orderby('name','ASC')->get();
		}
		else if($subcat>0)
		{
			$searched_products = Product::where('category_id',$subcat)->where('status','Active')->orderby('name','ASC')->get();          
		}

		else if($keyword != "")
		{
			$searched_products = Product::where('name','LIKE','%'.$keyword.'%')->where('status','Active')->orderby('name','ASC')->get();          
		}
		else if($subcat==0)
		{
			$searched_products = Product::where('status','Active')->orderby('name','ASC')->get();           
		}       

		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();

		return view('wealthmore.search-product',compact('maincategory','searched_products','total','lastproduct','max_price','cartproducts','quantity'));
	}

    // For Product Detail
	public function getProductDetail($id)
	{
		$bill = 0;
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
		$getproduct = Product::where('id',$id)->where('status','Active')->first();

		$products = Product::where('category_id',$getproduct->category_id)->where('status','Active')->get();

		$product_category = MainCategory::where('id',$getproduct->category_id)->where('status','Active')->first();
		$count_review = Review::where('product_id',$id)->count();
		$reviews = Review::where('product_id',$id)->orderBy('created_at','desc')->get();
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $pro){
				$product = Product::where('id',$pro->product_id)->first();
				$price = $product->sell_price;
				$bill = $bill + $pro->quantity*$price;
			}
		}		

		$cartproducts = array();
		$quantity = array();
		$cnt = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}
		
		return view('wealthmore.product-detail',compact('maincategory','getproduct','product_category','count_review','reviews','products','bill','id','cartproducts','quantity'));
	}

	public function submitReview($id,Request $request)
	{
		$review = new Review;
		$review->name = $request->name;
		$review->email = $request->email;
		$review->rating = $request->rating;
		$review->review = $request->review;
		$review->product_id = $id;        
		$review->save();

		return redirect()->back()->with('message','Thank You For Your Review');
	}

	public function aboutUs()
	{
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
		$testimonials = Testimonial::all();
		$cartproducts = array();
		$quantity = array();
		$cnt = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}
		
		return view('wealthmore.about-us',compact('maincategory','testimonials','cartproducts','quantity'));
	}	

	public function getWishList()
	{
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
		$cartproducts = array();
		$quantity = array();
		$cnt = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}
		else{
			$cartproducts = null;
			$quantity = 0;
		}

		return view('wealthmore.wishlist',compact('maincategory','cartproducts','quantity'));
	}	

	public function getCheckout()
	{
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();
		$countries = Country::all();
		$cartproducts = array();
		$quantity = array();
		$cnt = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}
		else{
			$cartproducts = null;
			$quantity = 0;
		}

		return view('wealthmore.checkout',compact('maincategory','countries','cartproducts','quantity'));
	}

	 //Dependent DropDown Of Countries , States & Cities
	public function getStateList(Request $request)
	{
		$request->country_id;
		$states = State::where("country_id",$request->country_id)->get();
		return response()->json($states);
	}

	public function getCityList(Request $request)
	{
		$request->state_id;
		$cities = City::where("state_id",$request->state_id)->get();
		$cities;
		
		return response()->json($cities);
	}


	// To return cart view
	public function getCart(Request $request)
	{
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();	
		$cartproducts = array();
		$quantity = array();
		$cnt = 0;

		$allproducts = array();
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}
		else{
			$cartproducts = null;
			$quantity = 0;
		}

		return view('wealthmore.cart',compact('maincategory','cartproducts','quantity'));
	}

	//Category Wise Products Selection
	public function getCategoryProducts($id,Request $request){
		$subcat = $request->subcategory;
		$keyword = $request->product_keyword;
		$max_price = Product::where('status','Active')->max("sell_price");

		$cartproducts = array();
		$quantity = array();
		$cnt = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
				$quantity[$cnt] = $cart->quantity;
				$cnt++;
			}
		}
		else{
			$cartproducts = null;
			$quantity = 0;
		}

		$lastproduct = Product::where('status','Active')->orderBy('created_at','desc')->first();
		
		$total = array();
		$i=0;
		$maincategory = MainCategory::where('category_id',0)->where('status','Active')->get();

		foreach($maincategory as $row){
			$count=0;
			$count=Product::where('category_id',$row->id)->where('status','Active')->count();        	
			$category = MainCategory::where('category_id',$row->id)->get();
			if($category){
				foreach($category as $col){
					$count+=Product::where('category_id',$col->id)->where('status','Active')->count();        		
					$subcategory = MainCategory::where('category_id',$col->id)->get();
					if($subcategory){
						foreach($subcategory as $set){
							$count+=Product::where('category_id',$set->id)->count();       			
						}
					}
				}
				if($count==0)
					$total[$i++] = 0;
				else					
					$total[$i++] = $count;   
			}
		}

		$searched_products = array();
		$cat_ids = array();

		$cat_ids[0] = MainCategory::where('id',$id)->where('status','Active')->first();

		$cid = 1;
		$cnt_pro = 0;
		$idss = MainCategory::where('category_id',$id)->where('status','Active')->get();
		if($idss != null){
			foreach($idss as $ids){
				$cat_ids[$cid++] = $ids;
				$idsss = MainCategory::where('category_id',$ids->id)->where('status','Active')->get();
				if($idsss != null){
					foreach($idsss as $ds){
						$cat_ids[$cid++] = $ds;
						
					}
				}
			}
		}

		
		for($j=0 ; $j<$cid ; $j++){
			$products = Product::where('category_id',$cat_ids[$j]->id)->get();
			if($products != null){
				foreach($products as $pro){
					$searched_products[$cnt_pro++] = $pro;
				}
			}			
		}
		
		return view('wealthmore.categorywise-products',compact('maincategory','searched_products','total','lastproduct','max_price','cartproducts','quantity','id'));
	}

	// For Ajax Call (Adding product to cart)
	public function addToCart(Request $request)
	{
		$count = new StdClass;
		$cart = new Cart;
		$product_id = $request->id;
		if (session()->get('cart') != null && session()->get('count') != null){
			$cart = session()->get('cart');
			$count = session()->get('count');			
		}
		else {
			$cart = array();
			$count=0;
		}

		if (isset($cart[$product_id]->quantity)){
			$cart[$product_id]->quantity = $cart[$product_id]->quantity + 1;
		}
		else{
			$item = new StdClass;
			$item->product_id = $product_id;
			$item->quantity = 1;
			$cart[$product_id] = $item;
			$count++;
		}

		$qnty = $cart[$product_id]->quantity;

		$request->session()->put('cart',$cart);
		$request->session()->put('count',$count);		

		if(!$cart)
		{
			echo "Something Went Worng";        
		}
		return response()->json(['status' => 200, 'message' => 'data retrieved', 'quant' => $qnty]); 
	}

    // For Ajax Call (Adding product to cart)
	public function deleteSessionData(Request $request,$id)
	{	
		$flag = 1 ;
		$count = session()->get('count');
		$count--;
		$bill = 0;
		$showcount = $count;
		$cart = session()->get('cart');
		$cart[$request->id] = null;
		$cart = array_filter($cart);
		$request->session()->put('cart',$cart);
		$request->session()->put('count',$count);
		foreach(session()->get('cart') as $pro){
			$product = Product::where('id',$pro->product_id)->first();
			$price = $product->sell_price;
			$bill = $bill + $pro->quantity*$price;
		}
		if($flag == 1)		
			return response()->json(['status' => 200, 'message' => 'Data Deleted Successfully', 'showcount' => $showcount,'bill' => $bill]);
		else		
			return response()->json(['status' => 201, 'message' => 'Something Went Wrong']);	
	}

	public function increment(Request $request,$id)
	{
		$response = new StdClass;
		$response->status = 200;
		$response->quant = 0;

		$cart = session()->get('cart');
		$ids = $request->id ;	
		if (array_key_exists($ids,$cart)) {
			$cart[$ids]->quantity = $cart[$ids]->quantity + 1;
			$response->quant = $cart[$ids]->quantity;
			echo "data updated ".$cart[$ids]->quantity;
		}                    
		else {
			echo "New inserted quantity"; 
			$cart[$ids]->quantity = 1;    
			$response->quant = $cart[$ids]->quantity;        
		}  
		$request->session()->put('cart', $cart);

		return response()->json($response);
	}

	public function decrement(Request $request,$id)
	{
		$response = new StdClass;
		$response->status = 200;
		$response->quant = 0;

		$cart = session()->get('cart');
		$ids = $request->id ;		

		if($cart[$ids]->quantity >0){
			$cart[$ids]->quantity = $cart[$ids]->quantity -1;
		}
		else { 

		}
		
		
		// if ($cart[$ids]->quantity > 0) {
		// 	$cart[$ids]->quantity = $cart[$ids]->quantity - 1;
		// 	$response->quant = $cart[$ids]->quantity;
		// 	echo "data updated";
		// }                    
		// else {
		// 	echo "No quantity in Cart"; 
		// 	$cart[$ids]->quantity = 0;    
		// 	$response->quant = $cart[$ids]->quantity;        
		// }  
		$request->session()->put('cart', $cart);
		return response()->json($response);
	}

	// For Ajax Call (Adding product to cart)
	public function addToWishList(Request $request)
	{
		$wishlist = new Cart;
		$product_id = $request->id;
		if (session()->get('wishlist') != null){
			$wishlist = session()->get('wishlist');
		}
		else 
			$wishlist = array();
		if (isset($wishlist[$product_id]->quantity)){
			$wishlist[$product_id]->quantity = $wishlist[$product_id]->quantity + 1;
		}
		else{
			$item = new StdClass;
			$item->product_id = $product_id;
			$item->quantity = 1;
			$wishlist[$product_id] = $item;
		}

		$request->session()->put('wishlist',$wishlist);

		if(!$wishlist)
		{
			echo "Something Went Worng";        
		}
		return response()->json(['status' => 200, 'message' => 'data retrieved']); 
	}

    // For Ajax Call (Adding product to wishlist)
	public function deleteWishList(Request $request,$id)
	{	
		$flag = 1;
		$wishlist = session()->get('wishlist');
		$wishlist[$request->id] = null;
		$wishlist = array_filter($wishlist);
		$request->session()->put('wishlist',$wishlist);
		if($flag == 1)		
			return response()->json(['status' => 200, 'message' => 'Data Deleted Successfully']);
		else		
			return response()->json(['status' => 201, 'message' => 'Something Went Wrong']);	
	}

	//Delete All Items From WishList
	public function clearWishList()
	{	
		$wishlist = session()->forget('wishlist');
		if($wishlist == null)
			return response()->json(['status' => 200, 'message' => 'Data Deleted Successfully']);
		else		
			return response()->json(['status' => 201, 'message' => 'Something Went Wrong']);	
	}

	//Search All
	public function searchAll(){
		$product = Product::where('status','Active')->get();
		return response()->json(['status' => 200, 'message' => 'Data Retrieved']);
	}

	// Add All To Cart
	public function addAllToCart(Request $request)
	{
		$wishlist = new Cart;
		if(session()->get('wishlist') != null){
			$wishlist = session()->get('wishlist');				

			foreach($wishlist as $list){
				$cart = new Cart;
				$product_id = $list->product_id;
				if (session()->get('cart') != null){
					$cart = session()->get('cart');
				}
				else 
					$cart = array();
				if (isset($cart[$product_id]->quantity)){
					$cart[$product_id]->quantity = $list->quantity ;
				}
				else{
					$item = new StdClass;
					$item->product_id = $product_id;
					$item->quantity =$list[$product_id]->quantity;
					$cart[$product_id] = $item;
				}
				$request->session()->put('cart',$cart);
				if(!$cart)
				{
					$message = "Something Went Worng";        
				}
				else{
					$message = "Data Added To Cart";
				}
			}
		}
		session()->flush('wishlist');
		return response()->json(['status' => 200, 'message' => $message]);
	}

	public function productsCat(Request $request){
		$searched_products = array();
		$count = 0 ;
		$id = array();
		$i=1;
		$id[0] = $request->cat_id;
		if($id[0] == 0)
		{
			$products = Product::where('status','Active')->get();
			foreach($products as $pro){
				$searched_products[$count++] = $pro;
			}
		}
		else{
			$cat_id = MainCategory::where('category_id',$id[0])->where('status','Active')->get();
			if($cat_id != null){
				foreach($cat_id as $cat){
					$id[$i] = $cat->id;
					$sub_id =MainCategory::where('category_id',$id[$i])->where('status','Active')->get();
					$i++;
					if($sub_id != null){
						foreach($sub_id as $sub){
							$id[$i] = $sub->id;
							$i++;
						}
					}
				}
			}
			for($j=0 ; $j<$i ; $j++){
				$products= Product::where('category_id',$id[$j])->where('status','Active')->get();
				foreach($products as $pro){
					$searched_products[$count++] = $pro;
				}
			}	
		}		
		return view('wealthmore.product_cat',compact('searched_products'));
	}

	//Filter products on the basis of there prices
	public function priceFilter(Request $request){
		$max = $request->max_value;
		$min = $request->min_value;
		$id = $request->category_id;

		$searched_products = Product::where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('status','Active')->get();					
		return view('wealthmore.product_cat',compact('searched_products'));
	}


	//For Category Wise Product Filter

	public function categoryWiseProduct(Request $request){
		$searched_products = array();
		$count = 0 ;
		$id = array();
		$i=1;
		$id[0] = $request->cat_id;
		if($id[0] == 0)
		{
			$products = Product::all();

			foreach($products as $pro){
				$searched_products[$count++] = $pro;
			}

		}
		else{
			$cat_id = MainCategory::where('category_id',$id[0])->where('status','Active')->get();
			if($cat_id != null){
				foreach($cat_id as $cat){
					$id[$i] = $cat->id;
					$sub_id =MainCategory::where('category_id',$id[$i])->where('status','Active')->get();
					$i++;
					if($sub_id != null){
						foreach($sub_id as $sub){
							$id[$i] = $sub->id;
							$i++;
						}
					}
				}
			}

			for($j=0 ; $j<$i ; $j++){
				$products= Product::where('category_id',$id[$j])->where('status','Active')->get();
				foreach($products as $pro){
					$searched_products[$count++] = $pro;
				}
			}	
		}

		// return response()->json($searched_products);
		return view('wealthmore.cat-wise-product',compact('searched_products'));

	}

	public function checkoutForm(Request $request,$id){
		$cart = new Cart;
		$order = new Order;
		$address = new Address; 
		$check = new Address; 

		
		$checkSessionID = Cart::where('session_id',session()->getId())->first();
		
		if($checkSessionID != null){

		}
		else{
			$cart->session_id = session()->getId();
			$cart->user_id = $id;
			$cart->cart = json_encode(session()->get('cart'));
			$cart->date = date('Y-m-d H:m:s');
			$cart->save();
		}		

		$check = Address::where('user_id',$request->member_id)->first();
		
		if($check != null){
			
			$check->address = $request->address;
			$check->country_id = 1;
			$check->state_id = $request->state_id;
			$check->city_id = $request->city_id;
			$check->pin_code = $request->pin_code;
			$check->phone = $request->phone;
			$check->update();
		}
		else{
			$address->user_id = $id;
			$address->address = $request->address;
			$address->country_id = 1;
			$address->state_id = $request->state_id;
			$address->city_id = $request->city_id;
			$address->pin_code = $request->pin_code;
			$address->phone = $request->phone;
			$address->save();
		}		

		$address_id = Address::where('user_id',$id)->first();
		$order->user_id = $id;
		$order->address_id =  $address_id->id;
		$order->price =  $request->total_bill;
		$order->payment_method =  'payumoney';
		$order->date = date('Y-m-d H:m:s');
		$order->save();

		$order_id = Order::where('user_id',$id)->first();
		$i=0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$order_item = new OrderItem;
				$order_item->order_id = $order_id->id;
				$order_item->item_id = $cart->product_id;
				$price = Product::where('id',$cart->product_id)->first();
				$order_item->quantity	= $cart->quantity;
				$order_item->price = $cart->quantity * $price->sell_price;
				$order_item->save();
			}	
		}

		$checkorder = Order::where('user_id',$id)->orderBy('created_at','DESC')->first();
		
		$user_info = Member::where('Member.id',$id)->leftjoin('users','Member.user_id','=','users.id')->select('Member.username','users.phone','users.email')->first();
		return view('wealthmore.payment',compact('user_info','checkorder'));

	}

	public function checkoutForm1(Request $request,$id){
		$success = "";
		if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') == 0){
	//Request hash
			$contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';	
			if(strcasecmp($contentType, 'application/json') == 0){
				$data = json_decode(file_get_contents('php://input'));
				$hash=hash('sha512', $data->key.'|'.$data->txnid.'|'.$data->amount.'|'.$data->pinfo.'|'.$data->fname.'|'.$data->email.'|||||'.$data->udf5.'||||||'.$data->salt);
				$json=array();			
			// $request->session()->put('success',$hash);
				$json['success'] = $hash;
				echo json_encode($json);		
			}
			exit(0);
		}

	}  

	public function getSuccess(Request $request){
		$postdata = $_POST;
		
		$user = User::where('email',$postdata['email'])->first();
		$member = Member::where('user_id',$user->id)->first();	

		$updateorder = new Order;

		$updateorder = Order::where('user_id',$member->id)->orderBy('created_at','DESC')->first();
		$updateorder->transaction_id = $postdata['txnid'];
		$updateorder->encrypted_payment_id = $postdata['encryptedPaymentId'];
		$updateorder->status = $postdata['txnStatus'];

		$updateorder->update();

		$transaction_details = Order::where('user_id',$updateorder->user_id)->whereNotNull('transaction_id')->orderBy('updated_at','DESC')->get();

		session()->forget('cart');
		return view('wealthmore.result',compact('transaction_details'));
	}

	//To show session data on Mini Cart
	public function showMiniCart(){
		$cartproducts = array();
		$quantity = array();
		$i = 0;
		$bill = 0;
		if(session()->get('cart') != null){
			foreach(session()->get('cart') as $cart){
				$cartproducts[$i] = Product::where('id',$cart->product_id)->first();
				$quantity[$i] = $cart->quantity;
				$i++;
			}
			$j=0;
			foreach(session()->get('cart') as $cart){
				$quantity[$j];
				$j++;
			}

			foreach(session()->get('cart') as $pro){
				$product = Product::where('id',$pro->product_id)->first();
				$price = $product->sell_price;
				$bill = $bill + $pro->quantity*$price;
			}
		}	

		return view('wealthmore.minicart',compact('cartproducts','quantity','bill'))->render();
	}

	//Show Sorted Products on Search Category Page
	public function sorting(Request $request){
		$searched_products = array();
		$i=0;
		$sort_type = $request->sort_type;
		if($sort_type == 'name'){
			$products = Product::where('status','Active')->orderBy('name','ASC')->get();
			foreach($products as $pro){
				$searched_products[$i++] = $pro;
			}
		}
		else if($sort_type == 'new'){
			$products = Product::where('status','Active')->orderBy('created_at','DESC')->get();
			foreach($products as $pro){
				$searched_products[$i++] = $pro;
			}
		}
		else if($sort_type == 'low-high'){
			$products = Product::where('status','Active')->orderBy('sell_price','ASC')->get();
			foreach($products as $pro){
				$searched_products[$i++] = $pro;
			}
		}
		else if($sort_type == 'high-low'){
			$products = Product::where('status','Active')->orderBy('sell_price','DESC')->get();
			foreach($products as $pro){
				$searched_products[$i++] = $pro;
			}
		}

		return view('wealthmore.product_cat',compact('searched_products'));
	}

	public function addProductToCart(Request $request)
	{
		$count = new StdClass;
		$cart = new Cart;
		$product_id = $request->id;
		$product_quantity = $request->quantity;
		$money_type = $request->money_type;

		$product=Product::where('id',$product_id)->first(); 

		if (session()->get('cart') != null && session()->get('count') != null){
			$cart = session()->get('cart');
			$count = session()->get('count');			
		}
		else {
			$cart = array();
			$count=0;
		}

		if (isset($cart[$product_id]->quantity)){
			$cart[$product_id]->quantity = $cart[$product_id]->quantity + 1;
		}
		else{
			$item = new StdClass;

			$key = $money_type;
			if($key == 'jbv'){
				$item->$key = $product->jbv;
			}
			else {
				$item->$key = $product->rvv;
			}			
			$item->product_id = $product_id;
			$item->quantity = $product_quantity;
			$cart[$product_id] = $item;
			$count++;

		}

		$qnty = $cart[$product_id]->quantity;
		$subTotal = 0;
		$request->session()->put('cart',$cart);
		$request->session()->put('count',$count);		
		$product = Product::where('id',$product_id)->first();
		$total = $product->sell_price * $cart[$product_id]->quantity;

		foreach(session()->get('cart') as $carts){
			$get = Product::where('id',$carts->product_id)->first();
			$subTotal = $subTotal + ($get->sell_price * $carts->quantity);
		}

		if(!$cart)
		{
			echo "Something Went Worng";        
		}
		return response()->json(['status' => 200, 'message' => 'data retrieved', 'quant' => $qnty, 'total' => $total, 'subTotal' => $subTotal]); 
	}



	public function decreaseQuantity(Request $request)
	{
		$count = new StdClass;
		$cart = new Cart;
		$product_id = $request->id;
		$product_quantity = $request->quantity;
		$subTotal = 0;

		$product = Product::where('id',$product_id)->first();

		$cart = session()->get('cart');
		if($cart[$product_id]->quantity > 0){
			$cart[$product_id]->quantity = $cart[$product_id]->quantity - 1;
		}		

		$qnty = $cart[$product_id]->quantity;
		$total = $product->sell_price * $cart[$product_id]->quantity;
		$request->session()->put('cart',$cart);


		foreach(session()->get('cart') as $carts){
			$get = Product::where('id',$carts->product_id)->first();
			$subTotal = $subTotal + ($get->sell_price * $carts->quantity);
		}

		return response()->json(['status' => 200, 'message' => 'data retrieved', 'quant' => $qnty, 'total' => $total, 'subTotal' => $subTotal]); 
	}




	public function sendContactUsForm(Request $request)
	{
		$response = new StdClass;
		$response->status = 200;
		$response->msg = 'Something went wrong';
		$contact_user = new Contact;

		$name = $request->contact_name;
		$email = $request->contact_email;
		$subject = $request->contact_subject;
		$content = $request->contact_message;

		$contact_user->name = $name;
		$contact_user->email = $email;
		$contact_user->subject = $subject;
		$contact_user->message_content = $content;

		$contact_user->save();
		// if($contact_user){

		// 	$data = array('Name'=>$contact_user->name,'Email'=>$contact_user->email, 'Subject'=>$contact_user->subject, 'Message'=>$contact_user->message_content);
		// 	Mail::send('wealthmore.contact-form', $data, function($message) use ( $name,$email,$subject,$content)
		// 	{   
		// 		$message->from('no-reply@m-biz.in', 'Mailer');
		// 		$message->to('jyotika@backstagesupporters.com', 'Jyotika')->subject('Query Mail');
		// 	});		

		// 	$response->msg ="Thank you .We will reply you soon";
		// 	return response()->json($response);
		// }
		// else{
		// 	echo " Something Went Wrong";
		// 	$response->msg =" Something Went Wrong";
		// }

		$response->msg ="Thank you .We will reply you soon";

		return response()->json($response);
		exit();             
	}



	public function submitNewsletter(Request $request)
	{
		$response = new StdClass;
		$response->status = 200;
		$response->message = 'Something went wrong';
		$newsletter = new Newsletter;
		$checkemail = Newsletter::where('email',$request->email)->first();

		if($checkemail){             
			$response->message = "Already Registered. Thank You"; 
			return response()->json($response);                         
		}
		else{
			if($request->email == null){
				$response->message = 'First Enter Your Email';
				$response->email = $request->email;                
				return response()->json($response);
			}
			else{
				$newsletter->email = $request->email;         
				$newsletter->save();
				if($newsletter){
					$response->message ="Thank you .We will reply you soon";
					return response()->json($response);
				}
				else{
					echo " Something Went Wrong";
				}
			}
		}
	} 



	public function updateMemberDetail(Request $request){
		$response = new StdClass;
		$response->message = "Something Went Wrong";
		$getMember = MemberDetail::where('member_id',$request->user_id)->first();
		$address = new Address;

		if(!empty($getMember)){
			$getMember->gender = $request->gender;
			$getMember->bank_name = $request->bank_name;
			$getMember->pan = $request->pan;
			$getMember->mobile_phone = $request->mobile_phone;
			$getMember->bank_account_number = $request->bank_account_number ;
			$getMember->bank_account_holder = $request->bank_account_holder ;
			$getMember->bank_branch = $request->bank_branch ;
			$getMember->date_of_birth = $request->date_of_birth ;
			$getMember->beneficiary_name = $request->beneficiary_name ;
			$getMember->address = $request->address ;
			$getMember->bank_address = $request->bank_address ;
			$getMember->adhaar = $request->adhaar ;
			$getMember->ifsc = $request->ifsc ;
			$getMember->relation_with_beneficiary = $request->relation_with_beneficiary ;

    	// $getMember->state = 'Delhi' ;
			$getMember->state = $request->state ;
			$getMember->status = 1 ;
			$getMember->update();

			$address->user_id = $request->user_id;
			$address->address = $request->address;
			$address->country_id = 1;
			$address->state_id = $request->state;
			$address->city_id = $request->city;
			$address->pin_code = $request->pin_code;
			$address->phone = $request->mobile_phone;

			$address->save(); 


			$response->message = "Data Updated Successfully";
		}

		return response()->json($response);

	}
}
