<?php

namespace App\Http\Controllers;
use App\MainCategory;
use App\Product;
use App\Banner;
use App\Review;
use App\Newsletter;
use App\AddToCart;
use StdClass;


use Illuminate\Http\Request;


class WealthmoreStaticPageController extends Controller
{
    // For Wealthmore Ecommerce Home Page
    public function index()
    {
        $products = Product::all();
        $products=Product::where('products.status','Active')->leftjoin('main_categories','main_categories.id','=','products.category_id')->select('main_categories.Mcategory_name','products.*')->orderBy('products.id','desc')->get();
        $maincategory = MainCategory::where('category_id',0)->get();
        $banners = Banner::all();
        return view('ecommerce.index',compact('maincategory','products', 'banners'));
    }

    // For Product Detail
    // public function getProductDetail($id)
    // {
    //     $maincategory = MainCategory::where('category_id',0)->get();
    //     $product = Product::where('id',$id)->first();

    //     return view('ecommerce.product-detail',compact('maincategory','product'));
    // }

    // public function submitReview($id,Request $request)
    // {
    //     $review = new Review;
    //     $review->name = $request->name;
    //     $review->email = $request->email;
    //     $review->rating = $request->rating;
    //     $review->review = $request->review;
    //     $review->product_id = $id;
        
    //     $review->save();
        
    //     return redirect()->back()->with('message','Thank You For Your Review');
    // }

    // public function submitNewsletter(Request $request)
    // {
    //     $response = new StdClass;
    //     $response->status = 200;
    //     $response->message = 'Something went wrong';
    //     $newsletter = new Newsletter;
    //     $checkemail = Newsletter::where('email',$request->email)->first();

    //     if($checkemail)
    //     {             
    //         $response->message = "Already Registered. Thank You"; 
    //         return response()->json($response);
                         
    //     }

    //     else
    //     {
    //         if($request->email == null)
    //         {
    //             $response->message = 'First Enter Your Email';
    //             $response->email = $request->email;
                
    //             return response()->json($response);
    //                        }

    //         else{
    //             $newsletter->email = $request->email;         
    //             $newsletter->save();
    //             if($newsletter){
    //                 $response->message ="Data Inserted Successfully";
    //                 return response()->json($response);
    //             }

    //             else{
    //                 echo " Something Went Wrong";
    //             }
    //         }
    //     }
    // }    

    // For Product List
    // public function getProductList($id)
    // {
    //     $maincategory = MainCategory::where('category_id',0)->get();
    //     $product = Product::where('category_id',$id)->get();        
    //     return view('ecommerce.product-list2',compact('maincategory','product'));
    // }

    // For Privacy Policy Page
    public function getPrivacyPolicy()
    {
        $maincategory = MainCategory::where('category_id',0)->get();
        $cartproducts = array();
        $quantity = array();
        $cnt = 0;
        if(session()->get('cart') != null){
            foreach(session()->get('cart') as $cart){
                $cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
                $quantity[$cnt] = $cart->quantity;
                $cnt++;
            }
        }
               
        return view('wealthmore.privecypolicy',compact('maincategory','quantity','cartproducts'));
    }

    // For Return WishList
    // public function getWishList()
    // {
    //     $maincategory = MainCategory::where('category_id',0)->get();
    //     return view('wealthmore.wishlist',compact('maincategory'));
    // }

    // For Cart
    // public function getCart()
    // {
    //     $maincategory = MainCategory::where('category_id',0)->get();
    //     return view('wealthmore.cart',compact('maincategory'));
    // }

    // For Return Policy Page
    public function getReturnPolicy()
    {
        $maincategory = MainCategory::where('category_id',0)->get();
        $cartproducts = array();
        $quantity = array();
        $cnt = 0;
        if(session()->get('cart') != null){
            foreach(session()->get('cart') as $cart){
                $cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
                $quantity[$cnt] = $cart->quantity;
                $cnt++;
            }
        }
               
        return view('wealthmore.returnpolicy',compact('maincategory','quantity','cartproducts'));
    }

    // For Terms and Conditions Page
    public function getTnc()
    {
        $maincategory = MainCategory::where('category_id',0)->get();
        $cartproducts = array();
        $quantity = array();
        $cnt = 0;
        if(session()->get('cart') != null){
            foreach(session()->get('cart') as $cart){
                $cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
                $quantity[$cnt] = $cart->quantity;
                $cnt++;
            }
        }
               
        return view('wealthmore.tnc',compact('maincategory','quantity','cartproducts'));
    }

    // For Contact Us Page
    public function getContact()
    {
        $maincategory = MainCategory::where('category_id',0)->get();
        $cartproducts = array();
        $quantity = array();
        $cnt = 0;
        if(session()->get('cart') != null){
            foreach(session()->get('cart') as $cart){
                $cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
                $quantity[$cnt] = $cart->quantity;
                $cnt++;
            }
        }
               
        return view('wealthmore.contact',compact('maincategory','quantity','cartproducts'));
    }

    // For Contact Us Page
    public function getAbout()
    {
        $maincategory = MainCategory::where('category_id',0)->get();
        $cartproducts = array();
        $quantity = array();
        $cnt = 0;
        if(session()->get('cart') != null){
            foreach(session()->get('cart') as $cart){
                $cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
                $quantity[$cnt] = $cart->quantity;
                $cnt++;
            }
        }
               
        return view('wealthmore.about-us',compact('maincategory','quantity','cartproducts'));
    }

    // For Checkout Page
    // public function getCheckout()
    // {
    //     $maincategory = MainCategory::where('category_id',0)->get();
    //     return view('ecommerce.checkout',compact('maincategory'));
    // }

    // For Checkout Page
    public function getFaq()
    {
        $maincategory = MainCategory::where('category_id',0)->get();
        $cartproducts = array();
        $quantity = array();
        $cnt = 0;
        if(session()->get('cart') != null){
            foreach(session()->get('cart') as $cart){
                $cartproducts[$cnt] = Product::where('id',$cart->product_id)->first();
                $quantity[$cnt] = $cart->quantity;
                $cnt++;
            }
        }
               
        return view('wealthmore.faq',compact('maincategory','quantity','cartproducts'));
    }

    // For Search Page
    // public function getSearchProduct(Request $request)
    // {

    //     $subcat = $request->subcategory;
    //     $keyword = $request->product_keyword;

    //     if($subcat>0 and $keyword!="" )
    //     {
    //         $searched_products = Product::where('category_id',$subcat)->where('name','LIKE','%'.$keyword.'%')->orderby('name','ASC')->get();
    //     }
    //     else if($subcat>0)
    //     {
    //         $searched_products = Product::where('category_id',$subcat)->orderby('name','ASC')->get();          
    //     }

    //     else if($keyword != "")
    //     {
    //         $searched_products = Product::where('name','LIKE','%'.$keyword.'%')->orderby('name','ASC')->get();          
    //     }
    //     else if($subcat==0)
    //     {
    //         $searched_products = Product::orderby('name','ASC')->get();           
    //     }       

    //     $maincategory = MainCategory::where('category_id',0)->get();

    //     return view('ecommerce.search-product',compact('maincategory','searched_products'));
    // }

    // For Ajax Call by Category Wise
    // public function productsCat(Request $request)
    // {
    //     $sub_cat_id = $request->id;
    //     echo $sub_cat_id;

    //     $data = Product::where('category_id',$sub_cat_id)->get();
    // }

    // // For Ajax Call (Adding product to cart)
    // public function addToCart(Request $request)
    // {
    //     $cart = new AddToCart;
    //     $product_id = $request->id;
    //     if (session()->get('cart') != null){
    //         $cart = session()->get('cart');
    //     }
    //     else 
    //         $cart = array();
    //     if (isset($cart[$product_id]->quantity)){
    //         $cart[$product_id]->quantity = $cart[$product_id]->quantity + 1;
    //     }
    //     else{
    //         $item = new StdClass;
    //         $item->product_id = $product_id;
    //         $item->quantity = 1;
    //         $cart[$product_id] = $item;
    //     }

    //     $request->session()->put('cart',$cart);

    //     if(!$cart)
    //     {
    //         echo "Something Wen Worng";        
    //     }
    //     return response()->json(['status' => 200, 'message' => 'data retrieved']); 
    // }

    // public function showToCart(Request $request)
    // {
    //     $cart = array();

    //     $response = new StdClass;
    //     $response->status = 200;
    //     $response->message = 'Something went wrong';

    //     if (session()->get('cart') == null){
    //         $response->message = "Nothing in Cart"; 
    //         return response()->json($response);
    //            //     }
    //     else{
    //         foreach(session()->get('cart') as $row)
    //         {
    //             $cart = $row;
    //         }
    //         $response->message = "Something in Cart"; 
    //         return response()->json($response);
    //            //     }       
    // }

}
