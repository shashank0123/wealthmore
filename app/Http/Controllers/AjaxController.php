<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use StdClass;
use Validator;
use Log;
use App\Cart;
use App\Product;
use App\Wishlist;
use App\MainCategory;
use App\Contact;
use App\Newsletter;
use App\Models\MemberDetail;
use App\Models\Member;

class AjaxController extends Controller
{

public function getMemberDetail(Request $request){
    $response = new StdClass;
    $response->message = "Something Went Wrong";
    $response->name = "";

     $name = $request->uname;
     $check = Member::where('username',$name)->first();

    if ($check ) {
                    $response->message = 'Valid ID';
                    $response->name = $check->username;
                }  
                else{
                    $response->message = 'Invalid ID';
                } 

    return response()->json($response); 
}

public function checkAvailability(Request $request){
    $response = new StdClass;
    $response->message = "Something Went Wrong";
    
    $check = Member::where('username',$request->uname)->first();

    if ($check ) {
                    $response->message = 'Username already exists. Try another';         
                }   

    return response()->json($response); 
}
	
	// For Ajax Call (Adding product to cart)
	public function addToCart(Request $request)
	{
		$count = new StdClass;
		$cart = new Cart;

		$product_id = $request->id;
		$quant = $request->quantity;
		$color = $request->color;
		$size = $request->size;		
		$purchase = $request->purchase;

		if (session()->get('cart') != null && session()->get('count') != null){
			$cart = session()->get('cart');
			$count = session()->get('count');			
		}
		else {
			$cart = array();
			$count=0;
		}

		if (isset($cart[$product_id]->quantity)){
			$cart[$product_id]->quantity = $quant;
			$cart[$product_id]->purchase_type = $purchase;
			$cart[$product_id]->color = $color;
			$cart[$product_id]->size = $size;
		}
		else{
			$item = new StdClass;
			$item->product_id = $product_id;
			$item->quantity = $quant;
			$item->purchase_type = $purchase;
			$item->color = $color;
			$item->size = $size;
			$cart[$product_id] = $item;
			$count++;
		}

		$qnty = $cart[$product_id]->quantity;

		$request->session()->put('cart',$cart);
		$request->session()->put('count',$count);		

		if(!$cart)
		{
			echo "Something Went Worng";        
		}
		return response()->json(['status' => 200, 'message' => 'data retrieved', 'quant' => $qnty]); 
	}

	// For Ajax Call (Adding product to cart)
	public function addToWishList(Request $request)
	{
		$wishlist = new Wishlist;
		$product_id = $request->id;
		$user_id = $request->user_id;

		$check = Wishlist::where('user_id',$user_id)->where('product_id',$product_id)->first();
		if($check){
		}
		else{
			$wishlist->user_id = $user_id;
			$wishlist->product_id = $product_id;
			$wishlist->user_id = $user_id;
			$wishlist->save();
		}

		return response()->json(['status' => 200, 'message' => 'data retrieved']); 
	}

	public function deleteWishlistProduct(Request $request){
		$wishlist = Wishlist::where('user_id',$request->user_id)->where('product_id',$request->product_id)->delete();

		return response()->json(['message'=>'Deleted Successfuly']);
	}

	public function increamentQuantity(Request $request){
		$cart = session()->get('cart');
		$type = $request->type;
		$bill = 0;

		if($type == 'inc'){
			$cart[$request->product_id]->quantity = $cart[$request->product_id]->quantity +1;			
		}

		if($type == 'dec'){
			$cart[$request->product_id]->quantity = $cart[$request->product_id]->quantity -1;			
		}
		$product = Product::where('id',$request->product_id)->first();

		$amount = $cart[$request->product_id]->quantity * $product->sell_price;		

		session()->put('cart',$cart);

		foreach(session()->get('cart') as $pro){
			$product = Product::where('id',$pro->product_id)->first();
			$price = $product->sell_price;
			$bill = $bill + $pro->quantity*$price;
		}


		return response()->json(['message'=>'Deleted Successfuly','amount'=>$amount,'bill'=>$bill]);


	}

	public function removeCartProduct(Request $request){
		
		$bill = 0;
		$cart = session()->get('cart');
		$cart[$request->product_id] = null;
		$cart = array_filter($cart);
		$request->session()->put('cart',$cart);
		$count = 0;
		
		foreach(session()->get('cart') as $pro){
			$product = Product::where('id',$pro->product_id)->first();
			$price = $product->sell_price;
			$bill = $bill + $pro->quantity*$price;
			$count++;
		}

		return response()->json(['status' => 200, 'message' => 'Data Deleted Successfully','bill' => $bill,'count'=>$count]);
		
	}

	public function destroyCart(Request $request){
		$cart = session()->get('cart');
		foreach(session()->get('cart') as $pro){
			$cart[$pro->product_id] = NULL;
		}

		$cart = array_filter($cart);
		$request->session()->put('cart',$cart);


		return response()->json(['status' => 200, 'message' => 'Data Deleted Successfully']);

	}

	public function submitNewsletter(Request $request){
		$email = $request->email;		
		$arr = explode('@',$email);
		$count=0;

		foreach($arr as $a){
			$count++;
		}
		if(!empty($arr[1])){
			$exp = explode('.',$arr[1]); 
			foreach($exp as $a ){				
				$count++;
			}
		}		

		if($count == 4){
			
			$check = Newsletter::where('email',$request->email)->first();
			$message = 'Something Went Wrong.';
			if(!empty($check)){
				$message = 'You have already applied for the newsletter.';
			}
			else{
				$news = new Newsletter;
				$news->email = $request->email;
				$news->status = 'Deactive';
				$news->save();
				$message = "Thank You. We will reply you soon.";
			}
		}

		else{
			$message ='Invalid Email Format';		
		}
		
		return response()->json(['message'=>$message]);
	}

	public function submitContactForm(Request $request){
		$email = $request->email;		
		$count=0;

		$arr = explode('@',$email);

		foreach($arr as $a){
			$count++;
		}
		if(!empty($arr[1])){
			$exp = explode('.',$arr[1]); 
			foreach($exp as $a ){				
				$count++;
			}
		}	

		if($count == 4){
		$news = new Contact;
		$news->email = $request->email;
		$news->name = $request->name;
		$news->subject = $request->subject;
		$news->message_content = $request->message;
		$news->response_status = 'pending';
		$news->save();
		$message = "Thank You For Your Feedback. Have a Nice Day.";
		}
		else{
			$message = "Invalid Email Format";
		}
		return response()->json(['message'=>$message]);
	}

	

	public function getFilteredProducts(Request $request){
		$color = $request->color;
		$size = $request->size;
		$weight = $request->weight;
		$dmin = str_replace("Rs.","",$request->min);
		$dmax = str_replace("Rs.","",$request->max);

		$min = str_replace(" ","",$dmin);
		$max = str_replace(" ","",$dmax);

		$cat_id = $request->cat_id;
		$sort = $request->sort;

		// echo $color." / ".$size." / ".$weight." / ".$min." / ".$max." / ".$cat_id." / ".$sort." / ";
		$id = array();
		$i=1; 
		$searched_products = array();

		$cat = MainCategory::where('slug_name',$cat_id)->first();
		
		$id[0] = $cat->id;

		$main = MainCategory::where('category_id',$cat->id)->where('status','Active')->get();

		if(!empty($main)){
			foreach ($main as $m ) {
				$id[$i++] = $m->id;
				$sub = MainCategory::where('category_id',$m->id)->get();
				if(!empty($sub)){
					foreach($sub as $s){
						$id[$i++] = $s->id;
					}
				}
			}
		}	


				//die;
		//All null
		if($color=="" && $size=="" && $weight==""){
			$j=0;
			// echo "all are null";
			foreach($id as $i){
				$products = Product::where('category_id',$i)->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('status','Active')->get();
				
				if(!empty($products)){
					foreach($products as $pro){
						$searched_products[$j++] = $pro;
					}
				}
			}
		}
		//Sort full
		elseif($color=="" && $size=="" && $weight!=""){
			$j=0;
			// echo "weight not null only";
			foreach($id as $i){
				$products = Product::where('category_id',$i)->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('weight',$weight)->where('status','Active')->get();

				if(!empty($products)){
					foreach($products as $pro){
						$searched_products[$j++] = $pro;
					}
				}
			}
		}
		//Weight full
		elseif($color=="" && $weight=="" && $size!=""){
			$j=0;
			// echo "size not null only";
			foreach($id as $i){
				$products = Product::where('category_id',$i)->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%,'.$size.',%')->orWhere('product_size','LIKE',$size.",%")->orWhere('product_size','LIKE','%,'.$size)->where('status','Active')->get();

				if(!empty($products)){
					foreach($products as $pro){
						$searched_products[$j++] = $pro;
					}
				}
			}
		}
		//Size full
		elseif($size=="" && $weight=="" && $color!=""){
			$j=0;
			// echo "color not null only";
			foreach($id as $i){
				$products = Product::where('category_id',$i)->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_color',$color)->where('status','Active')->get();

				if(!empty($products)){
					foreach($products as $pro){
						$searched_products[$j++] = $pro;
					}
				}
			}
		}
		//color Full
		elseif($weight=="" && $color!="" && $size!=""){
			$j=0;
			// echo "Weight null only";
			foreach($id as $i){
				$products = Product::where('category_id',$i)->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_color',$color)->where('product_size','LIKE','%,'.$size.',%')->orWhere('product_size','LIKE',$size.",%")->orWhere('product_size','LIKE','%,'.$size)->where('status','Active')->get();

				if(!empty($products)){
					foreach($products as $pro){
						$searched_products[$j++] = $pro;
					}
				}
			}
		}
		//weight & sort full
		elseif($color=="" && $size!="" && $weight!=""){
			$j=0;
			// echo "Color null only";
			foreach($id as $i){
				$products = Product::where('category_id',$i)->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('weight',$weight)->where('product_size','LIKE','%,'.$size.',%')->orWhere('product_size','LIKE',$size.",%")->orWhere('product_size','LIKE','%,'.$size)->where('status','Active')->get();

				if(!empty($products)){
					foreach($products as $pro){
						$searched_products[$j++] = $pro;
					}
				}
			}
		}
		//size & weight full
		elseif($size=="" && $weight!="" && $color!=""){
			$j=0;
			// echo "Size null only";
			foreach($id as $i){
				$products = Product::where('category_id',$i)->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('weight',$weight)->where('product_color',$color)->where('status','Active')->get();

				if(!empty($products)){
					foreach($products as $pro){
						$searched_products[$j++] = $pro;
					}
				}
			}
		}

		else{
			$j=0;
			foreach($id as $i){
				$products = Product::where('category_id',$i)->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('weight',$weight)->where('product_size','LIKE','%,'.$size.',%')->orWhere('product_size','LIKE',$size.",%")->orWhere('product_size','LIKE','%,'.$size)->where('product_color',$color)->where('status','Active')->get();

				if(!empty($products)){
					foreach($products as $pro){
						$searched_products[$j++] = $pro;
					}
				}
			}	
		}

		if(!empty($sort))
		{
			if($sort == 'latest'){
				$created_at = array_column($searched_products, 'created_at');
				array_multisort($created, SORT_DESC, $searched_products);
			}		
			else if($sort == 'l-h'){
				$sell_price = array_column($searched_products, 'sell_price');
				array_multisort($sell_price, SORT_ASC, $searched_products);
			}
			else if($sort == 'h-l'){
				$sell_price = array_column($searched_products, 'sell_price');
				array_multisort($sell_price, SORT_DESC, $searched_products);
			}
			else if($sort == 'a-z'){
				$name = array_column($searched_products, 'name');
				array_multisort($name, SORT_ASC, $searched_products);
			}
			else if($sort == 'z-a'){
				$name = array_column($searched_products, 'name');
				array_multisort($name, SORT_DESC, $searched_products);
			}

		}

		$searched_products = array_unique($searched_products);

		return view('envato-shop.product_cat',compact('searched_products'));
	}

	public function getSearchFilter(Request $request){
		$color = $request->color;
		$size = $request->size;
		$weight = $request->weight;
		$min = $request->min;
		$max = $request->max;
		$cat_id = $request->cat_id;
		$sort = $request->sort;
		$i=1; 
		$searched_products = array();

		// $products = Product::where('name','LIKE',"%".$cat_id."%")->orWhere('short_descriptions','LIKE',"%".$cat_id."%")->orWhere('long_descriptions','LIKE',"%".$cat_id."%")->where('status','Active')->get();
		

		

		// die;
		//All null
		if($color=="" && $size=="" && $weight==""){
			
			// echo "all are null";
			$products = Product::where('name','LIKE',"%".$cat_id."%")->where('short_descriptions','LIKE',"%".$cat_id."%")->where('long_descriptions','LIKE',"%".$cat_id."%")->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('status','Active')->get();	
			$j=0;
			if(!empty($products)){
				foreach($products as $pro){
					$searched_products[$j++] = $pro;
				}
			}		
		}
		//Sort full
		elseif($color=="" && $size=="" && $weight!="" ){
			
			// echo "weight not null";			
			$products = Product::where('name','LIKE',"%".$cat_id."%")->where('short_descriptions','LIKE',"%".$cat_id."%")->where('long_descriptions','LIKE',"%".$cat_id."%")->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('weight',$weight)->where('status','Active')->get();				
			$j=0;
			if(!empty($products)){
				foreach($products as $pro){
					$searched_products[$j++] = $pro;
				}
			}
		}
		//Weight full
		elseif($color=="" && $weight=="" && $size!=""){
			// echo "size not null";
			
			$products = Product::where('name','LIKE',"%".$cat_id."%")->where('short_descriptions','LIKE',"%".$cat_id."%")->where('long_descriptions','LIKE',"%".$cat_id."%")->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_size','LIKE','%,'.$size.',%')->orWhere('product_size','LIKE',$size.",%")->orWhere('product_size','LIKE','%,'.$size)->where('status','Active')->get();
			$j=0;
			if(!empty($products)){
				foreach($products as $pro){
					$searched_products[$j++] = $pro;
				}
			}
		}
		//Size full
		elseif($size=="" && $weight=="" && $color!=""){
			// echo "color not null";
			
			$products = Product::where('name','LIKE',"%".$cat_id."%")->where('short_descriptions','LIKE',"%".$cat_id."%")->where('long_descriptions','LIKE',"%".$cat_id."%")->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_color',$color)->where('status','Active')->get();
			$j=0;
			if(!empty($products)){
				foreach($products as $pro){
					$searched_products[$j++] = $pro;
				}
			}

		}
		//color Full
		elseif($weight=="" && $color!="" && $size!=""){
			// echo "weight null only";
			
			$products = Product::where('name','LIKE',"%".$cat_id."%")->where('short_descriptions','LIKE',"%".$cat_id."%")->where('long_descriptions','LIKE',"%".$cat_id."%")->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('product_color',$color)->where('product_size','LIKE','%,'.$size.',%')->orWhere('product_size','LIKE',$size.",%")->orWhere('product_size','LIKE','%,'.$size)->where('status','Active')->get();
			$j=0;
			if(!empty($products)){
				foreach($products as $pro){
					$searched_products[$j++] = $pro;
				}
			}

		}
		//weight & sort full
		elseif($color=="" && $weight!="" && $size!=""){
			// echo "color null only";
			
			$products = Product::where('name','LIKE',"%".$cat_id."%")->where('short_descriptions','LIKE',"%".$cat_id."%")->where('long_descriptions','LIKE',"%".$cat_id."%")->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('weight',$weight)->where('product_size','LIKE','%,'.$size.',%')->orWhere('product_size','LIKE',$size.",%")->orWhere('product_size','LIKE','%,'.$size)->where('status','Active')->get();
			$j=0;
			if(!empty($products)){
				foreach($products as $pro){
					$searched_products[$j++] = $pro;
				}
			}

			
		}
		//size & weight full
		elseif($size=="" && $weight!="" && $color!=""){
			// echo "size null only";
			
			$products = Product::where('name','LIKE',"%".$cat_id."%")->where('short_descriptions','LIKE',"%".$cat_id."%")->where('long_descriptions','LIKE',"%".$cat_id."%")->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('weight',$weight)->where('product_color',$color)->where('status','Active')->get();

			$j=0;
			if(!empty($products)){
				foreach($products as $pro){
					$searched_products[$j++] = $pro;
				}
			}
		}

		else{
			// echo "nothing null ";
			$j=0;
			$products = Product::where('name','LIKE',"%".$cat_id."%")->where('short_descriptions','LIKE',"%".$cat_id."%")->where('long_descriptions','LIKE',"%".$cat_id."%")->where('sell_price','>=',$min)->where('sell_price','<=',$max)->where('weight',$weight)->where('product_size','LIKE','%,'.$size.',%')->orWhere('product_size','LIKE',$size.",%")->orWhere('product_size','LIKE','%,'.$size)->where('product_color',$color)->where('status','Active')->get();
			if(!empty($products)){
				foreach($products as $pro){
					$searched_products[$j++] = $pro;
				}
			}

		}

		if(!empty($sort))
		{
			if($sort == 'latest'){
				$created_at = array_column($searched_products, 'created_at');
				array_multisort($created, SORT_DESC, $searched_products);
			}		
			else if($sort == 'l-h'){
				$sell_price = array_column($searched_products, 'sell_price');
				array_multisort($sell_price, SORT_ASC, $searched_products);
			}
			else if($sort == 'h-l'){
				$sell_price = array_column($searched_products, 'sell_price');
				array_multisort($sell_price, SORT_DESC, $searched_products);
			}
			else if($sort == 'a-z'){
				$name = array_column($searched_products, 'name');
				array_multisort($name, SORT_ASC, $searched_products);
			}
			else if($sort == 'z-a'){
				$name = array_column($searched_products, 'name');
				array_multisort($name, SORT_DESC, $searched_products);
			}

		}

		// $searched_products = array_unique($searched_products);

		return view('envato-shop.product_cat',compact('searched_products'));
	}
}
