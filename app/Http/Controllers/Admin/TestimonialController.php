<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Testimonial;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexTestimonial()
    {
        $testimonial = Testimonial::all();
        return view('back.ecommerce.testimonial',compact('testimonial'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createTestimonial()
    {
        return view('back.ecommerce.add-testimonial');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeTestimonial(Request $request)
    {
        $testimonial = new Testimonial;

        // for image_url
        if (Input::hasfile('image_url')) {
           $file=Input::file('image_url');
           $file->move(public_path(). '/assetsss/images/AdminProduct/', time().$file->getClientOriginalName());
           $testimonial->image_url=time().$file->getClientOriginalName();           
        }
        $testimonial->client_name = $request->client_name;
        $testimonial->rating = $request->rating;
        $testimonial->review = $request->review;
        $testimonial->city = $request->city;
        $testimonial->status = $request->status;
        $testimonial->save();

        return redirect()->back()->with('message','Data Successfully Inserted');
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editTestimonial($id)
    {
        $testimonial = Testimonial::find($id);
        return view('back.ecommerce.edit-testimonial',compact('testimonial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateTestimonial(Request $request, $id)
    {
        $testimonial = Testimonial::find($id);
         // for image_url
        if (Input::hasfile('image_url')) {
           $file=Input::file('image_url');
           $file->move(public_path(). '/assetsss/images/AdminProduct/', time().$file->getClientOriginalName());
           $testimonial->image_url=time().$file->getClientOriginalName();           
        }
        $testimonial->client_name = $request->client_name;
        $testimonial->rating = $request->rating;
        $testimonial->review = $request->review;
        $testimonial->city = $request->city;
        $testimonial->status = $request->status;
        $testimonial->update();

        return redirect()->back()->with('message','Data Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyTestimonial($id)
    {
        $testimonial = Testimonial::find($id);
        $testimonial->delete();

        return redirect()->back()->with('message','Testimonial Successfully Deleted');
    }
}
