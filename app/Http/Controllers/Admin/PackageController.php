<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Repositories\PackageRepository;
use App\Models\Member;

class PackageController extends Controller
{
	/**
     * The PackageRepository instance.
     *
     * @var \App\Repositories\PackageRepository
     */
    protected $PackageRepository;

    public function __construct(
        PackageRepository $PackageRepository
    ) {
        $this->middleware('admin');
        $this->PackageRepository = $PackageRepository;
    }

    /**
     * Update package settings
     * @return [type] [description]
     */
    public function postUpdate ($id) {
        if (!$model = $this->PackageRepository->findById(trim($id))) {
            return \Response::json([
                'type'  =>  'error',
                'message'   =>  'Package not found.'
            ]);
        }

        $data = \Input::get('data');
        $this->PackageRepository->update($model, $data);
        return \Response::json([
            'type'  =>  'success',
            'message'   =>  'Package updated.'
        ]);
    }

    public function upgradePackage(Request $request){
        $username = $request->data['username'];
        $package_id = $request->data['package_id'];

        $member = Member::where('username',$username)->first();
        if($member != null){
            $member->package_id = $package_id;
            $member->update();
            if($member)
            $message = "Data updated successfully";
            else
            $message = "Something Went Wrong"; 
        }      
        else
            $message = "User not found"; 
         
        return \Response::json([
            'type'  =>  'success',
            'message'   =>  $message
        ]);
    }
}
