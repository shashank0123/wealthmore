<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Update;

class UpdateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexUpdate()
    {
        $updates = Update::all();
        return view('back.ecommerce.all-updates',compact('updates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createUpdate()
    {
        return view('back.ecommerce.add-update');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeUpdate(Request $request)
    {
        $update = new Update;

        // for image_url
        if (Input::hasfile('image_url')) {
           $file=Input::file('image_url');
           $file->move(public_path(). '/assetsss/images/AdminProduct/updates/', time().$file->getClientOriginalName());
           $update->image_url=time().$file->getClientOriginalName();           
        }
        $update->description = $request->description;
        $update->title = $request->title;
        $update->tags = $request->tags;
        $update->status = $request->status;
        $update->save();

        return redirect()->back()->with('message','Data Successfully Inserted');
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editUpdate($id)
    {
        $update = Update::find($id);
        return view('back.ecommerce.edit-update',compact('update'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateUpdate(Request $request, $id)
    {
        $update = Update::find($id);
         // for image_url
        if (Input::hasfile('image_url')) {
           $file=Input::file('image_url');
           $file->move(public_path(). '/assetsss/images/AdminProduct/updates/', time().$file->getClientOriginalName());
           $update->image_url=time().$file->getClientOriginalName();           
        }
        $update->status = $request->status;
        $update->title = $request->title;
        $update->tags = $request->tags;
        $update->description = $request->description;
        $update->update();

        return redirect()->back()->with('message','Data Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyUpdate($id)
    {
        $update = Update::find($id);
        $update->delete();

        return redirect()->back()->with('message','Blog Successfully Deleted');
    }
}
