<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Banner;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banner = Banner::all();
        return view('back.ecommerce.banner',compact('banner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.ecommerce.add-banner');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $banner = new Banner;

        // for image_url
        if (Input::hasfile('image_url')) {
           $file=Input::file('image_url');
           $file->move(public_path(). '/assetsss/images/AdminProduct/banner/', time().$file->getClientOriginalName());
           $banner->image_url=time().$file->getClientOriginalName();           
        }
        $banner->image_type = $request->image_type;
        $banner->keyword = $request->keyword;
        $banner->status = $request->status;
        $banner->save();

        return redirect()->back()->with('message','Data Successfully Inserted');
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = Banner::find($id);
        return view('back.ecommerce.edit-banner',compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $banner = Banner::find($id);
         // for image_url
        if (Input::hasfile('image_url')) {
           $file=Input::file('image_url');
           $file->move(public_path(). '/assetsss/images/AdminProduct/banner/', time().$file->getClientOriginalName());
           $banner->image_url=time().$file->getClientOriginalName();           
        }
        $banner->status = $request->status;
        $banner->image_type = $request->image_type;
        $banner->keyword = $request->keyword;
        $banner->update();

        return redirect()->back()->with('message','Banner Image Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = Banner::find($id);
        $banner->delete();

        return redirect()->back()->with('message','Image Successfully Deleted');
    }
}
