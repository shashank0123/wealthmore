<?php

return [
	'withdrawAdminFee'	=>	5, // in percent
	'bonus'	=>	[
		'group'		=>	1, // in percent
		'override'	=>	12, // in percent
		'pairing'	=>	1050, // in USD
		'calculation' => [ // in percent
		'cash'	=>	100,
		'promotion'	=>	0
	],
],
'shares' => [
	'sellRange'	=>	0.003,
		'sellValue' => [ // all values in percent
		'cash'  =>  48,
		'buyBack'   =>  30,
		'point' =>  12,
		'fee'   =>  10,
	]
],
'countries' => [
		// 'Cambodia' => [
		// 	'currency'	=>	'KHR',
		// 	'buy'	=>	'5100.00',
		// 	'sell'	=>	'4000.00',
		// 	'banks'	=>	[
		// 		'Public Bank',
		// 		'MayBank',
		// 		'ACLEDA Bank'
		// 	]
		// ],
		// 'Indonesia'	=>	[
		// 	'currency'	=>	'IDR',
		// 	'buy'	=>	'14000.00',
		// 	'sell'	=>	'11500.00',
		// 	'banks'	=>	[
		// 		'BCA Bank',
		// 		'Mandiri Bank'
		// 	]
		// ],
		// 'Philippines'	=>	[
		// 	'currency'	=>	'PHP',
		// 	'buy'	=>	'48.00',
		// 	'sell'	=>	'41.00',
		// 	'banks'	=>	[
		// 		'RCBC Bank',
		// 		'BPI Bank',
		// 		'Metro Bank',
		// 		'BDO Bank',
		// 		'Land Bank of the Phillipines',
		// 		'PNB Phillipines'
		// 	]
		// ],
	'India'	=>	[
		'currency'	=>	'INR',
		'buy'	=>	'148.00',
		'sell'	=>	'141.00',
		'banks'	=>	[
				// 'RCBC Bank',
				// 'BPI Bank',
				// 'Metro Bank',
				// 'BDO Bank',
				// 'State Bank',
				// 'Land Bank of the Phillipines',
				// 'PNB Phillipines'
			'Allahabad Bank',
			'Andhra Bank',
			'Axis Bank',
			'Bank of Bahrain and Kuwait',
			'Bank of Baroda - Corporate Banking',
			'Bank of Baroda - Retail Banking',
			'Bank of India',
			'Bank of Maharashtra',
			'Canara Bank',
			'Central Bank of India',
			'City Union Bank',
			'Corporation Bank',
			'Deutsche Bank',
			'Development Credit Bank',
			'Dhanlaxmi Bank',
			'Federal Bank',
			'ICICI Bank',
			'IDBI Bank',
			'Indian Bank',
			'Indian Overseas Bank',
			'IndusInd Bank',
			'ING Vysya Bank',
			'Jammu and Kashmir Bank',
			'Karnataka Bank Ltd',
			'Karur Vysya Bank',
			'Kotak Bank',
			'Laxmi Vilas Bank',
			'Oriental Bank of Commerce',
			'Punjab National Bank - Corporate Banking',
			'Punjab National Bank - Retail Banking',
			'Punjab & Sind Bank',
			'Shamrao Vitthal Co-operative Bank',
			'South Indian Bank',
			'State Bank of Bikaner & Jaipur',
			'State Bank of Hyderabad',
			'State Bank of India',
			'State Bank of Mysore',
			'State Bank of Patiala',
			'State Bank of Travancore',
			'Syndicate Bank',
			'Tamilnad Mercantile Bank Ltd.',
			'UCO Bank',
			'Union Bank of India',
			'United Bank of India',
			'Vijaya Bank',
			'Yes Bank Ltd'
		]
	],
		// 'Thailand'	=>	[
		// 	'currency'	=>	'THB',
		// 	'buy'	=>	'40.00',
		// 	'sell'	=>	'32.00',
		// 	'banks'	=>	[
		// 		'TMB Bank',
		// 		'Krungsri Bank',
		// 		'Kasikom',
		// 		'Siam Commercial Bank',
		// 		'Siam City Bank',
		// 		'Bangkok Bank'
		// 	]
		// ],
		// 'Vietnam'	=>	[
		// 	'currency'	=>	'VND',
		// 	'buy'	=>	'25000.00',
		// 	'sell'	=>	'21000.00',
		// 	'banks'	=>	[
		// 		'ABC Asia Pacific',
		// 		'VietComBank',
		// 		'BIDV Bank'
		// 	]
		// ],
		// 'China'	=>	[
		// 	'currency'	=>	'CNY',
		// 	'buy'	=>	'7.00',
		// 	'sell'	=>	'6.00',
		// 	'banks'	=>	[
		// 		'ICBC Bank (工商银行)',
		// 		'CMB Bank (招商银行)',
		// 		'Bank of China (中国银行)',
		// 		'ABC Bank (农业银行)'
		// 	]
		// ],
		// 'Singapore'	=>	[
		// 	'currency'	=>	'SGD',
		// 	'buy'	=>	'1.60',
		// 	'sell'	=>	'1.30',
		// 	'banks'	=>	[
		// 		'UOB Bank'
		// 	]
		// ],
		// 'Malaysia'	=>	[
		// 	'currency'	=>	'MYR',
		// 	'buy'	=>	'4.50',
		// 	'sell'	=>	'4.00',
		// 	'banks'	=>	[
		// 		'Maybank',
		// 		'Public Bank',
		// 		'CIMB Bank',
		// 		'RHB Bank'
		// 	]
		// ],
		// 'Taiwan'	=>	[
		// 	'currency'	=>	'TWD',
		// 	'buy'	=>	'35.00',
		// 	'sell'	=>	'30.00',
		// 	'banks'	=>	[
		// 		'CTBC Bank (中国信托商业银行)',
		// 		'Bank of Taiwan (台湾银行)',
		// 		'Esun Bank (玉山银行)'
		// 	]
		// ],
],
'coin' => [
	'fee' => 2,
	'btc' => '38azbfm9iydhsjfkzzfawx4p7aljsaiwdt',
	'lite' => 'LbkQLTugNQaV8QMQynCMPVbH5h7CzjbUEH',
	'doge' => 'dso6du3bwtmun13k4ew6srjaxsfazc6bbh',
	'eth' => '0x7634eb364d6a0ff9fedbb17e8308d36872b8059d',
	'bcy' => 'CCqqcruarqEH9w8dAcdYxTCSvZ7hD73DgF'
]
];